var systemState = "";//系统状态
var userId = "";
var userName = "";
var avatarImg = "";
layui.use(function () {
    var $ = layui.jquery,
        layer = layui.layer;
    let lIndex = ctx + 'leading/index/';

    $.ajax({
        url: ctx + 'leadingCacheCar/getSession',
        type: "post",
        async: false,//设置同步不然全局变量会失效
        success: function (res) {
            systemState = res.code;
            if (res.code == '0') {
                userId = res.user.userId;
                userName = res.user.userName;
                if (res.user.avatar == "" || res.user.avatar == null) {
                    avatarImg = '/img/profile.jpg';//默认头像
                } else {
                    avatarImg = res.user.avatar;
                }
            }
        }
    });

    /*top导航*/
    let topNavigation = "";
    topNavigation += '<ul class="layui-nav xin-width">\n' +
        '<li class="layui-nav-item"><a href="' + ctx + 'leading/index"><i class="iconfont iconzhuye"></i>首页</a></li>\n' +
        '<li class="layui-nav-item right " id="backstagePermission" ><a href="' + ctx + 'index" target="_blank"><i class="iconfont iconxitongpeizhi"></i>后端系统</a></li>' +
        '<li class="layui-nav-item right " id="avatar" ><a href="javascript:;"><img src="' + avatarImg + '" class="layui-nav-img"><span class="hidden-xs">' + userName + '</span></a></li>\n' +
        '<li class="layui-nav-item right cacheCarListPermission" ><a href="' + lIndex + 'cacheCarIndex"><i class="iconfont iconshouye" ></i>购物车</a></li>\n' +
        '<li class="layui-nav-item right " id="Login" ><a href="javascript:;"><i class="iconfont icontijiao" ></i>登录</a></li>' +
        '<li class="layui-nav-item right layui-hide-xs" ><a href="javascript:;" id="register"><i class="iconfont iconzhuce" title="没有账号!申请注册账号!!!"></i>申请账号</a></li>' +
        '<li class="layui-nav-item right layui-hide-xs" ><a href="javascript:;"  id="opinion"><i class="iconfont iconfankui" title="意见反馈"></i>意见反馈</a></li>' +
        '</ul>';
    $("#topNavigation").append(topNavigation);


    /*center导航*/
    let centerNavigation = "";
    centerNavigation +=
        '<li class="layui-nav-item" id="classify-menu-item" style="float: left"><a href="javascript:;" class="classify-menu " id="classifyID"><i class="iconfont iconfenlei"></i>全部分类<i class="layui-icon layui-icon-down layui-font-12"></i></a></li>\n' +
        '<li class="layui-nav-item cacheCarListPermission" id="cacheCarIndex"><a href="' + lIndex + 'cacheCarIndex" ><i class="iconfont iconshouye"></i>购物车</a></li>\n' +
        '<li class="layui-nav-item" id="personalIndex"><a href="' + lIndex + 'personalIndex"><i class="iconfont icondingdan"></i>个人中心</a></li>\n' +
        '<li class="layui-nav-item layui-hide-xs xin-hide"><a href="javascript:;" class="helpHref" style="color: #FF5722"><i class="iconfont iconbangzhu"></i>使用手册</a></li>\n' +
        '<li class="layui-nav-item layui-hide-xs xin-hide"><a href="javascript:;" class="videoHref" style="color: #8c1aff"><i class="iconfont iconBAI-shiping"></i>视频教学</a></li>';
    $("#centerNavigation").append(centerNavigation);


    if (systemState == '0') {
        /*个人中心-订单导航*/
        let ordersForNavigation = "";
        ordersForNavigation +=
            '<dd><a href="' + lIndex + 'controlPanel" target="iframeMain" id="controlPanels"><i class="iconfont iconzhuye" style="margin-right: 5px"></i>个人中心<span class="layui-badge-dot"></span></a></dd>\n' +
            '<dd><a href="' + lIndex + 'expect" target="iframeMain" id="expect"><i class="iconfont icontijiao" style="margin-right: 5px"></i>待提交订单</a></dd>\n' +
            '<dd><a href="' + lIndex + 'reject" target="iframeMain" id="reject"><i class="iconfont iconshenpibohui" style="margin-right: 5px"></i>被驳回订单</a></dd>\n' +
            '<dd><a href="' + lIndex + 'toAudit" target="iframeMain" id="toAudit"><i class="iconfont icondengdai" style="margin-right: 5px"></i>待审核订单</a></dd>\n' +
            '<dd><a href="' + lIndex + 'overallData" target="iframeMain" id="overallData"><i class="iconfont iconquanbu" style="margin-right: 5px"></i>订单全览</a></dd>\n' +
            '<dd><a href="' + lIndex + 'customerGroup" target="iframeMain" id="customerGroup"><i class="iconfont iconkehuguanli" style="margin-right: 5px"></i>客户分组订单</a></dd>\n' +
            '<dd id="storefrontDisplay"><a href="' + lIndex + 'storefrontData" target="iframeMain" id="storefrontData"><i class="iconfont iconziyuan" style="margin-right: 5px"></i>店面订单</a></dd>\n' +
            '<dd id="leagueDisplay"><a href="' + ctx + 'backstage/information/allOrdersCw/2" target="iframeMain" id="leagueData"><i class="iconfont iconquanbu" style="margin-right: 5px"></i>加盟订单</a></dd>';
        $("#ordersForNavigation").append(ordersForNavigation);


        /*个人中心-控制面板*/
        let controlPanel = "";
        controlPanel += '<iframe id="iframe" src="' + lIndex + 'controlPanel" frameborder="0" class="frame-content" name="iframeMain" style="width: 100%; height:100%;"></iframe>';
        $("#controlPanel").append(controlPanel);

        let righticon = "<i class=\"layui-icon layui-icon-down layui-nav-more\"></i>";

        $("#controlPanels").bind('click', function () {
            $(".classify-menu").text("个人中心").prepend("<i class=\"iconfont iconkehuxinxi\" style=\"margin-right: 3px\"></i>").append(righticon)
        });
        $("#expect").bind('click', function () {
            $(".classify-menu").text("待提交订单").prepend("<i class=\"iconfont icontijiao\" style=\"margin-right: 3px\"></i>").append(righticon)
        });
        $("#reject").bind('click', function () {
            $(".classify-menu").text("被驳回订单").prepend("<i class=\"iconfont iconshenpibohui\" style=\"margin-right: 3px\"></i>").append(righticon)
        });
        $("#toAudit").bind('click', function () {
            $(".classify-menu").text("待审核订单").prepend("<i class=\"iconfont icondengdai\" style=\"margin-right: 3px\"></i>").append(righticon)
        });
        $("#overallData").bind('click', function () {
            $(".classify-menu").text("订单全览").prepend("<i class=\"iconfont iconquanbu\" style=\"margin-right: 3px\"></i>").append(righticon)
        });
        $("#customerGroup").bind('click', function () {
            $(".classify-menu").text("客户分组订单").prepend("<i class=\"iconfont iconquanbu\" style=\"margin-right: 3px\"></i>").append(righticon)
        });
        $("#storefrontData").bind('click', function () {
            $(".classify-menu").text("店面订单").prepend("<i class=\"iconfont iconquanbu\" style=\"margin-right: 3px\"></i>").append(righticon)
        });
        $("#leagueData").bind('click', function () {
            $(".classify-menu").text("加盟订单").prepend("<i class=\"iconfont iconquanbu\" style=\"margin-right: 3px\"></i>").append(righticon)
        });
    } else {
        /*个人中心-订单导航*/
        let ordersForNavigation = "";
        ordersForNavigation +=
            '<dd><a href="javascript:;" ><i class="iconfont icondaiqueding" style="margin-right: 5px"></i>登录后显示正常菜单<span class="layui-badge-dot"></span></a></dd>'
        $("#ordersForNavigation").append(ordersForNavigation);
        /*个人中心-控制面板*/
        let controlPanel = "";
        controlPanel += '<iframe id="iframe" src=' + lIndex + '"../login" frameborder="0" class="frame-content" name="iframeMain" style="width: 100%; height:100%;"></iframe>';
        $("#controlPanel").append(controlPanel);

        let shoppingTrolley = "";
        shoppingTrolley += '<iframe id="iframe" src=' + lIndex + '"../login" frameborder="0" class="frame-content" name="iframeMain" style="height:800px;max-height: 1000px;min-height: 500px; width:100%;"></iframe>';
        $("#shoppingTrolley").append(shoppingTrolley);
    }
    $("#avatar").bind('click', function () {
        layer.confirm("确定要进入个人中心修改个人数据吗? 修改完成 直接关闭浏览器窗口", {
            btn: ['确定', '不了']
        }, function (index) {
            parent.layer.close(parent.layer.index);//关闭弹窗
            window.open(ctx + "system/user/profile");
        }, function () {
            parent.layer.close(parent.layer.index);//关闭弹窗
        });

    });

    $(".helpHref").bind('click', function () {
        layer.confirm("确定查看使用教程吗?", {
            btn: ['确定', '不了']
        }, function (index) {
            parent.layer.close(parent.layer.index);//关闭弹窗
            window.open("https://docs.qq.com/doc/DSXlTSWxXTGhhQndy")
        }, function () {
            parent.layer.close(parent.layer.index);//关闭弹窗
        });

    });
    $(".videoHref").bind('click', function () {
        layer.confirm("确定查看教学视频吗?", {
            btn: ['确定', '不了']
        }, function (index) {
            parent.layer.close(parent.layer.index);//关闭弹窗
            window.open("https://www.ixigua.com/7105969483844026893?wid_try=1")
        }, function () {
            parent.layer.close(parent.layer.index);//关闭弹窗
        });

    });

    $("#register").bind('click', function () {
        layer.confirm("注册申请", {
            btn: ['确定', '不了']
        }, function (index) {
            parent.layer.close(parent.layer.index);//关闭弹窗
            layer.open({
                type: 2,
                title: '账号注册申请',
                shadeClose: true,
                shade: false,
                maxmin: true, //开启最大化最小化按钮
                area: ['80%', '80%'],
                content: ctx + 'leading/index/register'
            });
        }, function () {
            parent.layer.close(parent.layer.index);//关闭弹窗
        });
    });

    $("#opinion").bind('click', function () {
        layer.confirm("提交意见与建议", {
            btn: ['确定', '不了']
        }, function (index) {
            parent.layer.close(parent.layer.index);//关闭弹窗
            layer.open({
                type: 2,
                title: '填写意见与建议',
                shadeClose: true,
                shade: false,
                maxmin: true, //开启最大化最小化按钮
                area: ['80%', '80%'],
                content: ctx + 'backstage/opinion'
            });
        }, function () {
            parent.layer.close(parent.layer.index);//关闭弹窗
        });
    });


    /*details  Go 购物车 按钮*/
    let purchaseBtn = "";
    purchaseBtn += '<a class="layui-btn layui-btn-primary purchase-btn  cacheCarListPermission"   href="' + lIndex + 'cacheCarIndex">GO-购物车</a>';
    $("#purchaseBtn").append(purchaseBtn);

    if (systemState == '0') {
        $('#Login').css('display', 'none');//隐藏登录按钮
        if (userId == '1') {//登录后隐藏账号申请注册接口 超管除外
        } else {
            $('#register').css('display', 'none');
        }
    } else {
        $('#personalIndex').css('display', 'none');
        $('#opinion').css('display', 'none');
        $('#avatar').css('display', 'none');//隐藏 个人信息
    }

    if (backstagePermission === "hidden") {
        $('#backstagePermission').css('display', 'none');//隐藏登录按钮
    }
    /*cacheCarListPermission  购物车按钮权限 */
    if (cacheCarListPermission === "hidden") {
        $('.cacheCarListPermission').css('display', 'none');
    }


    /*头部搜索*/
        let topSeek = "";
        topSeek += '<div class="headerLayout">\n' +
            '        <a href="javascript:location.reload();" title="1新成品软装">\n' +
            '            <img src="' + ctx + 'leading/index/pictureDisplayLogo" class="xinLogo">\n' +
            '        </a>\n' +
            '        <div class="mallSearch">\n' +
            '            <form action="" class="layui-form" novalidate>\n' +
            '                <input type="text" name="yxNumber" autocomplete="off" class="search-input" >\n' +
            '                <button class="search-btn" lay-submit lay-filter="data-search-btn" >搜索\n' +
            '                    <i class="layui-icon layui-icon-search"></i>\n' +
            '                </button>\n' +
            '            </form>\n' +
            '        </div>\n' +
            '    </div>';
        $("#topSeek").append(topSeek);

    /*页尾*/
    let pageFooting = "";
    pageFooting += '<div class="footer" style="\'background:url(' + ctx + 'leading/images/index/boxbg.png) no-repeat center center;\'">\n' +
        '    <div class="mainbox">\n' +
        '        <ul class="tdlist clearfix">\n' +
        '            <li><a href="https://www.quanwujiaju.com/mfsj.html"><img\n' +
        '                    src="' + ctx + 'leading/images/index/tdico1.png"></a></li>\n' +
        '            <li><a href="https://www.quanwujiaju.com/mfsj.html"><img\n' +
        '                    src="' + ctx + 'leading/images/index/tdico2.png"></a></li>\n' +
        '            <li><a href="https://www.quanwujiaju.com/mfsj.html"><img\n' +
        '                    src="' + ctx + 'leading/images/index/tdico3.png"></a></li>\n' +
        '            <li><a href="https://www.quanwujiaju.com/mfsj.html"><img\n' +
        '                    src="' + ctx + 'leading/images/index/tdico4.png"></a></li>\n' +
        '            <li><a href="https://www.quanwujiaju.com/mfsj.html"><img\n' +
        '                    src="' + ctx + 'leading/images/index/tdico5.png"></a></li>\n' +
        '            <li><a href="https://www.quanwujiaju.com/mfsj.html"><img\n' +
        '                    src="' + ctx + 'leading/images/index/tdico6.png"></a></li>\n' +
        '        </ul>\n' +
        '    </div>\n' +
        '    <div class="mod_help w1200">\n' +
        '        <p>\n' +
        '            <a href="http://www.quanwujiaju.com/about.aspx" target="_blank">走进一新</a>\n' +
        '            <span>|</span>\n' +
        '            <a href="http://www.quanwujiaju.com/newslist.aspx" target="_blank">1新咨询</a>\n' +
        '            <span>|</span>\n' +
        '            <a href="http://www.quanwujiaju.com/axxd.aspx" target="_blank">爱心活动</a>\n' +
        '        </p>\n' +
        '    </div>\n' +
        '</div>';
    $("#pageFooting").append(pageFooting);


    $("#Login").bind('click', function () {
        layer.open({
            type: 2,
            shade: [0.6, '#ffffff'], //遮盖层
            closeBtn: 1,
            title: false,//关闭title
            shadeClose: false,//是否点击遮盖层关闭
            area: ['80%', '80%'],
            content: ctx + 'leading/index/login'
        });
    });

});

/**图片查看 设定范围 id="imagesView"  2022-11-04*/
function getImagesView() {
    let viewer = new Viewer(document.getElementById('imagesView'), {url: 'src'});
    return viewer;
}

/**
 * 轮播图
 */
function getCarousel($,layer,prefix,carousel) {
//轮播图
    $.ajax({
        url: prefix + '/carouselList?state=1',
        type: 'post',
        dataType: 'json',
        success: function (res) {
            if (res.code == '0') {
                for (i = 0; i < res.total; i++) {
                    var str = '<div id="lazyimgCarousel">\n' +
                        '         <img  src="' + prefix + '/thumbnailPreview?imgUrl=' + res.rows[i].image + '"   onerror="this.onerror=null;this.src=\'leading/images/notfound/404.svg\'" width="100%" height="100%" class="carouselImg">\n' +
                        '      </div>';
                    $('#preview').append(str);
                }
            }
            carousel.render({
                elem: '#carouselView'
                , width: '100%'
                , height: '650px'
                ,anim: 'default'
                , interval: 5000
            });
        },
        error: function () {
            layer.msg("轮播图数据请求失败,请联系管理员！", {icon: 2});
        }
    });
}

function getTreeClassify($,prefix,dropdown,cardTable,flow) {
    //下拉菜单
    $.ajax({
        url: prefix + '/treeClassifyData',
        type: "post",
        success: function (res) {
            dropdown.render({
                elem: '#classifyID'
                , trigger: 'hover'
                , align: 'center'
                , data: res.data
                , click: function (item) {
                    $(".category-banner").hide();//轮播图隐藏
                    cardTable.reload("currentTableId", {
                        url: prefix + '/leadingProductDisplay',
                        where: {
                            yxNumber: "",
                            classify: item.title,
                            state: ""
                        },
                        method: 'post',
                    });
                    flow.lazyimg();//重新加载懒加载
                    return false;
                }
            });
        }
    });
}