function getDyestuff(d) {
    let dyestuff;
    let specifications;
    let customName;
    let customSkin;
    let customQuality;
    let customSpecifications;
    let customDyestuff;
    let customOther;
    let customChangeColour;
    if (d.dyestuff == '自定义') {

        if (d.customDyestuff == null || d.customDyestuff == "") {
            dyestuff = "<label style=\"color: #b800e6\">请填写ZDY色号</label>&nbsp;"
        } else {
            dyestuff = ""
        }
    } else {
        dyestuff = "<label style=\"color: #999999\">‹色号›：</label><span>" + d.dyestuff + "</span>&nbsp;"
    }
    if (d.specifications == "自定义") {
        if (d.customSpecifications == null || d.customSpecifications == "") {
            specifications = "<label style=\"color: #ff0066\">请填写ZDY规格</label>&nbsp;"
        } else {
            specifications = ""
        }
    } else {
        specifications = "<label style=\"color: #999999\">‹规格›：</label><span>" + d.specifications + "</span>&nbsp;"
    }


    if (d.customName == null || d.customName == "") {
        customName = ""
    } else {
        customName = "<label style=\"color: #999999\">‹品类›：</label><span>" + d.customName + "</span>&nbsp;"
    }
    if (d.customSkin == null || d.customSkin == "") {
        customSkin = ""
    } else {
        customSkin = "<label style=\"color: #999999\">‹型号›：</label><span>" + d.customSkin + "</span>&nbsp;"
    }
    if (d.customQuality == null || d.customQuality == "") {
        customQuality = ""
    } else {
        customQuality = "<label style=\"color: #999999\">‹材质›：</label><span>" + d.customQuality + "</span>&nbsp;"
    }
    if (d.customSpecifications == null || d.customSpecifications == "") {
        customSpecifications = ""
    } else {
        customSpecifications = "<label style=\"color: #999999\">›规格›：</label><span>" + d.customSpecifications + "</span>&nbsp;"
    }
    if (d.customDyestuff == null || d.customDyestuff == "") {
        customDyestuff = ""
    } else {
        customDyestuff = "<label style=\"color: #999999\">‹色号›：</label><span>" + d.customDyestuff + "</span>&nbsp;"
    }
    if (d.customOther == null || d.customOther == "") {
        customOther = ""
    } else {
        customOther = "<label style=\"color: #999999\">‹其他›：</label><span>" + d.customOther + "</span>&nbsp;"
    }
    if (d.customChangeColour == null || d.customChangeColour == "") {
        customChangeColour = ""
    } else {
        customChangeColour = "<label style=\"color: #999999\">‹功能位方向›：</label><span>" + d.customChangeColour + "</span>&nbsp;"
    }
    return dyestuff + specifications + customName + customSkin + customQuality + customSpecifications + customDyestuff + customOther + customChangeColour;
}


function getOperatingButton(d){
    let operatingButton;
    operatingButton = "<button type=\"button\" class=\"layui-btn layui-btn-primary layui-btn-xs iconfont iconshouye1\" lay-event=\"other\" style=\"border: 1px solid #ff6600;color: #ff6600\">其他</button>"
    if(d.dyestuff == '自定义' || d.specifications == '自定义'){
        if(d.identification == 'SF' || d.identification == 'GNSF' || d.identification == 'FCG'){
            operatingButton = "<button type=\"button\" class=\"layui-btn layui-btn-primary layui-btn-xs iconfont iconshouye1\" lay-event=\"custom-sofa\" style=\"border: 1px solid #e6005a;color: #e6005a\">自定义</button>"
        }else{
            operatingButton = "<button type=\"button\" class=\"layui-btn layui-btn-primary layui-btn-xs iconfont iconshouye1\" lay-event=\"custom\" style=\"border: 1px solid #c34dff;color: #c34dff\">自定义</button>"
        }
    }else if(d.identification == 'GNSF'){
        operatingButton = "<button type=\"button\" class=\"layui-btn layui-btn-primary layui-btn-xs iconfont iconshouye1\" lay-event=\"custom-sofa-gn\" style=\"border: 1px solid #ff6600;color: #ff6600\">功能位方向</button>"
    }
    return operatingButton;
}


/**
 * 主图
 * @param d
 * @returns {string}
 */
function getProductImg(d) {
    return '<img src="' + ctx + 'leading/index/pictureDisplay?imgUrl=' + d.productImg + '" onerror="this.onerror=null;this.src=ctx+\'backstage/images/notfound/404.svg\'" height="45px" width="45px"  class="imgNav" >'
}

/**
 * 订单状态
 * @param d
 * @returns {string}
 */
function getStateDescription(d) {
    let stateDescription;
    if(d.statusCode == '0'){
        stateDescription = "<span style=\"color:#d9d9d9;text-decoration:underline;\">"+ d.stateDescription +"</span>"
    }else if(d.statusCode == '5'){
        stateDescription = "<span style=\"color:#ff0066;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '10'){
        stateDescription = "<span style=\"color:#8c1aff;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '15'){
        stateDescription = "<span style=\"color:#ff0066;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '20'){
        stateDescription = "<span style=\"color:#8080ff;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '25'){
        stateDescription = "<span style=\"color:#ff5500;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '27'){
        stateDescription = "<span style=\"color:#6600ff;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '30'){
        stateDescription = "<span style=\"color:#1a8cff;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '35'){
        stateDescription = "<span style=\"color:#00994d;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == null || d.statusCode == ""){
        stateDescription = ""
    }else{
        stateDescription = "<span style=\"color:#000;\">" +d.stateDescription +"</span>"
    }
    return stateDescription;
}


/**图片查看 设定范围 id="imagesView"  2022-11-04*/
function getImagesView() {
    let viewer = new Viewer(document.getElementById('imagesView'), {url: 'src'});
    return viewer;
}