package com.ruoyi.leading.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单主对象 product_order_information
 *
 * @author xin-xin
 * @date 2022-02-10
 */
public class LeadingOrderInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 客户组id */
    @Excel(name = "客户组id")
    private Long clientId;

    /** 产品id */
    @Excel(name = "产品id")
    private Long mainId;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private String clientNumber;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNumber;

    /** 下单人 */
    @Excel(name = "下单人")
    private String orders;

    /** 下单人身份 */
    @Excel(name = "下单人身份")
    private String ordersStatus;

    /** 下单人id */
    @Excel(name = "下单人id")
    private Long statusId;

    /** 下单人-联系方式 */
    @Excel(name = "下单人-联系方式")
    private String contactWay;

    /** 下单店面 */
    @Excel(name = "下单店面")
    private String storefront;

    /** 下单店面编号 */
    @Excel(name = "下单店面编号")
    private Integer storefrontCoding;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String customerName;

    /** 客户电话 */
    @Excel(name = "客户电话")
    private String customerTelephone;

    /** 下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderTime;

    /** 客户地址 */
    @Excel(name = "客户地址")
    private String customerAddress;

    /** 店面类型 */
    @Excel(name = "店面类型")
    private Long storefrontType;

    /** 自定义-品类 */
    @Excel(name = "自定义-品类")
    private String customName;

    /** 自定义-材质 */
    @Excel(name = "自定义-材质")
    private String customQuality;

    /** 自定义-规格 */
    @Excel(name = "自定义-规格")
    private String customSpecifications;

    /** 自定义-色号 */
    @Excel(name = "自定义-色号")
    private String customDyestuff;

    /** 自定义-型号 */
    @Excel(name = "自定义-型号")
    private String customSkin;

    /** 自定义-沙发改色 */
    @Excel(name = "自定义-沙发改色")
    private String customChangeColour;

    /** 自定义-其他 */
    @Excel(name = "自定义-其他")
    private String customOther;

    /** 状态描述 */
    @Excel(name = "状态描述")
    private String stateDescription;

    /** 状态码 */
    @Excel(name = "状态码")
    private Long statusCode;

    /** 自定义-内容 */
    @Excel(name = "自定义-内容")
    private String userDefined;

    /** 订单备注 */
    @Excel(name = "订单备注")
    private String remarks;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date auditTime;

    /** 下单数量 */
    @Excel(name = "下单数量")
    private Long number;

    /** 驳回理由 */
    @Excel(name = "驳回理由")
    private String overrule;

    /** 采购-下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购-下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cGiveTime;

    /** 采购时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cPurchaseTime;

    /** 计划交期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划交期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planDeliveryPeriod;

    /** 采购-周期 */
    @Excel(name = "采购-周期")
    private Long cPeriod;

    /** 采购-预计时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购-预计时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cScheduledTime;

    /** 发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deliveryTime;

    /** 发货备注 */
    @Excel(name = "发货备注")
    private String notesOnShipping;

    /** 采购-需到货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购-需到货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cWantTime;

    /** 提货方式 */
    @Excel(name = "提货方式")
    private String deliveryMethod;

    /** 采购-回厂时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购-回厂时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cRevertTime;

    /** 采购-厂家发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cDeliveryTime;

    /** 采购-采购价-成本价 */
    @Excel(name = "单价")
    private Long cPurchasePrice;

    /** 一新编号 */
    @Excel(name = "一新编号")
    private String yxNumber;

    /** 供应商编号 */
    @Excel(name = "供应商编号")
    private String supplierNumber;


    /** 采购-备注 */
    @Excel(name = "采购-备注")
    private String cProcurementRemark;

    /** 产品-名称 */
    @Excel(name = "产品-名称")
    private String productName;

    /** 财务-总金额 */
    @Excel(name = "财务-总金额")
    private String cAggregateAmount;

    /** 产品-材质 */
    @Excel(name = "产品-材质")
    private String materialQuality;

    /** 财务-运费 */
    @Excel(name = "财务-运费")
    private String cFreight;

    /** 产品-规格 */
    @Excel(name = "产品-规格")
    private String specifications;

    /** 财务-核回厂 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "财务-核回厂", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cCheckArrive;

    /** 产品-色号 */
    @Excel(name = "产品-色号")
    private String dyestuff;

    /** 财务-是否核对 */
    @Excel(name = "财务-是否核对")
    private String cCheckResult;

    /** 产品-分类 */
    @Excel(name = "产品-分类")
    private String classify;

    /** 财务-核对出库 */
    @Excel(name = "财务-核对出库")
    private String cCheckLeave;

    /** 产品-卖点 */
    @Excel(name = "产品-卖点")
    private String sellingPoint;

    /** 财务-核对备注 */
    @Excel(name = "财务-核对备注")
    private String cCheckRemark;

    /** 产品备注 */
    @Excel(name = "产品备注")
    private String productRemarks;

    /** 产品-主图 */
    @Excel(name = "产品-主图")
    private String productImg;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplier;

    /** 标签 */
    @Excel(name = "标签")
    private String label;

    /** 产品特殊标识:
     沙发:SF
     功能沙发:GNSF
     非常规:FCG */
    @Excel(name = "产品特殊标识")
    private String identification;

    /** 订单类型>1:正常单>2:维修单 */
    @Excel(name = "订单类型>1:正常单>2:维修单")
    private Long orderType;

    /** 维修类型>1:维修单>2:增补单 */
    @Excel(name = "维修类型>1:维修单>2:增补单")
    private Long maintainType;

    /** 编号类型 */
    @Excel(name = "编号类型")
    private String numberType;

    /** 原订单编号 */
    @Excel(name = "原订单编号")
    private String initialOrderNumber;

    /** 问题描述 */
    @Excel(name = "问题描述")
    private String problemDescription;

    /** 零售价 */
    @Excel(name = "零售价")
    private BigDecimal retailPrice;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date creationTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date refreshTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private String refreshName;

    /** 财务-核对时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "财务-核对时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cCheckTime;

    /** 财务-核对状态 */
    @Excel(name = "财务-核对状态")
    private Long cCheckState;

    /** 条码组状态 */
    @Excel(name = "条码组状态")
    private String barcodeGroupStatus;

    /** 仓库-入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "仓库-入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cEntranceTime;

    /** 打印状态 */
    @Excel(name = "打印状态")
    private Long printStatus;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setClientId(Long clientId)
    {
        this.clientId = clientId;
    }

    public Long getClientId()
    {
        return clientId;
    }

    public Long getMainId() {
        return mainId;
    }

    public void setMainId(Long mainId) {
        this.mainId = mainId;
    }

    public void setClientNumber(String clientNumber)
    {
        this.clientNumber = clientNumber;
    }

    public String getClientNumber()
    {
        return clientNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }
    public void setOrders(String orders)
    {
        this.orders = orders;
    }

    public String getOrders()
    {
        return orders;
    }
    public void setOrdersStatus(String ordersStatus)
    {
        this.ordersStatus = ordersStatus;
    }

    public String getOrdersStatus()
    {
        return ordersStatus;
    }
    public void setStatusId(Long statusId)
    {
        this.statusId = statusId;
    }

    public Long getStatusId()
    {
        return statusId;
    }
    public void setContactWay(String contactWay)
    {
        this.contactWay = contactWay;
    }

    public String getContactWay()
    {
        return contactWay;
    }
    public void setStorefront(String storefront)
    {
        this.storefront = storefront;
    }

    public String getStorefront()
    {
        return storefront;
    }
    public void setStorefrontCoding(Integer storefrontCoding)
    {
        this.storefrontCoding = storefrontCoding;
    }

    public Integer getStorefrontCoding()
    {
        return storefrontCoding;
    }
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerName()
    {
        return customerName;
    }
    public void setCustomerTelephone(String customerTelephone)
    {
        this.customerTelephone = customerTelephone;
    }

    public String getCustomerTelephone()
    {
        return customerTelephone;
    }
    public void setOrderTime(Date orderTime)
    {
        this.orderTime = orderTime;
    }

    public Date getOrderTime()
    {
        return orderTime;
    }
    public void setCustomerAddress(String customerAddress)
    {
        this.customerAddress = customerAddress;
    }

    public String getCustomerAddress()
    {
        return customerAddress;
    }
    public void setStorefrontType(Long storefrontType)
    {
        this.storefrontType = storefrontType;
    }

    public Long getStorefrontType()
    {
        return storefrontType;
    }
    public void setCustomName(String customName)
    {
        this.customName = customName;
    }

    public String getCustomName()
    {
        return customName;
    }
    public void setCustomQuality(String customQuality)
    {
        this.customQuality = customQuality;
    }

    public String getCustomQuality()
    {
        return customQuality;
    }
    public void setCustomSpecifications(String customSpecifications)
    {
        this.customSpecifications = customSpecifications;
    }

    public String getCustomSpecifications()
    {
        return customSpecifications;
    }
    public void setCustomDyestuff(String customDyestuff)
    {
        this.customDyestuff = customDyestuff;
    }

    public String getCustomDyestuff()
    {
        return customDyestuff;
    }
    public void setCustomSkin(String customSkin)
    {
        this.customSkin = customSkin;
    }

    public String getCustomSkin()
    {
        return customSkin;
    }
    public void setCustomChangeColour(String customChangeColour)
    {
        this.customChangeColour = customChangeColour;
    }

    public String getCustomChangeColour()
    {
        return customChangeColour;
    }
    public void setCustomOther(String customOther)
    {
        this.customOther = customOther;
    }

    public String getCustomOther()
    {
        return customOther;
    }
    public void setStateDescription(String stateDescription)
    {
        this.stateDescription = stateDescription;
    }

    public String getStateDescription()
    {
        return stateDescription;
    }
    public void setStatusCode(Long statusCode)
    {
        this.statusCode = statusCode;
    }

    public Long getStatusCode()
    {
        return statusCode;
    }
    public void setUserDefined(String userDefined)
    {
        this.userDefined = userDefined;
    }

    public String getUserDefined()
    {
        return userDefined;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }
    public void setAuditTime(Date auditTime)
    {
        this.auditTime = auditTime;
    }

    public Date getAuditTime()
    {
        return auditTime;
    }
    public void setNumber(Long number)
    {
        this.number = number;
    }

    public Long getNumber()
    {
        return number;
    }
    public void setOverrule(String overrule)
    {
        this.overrule = overrule;
    }

    public String getOverrule()
    {
        return overrule;
    }
    public void setcGiveTime(Date cGiveTime)
    {
        this.cGiveTime = cGiveTime;
    }

    public Date getcGiveTime()
    {
        return cGiveTime;
    }
    public void setcPurchaseTime(Date cPurchaseTime)
    {
        this.cPurchaseTime = cPurchaseTime;
    }

    public Date getcPurchaseTime()
    {
        return cPurchaseTime;
    }

    public void setPlanDeliveryPeriod(Date planDeliveryPeriod)
    {
        this.planDeliveryPeriod = planDeliveryPeriod;
    }
    public Date getPlanDeliveryPeriod()
    {
        return planDeliveryPeriod;
    }

    public void setcPeriod(Long cPeriod)
    {
        this.cPeriod = cPeriod;
    }

    public Long getcPeriod()
    {
        return cPeriod;
    }
    public void setcScheduledTime(Date cScheduledTime)
    {
        this.cScheduledTime = cScheduledTime;
    }

    public Date getcScheduledTime()
    {
        return cScheduledTime;
    }
    public void setDeliveryTime(Date deliveryTime)
    {
        this.deliveryTime = deliveryTime;
    }

    public Date getDeliveryTime()
    {
        return deliveryTime;
    }
    public void setNotesOnShipping(String notesOnShipping)
    {
        this.notesOnShipping = notesOnShipping;
    }

    public String getNotesOnShipping()
    {
        return notesOnShipping;
    }
    public void setcWantTime(Date cWantTime)
    {
        this.cWantTime = cWantTime;
    }

    public Date getcWantTime()
    {
        return cWantTime;
    }
    public void setDeliveryMethod(String deliveryMethod)
    {
        this.deliveryMethod = deliveryMethod;
    }

    public String getDeliveryMethod()
    {
        return deliveryMethod;
    }
    public void setcRevertTime(Date cRevertTime)
    {
        this.cRevertTime = cRevertTime;
    }

    public Date getcRevertTime()
    {
        return cRevertTime;
    }
    public void setYxNumber(String yxNumber)
    {
        this.yxNumber = yxNumber;
    }

    public String getYxNumber()
    {
        return yxNumber;
    }

    public void setSupplierNumber(String supplierNumber)
    {
        this.supplierNumber = supplierNumber;
    }

    public String getSupplierNumber()
    {
        return supplierNumber;
    }

    public void setcProcurementRemark(String cProcurementRemark)
    {
        this.cProcurementRemark = cProcurementRemark;
    }

    public String getcProcurementRemark()
    {
        return cProcurementRemark;
    }
    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getProductName()
    {
        return productName;
    }
    public void setcAggregateAmount(String cAggregateAmount)
    {
        this.cAggregateAmount = cAggregateAmount;
    }

    public String getcAggregateAmount()
    {
        return cAggregateAmount;
    }
    public void setMaterialQuality(String materialQuality)
    {
        this.materialQuality = materialQuality;
    }

    public String getMaterialQuality()
    {
        return materialQuality;
    }
    public void setcFreight(String cFreight)
    {
        this.cFreight = cFreight;
    }

    public String getcFreight()
    {
        return cFreight;
    }
    public void setSpecifications(String specifications)
    {
        this.specifications = specifications;
    }

    public String getSpecifications()
    {
        return specifications;
    }
    public void setcCheckArrive(Date cCheckArrive)
    {
        this.cCheckArrive = cCheckArrive;
    }

    public Date getcCheckArrive()
    {
        return cCheckArrive;
    }
    public void setDyestuff(String dyestuff)
    {
        this.dyestuff = dyestuff;
    }

    public String getDyestuff()
    {
        return dyestuff;
    }
    public void setcCheckResult(String cCheckResult)
    {
        this.cCheckResult = cCheckResult;
    }

    public String getcCheckResult()
    {
        return cCheckResult;
    }
    public void setClassify(String classify)
    {
        this.classify = classify;
    }

    public String getClassify()
    {
        return classify;
    }
    public void setcCheckLeave(String cCheckLeave)
    {
        this.cCheckLeave = cCheckLeave;
    }

    public String getcCheckLeave()
    {
        return cCheckLeave;
    }
    public void setSellingPoint(String sellingPoint)
    {
        this.sellingPoint = sellingPoint;
    }

    public String getSellingPoint()
    {
        return sellingPoint;
    }
    public void setcCheckRemark(String cCheckRemark)
    {
        this.cCheckRemark = cCheckRemark;
    }

    public String getcCheckRemark()
    {
        return cCheckRemark;
    }
    public void setProductRemarks(String productRemarks)
    {
        this.productRemarks = productRemarks;
    }

    public String getProductRemarks()
    {
        return productRemarks;
    }
    public void setProductImg(String productImg)
    {
        this.productImg = productImg;
    }

    public String getProductImg()
    {
        return productImg;
    }
    public void setSupplier(String supplier)
    {
        this.supplier = supplier;
    }

    public String getSupplier()
    {
        return supplier;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public Long getOrderType() {
        return orderType;
    }

    public void setOrderType(Long orderType) {
        this.orderType = orderType;
    }

    public Long getMaintainType() {
        return maintainType;
    }

    public void setMaintainType(Long maintainType) {
        this.maintainType = maintainType;
    }


    public String getNumberType() {
        return numberType;
    }

    public void setNumberType(String numberType) {
        this.numberType = numberType;
    }

    public String getInitialOrderNumber() {
        return initialOrderNumber;
    }

    public void setInitialOrderNumber(String initialOrderNumber) {
        this.initialOrderNumber = initialOrderNumber;
    }

    public String getProblemDescription() {
        return problemDescription;
    }

    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }
    public void setRetailPrice(BigDecimal retailPrice)
    {
        this.retailPrice = retailPrice;
    }

    public BigDecimal getRetailPrice()
    {
        return retailPrice;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreator()
    {
        return creator;
    }
    public void setCreationTime(Date creationTime)
    {
        this.creationTime = creationTime;
    }

    public Date getCreationTime()
    {
        return creationTime;
    }
    public void setRefreshTime(Date refreshTime)
    {
        this.refreshTime = refreshTime;
    }

    public Date getRefreshTime()
    {
        return refreshTime;
    }
    public void setRefreshName(String refreshName)
    {
        this.refreshName = refreshName;
    }

    public String getRefreshName()
    {
        return refreshName;
    }
    public void setcDeliveryTime(Date cDeliveryTime)
    {
        this.cDeliveryTime = cDeliveryTime;
    }

    public Date getcDeliveryTime()
    {
        return cDeliveryTime;
    }
    public void setcPurchasePrice(Long cPurchasePrice)
    {
        this.cPurchasePrice = cPurchasePrice;
    }

    public Long getcPurchasePrice()
    {
        return cPurchasePrice;
    }
    public void setcCheckTime(Date cCheckTime)
    {
        this.cCheckTime = cCheckTime;
    }

    public Date getcCheckTime()
    {
        return cCheckTime;
    }

    public void setcCheckState(Long cCheckState)
    {
        this.cCheckState = cCheckState;
    }

    public Long getcCheckState()
    {
        return cCheckState;
    }

    public String getBarcodeGroupStatus() {
        return barcodeGroupStatus;
    }

    public void setBarcodeGroupStatus(String barcodeGroupStatus) {
        this.barcodeGroupStatus = barcodeGroupStatus;
    }
    public void setcEntranceTime(Date cEntranceTime)
    {
        this.cEntranceTime = cEntranceTime;
    }

    public Date getcEntranceTime()
    {
        return cEntranceTime;
    }

    public Long getPrintStatus() {
        return printStatus;
    }

    public void setPrintStatus(Long printStatus) {
        this.printStatus = printStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("clientId", getClientId())
                .append("mainId", getMainId())
                .append("clientNumber", getClientNumber())
                .append("orderNumber", getOrderNumber())
                .append("orders", getOrders())
                .append("ordersStatus", getOrdersStatus())
                .append("statusId", getStatusId())
                .append("contactWay", getContactWay())
                .append("storefront", getStorefront())
                .append("storefrontCoding", getStorefrontCoding())
                .append("customerName", getCustomerName())
                .append("customerTelephone", getCustomerTelephone())
                .append("orderTime", getOrderTime())
                .append("customerAddress", getCustomerAddress())
                .append("storefrontType", getStorefrontType())
                .append("customName", getCustomName())
                .append("customQuality", getCustomQuality())
                .append("customSpecifications", getCustomSpecifications())
                .append("customDyestuff", getCustomDyestuff())
                .append("customSkin", getCustomSkin())
                .append("customChangeColour", getCustomChangeColour())
                .append("customOther", getCustomOther())
                .append("stateDescription", getStateDescription())
                .append("statusCode", getStatusCode())
                .append("userDefined", getUserDefined())
                .append("remarks", getRemarks())
                .append("auditTime", getAuditTime())
                .append("number", getNumber())
                .append("overrule", getOverrule())
                .append("cGiveTime", getcGiveTime())
                .append("cPurchaseTime", getcPurchaseTime())
                .append("planDeliveryPeriod", getPlanDeliveryPeriod())
                .append("cPeriod", getcPeriod())
                .append("cScheduledTime", getcScheduledTime())
                .append("deliveryTime", getDeliveryTime())
                .append("notesOnShipping", getNotesOnShipping())
                .append("cWantTime", getcWantTime())
                .append("deliveryMethod", getDeliveryMethod())
                .append("cRevertTime", getcRevertTime())
                .append("yxNumber", getYxNumber())
                .append("supplierNumber", getSupplierNumber())
                .append("cProcurementRemark", getcProcurementRemark())
                .append("productName", getProductName())
                .append("cAggregateAmount", getcAggregateAmount())
                .append("materialQuality", getMaterialQuality())
                .append("cFreight", getcFreight())
                .append("specifications", getSpecifications())
                .append("cCheckArrive", getcCheckArrive())
                .append("dyestuff", getDyestuff())
                .append("cCheckResult", getcCheckResult())
                .append("classify", getClassify())
                .append("cCheckLeave", getcCheckLeave())
                .append("sellingPoint", getSellingPoint())
                .append("cCheckRemark", getcCheckRemark())
                .append("productRemarks", getProductRemarks())
                .append("productImg", getProductImg())
                .append("supplier", getSupplier())
                .append("label", getLabel())
                .append("identification", getIdentification())
                .append("orderType", getOrderType())
                .append("maintainType", getMaintainType())
                .append("numberType", getNumberType())
                .append("initialOrderNumber", getInitialOrderNumber())
                .append("problemDescription", getProblemDescription())
                .append("retailPrice", getRetailPrice())
                .append("creator", getCreator())
                .append("creationTime", getCreationTime())
                .append("refreshTime", getRefreshTime())
                .append("refreshName", getRefreshName())
                .append("cDeliveryTime", getcDeliveryTime())
                .append("cPurchasePrice", getcPurchasePrice())
                .append("cCheckTime", getcCheckTime())
                .append("cCheckState", getcCheckState())
                .append("barcodeGroupStatus", getBarcodeGroupStatus())
                .append("cEntranceTime", getcEntranceTime())
                .append("printStatus", getPrintStatus())
                .toString();
    }
}
