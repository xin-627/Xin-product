package com.ruoyi.leading.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客户信息对象 p_client_information
 *
 * @author xin_xin
 * @date 2022-05-26
 */
public class PClientInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 下单人id */
    @Excel(name = "下单人id")
    private Long statusId;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String customerName;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private String clientNumber;

    /** 客户电话 */
    @Excel(name = "客户电话")
    private String customerTelephone;

    /** 下单人 */
    @Excel(name = "下单人")
    private String orders;

    /** 下单店面 */
    @Excel(name = "下单店面")
    private String storefront;

    /** 下单店面编号 */
    @Excel(name = "下单店面编号")
    private Integer storefrontCoding;

    /** 客户地址 */
    @Excel(name = "客户地址")
    private String customerAddress;

    /** 计划交期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划交期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planDeliveryPeriod;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date creationTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date refreshTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private String refreshName;

    /** 订单组状态 */
    @Excel(name = "订单组状态")
    private String orderGroupStatus;

    /** 订单数量 */
    @Excel(name = "订单数量")
    private Long orderQuantity;

    /** 是否有定制 */
    @Excel(name = "是否有定制")
    private String customization;

    /** 是否有窗帘 */
    @Excel(name = "是否有窗帘")
    private String curtain;


    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setStatusId(Long statusId)
    {
        this.statusId = statusId;
    }

    public Long getStatusId()
    {
        return statusId;
    }
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerName()
    {
        return customerName;
    }
    public void setClientNumber(String clientNumber)
    {
        this.clientNumber = clientNumber;
    }

    public String getClientNumber()
    {
        return clientNumber;
    }
    public void setCustomerTelephone(String customerTelephone)
    {
        this.customerTelephone = customerTelephone;
    }

    public String getCustomerTelephone()
    {
        return customerTelephone;
    }
    public void setOrders(String orders)
    {
        this.orders = orders;
    }

    public String getOrders()
    {
        return orders;
    }
    public void setCustomerAddress(String customerAddress)
    {
        this.customerAddress = customerAddress;
    }

    public String getCustomerAddress()
    {
        return customerAddress;
    }
    public void setPlanDeliveryPeriod(Date planDeliveryPeriod)
    {
        this.planDeliveryPeriod = planDeliveryPeriod;
    }

    public Date getPlanDeliveryPeriod()
    {
        return planDeliveryPeriod;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreator()
    {
        return creator;
    }
    public void setCreationTime(Date creationTime)
    {
        this.creationTime = creationTime;
    }

    public Date getCreationTime()
    {
        return creationTime;
    }
    public void setRefreshTime(Date refreshTime)
    {
        this.refreshTime = refreshTime;
    }

    public Date getRefreshTime()
    {
        return refreshTime;
    }
    public void setRefreshName(String refreshName)
    {
        this.refreshName = refreshName;
    }

    public String getRefreshName()
    {
        return refreshName;
    }
    public void setOrderGroupStatus(String orderGroupStatus)
    {
        this.orderGroupStatus = orderGroupStatus;
    }

    public String getOrderGroupStatus()
    {
        return orderGroupStatus;
    }
    public void setOrderQuantity(Long orderQuantity)
    {
        this.orderQuantity = orderQuantity;
    }

    public Long getOrderQuantity()
    {
        return orderQuantity;
    }
    public void setCustomization(String customization)
    {
        this.customization = customization;
    }

    public String getCustomization()
    {
        return customization;
    }

    public Integer getStorefrontCoding() {
        return storefrontCoding;
    }

    public void setStorefrontCoding(Integer storefrontCoding) {
        this.storefrontCoding = storefrontCoding;
    }

    public String getStorefront() {
        return storefront;
    }

    public void setStorefront(String storefront) {
        this.storefront = storefront;
    }

    public void setCurtain(String curtain)
    {
        this.curtain = curtain;
    }

    public String getCurtain()
    {
        return curtain;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("statusId", getStatusId())
                .append("customerName", getCustomerName())
                .append("clientNumber", getClientNumber())
                .append("customerTelephone", getCustomerTelephone())
                .append("orders", getOrders())
                .append("customerAddress", getCustomerAddress())
                .append("planDeliveryPeriod", getPlanDeliveryPeriod())
                .append("creator", getCreator())
                .append("creationTime", getCreationTime())
                .append("refreshTime", getRefreshTime())
                .append("refreshName", getRefreshName())
                .append("orderGroupStatus", getOrderGroupStatus())
                .append("orderQuantity", getOrderQuantity())
                .append("customization", getCustomization())
                .append("storefront", getStorefront())
                .append("storefrontCoding", getStorefrontCoding())
                .append("curtain", getCurtain())
                .toString();
    }
}
