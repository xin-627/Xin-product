package com.ruoyi.leading.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 购物车对象 product_cache_car
 *
 * @author xin-xin
 * @date 2022-03-08
 */
public class LeadingCacheCar extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 产品类id */
    @Excel(name = "产品类id")
    private Long mainId;

    /** 下单人 */
    @Excel(name = "下单人")
    private String orders;

    /** 下单身份 */
    @Excel(name = "下单身份")
    private String ordersStatus;

    /** 下单人id */
    @Excel(name = "下单人id")
    private Long statusId;

    /** 下单人-联系方式 */
    @Excel(name = "下单人-联系方式")
    private String contactWay;

    /** 下单-店面 */
    @Excel(name = "下单-店面")
    private String storefront;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String customerName;

    /** 下单店面-编号 */
    @Excel(name = "下单店面-编号")
    private Integer storefrontCoding;

    /** 客户电话 */
    @Excel(name = "客户电话")
    private String customerTelephone;

    /** 店面类型 */
    @Excel(name = "店面类型")
    private Long storefrontType;

    /** 客户地址 */
    @Excel(name = "客户地址")
    private String customerAddress;

    /** 下单数量 */
    @Excel(name = "下单数量")
    private Long number;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String state;

    /** 自定义-品类 */
    @Excel(name = "自定义-品类")
    private String customName;

    /** 自定义-材质 */
    @Excel(name = "自定义-材质")
    private String customQuality;

    /** 自定义-规格 */
    @Excel(name = "自定义-规格")
    private String customSpecifications;

    /** 自定义-色号 */
    @Excel(name = "自定义-色号")
    private String customDyestuff;

    /** 自定义-型号 */
    @Excel(name = "自定义-型号")
    private String customSkin;

    /** 自定义-沙发改色 */
    @Excel(name = "自定义-沙发改色")
    private String customChangeColour;

    /** 自定义-其他 */
    @Excel(name = "自定义-其他")
    private String customOther;

    /** 订单备注 */
    @Excel(name = "订单备注")
    private String remarks;

    /** 计划交期 */
    @Excel(name = "计划交期")
    private String planDeliveryPeriod;

    /** 一新编号 */
    @Excel(name = "一新编号")
    private String yxNumber;

    /** 供应商编号 */
    @Excel(name = "供应商编号")
    private String supplierNumber;

    /** 产品-名称 */
    @Excel(name = "产品-名称")
    private String productName;

    /** 产品-材质 */
    @Excel(name = "产品-材质")
    private String materialQuality;

    /** 产品-规格 */
    @Excel(name = "产品-规格")
    private String specifications;

    /** 产品-色号 */
    @Excel(name = "产品-色号")
    private String dyestuff;

    /** 产品-分类 */
    @Excel(name = "产品-分类")
    private String classify;

    /** 产品-卖点 */
    @Excel(name = "产品-卖点")
    private String sellingPoint;

    /** 产品备注 */
    @Excel(name = "产品备注")
    private String productRemarks;

    /** 产品-主图 */
    @Excel(name = "产品-主图")
    private String productImg;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplier;

    /** 标签 */
    @Excel(name = "标签")
    private String label;

    /** 产品特殊标识:
     沙发:SF
     功能沙发:GNSF
     非常规:FCG */
    @Excel(name = "产品特殊标识")
    private String identification;

    /** 零售价 */
    @Excel(name = "零售价")
    private BigDecimal retailPrice;

    /** 生产周期 */
    @Excel(name = "生产周期")
    private Long cPeriod;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date creationTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date refreshTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private String refreshName;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setMainId(Long mainId)
    {
        this.mainId = mainId;
    }

    public Long getMainId()
    {
        return mainId;
    }

    public void setOrders(String orders)
    {
        this.orders = orders;
    }

    public String getOrders()
    {
        return orders;
    }
    public void setOrdersStatus(String ordersStatus)
    {
        this.ordersStatus = ordersStatus;
    }

    public String getOrdersStatus()
    {
        return ordersStatus;
    }
    public void setStatusId(Long statusId)
    {
        this.statusId = statusId;
    }

    public Long getStatusId()
    {
        return statusId;
    }
    public void setContactWay(String contactWay)
    {
        this.contactWay = contactWay;
    }

    public String getContactWay()
    {
        return contactWay;
    }
    public void setStorefront(String storefront)
    {
        this.storefront = storefront;
    }

    public String getStorefront()
    {
        return storefront;
    }
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerName()
    {
        return customerName;
    }
    public void setStorefrontCoding(Integer storefrontCoding)
    {
        this.storefrontCoding = storefrontCoding;
    }

    public Integer getStorefrontCoding()
    {
        return storefrontCoding;
    }
    public void setCustomerTelephone(String customerTelephone)
    {
        this.customerTelephone = customerTelephone;
    }

    public String getCustomerTelephone()
    {
        return customerTelephone;
    }

    public Long getStorefrontType() {
        return storefrontType;
    }

    public void setStorefrontType(Long storefrontType) {
        this.storefrontType = storefrontType;
    }

    public void setCustomerAddress(String customerAddress)
    {
        this.customerAddress = customerAddress;
    }

    public String getCustomerAddress()
    {
        return customerAddress;
    }
    public void setNumber(Long number)
    {
        this.number = number;
    }

    public Long getNumber()
    {
        return number;
    }
    public void setState(String state)
    {
        this.state = state;
    }

    public String getState()
    {
        return state;
    }
    public void setCustomName(String customName)
    {
        this.customName = customName;
    }

    public String getCustomName()
    {
        return customName;
    }
    public void setCustomQuality(String customQuality)
    {
        this.customQuality = customQuality;
    }

    public String getCustomQuality()
    {
        return customQuality;
    }
    public void setCustomSpecifications(String customSpecifications)
    {
        this.customSpecifications = customSpecifications;
    }

    public String getCustomSpecifications()
    {
        return customSpecifications;
    }
    public void setCustomDyestuff(String customDyestuff)
    {
        this.customDyestuff = customDyestuff;
    }

    public String getCustomDyestuff()
    {
        return customDyestuff;
    }
    public void setCustomSkin(String customSkin)
    {
        this.customSkin = customSkin;
    }

    public String getCustomSkin()
    {
        return customSkin;
    }
    public void setCustomChangeColour(String customChangeColour)
    {
        this.customChangeColour = customChangeColour;
    }

    public String getCustomChangeColour()
    {
        return customChangeColour;
    }
    public void setCustomOther(String customOther)
    {
        this.customOther = customOther;
    }

    public String getCustomOther()
    {
        return customOther;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }
    public void setPlanDeliveryPeriod(String planDeliveryPeriod)
    {
        this.planDeliveryPeriod = planDeliveryPeriod;
    }

    public String getPlanDeliveryPeriod()
    {
        return planDeliveryPeriod;
    }
    public void setYxNumber(String yxNumber)
    {
        this.yxNumber = yxNumber;
    }

    public String getYxNumber()
    {
        return yxNumber;
    }
    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getProductName()
    {
        return productName;
    }
    public void setMaterialQuality(String materialQuality)
    {
        this.materialQuality = materialQuality;
    }

    public String getMaterialQuality()
    {
        return materialQuality;
    }
    public void setSpecifications(String specifications)
    {
        this.specifications = specifications;
    }

    public String getSpecifications()
    {
        return specifications;
    }
    public void setDyestuff(String dyestuff)
    {
        this.dyestuff = dyestuff;
    }

    public String getDyestuff()
    {
        return dyestuff;
    }
    public void setClassify(String classify)
    {
        this.classify = classify;
    }

    public String getClassify()
    {
        return classify;
    }
    public void setSellingPoint(String sellingPoint)
    {
        this.sellingPoint = sellingPoint;
    }

    public String getSellingPoint()
    {
        return sellingPoint;
    }
    public void setProductRemarks(String productRemarks)
    {
        this.productRemarks = productRemarks;
    }

    public String getProductRemarks()
    {
        return productRemarks;
    }
    public void setProductImg(String productImg)
    {
        this.productImg = productImg;
    }

    public String getProductImg()
    {
        return productImg;
    }
    public void setSupplier(String supplier)
    {
        this.supplier = supplier;
    }

    public String getSupplier()
    {
        return supplier;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }
    public void setRetailPrice(BigDecimal retailPrice)
    {
        this.retailPrice = retailPrice;
    }

    public BigDecimal getRetailPrice()
    {
        return retailPrice;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreator()
    {
        return creator;
    }
    public void setCreationTime(Date creationTime)
    {
        this.creationTime = creationTime;
    }

    public Date getCreationTime()
    {
        return creationTime;
    }
    public void setRefreshTime(Date refreshTime)
    {
        this.refreshTime = refreshTime;
    }

    public Date getRefreshTime()
    {
        return refreshTime;
    }
    public void setRefreshName(String refreshName)
    {
        this.refreshName = refreshName;
    }

    public String getRefreshName()
    {
        return refreshName;
    }

    public void setcPeriod(Long cPeriod)
    {
        this.cPeriod = cPeriod;
    }

    public Long getcPeriod()
    {
        return cPeriod;
    }

    public void setSupplierNumber(String supplierNumber)
    {
        this.supplierNumber = supplierNumber;
    }

    public String getSupplierNumber()
    {
        return supplierNumber;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("mainId", getMainId())
                .append("orders", getOrders())
                .append("ordersStatus", getOrdersStatus())
                .append("statusId", getStatusId())
                .append("contactWay", getContactWay())
                .append("storefront", getStorefront())
                .append("customerName", getCustomerName())
                .append("storefrontCoding", getStorefrontCoding())
                .append("customerTelephone", getCustomerTelephone())
                .append("storefrontType", getStorefrontType())
                .append("customerAddress", getCustomerAddress())
                .append("number", getNumber())
                .append("state", getState())
                .append("customName", getCustomName())
                .append("customQuality", getCustomQuality())
                .append("customSpecifications", getCustomSpecifications())
                .append("customDyestuff", getCustomDyestuff())
                .append("customSkin", getCustomSkin())
                .append("customChangeColour", getCustomChangeColour())
                .append("customOther", getCustomOther())
                .append("remarks", getRemarks())
                .append("planDeliveryPeriod", getPlanDeliveryPeriod())
                .append("yxNumber", getYxNumber())
                .append("productName", getProductName())
                .append("materialQuality", getMaterialQuality())
                .append("specifications", getSpecifications())
                .append("dyestuff", getDyestuff())
                .append("classify", getClassify())
                .append("sellingPoint", getSellingPoint())
                .append("productRemarks", getProductRemarks())
                .append("productImg", getProductImg())
                .append("supplier", getSupplier())
                .append("label", getLabel())
                .append("identification", getIdentification())
                .append("retailPrice", getRetailPrice())
                .append("creator", getCreator())
                .append("creationTime", getCreationTime())
                .append("refreshTime", getRefreshTime())
                .append("refreshName", getRefreshName())
                .append("cPeriod", getcPeriod())
                .append("supplierNumber", getSupplierNumber())
                .toString();
    }
}
