package com.ruoyi.leading.mapper;

import java.util.List;
import com.ruoyi.leading.domain.LeadingCacheCar;
import org.springframework.stereotype.Repository;

/**
 * 购物车Mapper接口
 * 
 * @author xin-xin
 * @date 2022-01-19
 */
@Repository
public interface LeadingCacheCarMapper 
{
    /**
     * 查询购物车
     * 
     * @param id 购物车主键
     * @return 购物车
     */
    public LeadingCacheCar selectLeadingCacheCarById(Long id);

    /**
     * 查询购物车列表
     * 
     * @param leadingCacheCar 购物车
     * @return 购物车集合
     */
    public List<LeadingCacheCar> selectLeadingCacheCarList(LeadingCacheCar leadingCacheCar);

    /**
     * 新增购物车
     * 
     * @param leadingCacheCar 购物车
     * @return 结果
     */
    public int insertLeadingCacheCar(LeadingCacheCar leadingCacheCar);

    /**
     * 修改购物车
     * 
     * @param leadingCacheCar 购物车
     * @return 结果
     */
    public int updateLeadingCacheCar(LeadingCacheCar leadingCacheCar);

    /**
     * 删除购物车
     * 
     * @param id 购物车主键
     * @return 结果
     */
    public int deleteLeadingCacheCarById(Long id);

    /**
     * 批量删除购物车
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLeadingCacheCarByIds(String[] ids);
}
