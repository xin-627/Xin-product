package com.ruoyi.leading.mapper;

import java.util.List;

import com.ruoyi.leading.domain.LeadingOrderInformation;
import com.ruoyi.leading.domain.PClientInformation;
import org.springframework.stereotype.Repository;

/**
 * 客户信息Mapper接口
 * 
 * @author xin_xin
 * @date 2022-05-21
 */
@Repository
public interface PClientInformationMapper 
{
    /**
     * 查询客户信息
     * 
     * @param id 客户信息主键
     * @return 客户信息
     */
    public PClientInformation selectPClientInformationById(Long id);

    /**
     * 查询客户信息
     *
     * @param id 客户信息主键
     * @return 客户信息
     */
    public PClientInformation listId(Long id);


    /**
     * 查询客户信息列表
     * 
     * @param pClientInformation 客户信息
     * @return 客户信息集合
     */
    public List<PClientInformation> selectPClientInformationList(PClientInformation pClientInformation);

    /**
     * 新增客户信息
     * 
     * @param pClientInformation 客户信息
     * @return 结果
     */
    public int insertPClientInformation(PClientInformation pClientInformation);

    /**
     * 修改客户信息
     * 
     * @param pClientInformation 客户信息
     * @return 结果
     */
    public int updatePClientInformation(PClientInformation pClientInformation);

    /**
     * 修改客户信息时同步修改
     * 订单数据 2022-06-21
     * @return 结果
     */
    public int updateLeadingOrderInformation(PClientInformation pClientInformation);

    /**
     * 删除客户信息
     * 
     * @param id 客户信息主键
     * @return 结果
     */
    public int deletePClientInformationById(Long id);

    /**
     * 批量删除客户信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePClientInformationByIds(String[] ids);

    /**
     * 订单创建单号查询
     * @return
     */
    public PClientInformation numberSearch();
}
