package com.ruoyi.leading.mapper;

import java.util.List;
import com.ruoyi.leading.domain.LeadingOrderInformation;
import org.springframework.stereotype.Repository;

/**
 * 订单主Mapper接口
 * 
 * @author xin-xin
 * @date 2022-02-10
 */
@Repository
public interface LeadingOrderInformationMapper 
{
    /**
     * 查询订单主
     * 
     * @param id 订单主主键
     * @return 订单主
     */
    public LeadingOrderInformation selectLeadingOrderInformationById(Long id);

    /**
     * 查询订单主列表
     * 
     * @param leadingOrderInformation 订单主
     * @return 订单主集合
     */
    public List<LeadingOrderInformation> selectLeadingOrderInformationList(LeadingOrderInformation leadingOrderInformation);

    /**
     * 新增订单主
     * 
     * @param leadingOrderInformation 订单主
     * @return 结果
     */
    public int insertLeadingOrderInformation(LeadingOrderInformation leadingOrderInformation);

    /**
     * 修改订单主
     * 
     * @param leadingOrderInformation 订单主
     * @return 结果
     */
    public int updateLeadingOrderInformation(LeadingOrderInformation leadingOrderInformation);

    /**
     * 删除订单主
     * 
     * @param id 订单主主键
     * @return 结果
     */
    public int deleteLeadingOrderInformationById(Long id);

    /**
     * 批量删除订单主
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLeadingOrderInformationByIds(String[] ids);

    /**
     * 批量修改订单数据
     * @param leadingOrderInformation
     * @return
     */
    public int orderBatchUpdate(LeadingOrderInformation leadingOrderInformation);



    /**
     * 订单创建单号查询
     * @return
     */
    public LeadingOrderInformation numberSearch();

    /**
     * 查询客户订单列表
     *
     * @param leadingOrderInformation 订单主
     * @return 订单主集合
     */
    public List<LeadingOrderInformation> selectCustomerGroupList(LeadingOrderInformation leadingOrderInformation);
}
