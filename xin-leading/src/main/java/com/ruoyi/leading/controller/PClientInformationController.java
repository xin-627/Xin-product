package com.ruoyi.leading.controller;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.security.PermissionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.leading.domain.PClientInformation;
import com.ruoyi.leading.service.IPClientInformationService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * 客户信息Controller
 * 
 * @author xin_xin
 * @date 2022-05-21
 */
@Controller
@RequestMapping("clientInformation")
public class PClientInformationController extends BaseController
{
    private String prefix = "leading/personal/client";

    @Autowired
    private IPClientInformationService pClientInformationService;

    @RequiresPermissions("leading:clientInformation:view")
    @GetMapping()
    public String clientInformation(ModelMap mmap)
    {
        mmap.put("userId", getSysUser().getUserId());
        return prefix + "/information";
    }

    /**
     * 查询客户信息列表
     */
    @RequiresPermissions("leading:clientInformation:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PClientInformation pClientInformation)
    {
        startPage();
        List<PClientInformation> list = pClientInformationService.selectPClientInformationList(pClientInformation);
        return getDataTable(list);
    }

    /**
     * 根据id查询客户信息列表
     * 售后订单详情页有用
     * 2022-06-09
     */
    @RequiresPermissions("leading:clientInformation:listId")
    @PostMapping("/listId/{clientId}")
    @ResponseBody
    public JSONObject listId(@PathVariable("clientId") Long clientId)
    {
        String userName = (String) PermissionUtils.getPrincipalProperty("userName");
        PClientInformation pClientInformation = pClientInformationService.listId(clientId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", pClientInformation);
        jsonObject.put("userName", userName);
        jsonObject.put("code", 200);
        jsonObject.put("msg", "查询成功");
        return jsonObject;
    }


    /**
     * 导出客户信息列表
     */
    @RequiresPermissions("leading:clientInformation:export")
    @Log(title = "客户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PClientInformation pClientInformation)
    {
        List<PClientInformation> list = pClientInformationService.selectPClientInformationList(pClientInformation);
        ExcelUtil<PClientInformation> util = new ExcelUtil<PClientInformation>(PClientInformation.class);
        return util.exportExcel(list, "客户信息数据");
    }

    /**
     * 新增客户信息
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        SysUser sysUser = pClientInformationService.getSysUser();
        mmap.put("sysUser", sysUser);
        return prefix + "/add";
    }

    /**
     * 新增保存客户信息
     */
    @RequiresPermissions("leading:clientInformation:add")
    @Log(title = "客户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PClientInformation pClientInformation)
    {
        return toAjax(pClientInformationService.insertPClientInformation(pClientInformation));
    }

    /**
     * 修改客户信息
     */
    @RequiresPermissions("leading:clientInformation:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap, HttpServletRequest request)
    {
        PClientInformation pClientInformation = pClientInformationService.selectPClientInformationById(id);
        String identification = request.getParameter("identification");//操作标识,用于刷新界面效果
        mmap.put("pClientInformation", pClientInformation);
        mmap.put("identification", identification);
        return prefix + "/edit";
    }




    /**
     * 修改保存客户信息
     */
    @RequiresPermissions("leading:clientInformation:edit")
    @Log(title = "客户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PClientInformation pClientInformation)
    {
        return toAjax(pClientInformationService.updatePClientInformation(pClientInformation));
    }

    /**
     * 删除客户信息
     */
    @RequiresPermissions("leading:clientInformation:remove")
    @Log(title = "客户信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pClientInformationService.deletePClientInformationByIds(ids));
    }
}
