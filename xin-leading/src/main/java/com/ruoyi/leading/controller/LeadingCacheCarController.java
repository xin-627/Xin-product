package com.ruoyi.leading.controller;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.backstage.domain.PLogo;
import com.ruoyi.backstage.mapper.PLogoMapper;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.mapper.SysUserRoleMapper;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.leading.domain.LeadingCacheCar;
import com.ruoyi.leading.service.LeadingCacheCarService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * 购物车Controller
 * 
 * @author xin-xin
 * @date 2022-01-19
 */
@Controller
@RequestMapping("/leadingCacheCar")
public class LeadingCacheCarController extends BaseController
{
    private String prefix = "leading/cacheCar";

    @Autowired
    private LeadingCacheCarService leadingCacheCarService;




    /**
     * 数据查看所有权限
     * 购物车 页面判断用-xin
     * 正常使用 只做记录
     * @return
     */
    //@RequiresPermissions("leadingCacheCar:cacheCar:check")



    /**
     * 获取登录状态-xin
     * 系统在没登录的时候默认返回{code: "1", msg: "未登录或登录超时。请重新登录"}
     */
    @PostMapping("/getSession")
    @ResponseBody
    public JSONObject session() {
        SysUser user = getSysUser();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 0);
        jsonObject.put("user", user);
        return jsonObject;
    }

    /**
     * 查询购物车列表
     */
    @RequiresPermissions("leadingCacheCar:cacheCar:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LeadingCacheCar leadingCacheCar)
    {
        startPage();
        List<LeadingCacheCar> list = leadingCacheCarService.selectLeadingCacheCarList(leadingCacheCar);
        return getDataTable(list);
    }


    /**
     * 新增购数据
     */
    @RequiresPermissions("leadingCacheCar:cacheCar:add")
    @Log(title = "新增选购数据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(LeadingCacheCar leadingCacheCar, HttpServletRequest request)
    {
        int i = leadingCacheCarService.insertLeadingCacheCar(leadingCacheCar, request);
        if (i > 0) {
            return AjaxResult.success("添加成功!");
        } else if (i == -1) {
            return AjaxResult.error("添加失败-色号数据获取失败-请联系管理员!!!");
        } else {
            return AjaxResult.error("添加失败-请联系管理员!!!");
        }
    }


    /**
     * 去购物车 自定义页(常规)
     * @return
     */
    @RequiresPermissions("leadingCacheCar:cacheCar:custom")
    @RequestMapping("/custom/{id}")
    public String custom(@PathVariable("id") Long id, ModelMap mmap) {
        LeadingCacheCar leadingCacheCar = leadingCacheCarService.selectLeadingCacheCarById(id);
        mmap.put("leadingCacheCar", leadingCacheCar);
        return  prefix+"/custom";
    }

    /**
     * 去购物车 自定义页(沙发/非常规)
     * @return
     */
    @RequiresPermissions("leadingCacheCar:cacheCar:customSofa")
    @RequestMapping("/customSofa/{id}")
    public String customSofa(@PathVariable("id") Long id, ModelMap mmap)
    {
        LeadingCacheCar leadingCacheCar = leadingCacheCarService.selectLeadingCacheCarById(id);
        mmap.put("leadingCacheCar", leadingCacheCar);
        return  prefix+"/custom-sofa";
    }

    /**
     * 修改保存购物车数据
     */
    @RequiresPermissions("leadingCacheCar:cacheCar:cacheCarEdit")
    @Log(title = "修改保存购物车数据", businessType = BusinessType.UPDATE)
    @PostMapping("/cacheCarEdit")
    @ResponseBody
    public AjaxResult cacheCarEdit(LeadingCacheCar leadingCacheCar)
    {
        return toAjax(leadingCacheCarService.updateLeadingCacheCar(leadingCacheCar));
    }








    /**
     * 修改购物车
     */
    @RequiresPermissions("leadingCacheCar:cacheCar:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        LeadingCacheCar leadingCacheCar = leadingCacheCarService.selectLeadingCacheCarById(id);
        mmap.put("leadingCacheCar", leadingCacheCar);
        return prefix + "/edit";
    }

    /**
     * 修改保存购物车
     */
    @RequiresPermissions("leadingCacheCar:cacheCar:edit")
    @Log(title = "购物车", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(LeadingCacheCar leadingCacheCar)
    {
        return toAjax(leadingCacheCarService.updateLeadingCacheCar(leadingCacheCar));
    }

    /**
     * 删除购物车数据
     */
    @RequiresPermissions("leadingCacheCar:cacheCar:remove")
    @Log(title = "删除购物车数据", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(leadingCacheCarService.deleteLeadingCacheCarByIds(ids));
    }



    /**
     * 导出购物车列表
     */
    @RequiresPermissions("leadingCacheCar:cacheCar:export")
    @Log(title = "购物车", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(LeadingCacheCar leadingCacheCar)
    {
        List<LeadingCacheCar> list = leadingCacheCarService.selectLeadingCacheCarList(leadingCacheCar);
        ExcelUtil<LeadingCacheCar> util = new ExcelUtil<LeadingCacheCar>(LeadingCacheCar.class);
        return util.exportExcel(list, "购物车数据");
    }

}
