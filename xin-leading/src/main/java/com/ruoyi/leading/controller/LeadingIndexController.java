package com.ruoyi.leading.controller;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.backstage.domain.*;
import com.ruoyi.backstage.mapper.PLogoMapper;
import com.ruoyi.backstage.service.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.leading.domain.LeadingOrderInformation;
import com.ruoyi.leading.service.ILeadingOrderInformationService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/leading/index")
public class LeadingIndexController extends BaseController {

    @Autowired
    private ProductClassifyService productClassifyService;

    @Autowired
    private ProductCarouselService productCarouselService;

    @Autowired
    private ProductMainService productMainService;

    @Autowired
    private ProductSpecificationsService productSpecificationsService;

    @Autowired
    private ProductPigmentService productPigmentService;

    @Autowired
    private ILeadingOrderInformationService leadingOrderInformationService;

    @Autowired
    private IPRegisterService pRegisterService;

    @Autowired
    private IPLogoService pLogoService;


    private String prefix = "leading/index";

    @GetMapping()
    public String carousel() {
        return prefix + "/index";
    }

    /**
     * 是否开启记住我功能
     */
    @Value("${shiro.rememberMe.enabled: false}")
    private boolean rememberMe;


    /**
     * 前端登录
     */
    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response, ModelMap mmap) {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request)) {
            return ServletUtils.renderString(response, "{\"code\":\"1\",\"msg\":\"未登录或登录超时。请重新登录\"}");
        }
        // 是否开启记住我
        mmap.put("isRemembered", rememberMe);
        return "logins";
    }

    /**
     * 是否显示后端跳转按钮(默认隐藏)
     * @return
     * 只做记录显示
     */
    // @RequiresPermissions("leading:index:backstage")

/**注册申请--------------------------------START*/
    /**
     * 注册申请
     */
    @GetMapping("/register")
    public String register() {
        return "leading/register/register";
    }

    /**
     * 查询注册申请列表
     */
    @PostMapping("/registerList")
    @ResponseBody
    public TableDataInfo registerList(PRegister pRegister) {
        startPage();
        List<PRegister> list = pRegisterService.selectPRegisterList(pRegister);
        return getDataTable(list);
    }

    /**
     * 新增注册申请
     */
    @GetMapping("/registerAdd")
    public String registerAdd() {
        return "leading/register/add";
    }

    /**
     * 新增保存注册申请
     */
    @Log(title = "注册申请", businessType = BusinessType.INSERT)
    @PostMapping("/registerAdd")
    @ResponseBody
    public AjaxResult addSave(PRegister pRegister) {
        return toAjax(pRegisterService.insertPRegister(pRegister));
    }

    /**
     * 校验手机号码
     */
    @PostMapping("/checkPhoneUnique")
    @ResponseBody
    public String checkPhoneUnique(PRegister pRegister) {
        return pRegisterService.checkPhoneUnique(pRegister);
    }

/**注册申请--------------------------------END*/


    /**
     * 分类菜单树
     * 2022-01-11
     *
     * @return
     */
    @PostMapping("/treeClassifyData")
    @ResponseBody
    public Map<String, Object> treeClassifyData() {
        return productClassifyService.TreeUtilClassify();

    }

    /**
     * 2022-01-14
     * 轮播图数据
     *
     * @param productCarousel
     * @return
     */
    @PostMapping("/carouselList")
    @ResponseBody
    public TableDataInfo list(ProductCarousel productCarousel) {
        startPage();
        List<ProductCarousel> list = productCarouselService.selectProductCarouselList(productCarousel);
        return getDataTable(list);
    }

    /**
     * 2022-01-11
     * 轮播图展示
     **/
    @RequestMapping("/thumbnailPreview")
    @ResponseBody
    public void showImg(HttpServletRequest request, HttpServletResponse response) {
        productCarouselService.showImg(request, response);
    }


    /**
     * 2022-03-30
     * svg展示
     **/
    @RequestMapping("/analysisOfSVG")
    @ResponseBody
    public void analysisOfSVG(HttpServletRequest request, HttpServletResponse response) {
        productCarouselService.analysisOfSVG(request, response);
    }

    /**
     * 产品展示
     *
     * @param productMain
     * @return
     */
    @GetMapping("/leadingProductDisplay")
    @ResponseBody
    public TableDataInfo list(ProductMain productMain) {
        startPage();
        List<ProductMain> list = productMainService.leadingProductMainList(productMain);
        return getDataTable(list);
    }

    /**
     * 产品详情
     *
     * @param id
     * @param mmap
     * @return
     */
    @GetMapping("/details/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        ProductMain productMain = productMainService.selectProductMainById(id);
        mmap.put("productMain", productMain);
        return prefix + "/details";
    }

    /**
     * 规格数据
     *
     * @param productSpecifications
     * @return
     */
    @PostMapping("/specificationsList")
    @ResponseBody
    public TableDataInfo list(ProductSpecifications productSpecifications) {
        startPage();
        List<ProductSpecifications> list = productSpecificationsService.selectProductSpecificationsList(productSpecifications);
        return getDataTable(list);
    }

    /**
     * 颜色 色号 数据
     *
     * @param productPigment
     * @return
     */
    @PostMapping("/pigmentList")
    @ResponseBody
    public TableDataInfo list(ProductPigment productPigment) {
        startPage();
        List<ProductPigment> list = productPigmentService.selectProductPigmentList(productPigment);
        return getDataTable(list);
    }


    /**
     * ******************************前端页面跳转 全部写在此处 写到别处 未登录会自动跳转到后端登录页  框架原因  ************************************
     */


    /**
     * 图片显示
     * @param request  请求
     * @param response 响应
     * @author xin_xin
     * @date 2022-11-05 15:03:18
     */

    @RequestMapping("/pictureDisplay")
    @ResponseBody
    public void pictureDisplay(HttpServletRequest request, HttpServletResponse response) {
        productMainService.pictureDisplay(request, response);
    }

    /**
     * 图片显示  logo
     * @param request  请求
     * @param response 响应
     * @author xin_xin
     * @date 2022-11-05 15:03:18
     */

    @RequestMapping("/pictureDisplayLogo")
    @ResponseBody
    public void pictureDisplayLogo(HttpServletRequest request, HttpServletResponse response) {
        pLogoService.pictureDisplayLogo(request, response);
    }

    /**
     * 去购物车页面
     *
     * @return
     */
    @RequestMapping("/cacheCarIndex")
    public String cacheCarIndex() {
        return "leading/cacheCar/cacheCar";
    }


    /**
     * 去个人中心页面
     *
     * @return
     */
    @RequestMapping("/personalIndex")
    public String personalIndex() {
        return "leading/personal/index";
    }

    /**
     * Go 个人中心  控制台
     *
     * @return
     */
    @RequestMapping("/controlPanel")
    public String controlPanel(ModelMap mmap) {
        mmap.put("userId", getSysUser().getUserId());
        return "leading/personal/controlPanel";
    }


    /**
     * Go 待提交页面
     *
     * @return
     */
    @RequestMapping("/expect")
    public String expect(ModelMap mmap) {
        mmap.put("userId", getSysUser().getUserId());
        return "leading/personal/expect";
    }


    /**
     * Go 订单归属
     *
     * @return
     */
    @RequiresPermissions("leading:information:orderBelonging")
    @RequestMapping("/orderBelonging/{ids}")
    public String orderBelonging(@PathVariable("ids") String ids, ModelMap mmap) {
        mmap.put("ids", ids);
        mmap.put("userId", getSysUser().getUserId());
        return "leading/personal/client/belonging";
    }

    /**
     * Go 驳回页面
     *
     * @return
     */
    @RequestMapping("/reject")
    public String reject(ModelMap mmap) {
        mmap.put("userId", getSysUser().getUserId());
        return "leading/personal/reject";
    }


    /**
     * Go 待审核页面
     *
     * @return
     */
    @RequestMapping("/toAudit")
    public String toAudit(ModelMap mmap) {
        mmap.put("userId", getSysUser().getUserId());
        return "leading/personal/toAudit";
    }

    /**
     * Go 全部数据页
     *
     * @return
     */
    @RequestMapping("/overallData")
    public String overallData(ModelMap mmap) {
        mmap.put("userId", getSysUser().getUserId());
        return "leading/personal/overallData";
    }


    /**
     * Go 客户分组订单
     *
     * @return
     */
    @RequestMapping("/customerGroup")
    public String customerGroup(ModelMap mmap) {
        mmap.put("userId", getSysUser().getUserId());
        return "leading/personal/customerGroup";
    }


    /**
     * 客户分组订单详情
     */
    @GetMapping("/customerDetails/{clientId}")
    public String customerDetails(@PathVariable("clientId") Long clientId, ModelMap mmap) {
        mmap.put("userId", getSysUser().getUserId());
        mmap.put("clientId", clientId);
        return "leading/personal/customerDetails";
    }

    /**
     * Go 店面订单
     *
     * @return
     */
    @RequestMapping("/storefrontData")
    public String storefrontData(ModelMap mmap) {
        SysUser user = getSysUser();
        mmap.put("storefrontId", user.getStorefront());
        return "leading/personal/storefront/orderForm";
    }


    /**
     * 店面订单详情
     */
    @GetMapping("/storefrontDetails/{clientId}")
    public String storefrontDetails(@PathVariable("clientId") Long clientId, ModelMap mmap) {
        SysUser user = getSysUser();
        mmap.put("storefrontId", user.getStorefront());
        mmap.put("clientId", clientId);
        return "leading/personal/storefront/orderFormDetails";
    }

    /**
     * GO 订单编辑页
     */
    @RequiresPermissions("leading:information:personalEdit")
    @GetMapping("/personalEdit/{id}")
    public String expectEdit(@PathVariable("id") Long id, ModelMap mmap, HttpServletRequest request) {
        LeadingOrderInformation leadingOrderInformation = leadingOrderInformationService.selectLeadingOrderInformationById(id);
        String statusCode = request.getParameter("statusCode");
        mmap.put("leadingOrderInformation", leadingOrderInformation);
        mmap.put("statusCode", statusCode);
        return "leading/personal/edit";
    }

    /**
     * GO 订单详情页
     */
    @RequiresPermissions("leading:information:personalDetails")
    @GetMapping("/personalDetails/{id}")
    public String expectDetails(@PathVariable("id") Long id, ModelMap mmap) {
        LeadingOrderInformation leadingOrderInformation = leadingOrderInformationService.selectLeadingOrderInformationById(id);
        mmap.put("leadingOrderInformation", leadingOrderInformation);
        return "leading/personal/details";
    }


}
