package com.ruoyi.leading.controller;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.backstage.domain.ProductOrderInformation;
import com.ruoyi.backstage.mapper.ProductOrderInformationMapper;
import com.ruoyi.backstage.service.ProductMainService;
import com.ruoyi.backstage.service.ProductOrderInformationService;
import com.ruoyi.backstage.service.ProductPigmentService;
import com.ruoyi.common.utils.security.PermissionUtils;
import com.ruoyi.leading.domain.PClientInformation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.leading.domain.LeadingOrderInformation;
import com.ruoyi.leading.service.ILeadingOrderInformationService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 个人中心Controller
 *
 * @author xin-xin
 * @date 2022-02-10
 */
@Controller
@RequestMapping("/leadingInformation")
public class LeadingOrderInformationController extends BaseController
{
    private String prefix = "leading/personal";

    @Autowired
    private ILeadingOrderInformationService leadingOrderInformationService;

    @Autowired
    private ProductPigmentService productPigmentService;

    @Autowired
    private ProductOrderInformationService productOrderInformationService;

    @Autowired
    private ProductMainService productMainService;

    /**
     * 个人中心 数据是否查看所有 权限
     * 页面判断用-xin
     * 正常使用
     * @return
     */
    //@RequiresPermissions("leading:information:personalDataAll")


    /**
     * 销量前二十产品
     */
    @PostMapping("/salesVolumeList")
    @ResponseBody
    public TableDataInfo salesVolumeList(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.searchSalesVolumeList(productOrderInformation);
        return getDataTable(list);
    }

    /**
     * 产品占比
     */
    @PostMapping("/classifyList")
    @ResponseBody
    public TableDataInfo classifyList(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.searchClassifyList(productOrderInformation);
        return getDataTable(list);
    }

    /**
     * 数据统计
     * 2022-06-26
     */
    @PostMapping("/dataStatistics/{statusId}")
    @ResponseBody
    public JSONObject dataStatistics(@PathVariable("statusId") Long statusId)
    {
        ProductOrderInformation dataStatistics = productOrderInformationService.dataStatistics(statusId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", dataStatistics);
        jsonObject.put("code", 200);
        jsonObject.put("msg", "查询成功");
        return jsonObject;
    }


    /**
     * 查询个人中心列表
     */
    @RequiresPermissions("leading:information:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LeadingOrderInformation leadingOrderInformation)
    {
        startPage();
        List<LeadingOrderInformation> list = leadingOrderInformationService.selectLeadingOrderInformationList(leadingOrderInformation);
        return getDataTable(list);
    }


    /**
     * 查询客户信息列表
     */
    @RequiresPermissions("leading:information:customerGroup")
    @PostMapping("/customerGroup")
    @ResponseBody
    public TableDataInfo customerGroup(LeadingOrderInformation leadingOrderInformation)
    {
        startPage();
        List<LeadingOrderInformation> list = leadingOrderInformationService.selectCustomerGroupList(leadingOrderInformation);
        return getDataTable(list);
    }

    /**
     * 批量修改保存订单数据 (客户组归属用)
     */
    @RequiresPermissions("leading:information:orderBatchUpdate")
    @Log(title = "批量修改保存订单数据", businessType = BusinessType.UPDATE)
    @PostMapping("/orderBatchUpdate")
    @ResponseBody
    public AjaxResult orderBatchUpdateSave(HttpServletRequest request, String id) {
        int i = leadingOrderInformationService.orderBatchUpdateSave(request, id);
        if (i > 0) {
            return AjaxResult.success("修改成功!");
        }else {
            return AjaxResult.error("修改失败!请联系管理员!!!");
        }
    }


    /**
     * 修改保存订单详情数据
     */
    @RequiresPermissions("leading:information:edit")
    @Log(title = "修改保存订单详情数据", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(LeadingOrderInformation leadingOrderInformation) {
        return toAjax(leadingOrderInformationService.updateLeadingOrderInformation(leadingOrderInformation));
    }



    /**
     * 订单修改
     * 订单提交审核
     */
    @RequiresPermissions("leading:information:submitAudit")
    @Log(title = "订单修改", businessType = BusinessType.UPDATE)
    @PostMapping("/submitAudit")
    @ResponseBody
    public AjaxResult submitAudit(LeadingOrderInformation leadingOrderInformation) {
        return toAjax(leadingOrderInformationService.updateLeadingOrderInformation(leadingOrderInformation));
    }

    /**
     * 订单批量提交审核
     * 2022-07-06
     */
    @RequiresPermissions("leading:information:bulkOperation")
    @Log(title = "订单批量提交审核", businessType = BusinessType.UPDATE)
    @PostMapping("/bulkOperation")
    @ResponseBody
    public AjaxResult cCheckObliterateUpdate(HttpServletRequest request, String id)
    {
        int i = leadingOrderInformationService.bulkOperation(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        }else if(i == -1){
            return AjaxResult.warn("当前订单-未分配客户,不允许提交.请选中订单-进行订单归属");
        }else if(i == -2){
            return AjaxResult.warn("自定义规格为空-不允许提交-请将数据补全后再次提交");
        }else if(i == -3){
            return AjaxResult.warn("自定义色号为空-不允许提交-请将数据补全后再次提交");
        }else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }



    /**
     * 导出订单主列表
     */
    @RequiresPermissions("leading:information:export")
    @Log(title = "导出个人中心数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(LeadingOrderInformation leadingOrderInformation) {
        List<LeadingOrderInformation> list = leadingOrderInformationService.selectLeadingOrderInformationList(leadingOrderInformation);
        ExcelUtil<LeadingOrderInformation> util = new ExcelUtil<LeadingOrderInformation>(LeadingOrderInformation.class);
        return util.exportExcel(list, "订单主数据");
    }



    /**
     * 新增保存订单主
     */
    @RequiresPermissions("leading:information:add")
    @Log(title = "生成订单数据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HttpServletRequest request, String id) {
        int i = leadingOrderInformationService.insertLeadingOrderInformation(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败-请联系管理员!!!");
        }
    }

    /**
     * 删除订单主
     */
    @RequiresPermissions("leading:information:remove")
    @Log(title = "删除个人中心数据", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(leadingOrderInformationService.deleteLeadingOrderInformationByIds(ids));
    }
}
