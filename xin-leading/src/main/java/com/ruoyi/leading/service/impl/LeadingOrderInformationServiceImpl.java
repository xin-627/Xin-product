package com.ruoyi.leading.service.impl;
import java.util.Arrays;
import java.util.List;

import com.ruoyi.backstage.domain.ProductOrderInformation;
import com.ruoyi.backstage.util.UtilDate;
import com.ruoyi.backstage.util.UtilGetSys;
import com.ruoyi.leading.domain.LeadingCacheCar;
import com.ruoyi.leading.domain.PClientInformation;
import com.ruoyi.leading.mapper.LeadingCacheCarMapper;
import com.ruoyi.leading.mapper.PClientInformationMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.leading.mapper.LeadingOrderInformationMapper;
import com.ruoyi.leading.domain.LeadingOrderInformation;
import com.ruoyi.leading.service.ILeadingOrderInformationService;
import com.ruoyi.common.core.text.Convert;
import javax.servlet.http.HttpServletRequest;

import static com.ruoyi.backstage.util.UtilDate.switchingTimeShort;


/**
 * 订单主Service业务层处理
 *
 * @author xin-xin
 * @date 2022-02-10
 */
@Service
public class LeadingOrderInformationServiceImpl implements ILeadingOrderInformationService
{
    @Autowired
    private LeadingOrderInformationMapper leadingOrderInformationMapper;

    @Autowired
    private LeadingCacheCarMapper leadingCacheCarMapper;

    @Autowired
    private PClientInformationMapper pClientInformationMapper;

    /**
     * 查询订单主
     *
     * @param id 订单主主键
     * @return 订单主
     */
    @Override
    public LeadingOrderInformation selectLeadingOrderInformationById(Long id)
    {
        return leadingOrderInformationMapper.selectLeadingOrderInformationById(id);
    }

    /**
     * 查询订单主列表
     *
     * @param leadingOrderInformation 订单主
     * @return 订单主
     */
    @Override
    public List<LeadingOrderInformation> selectLeadingOrderInformationList(LeadingOrderInformation leadingOrderInformation)
    {
        return leadingOrderInformationMapper.selectLeadingOrderInformationList(leadingOrderInformation);
    }

    /**
     * 查询订单主列表
     *
     * @param leadingOrderInformation 订单主
     * @return 订单主
     */
    @Override
    public List<LeadingOrderInformation> selectCustomerGroupList(LeadingOrderInformation leadingOrderInformation)
    {
        return leadingOrderInformationMapper.selectCustomerGroupList(leadingOrderInformation);
    }





    /**
     * 批量修改订单数据
     * @param request
     * @param id
     * @return
     */
    @Override
    public int orderBatchUpdateSave(HttpServletRequest request, String id) {
        int integer = 0;
        String clientId = request.getParameter("clientId");//客户组id
        PClientInformation clientData =  pClientInformationMapper.selectPClientInformationById(Long.valueOf(clientId));
        List<String> list = Arrays.asList(id.split(","));
        //必须判断空值 前端传值有空不然会覆盖
        for (int i = 0; i < list.size(); i++) {
            LeadingOrderInformation info = new LeadingOrderInformation();
            info.setId(Long.valueOf(list.get(i)));
            info.setClientId(Long.valueOf(clientId));
            info.setClientNumber(clientData.getClientNumber());//客户编号
            info.setPlanDeliveryPeriod(clientData.getPlanDeliveryPeriod());//计划交期
            info.setCustomerName(clientData.getCustomerName());//客户姓名
            info.setCustomerTelephone(clientData.getCustomerTelephone());//客户电话
            info.setCustomerAddress(clientData.getCustomerAddress());//客户地址
            info.setStorefront(clientData.getStorefront());//下单店面
            info.setStorefrontCoding(clientData.getStorefrontCoding());//下单店面id
            info.setRefreshName(UtilGetSys.getUserName());//更新者
            integer = leadingOrderInformationMapper.orderBatchUpdate(info);
        }
        return integer;
    }



    /**
     * 新增订单
     * @param request
     * @param id
     * @return
     */
    @Override
    public int insertLeadingOrderInformation(HttpServletRequest request, String id)
    {
        String orderNumber = String.valueOf(0);
        String maxOrderNumber;
        int interger = 0;
        List<String> list = Arrays.asList(id.split(","));//id集合
        for (int i = 0; i < list.size(); i++) {
            LeadingOrderInformation numberSearch = leadingOrderInformationMapper.numberSearch();// 从数据库查询出的最大编号
            if (numberSearch == null){
                 maxOrderNumber = "CP22020000";
            }else{
                 maxOrderNumber = numberSearch.getOrderNumber();
            }
            orderNumber = OrderNumGenerate.getInstance().generateNextNumber(maxOrderNumber);
            LeadingCacheCar LCCData = leadingCacheCarMapper.selectLeadingCacheCarById(Long.valueOf(list.get(i)));
            LeadingOrderInformation data = new LeadingOrderInformation();
            if(LCCData.getMainId() == 250){
                data.setProductName(LCCData.getCustomName());
            }else{
                data.setProductName(LCCData.getProductName());//名称
            }
            data.setCustomName(LCCData.getCustomName());//自定义-品类
            data.setCustomDyestuff(LCCData.getCustomDyestuff());//自定义-色号
            data.setCustomSpecifications(LCCData.getCustomSpecifications());//自定义-规格
            data.setCustomSkin(LCCData.getCustomSkin());//自定义-型号
            data.setCustomQuality(LCCData.getCustomQuality());//自定义材质
            data.setCustomOther(LCCData.getCustomOther());//自定义其他
            data.setCustomChangeColour(LCCData.getCustomChangeColour());//自定义-功能位方向
            data.setIdentification(LCCData.getIdentification());//特殊标识
            data.setYxNumber(LCCData.getYxNumber());//一新编号
            data.setSupplierNumber(LCCData.getSupplierNumber());//供应商编号
            data.setNumber(LCCData.getNumber());//数量
            data.setMainId(LCCData.getMainId());//产品列id
            data.setMaterialQuality(LCCData.getMaterialQuality());//材质
            data.setSpecifications(LCCData.getSpecifications());//规格
            data.setDyestuff(LCCData.getDyestuff());//色号
            data.setClassify(LCCData.getClassify());//分类
            data.setSellingPoint(LCCData.getSellingPoint());//卖点
            data.setProductRemarks(LCCData.getProductRemarks());//产品备注
            data.setcPeriod(LCCData.getcPeriod());//生产周期
            data.setProductImg(LCCData.getProductImg());//产品图片
            data.setSupplier(LCCData.getSupplier());//供应商
            data.setLabel(LCCData.getLabel());//标签
            data.setCreator(LCCData.getCreator());//零售价
            data.setOrdersStatus(LCCData.getOrdersStatus());//下单人职位
            data.setOrders(LCCData.getOrders());//下单人
            data.setCreator(LCCData.getOrders());//创建者
            data.setOrderNumber(orderNumber);//订单编号
            data.setStatusId(LCCData.getStatusId());//下单人id
            data.setStorefront(LCCData.getStorefront());//下单所属店面
            data.setContactWay(LCCData.getContactWay());//下单电话
            data.setStorefrontCoding(LCCData.getStorefrontCoding());//店面编号
            data.setStorefrontType(LCCData.getStorefrontType());//店面类型
            data.setStatusCode(Long.valueOf("5"));//状态
            data.setOrderType(1L);//正常单
            data.setStateDescription("待提交");
            data.setcCheckState(Long.valueOf(0));
            interger = leadingOrderInformationMapper.insertLeadingOrderInformation(data);
            if (interger > 0) {
                leadingCacheCarMapper.deleteLeadingCacheCarById(Long.valueOf(list.get(i)));
            } else {
                break;
            }
        }
        return interger;
    }


    /**
     * 修改订单主
     *
     * @param leadingOrderInformation 订单主
     * @return 结果
     */
    @Override
    public int updateLeadingOrderInformation(LeadingOrderInformation leadingOrderInformation)
    {
        if(leadingOrderInformation.getMainId() == 250){
            leadingOrderInformation.setProductName(leadingOrderInformation.getCustomName());
        }else{
            leadingOrderInformation.setProductName(leadingOrderInformation.getProductName());//名称
        }
        return leadingOrderInformationMapper.updateLeadingOrderInformation(leadingOrderInformation);
    }


    /**
     * 订单操作
     *
     * @param request
     * @param id
     * @return
     */
    @Override
    public int bulkOperation(HttpServletRequest request, String id) {
        int integer = 0;
        String checkStatus = request.getParameter("checkStatus");//操作标识
        List<String> list = Arrays.asList(id.split(","));
        //必须判断空值 前端传值有空不然会覆盖
        for (int i = 0; i < list.size(); i++) {
            LeadingOrderInformation info = new LeadingOrderInformation();
            info.setId(Long.valueOf(list.get(i)));
            if(StringUtils.isNotEmpty(checkStatus)) {
                if (checkStatus.equals("submitAudit")) {//批量提交审核
                    LeadingOrderInformation data =  leadingOrderInformationMapper.selectLeadingOrderInformationById(Long.valueOf(list.get(i)));
                    if(StringUtils.isEmpty(data.getClientNumber())){
                        integer = -1;
                        return integer;
                    }
                    if (data.getSpecifications().equals("自定义")){//自定义规格
                        if(StringUtils.isEmpty(data.getCustomSpecifications())){
                            integer = -2;
                            return integer;
                        }
                    }
                    if (data.getDyestuff().equals("自定义")){//自定义色号
                        if(StringUtils.isEmpty(data.getCustomDyestuff())){
                            integer = -3;
                            return integer;
                        }
                    }
                    if (StringUtils.isNotEmpty(data.getClientNumber())){
                        info.setStatusCode(Long.valueOf("10"));
                        info.setStateDescription("待审核");
                        info.setOrderTime(switchingTimeShort(UtilDate.getStringDateShort()));
                    }
                }
                info.setRefreshName(UtilGetSys.getUserName());//更新者
            }
            integer = leadingOrderInformationMapper.orderBatchUpdate(info);
        }
        return integer;
    }


    /**
     * 修改订单主
     *
     * @param leadingOrderInformation 订单主
     * @return 结果
     * 暂时不用 2022-05-18
     */
   /* @Override
    public int updateLeadingOrderInformation(LeadingOrderInformation leadingOrderInformation)
    {
        return leadingOrderInformationMapper.updateLeadingOrderInformation(leadingOrderInformation);
    }*/



    /**
     * 批量删除订单主
     *
     * @param ids 需要删除的订单主主键
     * @return 结果
     */
    @Override
    public int deleteLeadingOrderInformationByIds(String ids)
    {
        return leadingOrderInformationMapper.deleteLeadingOrderInformationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单主信息
     *
     * @param id 订单主主键
     * @return 结果
     */
    @Override
    public int deleteLeadingOrderInformationById(Long id)
    {
        return leadingOrderInformationMapper.deleteLeadingOrderInformationById(id);
    }


}
