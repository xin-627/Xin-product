package com.ruoyi.leading.service.impl;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.backstage.domain.ProductMain;
import com.ruoyi.backstage.domain.ProductPigment;
import com.ruoyi.backstage.mapper.ProductMainMapper;
import com.ruoyi.backstage.mapper.ProductPigmentMapper;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.security.PermissionUtils;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.mapper.SysUserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.leading.mapper.LeadingCacheCarMapper;
import com.ruoyi.leading.domain.LeadingCacheCar;
import com.ruoyi.leading.service.LeadingCacheCarService;
import com.ruoyi.common.core.text.Convert;

import javax.servlet.http.HttpServletRequest;

/**
 * 购物车Service业务层处理
 * 
 * @author xin-xin
 * @date 2022-01-19
 */
@Service
public class LeadingCacheCarServiceImpl implements LeadingCacheCarService
{
    @Autowired
    private LeadingCacheCarMapper leadingCacheCarMapper;

    @Autowired
    private ProductPigmentMapper productPigmentMapper;

    @Autowired
    private ProductMainMapper productMainMapper;

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private SysDictDataMapper dictDataMapper;


    /**
     * 查询购物车
     * 
     * @param id 购物车主键
     * @return 购物车
     */
    @Override
    public LeadingCacheCar selectLeadingCacheCarById(Long id)
    {
        return leadingCacheCarMapper.selectLeadingCacheCarById(id);
    }

    /**
     * 查询购物车列表
     * 
     * @param leadingCacheCar 购物车
     * @return 购物车
     */
    @Override
    public List<LeadingCacheCar> selectLeadingCacheCarList(LeadingCacheCar leadingCacheCar)
    {
        return leadingCacheCarMapper.selectLeadingCacheCarList(leadingCacheCar);
    }

    /**
     * 新增购物车
     * 
     * @param leadingCacheCar 购物车
     * @return 结果
     */
    @Override
    public int insertLeadingCacheCar(LeadingCacheCar leadingCacheCar, HttpServletRequest request) {
        SysUser sysUser = userMapper.selectUserById(getUserId());
        SysDictData dictData = dictDataMapper.selectDictDataById(Long.valueOf(sysUser.getStorefront()));
        Long mainId = Long.valueOf(request.getParameter("mainId"));//客户组id
        String pigmentId = request.getParameter("pigmentId");//id
        ProductMain productMainData = productMainMapper.selectProductMainById(mainId);
        int integer = 0;
        ProductPigment pigmentIdData = productPigmentMapper.pigmentIdDataSearch(pigmentId);//查询数据
        if(pigmentIdData != null && productMainData != null){
            leadingCacheCar.setMainId(productMainData.getId());//产品id
            leadingCacheCar.setYxNumber(productMainData.getYxNumber());//一新编号
            leadingCacheCar.setSupplierNumber(productMainData.getSupplierNumber());//供应商编号
            leadingCacheCar.setProductName(productMainData.getProductName());//产品名称
            leadingCacheCar.setMaterialQuality(productMainData.getMaterialQuality());//材质
            leadingCacheCar.setClassify(productMainData.getClassify());//分类
            leadingCacheCar.setSellingPoint(productMainData.getSellingPoint());//卖点
            leadingCacheCar.setProductRemarks(productMainData.getRemarks());//产品备注
            leadingCacheCar.setSupplier(productMainData.getSupplier());//供应商
            leadingCacheCar.setLabel(productMainData.getLabel());//标签
            leadingCacheCar.setIdentification(productMainData.getIdentification());//特殊标识
            leadingCacheCar.setcPeriod(productMainData.getcPeriod());//周期
            leadingCacheCar.setDyestuff(pigmentIdData.getPigment());//色号
            leadingCacheCar.setProductImg(pigmentIdData.getImages());//图片
            leadingCacheCar.setRetailPrice(pigmentIdData.getRetailPrice());//零售价
            leadingCacheCar.setOrders(sysUser.getUserName());//订购人
            leadingCacheCar.setOrdersStatus(sysUser.getRemark());//身份
            leadingCacheCar.setStatusId(sysUser.getUserId());//身份id
            leadingCacheCar.setCreator(sysUser.getUserName());//创建人
            leadingCacheCar.setContactWay(sysUser.getPhonenumber());//下单人电话
            leadingCacheCar.setStorefront(dictData.getDictLabel());//下单店面
            leadingCacheCar.setStorefrontCoding(sysUser.getStorefront());//店面编码
            leadingCacheCar.setStorefrontType(Long.valueOf(sysUser.getStorefrontType()));//店面类型
            integer = leadingCacheCarMapper.insertLeadingCacheCar(leadingCacheCar);
        }else{
            integer = -1;
        }
        return integer;
    }

    /**
     * 修改购物车
     * 
     * @param leadingCacheCar 购物车
     * @return 结果
     */
    @Override
    public int updateLeadingCacheCar(LeadingCacheCar leadingCacheCar)
    {
        return leadingCacheCarMapper.updateLeadingCacheCar(leadingCacheCar);
    }

    /**
     * 批量删除购物车
     * 
     * @param ids 需要删除的购物车主键
     * @return 结果
     */
    @Override
    public int deleteLeadingCacheCarByIds(String ids)
    {
        return leadingCacheCarMapper.deleteLeadingCacheCarByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除购物车信息
     * 
     * @param id 购物车主键
     * @return 结果
     */
    @Override
    public int deleteLeadingCacheCarById(Long id)
    {
        return leadingCacheCarMapper.deleteLeadingCacheCarById(id);
    }


    private Long getUserId() {
        return (Long) PermissionUtils.getPrincipalProperty("userId");
    }
}
