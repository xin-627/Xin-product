package com.ruoyi.leading.service;

import java.util.List;
import com.ruoyi.leading.domain.LeadingOrderInformation;

import javax.servlet.http.HttpServletRequest;

/**
 * 订单主Service接口
 * 
 * @author xin-xin
 * @date 2022-02-10
 */
public interface ILeadingOrderInformationService 
{
    /**
     * 查询订单主
     * 
     * @param id 订单主主键
     * @return 订单主
     */
    public LeadingOrderInformation selectLeadingOrderInformationById(Long id);

    /**
     * 查询订单主列表
     * 
     * @param leadingOrderInformation 订单主
     * @return 订单主集合
     */
    public List<LeadingOrderInformation> selectLeadingOrderInformationList(LeadingOrderInformation leadingOrderInformation);

    /**
     * 批量修改订单数据
     * @param request
     * @param id
     * @return
     */
    public int orderBatchUpdateSave(HttpServletRequest request, String id);


    /**
     * 批量修改订单数据
     * @param request
     * @param id
     * @return
     */
    public int bulkOperation(HttpServletRequest request, String id);


    /**
     * 新增订单
     * @param request
     * @param id
     * @return
     */
    public int insertLeadingOrderInformation(HttpServletRequest request, String id);

    /**
     * 修改订单主
     * 
     * @param leadingOrderInformation 订单主
     * @return 结果
     */
    public int updateLeadingOrderInformation(LeadingOrderInformation leadingOrderInformation);

    /**
     * 批量删除订单主
     * 
     * @param ids 需要删除的订单主主键集合
     * @return 结果
     */
    public int deleteLeadingOrderInformationByIds(String ids);

    /**
     * 删除订单主信息
     * 
     * @param id 订单主主键
     * @return 结果
     */
    public int deleteLeadingOrderInformationById(Long id);

    /**
     * 查询订单主列表
     *
     * @param leadingOrderInformation 订单主
     * @return 订单主集合
     */
    public List<LeadingOrderInformation> selectCustomerGroupList(LeadingOrderInformation leadingOrderInformation);
}
