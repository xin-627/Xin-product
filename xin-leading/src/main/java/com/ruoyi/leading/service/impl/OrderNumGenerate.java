package com.ruoyi.leading.service.impl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 生成统一格式（例如 XP2021040200001）订单号工具类
 *
 * @Author cht
 * @Date 2021/4/2 11:09:38
 */
public class OrderNumGenerate {
    private static final String SERIAL_NUMBER = "XXXX"; // 流水号格式
    private static OrderNumGenerate orderNumGenerater = null;

    private OrderNumGenerate() {
    }

    /**
     * 取得PrimaryGenerate的单例实现
     *
     * @return
     */
    public static OrderNumGenerate getInstance() {
        if (orderNumGenerater == null) {
            synchronized (OrderNumGenerate.class) {
                if (orderNumGenerater == null) {
                    orderNumGenerater = new OrderNumGenerate();
                }
            }
        }
        return orderNumGenerater;
    }

    /**
     * 订单编号生成
     */
    public static synchronized String generateNextNumber(String maxOrderNumber) {
        String nextNumber = null;
        String s = maxOrderNumber.substring(0, 2); //截取订单号前缀
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyMM");
        if (maxOrderNumber == null) {
            nextNumber = s + formatter.format(date) + "0001";
        } else {
            int count = SERIAL_NUMBER.length();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < count; i++) {
                sb.append("0");
            }
            String dateString = maxOrderNumber.substring(2, 6);
            if (formatter.format(date).compareTo(dateString) == 0) {
                DecimalFormat df = new DecimalFormat("0000");
                nextNumber = s + formatter.format(date) + df.format(1 + Integer.parseInt(maxOrderNumber.substring(6, 10)));
            } else {
                nextNumber = s + formatter.format(date) + "0001";
            }
        }
        return nextNumber;
    }

    /**
     * 维修单号生成
     */
    public static synchronized String generateMaintainNumber(String maxMaintainNumber) {
        String maintainNumber = null;
        String s = maxMaintainNumber.substring(0, 2); //截取订单号前缀
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyMM");
        if (maxMaintainNumber == null) {
            maintainNumber = s + formatter.format(date) + "001";
        } else {
            int count = SERIAL_NUMBER.length();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < count; i++) {
                sb.append("0");
            }
            String dateString = maxMaintainNumber.substring(2, 6);
            if (formatter.format(date).compareTo(dateString) == 0) {
                DecimalFormat df = new DecimalFormat("000");
                maintainNumber = s + formatter.format(date) + df.format(1 + Integer.parseInt(maxMaintainNumber.substring(6, 9)));
            } else {
                maintainNumber = s + formatter.format(date) + "001";
            }
        }
        return maintainNumber;
    }



    /**
     * 客户编号生成
     */
    public static synchronized String generateClientNextNumber(String maxClientNumber) {
        String clientNextNumber = null;
        String s = maxClientNumber.substring(0, 2); //截取订单号前缀
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyMM");
        if (maxClientNumber == null) {
            clientNextNumber = s + formatter.format(date) + "001";
        } else {
            int count = SERIAL_NUMBER.length();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < count; i++) {
                sb.append("0");
            }
            String dateString = maxClientNumber.substring(2, 6);
            if (formatter.format(date).compareTo(dateString) == 0) {
                DecimalFormat df = new DecimalFormat("000");
                clientNextNumber = s + formatter.format(date) + df.format(1 + Integer.parseInt(maxClientNumber.substring(6, 9)));
            } else {
                clientNextNumber = s + formatter.format(date) + "001";
            }
        }
        return clientNextNumber;
    }

/*    public static void main(String[] args) {
        String no = "CP2202003";
        no = OrderNumGenerate.getInstance().generateNextNumber(no);
        System.out.println(no);
    }*/

}
