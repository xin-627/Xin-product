package com.ruoyi.leading.service.impl;

import java.util.List;

import com.ruoyi.backstage.mapper.ProductOrderInformationMapper;
import com.ruoyi.backstage.util.UtilGetSys;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.leading.mapper.PClientInformationMapper;
import com.ruoyi.leading.domain.PClientInformation;
import com.ruoyi.leading.service.IPClientInformationService;
import com.ruoyi.common.core.text.Convert;

import static com.ruoyi.common.utils.ShiroUtils.getUserId;

/**
 * 客户信息Service业务层处理
 * 
 * @author xin_xin
 * @date 2022-05-21
 */
@Service
public class PClientInformationServiceImpl implements IPClientInformationService 
{
    @Autowired
    private PClientInformationMapper pClientInformationMapper;

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private ProductOrderInformationMapper productOrderInformationMapper;

    @Autowired
    private SysDictDataMapper dictDataMapper;
    /**
     * 查询客户信息
     * 
     * @param id 客户信息主键
     * @return 客户信息
     */
    @Override
    public PClientInformation selectPClientInformationById(Long id)
    {
        return pClientInformationMapper.selectPClientInformationById(id);
    }

    /**
     * 查询客户信息
     *
     * @param id 客户信息主键
     * @return 客户信息listId
     */
    @Override
    public PClientInformation listId(Long id)
    {
        return pClientInformationMapper.listId(id);
    }

    /**
     * 查询客户信息列表
     * 
     * @param pClientInformation 客户信息
     * @return 客户信息
     */
    @Override
    public List<PClientInformation> selectPClientInformationList(PClientInformation pClientInformation)
    {
        productOrderInformationMapper.updateClientInformation();//同步客户表数据
        return pClientInformationMapper.selectPClientInformationList(pClientInformation);
    }

    /**
     * 新增客户信息
     * 
     * @param pClientInformation 客户信息
     * @return 结果
     */
    @Override
    public int insertPClientInformation(PClientInformation pClientInformation)
    {
        SysUser sysUser = userMapper.selectUserById(getUserId());//查询用户信息
        SysDictData dictData = getSysDictData(pClientInformation);
        String clientNumber = String.valueOf(0);
        String maxClientNumber;
        PClientInformation numberSearchs = pClientInformationMapper.numberSearch();// 从数据库查询出的最大编号
        if (numberSearchs == null){
            maxClientNumber = "KH2202000";
        }else{
            maxClientNumber = numberSearchs.getClientNumber();
        }
        clientNumber = OrderNumGenerate.getInstance().generateClientNextNumber(maxClientNumber);
        pClientInformation.setStorefront(dictData.getDictLabel());//下单店面
        pClientInformation.setClientNumber(clientNumber);//客户编号
        pClientInformation.setOrders(sysUser.getUserName());
        pClientInformation.setStatusId(sysUser.getUserId());
        pClientInformation.setCreator(sysUser.getUserName());
        return pClientInformationMapper.insertPClientInformation(pClientInformation);
    }

    private SysDictData getSysDictData(PClientInformation pClientInformation) {
        SysDictData dictData = dictDataMapper.selectDictDataById(Long.valueOf(pClientInformation.getStorefrontCoding()));
        return dictData;
    }

    /**
     * @return 结果
     */
    @Override
    public SysUser getSysUser()
    {
        return userMapper.selectUserById(getUserId());
    }

    /**
     * 修改客户信息
     * 
     * @param pClientInformation 客户信息
     * @return 结果
     */
    @Override
    public int updatePClientInformation(PClientInformation pClientInformation)
    {
        SysDictData dictData = getSysDictData(pClientInformation);
        pClientInformation.setStorefront(dictData.getDictLabel());//下单店面
        pClientInformation.setRefreshName(UtilGetSys.getUserName());//更新者
        pClientInformationMapper.updateLeadingOrderInformation(pClientInformation);
        return pClientInformationMapper.updatePClientInformation(pClientInformation);
    }

    /**
     * 批量删除客户信息
     * 
     * @param ids 需要删除的客户信息主键
     * @return 结果
     */
    @Override
    public int deletePClientInformationByIds(String ids)
    {
        return pClientInformationMapper.deletePClientInformationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除客户信息信息
     * 
     * @param id 客户信息主键
     * @return 结果
     */
    @Override
    public int deletePClientInformationById(Long id)
    {
        return pClientInformationMapper.deletePClientInformationById(id);
    }
}
