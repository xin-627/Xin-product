package com.ruoyi.leading.service;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.leading.domain.PClientInformation;

/**
 * 客户信息Service接口
 * 
 * @author xin_xin
 * @date 2022-05-21
 */
public interface IPClientInformationService 
{
    /**
     * 查询客户信息
     * 
     * @param id 客户信息主键
     * @return 客户信息
     */
    public PClientInformation selectPClientInformationById(Long id);


    /**
     * 查询客户信息
     *
     * @param id 客户信息主键
     * @return 客户信息
     */
    public PClientInformation listId(Long id);

    /**
     * 查询客户信息列表
     * 
     * @param pClientInformation 客户信息
     * @return 客户信息集合
     */
    public List<PClientInformation> selectPClientInformationList(PClientInformation pClientInformation);

    /**
     * 新增客户信息
     * 
     * @param pClientInformation 客户信息
     * @return 结果
     */
    public int insertPClientInformation(PClientInformation pClientInformation);


    /**
     * 获取信息
     * @return 结果
     */
    public SysUser getSysUser();

    /**
     * 修改客户信息
     * 
     * @param pClientInformation 客户信息
     * @return 结果
     */
    public int updatePClientInformation(PClientInformation pClientInformation);

    /**
     * 批量删除客户信息
     * 
     * @param ids 需要删除的客户信息主键集合
     * @return 结果
     */
    public int deletePClientInformationByIds(String ids);

    /**
     * 删除客户信息信息
     * 
     * @param id 客户信息主键
     * @return 结果
     */
    public int deletePClientInformationById(Long id);
}
