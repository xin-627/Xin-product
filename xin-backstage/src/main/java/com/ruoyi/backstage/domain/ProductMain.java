package com.ruoyi.backstage.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品管理对象 product_main
 *
 * @author xin-xin
 * @date 2022-04-12
 */
public class ProductMain extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 一新编号 */
    @Excel(name = "一新编号")
    private String yxNumber;

    /** 供应商编号 */
    @Excel(name = "供应商编号")
    private String supplierNumber;

    /** 产品-名称 */
    @Excel(name = "产品-名称")
    private String productName;

    /** 产品-材质 */
    @Excel(name = "产品-材质")
    private String materialQuality;

    /** 产品-规格 */
    @Excel(name = "产品-规格")
    private String specifications;

    /** 产品-色号 */
    @Excel(name = "产品-色号")
    private String dyestuff;

    /** 产品-分类 */
    @Excel(name = "产品-分类")
    private String classify;

    /** 产品-卖点 */
    @Excel(name = "产品-卖点")
    private String sellingPoint;

    /** 产品-备注 */
    @Excel(name = "产品-备注")
    private String remarks;

    /** 产品-主图 */
    @Excel(name = "产品-缩略图")
    private String productImg;

    /** 产品-高清图 */
    @Excel(name = "产品-高清图")
    private String clearImages;

    /** 产品-上样区域 */
    @Excel(name = "产品-上样区域")
    private String displayArea;

    /** 状态 0 1 */
    @Excel(name = "状态 0 1")
    private Long state;

    /** 供应商(厂家) */
    @Excel(name = "供应商(厂家)")
    private String supplier;

    /** 产品-可选版本 */
    @Excel(name = "产品-可选版本")
    private String optionalVersion;

    /** 下架原因 */
    @Excel(name = "下架原因")
    private String account;

    /** 产品-转角带妃 */
    @Excel(name = "产品-转角带妃")
    private String corner;

    /** 下架时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下架时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date accountTime;

    /** 产品-双扶一字型 */
    @Excel(name = "产品-双扶一字型")
    private String handrail;

    /** 排序-优先级 */
    @Excel(name = "排序-优先级")
    private Long sort;

    /** 标签 */
    @Excel(name = "标签")
    private String label;

    /** 库存 */
    @Excel(name = "库存")
    private Long inventory;

    /** 零售价 */
    @Excel(name = "零售价")
    private BigDecimal retailPrice;

    /** 生产周期 */
    @Excel(name = "生产周期")
    private Long cPeriod;

    /** 产品销量 */
    @Excel(name = "产品销量")
    private Long salesVolume;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date creationTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date refreshTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private String refreshName;

    /** 色号数量 */
    private String pNumber;
    /** 规格数量 */
    private String sNumber;

    /** 产品特殊标识:
     沙发:SF
     功能沙发:GNSF
     非常规:FCG */
    @Excel(name = "产品特殊标识")
    private String identification;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setYxNumber(String yxNumber)
    {
        this.yxNumber = yxNumber;
    }

    public String getYxNumber()
    {
        return yxNumber;
    }
    public void setSupplierNumber(String supplierNumber)
    {
        this.supplierNumber = supplierNumber;
    }

    public String getSupplierNumber()
    {
        return supplierNumber;
    }
    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getProductName()
    {
        return productName;
    }
    public void setMaterialQuality(String materialQuality)
    {
        this.materialQuality = materialQuality;
    }

    public String getMaterialQuality()
    {
        return materialQuality;
    }
    public void setSpecifications(String specifications)
    {
        this.specifications = specifications;
    }

    public String getSpecifications()
    {
        return specifications;
    }
    public void setDyestuff(String dyestuff)
    {
        this.dyestuff = dyestuff;
    }

    public String getDyestuff()
    {
        return dyestuff;
    }
    public void setClassify(String classify)
    {
        this.classify = classify;
    }

    public String getClassify()
    {
        return classify;
    }
    public void setSellingPoint(String sellingPoint)
    {
        this.sellingPoint = sellingPoint;
    }

    public String getSellingPoint()
    {
        return sellingPoint;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }
    public void setProductImg(String productImg)
    {
        this.productImg = productImg;
    }

    public String getProductImg()
    {
        return productImg;
    }

    public String getClearImages() {
        return clearImages;
    }

    public void setClearImages(String clearImages) {
        this.clearImages = clearImages;
    }

    public void setDisplayArea(String displayArea)
    {
        this.displayArea = displayArea;
    }

    public String getDisplayArea()
    {
        return displayArea;
    }
    public void setState(Long state)
    {
        this.state = state;
    }

    public Long getState()
    {
        return state;
    }
    public void setSupplier(String supplier)
    {
        this.supplier = supplier;
    }

    public String getSupplier()
    {
        return supplier;
    }
    public void setOptionalVersion(String optionalVersion)
    {
        this.optionalVersion = optionalVersion;
    }

    public String getOptionalVersion()
    {
        return optionalVersion;
    }
    public void setAccount(String account)
    {
        this.account = account;
    }

    public String getAccount()
    {
        return account;
    }
    public void setCorner(String corner)
    {
        this.corner = corner;
    }

    public String getCorner()
    {
        return corner;
    }
    public void setAccountTime(Date accountTime)
    {
        this.accountTime = accountTime;
    }

    public Date getAccountTime()
    {
        return accountTime;
    }
    public void setHandrail(String handrail)
    {
        this.handrail = handrail;
    }

    public String getHandrail()
    {
        return handrail;
    }
    public void setSort(Long sort)
    {
        this.sort = sort;
    }

    public Long getSort()
    {
        return sort;
    }
    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }
    public void setInventory(Long inventory)
    {
        this.inventory = inventory;
    }

    public Long getInventory()
    {
        return inventory;
    }
    public void setRetailPrice(BigDecimal retailPrice)
    {
        this.retailPrice = retailPrice;
    }

    public BigDecimal getRetailPrice()
    {
        return retailPrice;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreator()
    {
        return creator;
    }
    public void setCreationTime(Date creationTime)
    {
        this.creationTime = creationTime;
    }


    public Date getCreationTime()
    {
        return creationTime;
    }
    public void setRefreshTime(Date refreshTime)
    {
        this.refreshTime = refreshTime;
    }

    public Date getRefreshTime()
    {
        return refreshTime;
    }
    public void setRefreshName(String refreshName)
    {
        this.refreshName = refreshName;
    }

    public String getRefreshName()
    {
        return refreshName;
    }

    public void setcPeriod(Long cPeriod)
    {
        this.cPeriod = cPeriod;
    }

    public Long getcPeriod()
    {
        return cPeriod;
    }

    public Long getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(Long salesVolume) {
        this.salesVolume = salesVolume;
    }

    public String getpNumber() {
        return pNumber;
    }

    public void setpNumber(String pNumber) {
        this.pNumber = pNumber;
    }

    public String getsNumber() {
        return sNumber;
    }

    public void setsNumber(String sNumber) {
        this.sNumber = sNumber;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("yxNumber", getYxNumber())
                .append("supplierNumber", getSupplierNumber())
                .append("productName", getProductName())
                .append("materialQuality", getMaterialQuality())
                .append("specifications", getSpecifications())
                .append("dyestuff", getDyestuff())
                .append("classify", getClassify())
                .append("sellingPoint", getSellingPoint())
                .append("remarks", getRemarks())
                .append("productImg", getProductImg())
                .append("clearImages", getClearImages())
                .append("displayArea", getDisplayArea())
                .append("state", getState())
                .append("supplier", getSupplier())
                .append("optionalVersion", getOptionalVersion())
                .append("account", getAccount())
                .append("corner", getCorner())
                .append("accountTime", getAccountTime())
                .append("handrail", getHandrail())
                .append("sort", getSort())
                .append("label", getLabel())
                .append("inventory", getInventory())
                .append("retailPrice", getRetailPrice())
                .append("creator", getCreator())
                .append("creationTime", getCreationTime())
                .append("refreshTime", getRefreshTime())
                .append("refreshName", getRefreshName())
                .append("cPeriod", getcPeriod())
                .append("salesVolume", getSalesVolume())
                .append("pNumber", getpNumber())
                .append("sNumber", getsNumber())
                .append("identification", getIdentification())
                .toString();
    }
}
