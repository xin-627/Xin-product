package com.ruoyi.backstage.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品规格组对象 product_specifications
 * 
 * @author xin-xin
 * @date 2022-01-05
 */
public class ProductSpecifications extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 产品id */
    private Long productId;

    /** 一新编号 */
    @Excel(name = "一新编号")
    private String yxNumber;

    /** 规格 */
    @Excel(name = "规格")
    private String specifications;

    /** 状态 0 1 */
    @Excel(name = "状态 0 1")
    private Long state;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    /** 创建时间 */
    private Date creationTime;

    /** 更新时间 */
    private Date refreshTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private String refreshName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setYxNumber(String yxNumber) 
    {
        this.yxNumber = yxNumber;
    }

    public String getYxNumber() 
    {
        return yxNumber;
    }
    public void setSpecifications(String specifications) 
    {
        this.specifications = specifications;
    }

    public String getSpecifications() 
    {
        return specifications;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setCreator(String creator) 
    {
        this.creator = creator;
    }

    public String getCreator() 
    {
        return creator;
    }
    public void setCreationTime(Date creationTime) 
    {
        this.creationTime = creationTime;
    }

    public Date getCreationTime() 
    {
        return creationTime;
    }
    public void setRefreshTime(Date refreshTime) 
    {
        this.refreshTime = refreshTime;
    }

    public Date getRefreshTime() 
    {
        return refreshTime;
    }
    public void setRefreshName(String refreshName) 
    {
        this.refreshName = refreshName;
    }

    public String getRefreshName() 
    {
        return refreshName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("yxNumber", getYxNumber())
            .append("specifications", getSpecifications())
            .append("state", getState())
            .append("creator", getCreator())
            .append("creationTime", getCreationTime())
            .append("refreshTime", getRefreshTime())
            .append("refreshName", getRefreshName())
            .toString();
    }
}
