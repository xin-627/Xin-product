package com.ruoyi.backstage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库位缓存对象 p_warehouse_cache_file
 *
 * @author xin_xin
 * @date 2022-09-17
 */
public class PWarehouseCacheFile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 货架编号 */
    @Excel(name = "货架编号")
    private String shelfNumber;

    /** 货架id */
    @Excel(name = "货架id")
    private Long shelfId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 货架条码 */
    @Excel(name = "货架条码")
    private String shelfBarCode;

    /** 状态: */
    @Excel(name = "状态:")
    private Long shelfState;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setShelfNumber(String shelfNumber)
    {
        this.shelfNumber = shelfNumber;
    }

    public String getShelfNumber()
    {
        return shelfNumber;
    }
    public void setShelfId(Long shelfId)
    {
        this.shelfId = shelfId;
    }

    public Long getShelfId()
    {
        return shelfId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setShelfBarCode(String shelfBarCode)
    {
        this.shelfBarCode = shelfBarCode;
    }

    public String getShelfBarCode()
    {
        return shelfBarCode;
    }
    public void setShelfState(Long shelfState)
    {
        this.shelfState = shelfState;
    }

    public Long getShelfState()
    {
        return shelfState;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreator()
    {
        return creator;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("shelfNumber", getShelfNumber())
                .append("shelfId", getShelfId())
                .append("userId", getUserId())
                .append("shelfBarCode", getShelfBarCode())
                .append("shelfState", getShelfState())
                .append("creator", getCreator())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}