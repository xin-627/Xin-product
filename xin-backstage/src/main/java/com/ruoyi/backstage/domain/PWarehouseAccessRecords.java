package com.ruoyi.backstage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 仓库出入库记录对象 p_warehouse_access_records
 * 
 * @author xin_xin
 * @date 2022-09-16
 */
public class PWarehouseAccessRecords extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 订单_id */
    @Excel(name = "订单_id")
    private Long ordersId;

    /** 条码_id */
    @Excel(name = "条码_id")
    private Long barCodeId;

    /** 库位_id */
    @Excel(name = "库位_id")
    private Long shelfId;

    /** 状态"1"入库"2"出库 */
    @Excel(name = "状态  0入库 1出库")
    private Long statusCode;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrdersId(Long ordersId) 
    {
        this.ordersId = ordersId;
    }

    public Long getOrdersId() 
    {
        return ordersId;
    }
    public void setBarCodeId(Long barCodeId) 
    {
        this.barCodeId = barCodeId;
    }

    public Long getBarCodeId() 
    {
        return barCodeId;
    }
    public void setShelfId(Long shelfId) 
    {
        this.shelfId = shelfId;
    }

    public Long getShelfId() 
    {
        return shelfId;
    }
    public void setStatusCode(Long statusCode) 
    {
        this.statusCode = statusCode;
    }

    public Long getStatusCode() 
    {
        return statusCode;
    }
    public void setCreator(String creator) 
    {
        this.creator = creator;
    }

    public String getCreator() 
    {
        return creator;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ordersId", getOrdersId())
            .append("barCodeId", getBarCodeId())
            .append("shelfId", getShelfId())
            .append("statusCode", getStatusCode())
            .append("creator", getCreator())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
