package com.ruoyi.backstage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 注册申请对象 p_register
 * 
 * @author xin_xin
 * @date 2022-05-13
 */
public class PRegister extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String userName;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phonenumber;

    /** 店面名 */
    @Excel(name = "店面名")
    private String storefront;

    /** 店面类型 */
    @Excel(name = "店面类型")
    private String storefrontType;

    /** 用户性别（0男 1女 2未知） */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 注册状态（0未处理 1已处理） */
    @Excel(name = "注册状态", readConverterExp = "0=未处理,1=已处理")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setPhonenumber(String phonenumber) 
    {
        this.phonenumber = phonenumber;
    }

    public String getPhonenumber() 
    {
        return phonenumber;
    }
    public void setStorefront(String storefront) 
    {
        this.storefront = storefront;
    }

    public String getStorefront() 
    {
        return storefront;
    }
    public void setStorefrontType(String storefrontType) 
    {
        this.storefrontType = storefrontType;
    }

    public String getStorefrontType() 
    {
        return storefrontType;
    }
    public void setSex(String sex) 
    {
        this.sex = sex;
    }

    public String getSex() 
    {
        return sex;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userName", getUserName())
            .append("phonenumber", getPhonenumber())
            .append("storefront", getStorefront())
            .append("storefrontType", getStorefrontType())
            .append("sex", getSex())
            .append("password", getPassword())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
