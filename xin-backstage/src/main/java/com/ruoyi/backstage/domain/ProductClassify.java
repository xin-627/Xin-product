package com.ruoyi.backstage.domain;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

import java.util.List;

/**
 * 分类菜单/分类管理对象 product_classify
 * 
 * @author xin-xin
 * @date 2022-01-11
 */
public class ProductClassify extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 父节点ID */
    @Excel(name = "父节点ID")
    private Long pid;

    /** 标题(/显示名称) */
    @Excel(name = "标题(/显示名称)")
    private String title;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 链接 */
    @Excel(name = "链接")
    private String href;

    /** 打开方式 */
    @Excel(name = "打开方式")
    private String target;

    /** 自定义当前分类项模板 */
    @Excel(name = "自定义当前分类项模板")
    private String templet;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 选中值(分类管理) */
    @Excel(name = "选中值(分类管理)")
    private String value;

    /** 是否选中(分类管理) */
    @Excel(name = "是否选中(分类管理)")
    private String selected;

    /** 是否警用(分类管理) */
    @Excel(name = "是否警用(分类管理)")
    private String disabled;

    private String name;

    private List<ProductClassify> children;

    private List<ProductClassify> child;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPid(Long pid) 
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setHref(String href) 
    {
        this.href = href;
    }

    public String getHref() 
    {
        return href;
    }
    public void setTarget(String target) 
    {
        this.target = target;
    }

    public String getTarget() 
    {
        return target;
    }
    public void setTemplet(String templet) 
    {
        this.templet = templet;
    }

    public String getTemplet() 
    {
        return templet;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setValue(String value) 
    {
        this.value = value;
    }

    public String getValue() 
    {
        return value;
    }
    public void setSelected(String selected) 
    {
        this.selected = selected;
    }

    public String getSelected() 
    {
        return selected;
    }
    public void setDisabled(String disabled) 
    {
        this.disabled = disabled;
    }

    public String getDisabled() 
    {
        return disabled;
    }

    public List<ProductClassify> getChildren() {
        return children;
    }

    public void setChildren(List<ProductClassify> children) {
        this.children = children;
    }

    public List<ProductClassify> getChild() {
        return child;
    }

    public void setChild(List<ProductClassify> child) {
        this.child = child;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProductClassify{" +
                "id=" + id +
                ", pid=" + pid +
                ", title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", href='" + href + '\'' +
                ", target='" + target + '\'' +
                ", templet='" + templet + '\'' +
                ", sort=" + sort +
                ", value='" + value + '\'' +
                ", selected='" + selected + '\'' +
                ", disabled='" + disabled + '\'' +
                ", name='" + name + '\'' +
                ", children=" + children +
                ", child=" + child +
                '}';
    }
}
