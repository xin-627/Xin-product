package com.ruoyi.backstage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 仓库库位对象 p_warehouse_number
 * 
 * @author xin_xin
 * @date 2022-09-13
 */
public class PWarehouseNumber extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 货架编号 */
    @Excel(name = "货架编号")
    private String shelfNumber;

    /** 货架存放产品 */
    @Excel(name = "货架存放产品")
    private String shelfProducts;

    /** 货架条码 */
    @Excel(name = "货架条码")
    private String shelfBarCode;

    /** 存放数量 */
    @Excel(name = "存放数量")
    private Long shelfStoreQuantity;

    /** 查询条件 */
    @Excel(name = "查询条件")
    private String shelfSeek;

    /** 货架状态描述 */
    @Excel(name = "货架状态描述")
    private String shelfState;

    /** 货架状态 1 "空闲" 2"占用" */
    @Excel(name = "货架状态 1 空闲 2占用")
    private Long shelfStateCode;

    /** 货架层数 */
    @Excel(name = "货架层数")
    private String shelfStorey;

    /** 货架列数信息 */
    @Excel(name = "货架列数信息")
    private String shelfColumn;

    /** 货架信息 */
    @Excel(name = "货架信息")
    private String shelfInformation;

    /** 排序 */
    @Excel(name = "排序")
    private Integer shelfSort;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String shelfStatus;

    /**订单编号  仓储位查询用*/
    private String orderNumber;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setShelfNumber(String shelfNumber) 
    {
        this.shelfNumber = shelfNumber;
    }

    public String getShelfNumber() 
    {
        return shelfNumber;
    }
    public void setShelfProducts(String shelfProducts) 
    {
        this.shelfProducts = shelfProducts;
    }

    public String getShelfProducts() 
    {
        return shelfProducts;
    }
    public void setShelfBarCode(String shelfBarCode) 
    {
        this.shelfBarCode = shelfBarCode;
    }

    public String getShelfBarCode() 
    {
        return shelfBarCode;
    }
    public void setShelfStoreQuantity(Long shelfStoreQuantity) 
    {
        this.shelfStoreQuantity = shelfStoreQuantity;
    }

    public Long getShelfStoreQuantity() 
    {
        return shelfStoreQuantity;
    }
    public void setShelfSeek(String shelfSeek) 
    {
        this.shelfSeek = shelfSeek;
    }

    public String getShelfSeek() 
    {
        return shelfSeek;
    }
    public void setShelfState(String shelfState) 
    {
        this.shelfState = shelfState;
    }

    public String getShelfState() 
    {
        return shelfState;
    }
    public void setShelfStateCode(Long shelfStateCode) 
    {
        this.shelfStateCode = shelfStateCode;
    }

    public Long getShelfStateCode() 
    {
        return shelfStateCode;
    }
    public void setShelfStorey(String shelfStorey) 
    {
        this.shelfStorey = shelfStorey;
    }

    public String getShelfStorey() 
    {
        return shelfStorey;
    }
    public void setShelfColumn(String shelfColumn) 
    {
        this.shelfColumn = shelfColumn;
    }

    public String getShelfColumn() 
    {
        return shelfColumn;
    }
    public void setShelfInformation(String shelfInformation) 
    {
        this.shelfInformation = shelfInformation;
    }

    public String getShelfInformation() 
    {
        return shelfInformation;
    }
    public void setShelfSort(Integer shelfSort) 
    {
        this.shelfSort = shelfSort;
    }

    public Integer getShelfSort() 
    {
        return shelfSort;
    }
    public void setShelfStatus(String shelfStatus) 
    {
        this.shelfStatus = shelfStatus;
    }

    public String getShelfStatus() 
    {
        return shelfStatus;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shelfNumber", getShelfNumber())
            .append("shelfProducts", getShelfProducts())
            .append("shelfBarCode", getShelfBarCode())
            .append("shelfStoreQuantity", getShelfStoreQuantity())
            .append("shelfSeek", getShelfSeek())
            .append("shelfState", getShelfState())
            .append("shelfStateCode", getShelfStateCode())
            .append("shelfStorey", getShelfStorey())
            .append("shelfColumn", getShelfColumn())
            .append("shelfInformation", getShelfInformation())
            .append("shelfSort", getShelfSort())
            .append("shelfStatus", getShelfStatus())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
                .append("orderNumber", getOrderNumber())
            .toString();
    }
}
