package com.ruoyi.backstage.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 仓库标签对象 p_warehouse
 *
 * @author xin_xin
 * @date 2022-07-22
 */
public class PWarehouse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单_id */
    @Excel(name = "订单_id")
    private Long ordersId;

    /** 库位_id */
    @Excel(name = "库位_id")
    private Long locationId;

    /** 条码 */
    @Excel(name = "条码")
    private String barCode;

    /** 状态"1"入库"2"出库 */
    @Excel(name = "状态")
    private Long statusCode;


    /** 入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date entranceTime;

    /** 入库人 */
    @Excel(name = "入库人")
    private String entranceName;

    /** 扫描时间(出库) */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "扫描时间(出库)", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sweepTime;

    /** 出库人 */
    @Excel(name = "出库人")
    private String sweepName;

    /** 数量 */
    @Excel(name = "数量")
    private Long quantity;

    /** 序号 */
    @Excel(name = "序号")
    private Long serialNumber;

    /** 存放位置 */
    @Excel(name = "存放位置")
    private String location;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date creationTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date refreshTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private String refreshName;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrdersId(Long ordersId)
    {
        this.ordersId = ordersId;
    }

    public Long getOrdersId()
    {
        return ordersId;
    }


    public void setLocationId(Long locationId)
    {
        this.locationId = locationId;
    }

    public Long getLocationId()
    {
        return locationId;
    }
    public void setBarCode(String barCode)
    {
        this.barCode = barCode;
    }

    public String getBarCode()
    {
        return barCode;
    }
    public void setEntranceTime(Date entranceTime)
    {
        this.entranceTime = entranceTime;
    }

    public Date getEntranceTime()
    {
        return entranceTime;
    }
    public void setEntranceName(String entranceName)
    {
        this.entranceName = entranceName;
    }

    public String getEntranceName()
    {
        return entranceName;
    }
    public void setStatusCode(Long statusCode)
    {
        this.statusCode = statusCode;
    }
    public Long getStatusCode()
    {
        return statusCode;
    }
    public void setSweepTime(Date sweepTime)
    {
        this.sweepTime = sweepTime;
    }

    public Date getSweepTime()
    {
        return sweepTime;
    }

    public void setSweepName(String sweepName)
    {
        this.sweepName = sweepName;
    }

    public String getSweepName()
    {
        return sweepName;
    }

    public void setQuantity(Long quantity)
    {
        this.quantity = quantity;
    }

    public Long getQuantity()
    {
        return quantity;
    }
    public void setSerialNumber(Long serialNumber)
    {
        this.serialNumber = serialNumber;
    }

    public Long getSerialNumber()
    {
        return serialNumber;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getLocation()
    {
        return location;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreator()
    {
        return creator;
    }
    public void setCreationTime(Date creationTime)
    {
        this.creationTime = creationTime;
    }

    public Date getCreationTime()
    {
        return creationTime;
    }
    public void setRefreshTime(Date refreshTime)
    {
        this.refreshTime = refreshTime;
    }

    public Date getRefreshTime()
    {
        return refreshTime;
    }
    public void setRefreshName(String refreshName)
    {
        this.refreshName = refreshName;
    }

    public String getRefreshName()
    {
        return refreshName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("ordersId", getOrdersId())
                .append("locationId", getLocationId())
                .append("barCode", getBarCode())
                .append("statusCode", getStatusCode())
                .append("entranceTime", getEntranceTime())
                .append("entranceName", getEntranceName())
                .append("sweepTime", getSweepTime())
                .append("sweepName", getSweepName())
                .append("quantity", getQuantity())
                .append("serialNumber", getSerialNumber())
                .append("location", getLocation())
                .append("creator", getCreator())
                .append("creationTime", getCreationTime())
                .append("refreshTime", getRefreshTime())
                .append("refreshName", getRefreshName())
                .toString();
    }
}