package com.ruoyi.backstage.domain.DTree;

/***动态菜单*/

public class CheckArr {
    /** 复选框标记*/
    private int type;
    /** 复选框是否选中*/
    private int checked;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }
}
