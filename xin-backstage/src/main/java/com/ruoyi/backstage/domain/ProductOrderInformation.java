package com.ruoyi.backstage.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单管理对象 product_order_information
 *
 * @author xin-xin
 * @date 2022-03-05
 */
public class ProductOrderInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 客户组id */
    @Excel(name = "客户组id")
    private Long clientId;

    /** 产品id */
    @Excel(name = "产品id")
    private Long mainId;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private String clientNumber;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNumber;

    /** 下单人 */
    @Excel(name = "下单人")
    private String orders;

    /** 下单人身份 */
    @Excel(name = "下单人身份")
    private String ordersStatus;

    /** 下单人id */
    @Excel(name = "下单人id")
    private Long statusId;

    /** 下单人-联系方式 */
    @Excel(name = "下单人-联系方式")
    private String contactWay;

    /** 下单店面 */
    @Excel(name = "下单店面")
    private String storefront;

    /** 下单店面编号 */
    @Excel(name = "下单店面编号")
    private Integer storefrontCoding;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String customerName;

    /** 客户电话 */
    @Excel(name = "客户电话")
    private String customerTelephone;

    /** 下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderTime;

    /** 客户地址 */
    @Excel(name = "客户地址")
    private String customerAddress;

    /** 店面类型 */
    @Excel(name = "店面类型")
    private Long storefrontType;

    /** 自定义-品类 */
    @Excel(name = "自定义-品类")
    private String customName;

    /** 自定义-材质 */
    @Excel(name = "自定义-材质")
    private String customQuality;

    /** 自定义-规格 */
    @Excel(name = "自定义-规格")
    private String customSpecifications;

    /** 自定义-色号 */
    @Excel(name = "自定义-色号")
    private String customDyestuff;

    /** 自定义-沙发皮号 */
    @Excel(name = "自定义-沙发皮号")
    private String customSkin;

    /** 自定义-沙发改色 */
    @Excel(name = "自定义-沙发改色")
    private String customChangeColour;

    /** 自定义-其他 */
    @Excel(name = "自定义-其他")
    private String customOther;

    /** 状态描述 */
    @Excel(name = "状态描述")
    private String stateDescription;

    /** 状态码 */
    @Excel(name = "状态码")
    private Long statusCode;

    /** 自定义-内容 */
    @Excel(name = "自定义-内容")
    private String userDefined;

    /** 订单备注 */
    @Excel(name = "订单备注")
    private String remarks;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date auditTime;

    /** 下单数量 */
    @Excel(name = "下单数量")
    private Long number;

    /** 驳回理由 */
    @Excel(name = "驳回理由")
    private String overrule;

    /** 采购-下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购-下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cGiveTime;

    /** 采购时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cPurchaseTime;

    /** 计划交期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划交期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planDeliveryPeriod;

    /** 采购-周期 */
    @Excel(name = "采购-周期")
    private Long cPeriod;

    /** 采购-预计时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购-预计时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cScheduledTime;

    /** 发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deliveryTime;

    /** 发货备注 */
    @Excel(name = "发货备注")
    private String notesOnShipping;

    /** 采购-需到货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购-需到货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cWantTime;

    /** 提货方式 */
    @Excel(name = "提货方式")
    private String deliveryMethod;

    /** 采购-回厂时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采购-回厂时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cRevertTime;

    /** 采购-厂家发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cDeliveryTime;

    /** 采购-采购价-成本价 */
    @Excel(name = "单价")
    private Long cPurchasePrice;

    /** 一新编号 */
    @Excel(name = "一新编号")
    private String yxNumber;

    /** 供应商编号 */
    @Excel(name = "供应商编号")
    private String supplierNumber;

    /** 采购-备注 */
    @Excel(name = "采购-备注")
    private String cProcurementRemark;

    /** 产品-名称 */
    @Excel(name = "产品-名称")
    private String productName;

    /** 财务-总金额 */
    @Excel(name = "财务-总金额")
    private String cAggregateAmount;

    /** 产品-材质 */
    @Excel(name = "产品-材质")
    private String materialQuality;

    /** 财务-运费 */
    @Excel(name = "财务-运费")
    private String cFreight;

    /** 产品-规格 */
    @Excel(name = "产品-规格")
    private String specifications;

    /** 财务-核回厂 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "财务-核回厂", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cCheckArrive;

    /** 产品-色号 */
    @Excel(name = "产品-色号")
    private String dyestuff;

    /** 财务-是否核对 */
    @Excel(name = "财务-是否核对")
    private String cCheckResult;

    /** 产品-分类 */
    @Excel(name = "产品-分类")
    private String classify;

    /** 财务-核对出库 */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Excel(name = "财务-核对出库")
    private String cCheckLeave;

    /** 产品-卖点 */
    @Excel(name = "产品-卖点")
    private String sellingPoint;

    /** 财务-核对备注 */
    @Excel(name = "财务-核对备注")
    private String cCheckRemark;


    /** 产品备注 */
    @Excel(name = "产品备注")
    private String productRemarks;

    /** 产品-主图 */
    @Excel(name = "产品-主图")
    private String productImg;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplier;

    /** 标签 */
    @Excel(name = "标签")
    private String label;

    /** 产品特殊标识:
     沙发:SF
     功能沙发:GNSF
     非常规:FCG */
    @Excel(name = "产品特殊标识")
    private String identification;

    /** 订单类型>1:正常单>2:维修单 */
    @Excel(name = "订单类型>1:正常单>2:维修单")
    private Long orderType;

    /** 维修类型>1:维修单>2:增补单 */
    @Excel(name = "维修类型>1:维修单>2:增补单")
    private Long maintainType;

    /** 编号类型 */
    @Excel(name = "编号类型")
    private String numberType;

    /** 原订单编号 */
    @Excel(name = "原订单编号")
    private String initialOrderNumber;


    /** 问题描述 */
    @Excel(name = "问题描述")
    private String problemDescription;

    /** 零售价 */
    @Excel(name = "零售价")
    private BigDecimal retailPrice;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date creationTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date refreshTime;

    /** 更新者 */
    @Excel(name = "更新者")
    private String refreshName;


    /** 审核时间 范围开始 */
    private String auditTimeStartDate;

    /** 审核时间 范围结束 */
    private String auditTimeEndDate;

    /** 计划交期 范围开始 */
    private String planDeliveryPeriodStartDate;

    /** 计划交期 范围结束 */
    private String planDeliveryPeriodEndDate;

    /** 财务-核对时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "财务-核对时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date cCheckTime;

    /** 财务-核对状态 */
    @Excel(name = "财务-核对状态")
    private Long cCheckState;

    /** 是否有定制 客户表 */
    @Excel(name = "是否有定制")
    private String customization;

    /** 仓库-入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "仓库-入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cEntranceTime;

    /*数据统计*/
    /**待提交*/
    private Long putIn;
    /**被驳回*/
    private Long reject;
    /**待审核*/
    private Long audit;
    /**待采购*/
    private Long purchase;
    /**带回厂*/
    private Long arrive;
    /**已入库*/
    private Long storage;
    /**已出库*/
    private Long departure;

    /**去年客户 */
    private Long lastYearClient;
    /**今年客户 */
    private Long thisYearClient;
    /**今年客户环比增长率 */
    private BigDecimal thisYearClientContrast;

    /**上月客户*/
    private Long lastMonthClient;
    /**当月客户*/
    private Long theSameMonthClient;
    /**当月客户环比增长率 */
    private BigDecimal theSameMonthClientContrast;
    /**总客户*/
    private Long grossClient;

    /**去年总订单*/
    private Long lastYearAltogether;
    /**今年总订单*/
    private Long thisYearAltogether;
    /**今年总订单环比增长率 */
    private BigDecimal thisYearAltogetherContrast;

    /**上月总订单*/
    private Long lastMonthAltogether;
    /**当月总订单*/
    private Long theSameMonthAltogether;
    /**当月总订单环比增长率 */
    private BigDecimal theSameMonthAltogetherContrast;

    /**总订单*/
    private Long altogether;

    /** 核回厂时间 范围开始 */
    private String cCheckArriveStartDate;
    /** 核回厂时间 范围结束 */
    private String cCheckArriveEndDate;

    /** 核出库时间 范围开始 */
    private String cCheckLeaveStartDate;
    /** 核出库时间 范围结束 */
    private String cCheckLeaveEndDate;

    /** 核对时间 范围开始 */
    private String cCheckTimeStartDate;
    /** 核对时间 范围结束 */
    private String cCheckTimeEndDate;

    /** 回厂时间 范围开始 */
    private String cRevertTimeStartDate;
    /** 回厂时间 范围结束 */
    private String cRevertTimeEndDate;

    /** 出库时间 范围开始 */
    private String deliveryTimeStartDate;
    /** 出库时间 范围结束 */
    private String deliveryTimeEndDate;

    /** 订单_id */
    private Long ordersId;

    /** 条码 */
    private String barCode;

    /** 条码状态 */
    private Long wStatusCode;

    /** 数量 */
    private Long quantity;

    /** 存放位置 */
    private String location;

    /** 序号 */
    private Long serialNumber;

    /** 条码组状态 */
    @Excel(name = "条码组状态")
    private String barcodeGroupStatus;

    /** 条码数量 */
    private Long  wStatusCodeCount;

    /** 入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date entranceTime;

    /** 入库人 */
    private String entranceName;

    /** 扫描时间(出库) */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date sweepTime;

    /** 出库人 */
    private String sweepName;

    /** 出入库记录类型*/
    private String parStatusCode;

    /*** 数据统计-在线产品*/
    private Long putaway;
    /*** 数据统计-已下架产品*/
    private Long conceal;

    /** 财务待审数量 */
    private String pendingQuantity;

    /** 下单时间 范围开始 */
    private String orderTimeStartDate;
    /** 下单时间 范围结束 */
    private String orderTimeEndDate;

    /** 采购接单时间 范围开始 */
    private String cGiveTimeStartDate;
    /** 采购接单时间 范围结束 */
    private String cGiveTimeEndDate;

    /** 打印状态 */
    @Excel(name = "打印状态")
    private Long printStatus;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setClientId(Long clientId)
    {
        this.clientId = clientId;
    }

    public Long getClientId()
    {
        return clientId;
    }

    public void setMainId(Long mainId)
    {
        this.mainId = mainId;
    }

    public Long getMainId()
    {
        return mainId;
    }

    public void setClientNumber(String clientNumber)
    {
        this.clientNumber = clientNumber;
    }

    public String getClientNumber()
    {
        return clientNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }
    public void setOrders(String orders)
    {
        this.orders = orders;
    }

    public String getOrders()
    {
        return orders;
    }
    public void setOrdersStatus(String ordersStatus)
    {
        this.ordersStatus = ordersStatus;
    }

    public String getOrdersStatus()
    {
        return ordersStatus;
    }
    public void setStatusId(Long statusId)
    {
        this.statusId = statusId;
    }

    public Long getStatusId()
    {
        return statusId;
    }
    public void setContactWay(String contactWay)
    {
        this.contactWay = contactWay;
    }

    public String getContactWay()
    {
        return contactWay;
    }
    public void setStorefront(String storefront)
    {
        this.storefront = storefront;
    }

    public String getStorefront()
    {
        return storefront;
    }
    public void setStorefrontCoding(Integer storefrontCoding)
    {
        this.storefrontCoding = storefrontCoding;
    }

    public Integer getStorefrontCoding()
    {
        return storefrontCoding;
    }
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerName()
    {
        return customerName;
    }
    public void setCustomerTelephone(String customerTelephone)
    {
        this.customerTelephone = customerTelephone;
    }

    public String getCustomerTelephone()
    {
        return customerTelephone;
    }
    public void setOrderTime(Date orderTime)
    {
        this.orderTime = orderTime;
    }

    public Date getOrderTime()
    {
        return orderTime;
    }
    public void setCustomerAddress(String customerAddress)
    {
        this.customerAddress = customerAddress;
    }

    public String getCustomerAddress()
    {
        return customerAddress;
    }
    public void setStorefrontType(Long storefrontType)
    {
        this.storefrontType = storefrontType;
    }

    public Long getStorefrontType()
    {
        return storefrontType;
    }
    public void setCustomName(String customName)
    {
        this.customName = customName;
    }

    public String getCustomName()
    {
        return customName;
    }
    public void setCustomQuality(String customQuality)
    {
        this.customQuality = customQuality;
    }

    public String getCustomQuality()
    {
        return customQuality;
    }
    public void setCustomSpecifications(String customSpecifications)
    {
        this.customSpecifications = customSpecifications;
    }

    public String getCustomSpecifications()
    {
        return customSpecifications;
    }
    public void setCustomDyestuff(String customDyestuff)
    {
        this.customDyestuff = customDyestuff;
    }

    public String getCustomDyestuff()
    {
        return customDyestuff;
    }
    public void setCustomSkin(String customSkin)
    {
        this.customSkin = customSkin;
    }

    public String getCustomSkin()
    {
        return customSkin;
    }
    public void setCustomChangeColour(String customChangeColour)
    {
        this.customChangeColour = customChangeColour;
    }

    public String getCustomChangeColour()
    {
        return customChangeColour;
    }
    public void setCustomOther(String customOther)
    {
        this.customOther = customOther;
    }

    public String getCustomOther()
    {
        return customOther;
    }
    public void setStateDescription(String stateDescription)
    {
        this.stateDescription = stateDescription;
    }

    public String getStateDescription()
    {
        return stateDescription;
    }
    public void setStatusCode(Long statusCode)
    {
        this.statusCode = statusCode;
    }

    public Long getStatusCode()
    {
        return statusCode;
    }
    public void setUserDefined(String userDefined)
    {
        this.userDefined = userDefined;
    }

    public String getUserDefined()
    {
        return userDefined;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }
    public void setAuditTime(Date auditTime)
    {
        this.auditTime = auditTime;
    }

    public Date getAuditTime()
    {
        return auditTime;
    }
    public void setNumber(Long number)
    {
        this.number = number;
    }

    public Long getNumber()
    {
        return number;
    }
    public void setOverrule(String overrule)
    {
        this.overrule = overrule;
    }

    public String getOverrule()
    {
        return overrule;
    }
    public void setcGiveTime(Date cGiveTime)
    {
        this.cGiveTime = cGiveTime;
    }

    public Date getcGiveTime()
    {
        return cGiveTime;
    }
    public void setcPurchaseTime(Date cPurchaseTime)
    {
        this.cPurchaseTime = cPurchaseTime;
    }

    public Date getcPurchaseTime()
    {
        return cPurchaseTime;
    }

    public void setPlanDeliveryPeriod(Date planDeliveryPeriod)
    {
        this.planDeliveryPeriod = planDeliveryPeriod;
    }
    public Date getPlanDeliveryPeriod()
    {
        return planDeliveryPeriod;
    }

    public void setcPeriod(Long cPeriod)
    {
        this.cPeriod = cPeriod;
    }

    public Long getcPeriod()
    {
        return cPeriod;
    }
    public void setcScheduledTime(Date cScheduledTime)
    {
        this.cScheduledTime = cScheduledTime;
    }

    public Date getcScheduledTime()
    {
        return cScheduledTime;
    }
    public void setDeliveryTime(Date deliveryTime)
    {
        this.deliveryTime = deliveryTime;
    }

    public Date getDeliveryTime()
    {
        return deliveryTime;
    }
    public void setNotesOnShipping(String notesOnShipping)
    {
        this.notesOnShipping = notesOnShipping;
    }

    public String getNotesOnShipping()
    {
        return notesOnShipping;
    }
    public void setcWantTime(Date cWantTime)
    {
        this.cWantTime = cWantTime;
    }

    public Date getcWantTime()
    {
        return cWantTime;
    }
    public void setDeliveryMethod(String deliveryMethod)
    {
        this.deliveryMethod = deliveryMethod;
    }

    public String getDeliveryMethod()
    {
        return deliveryMethod;
    }
    public void setcRevertTime(Date cRevertTime)
    {
        this.cRevertTime = cRevertTime;
    }

    public Date getcRevertTime()
    {
        return cRevertTime;
    }
    public void setYxNumber(String yxNumber)
    {
        this.yxNumber = yxNumber;
    }

    public String getYxNumber()
    {
        return yxNumber;
    }

    public void setSupplierNumber(String supplierNumber)
    {
        this.supplierNumber = supplierNumber;
    }

    public String getSupplierNumber()
    {
        return supplierNumber;
    }

    public void setcProcurementRemark(String cProcurementRemark)
    {
        this.cProcurementRemark = cProcurementRemark;
    }

    public String getcProcurementRemark()
    {
        return cProcurementRemark;
    }
    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getProductName()
    {
        return productName;
    }
    public void setcAggregateAmount(String cAggregateAmount)
    {
        this.cAggregateAmount = cAggregateAmount;
    }

    public String getcAggregateAmount()
    {
        return cAggregateAmount;
    }
    public void setMaterialQuality(String materialQuality)
    {
        this.materialQuality = materialQuality;
    }

    public String getMaterialQuality()
    {
        return materialQuality;
    }
    public void setcFreight(String cFreight)
    {
        this.cFreight = cFreight;
    }

    public String getcFreight()
    {
        return cFreight;
    }
    public void setSpecifications(String specifications)
    {
        this.specifications = specifications;
    }

    public String getSpecifications()
    {
        return specifications;
    }
    public void setcCheckArrive(Date cCheckArrive)
    {
        this.cCheckArrive = cCheckArrive;
    }

    public Date getcCheckArrive()
    {
        return cCheckArrive;
    }
    public void setDyestuff(String dyestuff)
    {
        this.dyestuff = dyestuff;
    }

    public String getDyestuff()
    {
        return dyestuff;
    }
    public void setcCheckResult(String cCheckResult)
    {
        this.cCheckResult = cCheckResult;
    }

    public String getcCheckResult()
    {
        return cCheckResult;
    }
    public void setClassify(String classify)
    {
        this.classify = classify;
    }

    public String getClassify()
    {
        return classify;
    }
    public void setcCheckLeave(String cCheckLeave)
    {
        this.cCheckLeave = cCheckLeave;
    }

    public String getcCheckLeave()
    {
        return cCheckLeave;
    }
    public void setSellingPoint(String sellingPoint)
    {
        this.sellingPoint = sellingPoint;
    }

    public String getSellingPoint()
    {
        return sellingPoint;
    }
    public void setcCheckRemark(String cCheckRemark)
    {
        this.cCheckRemark = cCheckRemark;
    }

    public String getcCheckRemark()
    {
        return cCheckRemark;
    }
    public void setProductRemarks(String productRemarks)
    {
        this.productRemarks = productRemarks;
    }

    public String getProductRemarks()
    {
        return productRemarks;
    }
    public void setProductImg(String productImg)
    {
        this.productImg = productImg;
    }

    public String getProductImg()
    {
        return productImg;
    }
    public void setSupplier(String supplier)
    {
        this.supplier = supplier;
    }

    public String getSupplier()
    {
        return supplier;
    }
    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public Long getOrderType() {
        return orderType;
    }

    public void setOrderType(Long orderType) {
        this.orderType = orderType;
    }

    public Long getMaintainType() {
        return maintainType;
    }

    public void setMaintainType(Long maintainType) {
        this.maintainType = maintainType;
    }

    public String getInitialOrderNumber() {
        return initialOrderNumber;
    }

    public void setInitialOrderNumber(String initialOrderNumber) {
        this.initialOrderNumber = initialOrderNumber;
    }

    public String getNumberType() {
        return numberType;
    }

    public void setNumberType(String numberType) {
        this.numberType = numberType;
    }

    public String getProblemDescription() {
        return problemDescription;
    }

    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    public void setRetailPrice(BigDecimal retailPrice)
    {
        this.retailPrice = retailPrice;
    }

    public BigDecimal getRetailPrice()
    {
        return retailPrice;
    }
    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getCreator()
    {
        return creator;
    }
    public void setCreationTime(Date creationTime)
    {
        this.creationTime = creationTime;
    }

    public Date getCreationTime()
    {
        return creationTime;
    }
    public void setRefreshTime(Date refreshTime)
    {
        this.refreshTime = refreshTime;
    }

    public Date getRefreshTime()
    {
        return refreshTime;
    }
    public void setRefreshName(String refreshName)
    {
        this.refreshName = refreshName;
    }

    public String getRefreshName()
    {
        return refreshName;
    }

    public void setcDeliveryTime(Date cDeliveryTime)
    {
        this.cDeliveryTime = cDeliveryTime;
    }

    public Date getcDeliveryTime()
    {
        return cDeliveryTime;
    }
    public void setcPurchasePrice(Long cPurchasePrice)
    {
        this.cPurchasePrice = cPurchasePrice;
    }

    public Long getcPurchasePrice()
    {
        return cPurchasePrice;
    }

    public String getAuditTimeStartDate() {
        return auditTimeStartDate;
    }

    public void setAuditTimeStartDate(String auditTimeStartDate) {
        this.auditTimeStartDate = auditTimeStartDate;
    }

    public String getAuditTimeEndDate() {
        return auditTimeEndDate;
    }

    public void setAuditTimeEndDate(String auditTimeEndDate) {
        this.auditTimeEndDate = auditTimeEndDate;
    }

    public String getPlanDeliveryPeriodStartDate() {
        return planDeliveryPeriodStartDate;
    }

    public void setPlanDeliveryPeriodStartDate(String planDeliveryPeriodStartDate) {
        this.planDeliveryPeriodStartDate = planDeliveryPeriodStartDate;
    }

    public String getPlanDeliveryPeriodEndDate() {
        return planDeliveryPeriodEndDate;
    }

    public void setPlanDeliveryPeriodEndDate(String planDeliveryPeriodEndDate) {
        this.planDeliveryPeriodEndDate = planDeliveryPeriodEndDate;
    }

    public void setcCheckTime(Date cCheckTime)
    {
        this.cCheckTime = cCheckTime;
    }

    public Date getcCheckTime()
    {
        return cCheckTime;
    }

    public void setcCheckState(Long cCheckState)
    {
        this.cCheckState = cCheckState;
    }

    public Long getcCheckState()
    {
        return cCheckState;
    }

    public String getCustomization() {
        return customization;
    }

    public void setCustomization(String customization) {
        this.customization = customization;
    }

    public Long getPutIn() {
        return putIn;
    }

    public void setPutIn(Long putIn) {
        this.putIn = putIn;
    }

    public Long getReject() {
        return reject;
    }

    public void setReject(Long reject) {
        this.reject = reject;
    }

    public Long getAudit() {
        return audit;
    }

    public void setAudit(Long audit) {
        this.audit = audit;
    }

    public Long getPurchase() {
        return purchase;
    }

    public void setPurchase(Long purchase) {
        this.purchase = purchase;
    }

    public Long getArrive() {
        return arrive;
    }

    public void setArrive(Long arrive) {
        this.arrive = arrive;
    }

    public Long getStorage() {
        return storage;
    }

    public void setStorage(Long storage) {
        this.storage = storage;
    }

    public Long getDeparture() {
        return departure;
    }

    public void setDeparture(Long departure) {
        this.departure = departure;
    }

    public Long getLastYearClient() {
        return lastYearClient;
    }

    public void setLastYearClient(Long lastYearClient) {
        this.lastYearClient = lastYearClient;
    }

    public Long getThisYearClient() {
        return thisYearClient;
    }

    public void setThisYearClient(Long thisYearClient) {
        this.thisYearClient = thisYearClient;
    }

    public BigDecimal getThisYearClientContrast() {
        return thisYearClientContrast;
    }

    public void setThisYearClientContrast(BigDecimal thisYearClientContrast) {
        this.thisYearClientContrast = thisYearClientContrast;
    }

    public Long getLastMonthClient() {
        return lastMonthClient;
    }

    public void setLastMonthClient(Long lastMonthClient) {
        this.lastMonthClient = lastMonthClient;
    }

    public Long getTheSameMonthClient() {
        return theSameMonthClient;
    }

    public void setTheSameMonthClient(Long theSameMonthClient) {
        this.theSameMonthClient = theSameMonthClient;
    }

    public BigDecimal getTheSameMonthClientContrast() {
        return theSameMonthClientContrast;
    }

    public void setTheSameMonthClientContrast(BigDecimal theSameMonthClientContrast) {
        this.theSameMonthClientContrast = theSameMonthClientContrast;
    }

    public Long getGrossClient() {
        return grossClient;
    }

    public void setGrossClient(Long grossClient) {
        this.grossClient = grossClient;
    }

    public Long getLastYearAltogether() {
        return lastYearAltogether;
    }

    public void setLastYearAltogether(Long lastYearAltogether) {
        this.lastYearAltogether = lastYearAltogether;
    }

    public Long getThisYearAltogether() {
        return thisYearAltogether;
    }

    public void setThisYearAltogether(Long thisYearAltogether) {
        this.thisYearAltogether = thisYearAltogether;
    }

    public BigDecimal getThisYearAltogetherContrast() {
        return thisYearAltogetherContrast;
    }

    public void setThisYearAltogetherContrast(BigDecimal thisYearAltogetherContrast) {
        this.thisYearAltogetherContrast = thisYearAltogetherContrast;
    }

    public Long getLastMonthAltogether() {
        return lastMonthAltogether;
    }

    public void setLastMonthAltogether(Long lastMonthAltogether) {
        this.lastMonthAltogether = lastMonthAltogether;
    }

    public Long getTheSameMonthAltogether() {
        return theSameMonthAltogether;
    }

    public void setTheSameMonthAltogether(Long theSameMonthAltogether) {
        this.theSameMonthAltogether = theSameMonthAltogether;
    }

    public BigDecimal getTheSameMonthAltogetherContrast() {
        return theSameMonthAltogetherContrast;
    }

    public void setTheSameMonthAltogetherContrast(BigDecimal theSameMonthAltogetherContrast) {
        this.theSameMonthAltogetherContrast = theSameMonthAltogetherContrast;
    }

    public Long getAltogether() {
        return altogether;
    }

    public void setAltogether(Long altogether) {
        this.altogether = altogether;
    }

    public String getcCheckArriveStartDate() {
        return cCheckArriveStartDate;
    }

    public void setcCheckArriveStartDate(String cCheckArriveStartDate) {
        this.cCheckArriveStartDate = cCheckArriveStartDate;
    }

    public String getcCheckArriveEndDate() {
        return cCheckArriveEndDate;
    }

    public void setcCheckArriveEndDate(String cCheckArriveEndDate) {
        this.cCheckArriveEndDate = cCheckArriveEndDate;
    }

    public String getcCheckLeaveStartDate() {
        return cCheckLeaveStartDate;
    }

    public void setcCheckLeaveStartDate(String cCheckLeaveStartDate) {
        this.cCheckLeaveStartDate = cCheckLeaveStartDate;
    }

    public String getcCheckLeaveEndDate() {
        return cCheckLeaveEndDate;
    }

    public void setcCheckLeaveEndDate(String cCheckLeaveEndDate) {
        this.cCheckLeaveEndDate = cCheckLeaveEndDate;
    }

    public String getcCheckTimeStartDate() {
        return cCheckTimeStartDate;
    }

    public void setcCheckTimeStartDate(String cCheckTimeStartDate) {
        this.cCheckTimeStartDate = cCheckTimeStartDate;
    }

    public String getcCheckTimeEndDate() {
        return cCheckTimeEndDate;
    }

    public void setcCheckTimeEndDate(String cCheckTimeEndDate) {
        this.cCheckTimeEndDate = cCheckTimeEndDate;
    }

    public String getcRevertTimeStartDate() {
        return cRevertTimeStartDate;
    }

    public void setcRevertTimeStartDate(String cRevertTimeStartDate) {
        this.cRevertTimeStartDate = cRevertTimeStartDate;
    }

    public String getcRevertTimeEndDate() {
        return cRevertTimeEndDate;
    }

    public void setcRevertTimeEndDate(String cRevertTimeEndDate) {
        this.cRevertTimeEndDate = cRevertTimeEndDate;
    }

    public String getDeliveryTimeStartDate() {
        return deliveryTimeStartDate;
    }

    public void setDeliveryTimeStartDate(String deliveryTimeStartDate) {
        this.deliveryTimeStartDate = deliveryTimeStartDate;
    }

    public String getDeliveryTimeEndDate() {
        return deliveryTimeEndDate;
    }

    public void setDeliveryTimeEndDate(String deliveryTimeEndDate) {
        this.deliveryTimeEndDate = deliveryTimeEndDate;
    }

    public Long getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(Long ordersId) {
        this.ordersId = ordersId;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public Long getwStatusCode() {
        return wStatusCode;
    }

    public void setwStatusCode(Long wStatusCode) {
        this.wStatusCode = wStatusCode;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getBarcodeGroupStatus() {
        return barcodeGroupStatus;
    }

    public void setBarcodeGroupStatus(String barcodeGroupStatus) {
        this.barcodeGroupStatus = barcodeGroupStatus;
    }

    public Long getwStatusCodeCount() {
        return wStatusCodeCount;
    }

    public void setwStatusCodeCount(Long wStatusCodeCount) {
        this.wStatusCodeCount = wStatusCodeCount;
    }

    public Date getEntranceTime() {
        return entranceTime;
    }

    public void setEntranceTime(Date entranceTime) {
        this.entranceTime = entranceTime;
    }

    public String getEntranceName() {
        return entranceName;
    }

    public void setEntranceName(String entranceName) {
        this.entranceName = entranceName;
    }

    public Date getSweepTime() {
        return sweepTime;
    }

    public void setSweepTime(Date sweepTime) {
        this.sweepTime = sweepTime;
    }

    public String getSweepName() {
        return sweepName;
    }

    public void setSweepName(String sweepName) {
        this.sweepName = sweepName;
    }

    public Long getPutaway() {
        return putaway;
    }

    public void setPutaway(Long putaway) {
        this.putaway = putaway;
    }

    public Long getConceal() {
        return conceal;
    }

    public void setConceal(Long conceal) {
        this.conceal = conceal;
    }
    public void setcEntranceTime(Date cEntranceTime)
    {
        this.cEntranceTime = cEntranceTime;
    }

    public Date getcEntranceTime()
    {
        return cEntranceTime;
    }

    public String getParStatusCode() {
        return parStatusCode;
    }

    public void setParStatusCode(String parStatusCode) {
        this.parStatusCode = parStatusCode;
    }

    public String getPendingQuantity() {
        return pendingQuantity;
    }

    public void setPendingQuantity(String pendingQuantity) {
        this.pendingQuantity = pendingQuantity;
    }

    public String getOrderTimeStartDate() {
        return orderTimeStartDate;
    }

    public void setOrderTimeStartDate(String orderTimeStartDate) {
        this.orderTimeStartDate = orderTimeStartDate;
    }

    public String getOrderTimeEndDate() {
        return orderTimeEndDate;
    }

    public void setOrderTimeEndDate(String orderTimeEndDate) {
        this.orderTimeEndDate = orderTimeEndDate;
    }

    public String getcGiveTimeStartDate() {
        return cGiveTimeStartDate;
    }

    public void setcGiveTimeStartDate(String cGiveTimeStartDate) {
        this.cGiveTimeStartDate = cGiveTimeStartDate;
    }

    public String getcGiveTimeEndDate() {
        return cGiveTimeEndDate;
    }

    public void setcGiveTimeEndDate(String cGiveTimeEndDate) {
        this.cGiveTimeEndDate = cGiveTimeEndDate;
    }


    public Long getPrintStatus() {
        return printStatus;
    }

    public void setPrintStatus(Long printStatus) {
        this.printStatus = printStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("clientId", getClientId())
                .append("mainId", getMainId())
                .append("clientNumber", getClientNumber())
                .append("orderNumber", getOrderNumber())
                .append("orders", getOrders())
                .append("ordersStatus", getOrdersStatus())
                .append("statusId", getStatusId())
                .append("contactWay", getContactWay())
                .append("storefront", getStorefront())
                .append("storefrontCoding", getStorefrontCoding())
                .append("customerName", getCustomerName())
                .append("customerTelephone", getCustomerTelephone())
                .append("orderTime", getOrderTime())
                .append("customerAddress", getCustomerAddress())
                .append("storefrontType", getStorefrontType())
                .append("customName", getCustomName())
                .append("customQuality", getCustomQuality())
                .append("customSpecifications", getCustomSpecifications())
                .append("customDyestuff", getCustomDyestuff())
                .append("customSkin", getCustomSkin())
                .append("customChangeColour", getCustomChangeColour())
                .append("customOther", getCustomOther())
                .append("stateDescription", getStateDescription())
                .append("statusCode", getStatusCode())
                .append("userDefined", getUserDefined())
                .append("remarks", getRemarks())
                .append("auditTime", getAuditTime())
                .append("number", getNumber())
                .append("overrule", getOverrule())
                .append("cGiveTime", getcGiveTime())
                .append("cPurchaseTime", getcPurchaseTime())
                .append("planDeliveryPeriod", getPlanDeliveryPeriod())
                .append("cPeriod", getcPeriod())
                .append("cScheduledTime", getcScheduledTime())
                .append("deliveryTime", getDeliveryTime())
                .append("notesOnShipping", getNotesOnShipping())
                .append("cWantTime", getcWantTime())
                .append("deliveryMethod", getDeliveryMethod())
                .append("cRevertTime", getcRevertTime())
                .append("yxNumber", getYxNumber())
                .append("supplierNumber", getSupplierNumber())
                .append("cProcurementRemark", getcProcurementRemark())
                .append("productName", getProductName())
                .append("cAggregateAmount", getcAggregateAmount())
                .append("materialQuality", getMaterialQuality())
                .append("cFreight", getcFreight())
                .append("specifications", getSpecifications())
                .append("cCheckArrive", getcCheckArrive())
                .append("dyestuff", getDyestuff())
                .append("cCheckResult", getcCheckResult())
                .append("classify", getClassify())
                .append("cCheckLeave", getcCheckLeave())
                .append("sellingPoint", getSellingPoint())
                .append("cCheckRemark", getcCheckRemark())
                .append("productRemarks", getProductRemarks())
                .append("productImg", getProductImg())
                .append("supplier", getSupplier())
                .append("label", getLabel())
                .append("identification", getIdentification())
                .append("orderType", getOrderType())
                .append("maintainType", getMaintainType())
                .append("initialOrderNumber", getInitialOrderNumber())
                .append("numberType", getNumberType())
                .append("problemDescription", getProblemDescription())
                .append("retailPrice", getRetailPrice())
                .append("creator", getCreator())
                .append("creationTime", getCreationTime())
                .append("refreshTime", getRefreshTime())
                .append("refreshName", getRefreshName())
                .append("cDeliveryTime", getcDeliveryTime())
                .append("cPurchasePrice", getcPurchasePrice())
                .append("auditTimeStartDate", getAuditTimeStartDate())
                .append("auditTimeEndDate", getAuditTimeEndDate())
                .append("planDeliveryPeriodStartDate", getPlanDeliveryPeriodStartDate())
                .append("planDeliveryPeriodEndDate", getPlanDeliveryPeriodEndDate())
                .append("cCheckTime", getcCheckTime())
                .append("cCheckState", getcCheckState())
                .append("customization", getCustomization())
                .append("putIn", getPutIn())
                .append("reject", getReject())
                .append("audit", getAudit())
                .append("purchase", getPurchase())
                .append("arrive", getArrive())
                .append("storage", getStorage())
                .append("departure", getDeparture())
                .append("lastYearClient", getLastYearClient())
                .append("thisYearClient", getThisYearClient())
                .append("thisYearClientContrast", getThisYearAltogetherContrast())
                .append("lastMonthClient", getLastMonthClient())
                .append("theSameMonthClient", getTheSameMonthClient())
                .append("grossClient", getGrossClient())
                .append("theSameMonthClientContrast", getTheSameMonthAltogetherContrast())
                .append("lastYearAltogether", getLastYearAltogether())
                .append("thisYearAltogether", getThisYearAltogether())
                .append("thisYearAltogetherContrast", getThisYearAltogetherContrast())
                .append("lastMonthAltogether", getLastMonthAltogether())
                .append("theSameMonthAltogether", getTheSameMonthAltogether())
                .append("theSameMonthAltogetherContrast", getTheSameMonthAltogetherContrast())
                .append("altogether", getAltogether())
                .append("cCheckArriveStartDate", getcCheckArriveStartDate())
                .append("cCheckArriveEndDate", getcCheckArriveEndDate())
                .append("cCheckLeaveStartDate", getcCheckLeaveStartDate())
                .append("cCheckLeaveEndDate", getcCheckLeaveEndDate())
                .append("cCheckTimeStartDate", getcCheckTimeStartDate())
                .append("cCheckTimeEndDate", getcCheckTimeEndDate())
                .append("cRevertTimeStartDate", getcRevertTimeStartDate())
                .append("cRevertTimeEndDate", getcRevertTimeEndDate())
                .append("deliveryTimeStartDate", getDeliveryTimeStartDate())
                .append("deliveryTimeEndDate", getDeliveryTimeEndDate())
                .append("ordersId", getOrdersId())
                .append("barCode", getBarCode())
                .append("wStatusCode", getwStatusCode())
                .append("quantity", getQuantity())
                .append("location", getLocation())
                .append("serialNumber", getSerialNumber())
                .append("barcodeGroupStatus", getBarcodeGroupStatus())
                .append("wStatusCodeCount", getwStatusCodeCount())
                .append("entranceTime", getEntranceTime())
                .append("entranceName", getEntranceName())
                .append("sweepTime", getSweepTime())
                .append("sweepName", getSweepName())
                .append("putaway", getPutaway())
                .append("conceal", getConceal())
                .append("cEntranceTime", getcEntranceTime())
                .append("parStatusCode", getParStatusCode())
                .append("pendingQuantity", getPendingQuantity())
                .append("orderTimeStartDate", getOrderTimeStartDate())
                .append("orderTimeEndDate", getOrderTimeEndDate())
                .append("cGiveTimeStartDate", getcGiveTimeStartDate())
                .append("cGiveTimeEndDate", getcGiveTimeEndDate())
                .append("printStatus", getPrintStatus())
                .toString();
    }
}
