package com.ruoyi.backstage.controller;

import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.utils.file.FileUploadUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.ProductPigment;
import com.ruoyi.backstage.service.ProductPigmentService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 产品色号组Controller
 * 
 * @author xin-xin
 * @date 2022-01-05
 */
@Controller
@RequestMapping("/backstage/pigment")
public class ProductPigmentController extends BaseController
{
    private String prefix = "backstage/pigment";

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ProductPigmentService productPigmentService;

    @RequiresPermissions("backstage:pigment:view")
    @GetMapping()
    public String pigment()
    {
        return prefix + "/pigment";
    }

    /**
     * 查询产品色号组列表
     */
    @RequiresPermissions("backstage:pigment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductPigment productPigment)
    {
        startPage();
        List<ProductPigment> list = productPigmentService.selectProductPigmentList(productPigment);
        return getDataTable(list);
    }

    /**
     * 导出产品色号组列表
     */
    @RequiresPermissions("backstage:pigment:export")
    @Log(title = "产品色号组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductPigment productPigment)
    {
        List<ProductPigment> list = productPigmentService.selectProductPigmentList(productPigment);
        ExcelUtil<ProductPigment> util = new ExcelUtil<ProductPigment>(ProductPigment.class);
        return util.exportExcel(list, "产品色号组数据");
    }

    /**
     * 新增产品色号组
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存产品色号组
     */
    @RequiresPermissions("backstage:pigment:add")
    @Log(title = "产品色号添加", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductPigment productPigment)
    {
        int i = productPigmentService.pigmentAdd(productPigment);
        if (i > 0) {
            return AjaxResult.success("添加成功!");
        } else if (i == -1) {
            return AjaxResult.error("当前 色号数据 已存在!请检查核对后再操作!!");
        } else {
            return AjaxResult.error("添加失败!请联系管理员!!!");
        }
    }

    @RequiresPermissions("backstage:pigment:add")
    @RequestMapping(value = "/pigmentDuplicateChecking", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult pigmentDuplicateChecking(ProductPigment productPigment) {
        int i = productPigmentService.pigmentDuplicateChecking(productPigment);
        if(i > 0){
            return AjaxResult.error("当前编号已存在");
        }else{
            return AjaxResult.success("当前编号无重复");
        }
    }

    /**
     * 修改产品色号组
     */
    @RequiresPermissions("backstage:pigment:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductPigment productPigment = productPigmentService.selectProductPigmentById(id);
        mmap.put("productPigment", productPigment);
        return prefix + "/edit";
    }


    /**
     * 修改保存产品色号组
     */
    @RequiresPermissions("backstage:pigment:edit")
    @Log(title = "产品色号修改保存", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProductPigment productPigment)
    {
        return toAjax(productPigmentService.updateProductPigment(productPigment));
    }



    /**
     * 删除产品色号组
     */
    @RequiresPermissions("backstage:pigment:remove")
    @Log(title = "产品色号删除", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(productPigmentService.deleteProductPigmentByIds(ids));

    }


    /**
     * 删除img
     *
     * @param request 请求
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-10-31 12:04:08
     */
    @RequiresPermissions("backstage:pigment:remove")
    @Log(title = "色号图片删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/deleteImg", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteImg(HttpServletRequest request) {
        int i = productPigmentService.deleteImg(request);
        if(i == 200){
            return AjaxResult.success("图片-删除成功!");
        }else if (i == 404){
            return AjaxResult.warn("删除失败,图片不存在!!!");
        }else{
            return AjaxResult.error("图片-删除失败,未知错误!请联系管理员!!!");
        }
    }


    /**
     * 色号主图上传
     *
     * @param file 文件
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-10-29 16:11:59
     */
    @RequiresPermissions("backstage:pigment:artworkMasterUpload")
    @PostMapping("/artworkMasterUpload")
    @ResponseBody
    public AjaxResult artworkMasterUpload(MultipartFile file) throws Exception {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPathArtworkMaster();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }



}
