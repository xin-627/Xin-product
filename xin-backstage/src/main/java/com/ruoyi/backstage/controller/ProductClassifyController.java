package com.ruoyi.backstage.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.ProductClassify;
import com.ruoyi.backstage.service.ProductClassifyService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 分类菜单/分类管理Controller
 * 
 * @author xin-xin
 * @date 2022-01-11
 */
@Controller
@RequestMapping("/backstage/classify")
public class ProductClassifyController extends BaseController
{
    private String prefix = "backstage/classify";

    @Autowired
    private ProductClassifyService productClassifyService;

    @RequiresPermissions("backstage:classify:view")
    @GetMapping()
    public String classify()
    {
        return prefix + "/classify";
    }

    /**
     * 查询分类菜单/分类管理树列表
     */
    @RequiresPermissions("backstage:classify:list")
    @PostMapping("/list")
    @ResponseBody
    public List<ProductClassify> list(ProductClassify productClassify)
    {
        List<ProductClassify> list = productClassifyService.selectProductClassifyList(productClassify);
        return list;
    }

    /**
     * 导出分类菜单/分类管理列表
     */
    @RequiresPermissions("backstage:classify:export")
    @Log(title = "分类菜单/分类管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductClassify productClassify)
    {
        List<ProductClassify> list = productClassifyService.selectProductClassifyList(productClassify);
        ExcelUtil<ProductClassify> util = new ExcelUtil<ProductClassify>(ProductClassify.class);
        return util.exportExcel(list, "分类菜单/分类管理数据");
    }

    /**
     * 新增分类菜单/分类管理
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("productClassify", productClassifyService.selectProductClassifyById(id));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存分类菜单/分类管理
     */
    @RequiresPermissions("backstage:classify:add")
    @Log(title = "分类菜单/分类管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductClassify productClassify)
    {
        return toAjax(productClassifyService.insertProductClassify(productClassify));
    }

    /**
     * 修改分类菜单/分类管理
     */
    @RequiresPermissions("backstage:classify:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductClassify productClassify = productClassifyService.selectProductClassifyById(id);
        mmap.put("productClassify", productClassify);
        return prefix + "/edit";
    }

    /**
     * 修改保存分类菜单/分类管理
     */
    @RequiresPermissions("backstage:classify:edit")
    @Log(title = "分类菜单/分类管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProductClassify productClassify)
    {
        return toAjax(productClassifyService.updateProductClassify(productClassify));
    }

    /**
     * 删除
     */
    @RequiresPermissions("backstage:classify:remove")
    @Log(title = "分类菜单/分类管理", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id)
    {
        return toAjax(productClassifyService.deleteProductClassifyById(id));
    }

    /**
     * 选择分类菜单/分类管理树
     */
    @GetMapping(value = { "/selectClassifyTree/{id}", "/selectClassifyTree/" })
    public String selectClassifyTree(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("productClassify", productClassifyService.selectProductClassifyById(id));
        }
        return prefix + "/tree";
    }

    /**
     * 加载分类菜单/分类管理树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = productClassifyService.selectProductClassifyTree();
        return ztrees;
    }

    /**
     * 分类管理树
     * 2022-01-11
     * @return
     */
    @GetMapping("/treeClassificationData")
    @ResponseBody
    public Map<String,Object> treeClassificationData()
    {
        return productClassifyService.TreeUtilClassification();

    }



}
