package com.ruoyi.backstage.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.ProductSpecifications;
import com.ruoyi.backstage.service.ProductSpecificationsService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品规格组Controller
 * 
 * @author xin-xin
 * @date 2022-01-05
 */
@Controller
@RequestMapping("/backstage/specifications")
public class ProductSpecificationsController extends BaseController
{
    private String prefix = "backstage/specifications";

    @Autowired
    private ProductSpecificationsService productSpecificationsService;

    @RequiresPermissions("backstage:specifications:view")
    @GetMapping()
    public String specifications()
    {
        return prefix + "/specifications";
    }

    /**
     * 查询产品规格组列表
     */
    @RequiresPermissions("backstage:specifications:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductSpecifications productSpecifications)
    {
        startPage();
        List<ProductSpecifications> list = productSpecificationsService.selectProductSpecificationsList(productSpecifications);
        return getDataTable(list);
    }

    /**
     * 导出产品规格组列表
     */
    @RequiresPermissions("backstage:specifications:export")
    @Log(title = "产品规格组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductSpecifications productSpecifications)
    {
        List<ProductSpecifications> list = productSpecificationsService.selectProductSpecificationsList(productSpecifications);
        ExcelUtil<ProductSpecifications> util = new ExcelUtil<ProductSpecifications>(ProductSpecifications.class);
        return util.exportExcel(list, "产品规格组数据");
    }

    /**
     * 新增产品规格组
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存产品规格组
     */
    @RequiresPermissions("backstage:specifications:add")
    @Log(title = "添加保存产品规格数据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductSpecifications data)
    {
        int i = productSpecificationsService.specificationsAdd(data);
        if (i > 0) {
            return AjaxResult.success("添加成功!");
        } else if (i == -1) {
            return AjaxResult.warn("当前 规格 已存在!请检查核对后操作!!");
        } else {
            return AjaxResult.error("添加失败!,请联系管理员!!!");
        }
    }

    /**
     * 修改产品规格组
     */
    @RequiresPermissions("backstage:specifications:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductSpecifications productSpecifications = productSpecificationsService.selectProductSpecificationsById(id);
        mmap.put("productSpecifications", productSpecifications);
        return prefix + "/edit";
    }

    /**
     * 修改保存产品规格
     */
    @RequiresPermissions("backstage:specifications:edit")
    @Log(title = "修改保存产品数据", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProductSpecifications productSpecifications)
    {
        int i = productSpecificationsService.updateProductSpecifications(productSpecifications);
        if (i > 0) {
            return AjaxResult.success("修改成功!");
        } else if (i == -1) {
            return AjaxResult.warn("当前产品的规格存在重复!请检查核对后操作!!");
        } else {
            return AjaxResult.error("修改失败!请联系管理员!!!");
        }
    }

    /**
     * 删除产品规格组
     */
    @RequiresPermissions("backstage:specifications:remove")
    @Log(title = "删除产品规格", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(productSpecificationsService.deleteProductSpecificationsByIds(ids));
    }


}
