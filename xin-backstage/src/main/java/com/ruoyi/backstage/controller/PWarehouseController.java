package com.ruoyi.backstage.controller;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.ruoyi.backstage.domain.PWarehouseNumber;
import com.ruoyi.backstage.domain.ProductOrderInformation;
import com.ruoyi.backstage.service.ProductOrderInformationService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.PWarehouse;
import com.ruoyi.backstage.service.IPWarehouseService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 仓库标签Controller
 *
 * @author xin_xin
 * @date 2022-07-12
 */
@Controller
@RequestMapping("/backstage/warehouse")
public class PWarehouseController extends BaseController {
    private String prefix = "backstage/warehouse";

    @Autowired
    private IPWarehouseService pWarehouseService;

    @Autowired
    private ProductOrderInformationService productOrderInformationService;

    @RequiresPermissions("backstage:warehouse:view")
    @GetMapping()
    public String warehouse() {
        return prefix + "/warehouse";
    }

    /**
     * 仓储订单详情
     *
     * @return
     */
    @RequiresPermissions("backstage:warehouse:orderDetail")
    @RequestMapping("/orderDetail/{id}")
    public String orderDetail(@PathVariable("id") Long id, ModelMap mmap) {
        mmap.put("locationId", id);
        return  "backstage/warehouseNumber/orderDetail";
    }

    /**
     * 仓储位 订单详情
     */
    @RequiresPermissions("backstage:information:locationIdList")
    @PostMapping("/locationIdList")
    @ResponseBody
    public TableDataInfo selectLocationIdList(ProductOrderInformation productOrderInformation) {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectLocationIdList(productOrderInformation);
        return getDataTable(list);
    }




    /**
     * Go 待回厂订单
     *
     * @return
     */
    @RequiresPermissions("backstage:warehouse:canBePutInStorage")
    @RequestMapping("/canBePutInStorage")
    public String CanBePutInStorage() {
        return prefix + "/canBePutInStorage";

    }

    /**
     * Go已回厂_待入库订单
     *
     * @return
     */
    @RequiresPermissions("backstage:warehouse:WaitingForTheWarehousing")
    @RequestMapping("/WaitingForTheWarehousing")
    public String WaitingForTheWarehousing() {
        return prefix + "/WaitingForTheWarehousing";

    }


    /**
     * Go 在库订单
     *
     * @return
     */
    @RequiresPermissions("backstage:warehouse:inTheWarehouse")
    @RequestMapping("/inTheWarehouse")
    public String inTheWarehouse() {
        return prefix + "/inTheWarehouse";

    }

    /**
     * Go 已出库订单
     *
     * @return
     */
    @RequiresPermissions("backstage:warehouse:haveOutbound")
    @RequestMapping("/haveOutbound")
    public String haveOutbound() {
        return prefix + "/haveOutbound";

    }


    /**
     * 扫码入库 -选库
     *
     * @return
     */
    @RequiresPermissions("backstage:warehouse:sweepCodeAccess")
    @RequestMapping("/sweepCodeAccess")
    public String sweepCodeAccess() {
        return prefix + "/sweepCodeAccess";

    }


    /**
     * 扫码入库 -扫库位码
     *
     * @return
     */
    @RequiresPermissions("backstage:warehouse:shelfBarCodeAccess")
    @RequestMapping("/shelfBarCodeAccess")
    public String shelfBarCodeAccess() {
        return prefix + "/shelfBarCodeAccess";

    }


    /**
     * 扫码出库
     *
     * @return
     */
    @RequiresPermissions("backstage:warehouse:sweepCodeOutbound")
    @RequestMapping("/sweepCodeOutbound")
    public String sweepCodeOutbound() {
        return prefix + "/sweepCodeOutbound";

    }

    /**
     * 订单回厂
     */
    @RequiresPermissions("backstage:warehouse:ordersAccess")
    @Log(title = "订单回厂", businessType = BusinessType.UPDATE)
    @PostMapping("/ordersAccess")
    @ResponseBody
    public AjaxResult ordersAccess(Long id, Long number) {
        int i = pWarehouseService.ordersAccess(id, number);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * 查询订单管理列表
     */
    @RequiresPermissions("backstage:information:barCodeList")
    @PostMapping("/barCodeList")
    @ResponseBody
    public TableDataInfo barCodeList(ProductOrderInformation productOrderInformation) {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectBarCodeList(productOrderInformation);
        return getDataTable(list);
    }


    /**
     * 扫码入库  - 选中库位入库
     */
    @RequiresPermissions("backstage:warehouse:sweepCodeAccessEdit")
    @Log(title = "扫码入库-选库位", businessType = BusinessType.UPDATE)
    @PostMapping("/sweepCodeAccessEdit")
    @ResponseBody
    public JSONObject sweepCodeAccessEdit(PWarehouse pWarehouse) {
        int i = pWarehouseService.sweepCodeAccessEdit(pWarehouse);
        JSONObject jsonObject = new JSONObject();
        if (i == 1) {
            jsonObject.put("code", "0");
            jsonObject.put("msg", "操作成功!当前条码入库成功");
        } else if(i == -1) {
            jsonObject.put("code", "5001");
            jsonObject.put("msg", "操作失败!当前条码已入库");
        } else if(i == -2) {
            jsonObject.put("code", "5002");
            jsonObject.put("msg", "操作失败!当前条码已出库");
        }else if(i == -3) {
            jsonObject.put("code", "5003");
            jsonObject.put("msg", "操作失败!没有找到相关条码的信息");
        } else{
            jsonObject.put("code", "500");
            jsonObject.put("msg", "操作失败!未知错误-请联系管理员!!!");
        }
        return jsonObject;
    }


    /**
     * 扫码入库  - 根据库位标签入库
     */
    @RequiresPermissions("backstage:warehouse:shelfBarCodeAccessEdit")
    @Log(title = "扫码入库-扫库位码", businessType = BusinessType.UPDATE)
    @PostMapping("/shelfBarCodeAccessEdit")
    @ResponseBody
    public JSONObject shelfBarCodeAccessEdit(PWarehouse pWarehouse) {
        int i = pWarehouseService.shelfBarCodeAccessEdit(pWarehouse);
        JSONObject jsonObject = new JSONObject();
        if (i == 1) {
            jsonObject.put("code", "0");
            jsonObject.put("msg", "操作成功!当前条码入库成功");
        } else if (i == -1) {
            jsonObject.put("code", "5001");
            jsonObject.put("msg", "操作失败!当前条码已入库");
        } else if (i == -2) {
            jsonObject.put("code", "5002");
            jsonObject.put("msg", "操作失败!当前条码已出库");
        } else if (i == -3) {
            jsonObject.put("code", "5003");
            jsonObject.put("msg", "操作失败!没有找到库位信息-请先扫库位码");
        } else if (i == -4) {
            jsonObject.put("code", "001");
            jsonObject.put("msg", "操作成功!库位码已识别,请继续扫条码");
        } else if (i == -5) {
            jsonObject.put("code", "5004");
            jsonObject.put("msg", "操作失败!库位数据数据创建失败 请重重扫库位码");
        } else {
            jsonObject.put("code", "500");
            jsonObject.put("msg", "操作失败!未知错误-请联系管理员!!!");
        }
        return jsonObject;
    }



    /**
     * 扫码出库
     */
    @RequiresPermissions("backstage:warehouse:sweepCodeOutboundEdit")
    @Log(title = "扫码出库", businessType = BusinessType.UPDATE)
    @PostMapping("/sweepCodeOutboundEdit")
    @ResponseBody
    public JSONObject sweepCodeOutboundEdit(PWarehouse pWarehouse) {
        int i = pWarehouseService.sweepCodeOutboundEdit(pWarehouse);
        JSONObject jsonObject = new JSONObject();
        if (i == 1) {
            jsonObject.put("code", "0");
            jsonObject.put("msg", "操作成功!当前条码出库成功");
        } else if(i == -1) {
            jsonObject.put("code", "5001");
            jsonObject.put("msg", "操作失败!当前条码还未入库-请先入库");
        } else if(i == -2) {
            jsonObject.put("code", "5002");
            jsonObject.put("msg", "操作失败!当前条码已出库");
        } else{
            jsonObject.put("code", "500");
            jsonObject.put("msg", "操作失败!未知错误-请联系管理员!!!");
        }
        return jsonObject;
    }


    /**
     * 已出库订单查询
     */
    @RequiresPermissions("backstage:warehouse:selectHaveOutboundList")
    @PostMapping("/selectHaveOutboundList")
    @ResponseBody
    public TableDataInfo selectHaveOutboundList(ProductOrderInformation productOrderInformation) {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectHaveOutboundList(productOrderInformation);
        return getDataTable(list);
    }


    /**
     * 订单条码详情
     */
    @RequiresPermissions("backstage:warehouse:barCodeDetails")
    @GetMapping("/barCodeDetails/{id}")
    public String barCodeDetails(@PathVariable("id") Long id, ModelMap mmap) {
        mmap.put("ordersId", id);
        return prefix + "/barCodeDetails";
    }



    /**
     * 查询仓库标签列表
     */
    @RequiresPermissions("backstage:warehouse:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PWarehouse pWarehouse) {
        startPage();
        List<PWarehouse> list = pWarehouseService.selectPWarehouseList(pWarehouse);
        return getDataTable(list);
    }



    /**
     * 出库时
     * 订单查询
     */
    @RequiresPermissions("backstage:warehouse:sweepCodeOutboundList")
    @PostMapping("/sweepCodeOutboundList")
    @ResponseBody
    public TableDataInfo sweepCodeOutboundList(ProductOrderInformation productOrderInformation) {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectSweepCodeOutboundList(productOrderInformation);
        return getDataTable(list);
    }


    /**
     * 已入库 但没有条码的订单
     */
    @RequiresPermissions("backstage:warehouse:noBarCodeNumber")
    @GetMapping("/noBarCodeNumber")
    public String noBarCodeNumber() {
        return prefix + "/noBarCodeNumber";
    }


    /**
     *已入库 但没有条码的订单 List
     */
    @RequiresPermissions("backstage:warehouse:noBarCodeNumberList")
    @PostMapping("/noBarCodeNumberList")
    @ResponseBody
    public TableDataInfo noBarCodeNumberList(ProductOrderInformation productOrderInformation) {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectNoBarCodeNumberList(productOrderInformation);
        return getDataTable(list);
    }

    /**
     * 已入库 但没有条码的订单 update
     */
    @RequiresPermissions("backstage:warehouse:noBarCodeNumberUpdate")
    @Log(title = "已入库订单-生成条码-并出库", businessType = BusinessType.UPDATE)
    @PostMapping("/noBarCodeNumberUpdate")
    @ResponseBody
    public AjaxResult noBarCodeNumberUpdate(String id) {
        int i = productOrderInformationService.noBarCodeNumberUpdate(id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * 导出仓库标签列表
     */
    @RequiresPermissions("backstage:warehouse:export")
    @Log(title = "仓库标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PWarehouse pWarehouse) {
        List<PWarehouse> list = pWarehouseService.selectPWarehouseList(pWarehouse);
        ExcelUtil<PWarehouse> util = new ExcelUtil<PWarehouse>(PWarehouse.class);
        return util.exportExcel(list, "仓库标签数据");
    }

    /**
     * 新增仓库标签
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存仓库标签
     */
    @RequiresPermissions("backstage:warehouse:add")
    @Log(title = "仓库标签", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PWarehouse pWarehouse) {
        return toAjax(pWarehouseService.insertPWarehouse(pWarehouse));
    }

    /**
     * 修改仓库标签
     */
    @RequiresPermissions("backstage:warehouse:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        PWarehouse pWarehouse = pWarehouseService.selectPWarehouseById(id);
        mmap.put("pWarehouse", pWarehouse);
        return prefix + "/edit";
    }

    /**
     * 修改保存仓库标签
     */
    @RequiresPermissions("backstage:warehouse:edit")
    @Log(title = "仓库标签", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PWarehouse pWarehouse) {
        return toAjax(pWarehouseService.updatePWarehouse(pWarehouse));
    }

    /**
     * 删除仓库标签
     */
    @RequiresPermissions("backstage:warehouse:remove")
    @Log(title = "仓库标签", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(pWarehouseService.deletePWarehouseByIds(ids));
    }
}
