package com.ruoyi.backstage.controller;

import java.util.List;

import com.ruoyi.backstage.domain.PWarehouse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.PWarehouseNumber;
import com.ruoyi.backstage.service.IPWarehouseNumberService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 仓库库位Controller
 * 
 * @author xin_xin
 * @date 2022-09-13
 */
@Controller
@RequestMapping("/backstage/warehouseNumber")
public class PWarehouseNumberController extends BaseController
{
    private String prefix = "backstage/warehouseNumber";

    @Autowired
    private IPWarehouseNumberService pWarehouseNumberService;

    @RequiresPermissions("backstage:warehouseNumber:view")
    @GetMapping()
    public String warehouseNumber()
    {
        return prefix + "/warehouseNumber";
    }

    /**
     * 查询仓库库位列表
     * 根据订单信息查询
     * 2022-09-27
     */
    @RequiresPermissions("backstage:warehouseNumber:locationList")
    @GetMapping("/locationList")
    @ResponseBody
    public TableDataInfo locationList(PWarehouseNumber pWarehouseNumber)
    {
        startPage();
        List<PWarehouseNumber> list = pWarehouseNumberService.selectLocationList(pWarehouseNumber);
        return getDataTable(list);
    }

    /**
     * 查询仓库库位列表
     */
    @RequiresPermissions("backstage:warehouseNumber:list")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo list(PWarehouseNumber pWarehouseNumber)
    {
        startPage();
        List<PWarehouseNumber> list = pWarehouseNumberService.selectPWarehouseNumberList(pWarehouseNumber);
        return getDataTable(list);
    }

    /**
     * 成品库位
     *
     * @return
     */
    @RequiresPermissions("backstage:warehouseNumber:location")
    @RequestMapping("/location")
    public String location() {
        return  prefix+"/location";
    }



    /**
     * 导出仓库库位列表
     */
    @RequiresPermissions("backstage:warehouseNumber:export")
    @Log(title = "仓库库位", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PWarehouseNumber pWarehouseNumber)
    {
        List<PWarehouseNumber> list = pWarehouseNumberService.selectPWarehouseNumberList(pWarehouseNumber);
        ExcelUtil<PWarehouseNumber> util = new ExcelUtil<PWarehouseNumber>(PWarehouseNumber.class);
        return util.exportExcel(list, "仓库库位数据");
    }

    /**
     * 新增仓库库位
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存仓库库位
     */
    @RequiresPermissions("backstage:warehouseNumber:add")
    @Log(title = "仓库库位", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PWarehouseNumber pWarehouseNumber)
    {
        return toAjax(pWarehouseNumberService.insertPWarehouseNumber(pWarehouseNumber));
    }

    /**
     * 修改仓库库位
     */
    @RequiresPermissions("backstage:warehouseNumber:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PWarehouseNumber pWarehouseNumber = pWarehouseNumberService.selectPWarehouseNumberById(id);
        mmap.put("pWarehouseNumber", pWarehouseNumber);
        return prefix + "/edit";
    }

    /**
     * 修改保存仓库库位
     */
    @RequiresPermissions("backstage:warehouseNumber:edit")
    @Log(title = "仓库库位", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PWarehouseNumber pWarehouseNumber)
    {
        return toAjax(pWarehouseNumberService.updatePWarehouseNumber(pWarehouseNumber));
    }

    /**
     * 删除仓库库位
     */
    @RequiresPermissions("backstage:warehouseNumber:remove")
    @Log(title = "仓库库位", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pWarehouseNumberService.deletePWarehouseNumberByIds(ids));
    }
}
