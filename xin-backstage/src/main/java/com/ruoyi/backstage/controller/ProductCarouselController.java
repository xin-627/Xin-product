package com.ruoyi.backstage.controller;

import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.utils.file.FileUploadUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.ProductCarousel;
import com.ruoyi.backstage.service.ProductCarouselService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 轮播图Controller
 * 
 * @author xin-xin
 * @date 2022-01-11
 */
@Controller
@RequestMapping("/backstage/carousel")
public class ProductCarouselController extends BaseController
{
    private String prefix = "backstage/carousel";

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ProductCarouselService productCarouselService;

    @RequiresPermissions("backstage:carousel:view")
    @GetMapping()
    public String carousel()
    {
        return prefix + "/carousel";
    }

    /**
     * 查询轮播图列表
     */
    @RequiresPermissions("backstage:carousel:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductCarousel productCarousel)
    {
        startPage();
        List<ProductCarousel> list = productCarouselService.selectProductCarouselList(productCarousel);
        return getDataTable(list);
    }

    /**
     * 导出轮播图列表
     */
    @RequiresPermissions("backstage:carousel:export")
    @Log(title = "轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductCarousel productCarousel)
    {
        List<ProductCarousel> list = productCarouselService.selectProductCarouselList(productCarousel);
        ExcelUtil<ProductCarousel> util = new ExcelUtil<ProductCarousel>(ProductCarousel.class);
        return util.exportExcel(list, "轮播图数据");
    }

    /**
     * 新增轮播图
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存轮播图
     */
    @RequiresPermissions("backstage:carousel:add")
    @Log(title = "轮播图", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductCarousel productCarousel)
    {
        return toAjax(productCarouselService.insertProductCarousel(productCarousel));
    }

    /**
     * 修改轮播图
     */
    @RequiresPermissions("backstage:carousel:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductCarousel productCarousel = productCarouselService.selectProductCarouselById(id);
        mmap.put("productCarousel", productCarousel);
        return prefix + "/edit";
    }

    /**
     * 修改保存轮播图
     */
    @RequiresPermissions("backstage:carousel:edit")
    @Log(title = "轮播图修改", businessType = BusinessType.UPDATE)
    @PostMapping("/editState")
    @ResponseBody
    public AjaxResult editSave(ProductCarousel productCarousel)
    {
        return toAjax(productCarouselService.updateProductCarousel(productCarousel));
    }


    /**
     * 轮播图上传
     *
     * @param file 文件
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-11-01 10:25:40
     */
    @RequiresPermissions("backstage:main:carouselUpload")
    @PostMapping("/carouselUpload")
    @ResponseBody
    public AjaxResult carouselUpload(MultipartFile file) throws Exception {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPathCarousel();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }


    /**
     * 删除轮播图
     */
    @RequiresPermissions("backstage:carousel:remove")
    @Log(title = "轮播图", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(productCarouselService.deleteProductCarouselByIds(ids));
    }


    /**
     * 2022-01-11
     * 轮播图展示
     **/
    @RequestMapping("/thumbnailPreview")
    @ResponseBody
    public void showImg(HttpServletRequest request, HttpServletResponse response) {
        productCarouselService.showImg(request,response);
    }

    /**
     * 2022-03-30
     * svg展示
     **/
    @RequestMapping("/analysisOfSVG")
    @ResponseBody
    public void analysisOfSVG(HttpServletRequest request, HttpServletResponse response) {
        productCarouselService.analysisOfSVG(request,response);
    }

    /**
     * 2022-01-12
     * 轮播图删除
     */
    @RequiresPermissions("backstage:carousel:deleteImg")
    @Log(title = "轮播图删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/deleteImg", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteImg(HttpServletRequest request) {
        int i = productCarouselService.deleteImg(request);
        if(i == 200){
            return AjaxResult.success("图片-删除成功!");
        }else if (i == 404){
            return AjaxResult.warn("图片不存在!!");
        }else{
            return AjaxResult.error("图片-删除失败!请联系管理员!!!");
        }
    }

}
