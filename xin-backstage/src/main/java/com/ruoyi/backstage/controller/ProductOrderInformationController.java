package com.ruoyi.backstage.controller;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.security.PermissionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.ProductOrderInformation;
import com.ruoyi.backstage.service.ProductOrderInformationService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * 订单管理Controller
 * 
 * @author xin-xin
 * @date 2022-02-16
 */
@Controller
@RequestMapping("/backstage/information")
public class ProductOrderInformationController extends BaseController {
    private String prefix = "backstage/order-management";

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ProductOrderInformationService productOrderInformationService;



    /**
     * 下单量排行
     */
    @PostMapping("/placeAnOrderNumList")
    @ResponseBody
    public TableDataInfo placeAnOrderNumList()
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.searchPlaceAnOrderNumList();
        return getDataTable(list);
    }

    /**
     * 销量前二十产品
     */
    @PostMapping("/salesVolumeList")
    @ResponseBody
    public TableDataInfo salesVolumeList(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.searchSalesVolumeList(productOrderInformation);
        return getDataTable(list);
    }

    /**
     * 产品占比
     */
    @PostMapping("/classifyList")
    @ResponseBody
    public TableDataInfo classifyList(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.searchClassifyList(productOrderInformation);
        return getDataTable(list);
    }


    /**
     * 数据统计
     * 2022-06-29
     */
    @PostMapping("/dataStatistics/{statusId}")
    @ResponseBody
    public JSONObject dataStatistics(@PathVariable("statusId") Long statusId)
    {
        ProductOrderInformation dataStatistics = productOrderInformationService.dataStatistics(statusId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data", dataStatistics);
        jsonObject.put("code", 200);
        jsonObject.put("msg", "查询成功");
        return jsonObject;
    }


    /**
     * 查询全部订单
     * @return
     * 2022-06-14
     */
    @RequiresPermissions("backstage:information:afterSalePage")
    @GetMapping("/afterSalePage")
    public String afterSalePage() { return prefix + "/after-sale/afterSalePage"; }


    /**
     * 查询全部订单管理列表
     */
    @RequiresPermissions("backstage:information:allOrderList")
    @PostMapping("/allOrderList")
    @ResponseBody
    public TableDataInfo allOrderList(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectAllOrdersList(productOrderInformation);
        return getDataTable(list);
    }


    /**
     * 客服 全部订单
     * @return
     * 2022-06-09
     */
    @RequiresPermissions("backstage:information:afterSaleAll")
    @GetMapping("/afterSaleAll")
    public String afterSaleAll() { return prefix + "/after-sale/afterSale-all"; }

    /**
     * 客服 订单
     * @return
     * 2022-06-09
     */
    @RequiresPermissions("backstage:information:afterSale")
    @GetMapping("/afterSale")
    public String afterSale() { return prefix + "/after-sale/afterSale"; }


    /**
     * 售后打印列表
     *
     * @param productOrderInformation 产品订单信息
     * @return {@link TableDataInfo }
     * @author xin_xin
     * @date 2022-10-20 10:06:20
     */
    @RequiresPermissions("backstage:information:afterSalePrint")
    @PostMapping("/afterSalePrint")
    @ResponseBody
    public TableDataInfo afterSalePrint(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectAfterSalesPrint(productOrderInformation);
        return getDataTable(list);
    }

    /**
     * 售后打印更新
     *
     * @param request 请求
     * @param id      id
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-11-14 13:58:48
     */
    @RequiresPermissions("backstage:information:afterSalePrint")
    @Log(title = "售后打印操作", businessType = BusinessType.UPDATE)
    @PostMapping("/afterSalesPrintUpdate")
    @ResponseBody
    public AjaxResult afterSalesPrintUpdate(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.check(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }

    /**
     * 客服 -客户订单详情-打印
     * 2022-06-09
     */
    @RequiresPermissions("backstage:information:afterSalePrint")
    @GetMapping("/afterSalePrint/{clientId}")
    public String afterSalePrint(@PathVariable("clientId") Long clientId, ModelMap mmap)
    {
        mmap.put("clientId", clientId);
        return  prefix+"/after-sale/afterSale-print";
    }


    /**
     * 售后 打印 部分
     *
     * @param clientId 客户机id
     * @param mmap
     * @return {@link String }
     * @author xin_xin
     * @date 2022-11-14 11:25:07
     */
    @RequiresPermissions("backstage:information:afterSalePrint")
    @GetMapping("/afterSalePrintPart/{clientId}")
    public String afterSalePrintPart(@PathVariable("clientId") Long clientId, ModelMap mmap)
    {
        mmap.put("clientId", clientId);
        return  prefix+"/after-sale/afterSale-print-part";
    }

    /**
     * 售后打印设置
     *
     * @param clientId 客户机id
     * @param mmap     mmap
     * @return {@link String }
     * @author xin_xin
     * @date 2022-11-14 11:25:32
     */
    @RequiresPermissions("backstage:information:afterSalePrint")
    @GetMapping("/afterSalePrintEdit/{clientId}")
    public String afterSalePrintEdit(@PathVariable("clientId") Long clientId, ModelMap mmap)
    {
        mmap.put("clientId", clientId);
        return  prefix+"/after-sale/afterSale-print-edit";
    }
    /**
     * 客服 -客户订单列表
     */
    @RequiresPermissions("backstage:information:afterSaleList")
    @PostMapping("/afterSaleList")
    @ResponseBody
    public TableDataInfo afterSaleList(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectAfterSaleList(productOrderInformation);
        return getDataTable(list);
    }


    
    /**
     * 全部订单 财务
     * @return
     */
    @RequiresPermissions("backstage:information:allOrdersCw")
    @GetMapping("/allOrdersCw/{storefrontType}")
    public String allOrdersCw(@PathVariable("storefrontType") Long storefrontType, ModelMap mmap)
    {
        mmap.put("storefrontType", storefrontType);
        return prefix + "/all-orders/allOrdersCw";
    }


    /**
     * Go 待审核
     * @return
     */
    @RequiresPermissions("backstage:information:unaudited")
    @RequestMapping("/unaudited/{storefrontType}")
    public String unaudited(@PathVariable("storefrontType") Long storefrontType, ModelMap mmap)
    {
        mmap.put("storefrontType", storefrontType);
        return  prefix+"/unaudited/unaudited";

    }

    /**
     * 待审核-维修单
     *
     * @param storefrontType 店面类型
     * @param mmap           mmap
     * @return {@link String }
     * @author xin_xin
     * @date 2022-11-18 14:39:18
     */
    @RequiresPermissions("backstage:information:unaudited")
    @RequestMapping("/unauditedPurchase/{storefrontType}")
    public String unauditedPurchase(@PathVariable("storefrontType") Long storefrontType, ModelMap mmap)
    {
        mmap.put("storefrontType", storefrontType);
        return  prefix+"/unaudited/unaudited-purchase";

    }

    /**
     * 查询订单管理列表
     */
    @RequiresPermissions("backstage:information:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectProductOrderInformationList(productOrderInformation);
        return getDataTable(list);
    }

    /**
     * 查询客户信息列表
     */
    @RequiresPermissions("backstage:information:customerGroup")
    @PostMapping("/customerGroup")
    @ResponseBody
    public TableDataInfo customerGroup(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectCustomerGroupList(productOrderInformation);
        return getDataTable(list);
    }
    /**
     * 客户订单详情
     */
    @RequiresPermissions("backstage:information:unauditedDetails")
    @GetMapping("/unauditedDetails/{clientId}")
    public String unauditedDetails(@PathVariable("clientId") Long clientId, ModelMap mmap)
    {
        mmap.put("clientId", clientId);
        return  prefix+"/unaudited/unaudited-details";
    }

    /**
     * 财务 -客户订单详情
     * 2022-06-06
     */
    @RequiresPermissions("backstage:information:allOrdersCwDetails")
    @GetMapping("/allOrdersCwDetails/{clientId}")
    public String allOrdersCwDetails(@PathVariable("clientId") Long clientId, ModelMap mmap)
    {
        mmap.put("clientId", clientId);
        return  prefix+"/all-orders/allOrdersCw-details";
    }


    /**
     * 订单财务审核通过
     */
    @RequiresPermissions("backstage:information:approve")
    @Log(title = "订单财务审核通过", businessType = BusinessType.UPDATE)
    @PostMapping("/approve")
    @ResponseBody
    public AjaxResult check(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.check(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * 订单财务审核拒绝
     */
    @RequiresPermissions("backstage:information:refuse")
    @Log(title = "订单财务审核不通过", businessType = BusinessType.UPDATE)
    @PostMapping("/refuse")
    @ResponseBody
    public AjaxResult refuse(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.check(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }



    /**
     * Go 待采购页面
     * @return
     */
    @RequiresPermissions("backstage:information:awaitPurchase")
    @RequestMapping("/awaitPurchase")
    public String awaitPurchase()
    {
        return  prefix+"/purchase/await-purchase";

    }

    /**
     * Go 维修单- 待采购页面
     * @return
     */
    @RequiresPermissions("backstage:information:maintainAwaitPurchase")
    @RequestMapping("/maintainAwaitPurchase")
    public String maintainAwaitPurchase()
    {
        return  prefix+"/purchase/maintain-await-purchase";

    }

    /**
     * 采购操作
     */
    @RequiresPermissions("backstage:information:purchasingDataUpdate")
    @Log(title = "订单采购", businessType = BusinessType.UPDATE)
    @PostMapping("/purchasingDataUpdate")
    @ResponseBody
    public AjaxResult purchasingDataUpdate(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.purchasingDataUpdate(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }

    /**
     * 采购操作
     */
    @RequiresPermissions("backstage:information:purchasingRefused")
    @Log(title = "订单采购退回", businessType = BusinessType.UPDATE)
    @PostMapping("/purchasingRefused")
    @ResponseBody
    public AjaxResult purchasingRefused(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.purchasingDataUpdate(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }

    /**
     * 采购操作-采购退回至财务
     */
    @RequiresPermissions("backstage:information:purchasingRefusedCw")
    @Log(title = "订单采购退回至财务", businessType = BusinessType.UPDATE)
    @PostMapping("/purchasingRefusedCw")
    @ResponseBody
    public AjaxResult purchasingRefusedCw(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.purchasingDataUpdate(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * 采购数据更新
     */
    @RequiresPermissions("backstage:information:purchaseUpdates")
    @Log(title = "采购数据更新", businessType = BusinessType.UPDATE)
    @PostMapping("/purchaseUpdates")
    @ResponseBody
    public AjaxResult purchaseUpdates(ProductOrderInformation productOrderInformation)
    {
        int i = productOrderInformationService.updateProductOrderInformation(productOrderInformation);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        }else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }



    /**
     * Go 已采购页面
     * @return
     */
    @RequiresPermissions("backstage:information:finishPurchase")
    @RequestMapping("/finishPurchase")
    public String finishPurchase()
    {
        return  prefix+"/purchase/finish-purchase";

    }

    /**
     * Go 维修-已采购页面
     * @return
     */
    @RequiresPermissions("backstage:information:maintainFinishPurchase")
    @RequestMapping("/maintainFinishPurchase")
    public String maintainFinishPurchase()
    {
        return  prefix+"/purchase/maintain-finish-purchase";

    }

    /**
     * 采购取消采购
     */
    @RequiresPermissions("backstage:information:cancel")
    @Log(title = "采购取消采购", businessType = BusinessType.UPDATE)
    @PostMapping("/cancel")
    @ResponseBody
    public AjaxResult cancel(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.check(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }

    /**
     * 仓库确认回厂
     */
    @RequiresPermissions("backstage:information:confirmArrive")
    @Log(title = "仓库确认回厂", businessType = BusinessType.UPDATE)
    @PostMapping("/confirmArrive")
    @ResponseBody
    public AjaxResult confirmArrive(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.check(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * Go 财务 成本核算 待审订单
     * @return
     */
    @RequiresPermissions("backstage:information:costAccounting")
    @RequestMapping("/costAccounting")
    public String costAccounting()
    {
        return  prefix+"/financial-audit/cost-accounting";
    }

    /**
     * Go 财务 成本核算 全部订单
     * @return
     */
    @RequiresPermissions("backstage:information:costAccountingAll")
    @RequestMapping("/costAccountingAll")
    public String costAccountingAll()
    {
        return  prefix+"/financial-audit/cost-accountingAll";
    }

    /**
     * 财务核对数据更新
     */
    @RequiresPermissions("backstage:information:financialCheckUpdates")
    @Log(title = "财务核对数据更新", businessType = BusinessType.UPDATE)
    @PostMapping("/financialCheckUpdates")
    @ResponseBody
    public AjaxResult financialCheckUpdates(ProductOrderInformation productOrderInformation)
    {
        int i = productOrderInformationService.financialCheckUpdates(productOrderInformation);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        }else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * 财务核算操作
     * 财务核算清除供应商数据
     * 2022-06-18
     */
    @RequiresPermissions("backstage:information:cCheckEmptyDataUpdate")
    @Log(title = "财务核算清除供应商数据", businessType = BusinessType.UPDATE)
    @PostMapping("/cCheckEmptyDataUpdate")
    @ResponseBody
    public AjaxResult cCheckEmptyDataUpdate(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.check(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }

    /**
     * 财务核算操作
     * 财务核算-撤销核对
     * 2022-06-29
     */
    @RequiresPermissions("backstage:information:cCheckRevocationUpdate")
    @Log(title = "财务核算-撤销核对", businessType = BusinessType.UPDATE)
    @PostMapping("/cCheckRevocationUpdate")
    @ResponseBody
    public AjaxResult cCheckRevocationUpdate(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.check(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }

    /**
     * 财务核算操作
     * 财务核算-批量核对完成
     * 2022-06-29
     */
    @RequiresPermissions("backstage:information:cCheckAccomplishUpdate")
    @Log(title = "财务核算-批量核对完成", businessType = BusinessType.UPDATE)
    @PostMapping("/cCheckAccomplishUpdate")
    @ResponseBody
    public AjaxResult cCheckAccomplishUpdate(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.check(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * 财务核算操作
     * 财务核算-清除核回厂时间
     * 2022-06-30
     */
    @RequiresPermissions("backstage:information:cCheckObliterateUpdate")
    @Log(title = "财务核算-清除核回厂时间", businessType = BusinessType.UPDATE)
    @PostMapping("/cCheckObliterateUpdate")
    @ResponseBody
    public AjaxResult cCheckObliterateUpdate(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.cCheckObliterateUpdate(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }

    /**
     * 仓库 订单确定出库
     */
    @RequiresPermissions("backstage:information:warehouseOutboundUpdate")
    @Log(title = "订单确定出库", businessType = BusinessType.UPDATE)
    @PostMapping("/warehouseOutboundUpdate")
    @ResponseBody
    public AjaxResult warehouseOutboundUpdate(HttpServletRequest request, String id)
    {
        int i = productOrderInformationService.check(request, id);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }

    /**
     * 仓库对订单操作
     */
    @RequiresPermissions("backstage:information:warehouseUpdate")
    @Log(title = "仓库对订单操作", businessType = BusinessType.UPDATE)
    @PostMapping("/warehouseUpdate")
    @ResponseBody
    public AjaxResult warehouseUpdate(ProductOrderInformation productOrderInformation)
    {
        int i = productOrderInformationService.warehouseUpdate(productOrderInformation);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * 全部订单
     * @return
     */
    @RequiresPermissions("backstage:information:allOrders")
    @GetMapping("allOrders")
    public String allOrders()
    {
        return prefix + "/all-orders/information";
    }

    /**
     * 导出订单管理列表
     */
    @RequiresPermissions("backstage:information:export")
    @Log(title = "订单管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductOrderInformation productOrderInformation)
    {
        List<ProductOrderInformation> list = productOrderInformationService.selectProductOrderInformationList(productOrderInformation);
        ExcelUtil<ProductOrderInformation> util = new ExcelUtil<ProductOrderInformation>(ProductOrderInformation.class);
        return util.exportExcel(list, "订单管理数据");
    }


    /**
     * 维修图片上传
     *
     * @param file 文件
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-10-28 09:18:47
     */
    @PostMapping("/maintainFilesUpload")
    @ResponseBody
    public AjaxResult maintainFilesUpload(MultipartFile file) throws Exception {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPathMaintain();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 维修单管理
     *
     * @return {@link String }
     * @author xin_xin
     * @date 2022-10-13 14:52:01
     */
    @RequiresPermissions("backstage:information:maintain")
    @GetMapping("maintain")
    public String maintain(ModelMap mmap)
    {
        String userName = (String) PermissionUtils.getPrincipalProperty("userName");
        mmap.put("printName",userName);
        return prefix + "/maintain/maintain";
    }

    /**
     * 拒绝订单数据
     *
     * @return {@link Long }
     * @author xin_xin
     * @date 2022-11-18 17:19:40
     * /backstage/information/reject
     */
    @RequiresPermissions("backstage:information:maintain")
    @PostMapping("reject")
    @ResponseBody
    public JSONObject reject() {
        JSONObject jsonObject = new JSONObject();
        ProductOrderInformation productOrderInformation = productOrderInformationService.selectMaintainReject();
        jsonObject.put("code", 0);
        jsonObject.put("msg", "查询成功");
        jsonObject.put("reject", productOrderInformation.getReject());
        return jsonObject;
    }
    /**
     * 维修单添加
     *
     * @return {@link String }
     * @author xin_xin
     * @date 2022-10-13 16:59:51
     */
    @RequiresPermissions("backstage:information:maintainAdd")
    @GetMapping("maintainAdd")
    public String maintainAdd()
    {
        return prefix + "/maintain/maintain-add";
    }

    /**
     * 维修单创建
     *
     * @param productOrderInformation 产品订单信息
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-10-13 14:12:23
     */
    @RequiresPermissions("backstage:information:insertMaintain")
    @Log(title = "维修单创建", businessType = BusinessType.INSERT)
    @PostMapping("/insertMaintain")
    @ResponseBody
    public AjaxResult insertMaintain(ProductOrderInformation productOrderInformation)
    {
        int i = productOrderInformationService.insertProductOrderInformation(productOrderInformation);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else if (i == -1) {
            return AjaxResult.error("未查到此订单编号的数据,请检查后重试,或切换编号类型!!!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * 维修单编辑
     *
     * @param id   id
     * @param mmap mmap
     * @return {@link String }
     * @author xin_xin
     * @date 2022-10-15 14:43:03
     */
    @RequiresPermissions("backstage:information:maintainEdit")
    @GetMapping("/maintainEdit/{id}")
    public String maintainEdit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductOrderInformation productOrderInformation = productOrderInformationService.selectProductOrderInformationById(id);
        mmap.put("productOrderInformation", productOrderInformation);
        return prefix + "/maintain/maintain-edit";
    }


    /**
     * 维修单修改
     *
     * @param productOrderInformation 产品订单信息
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-10-15 14:43:18
     */
    @RequiresPermissions("backstage:information:maintainEdit")
    @Log(title = "维修单修改", businessType = BusinessType.UPDATE)
    @PostMapping("/maintainUpdate")
    @ResponseBody
    public AjaxResult maintainUpdate(ProductOrderInformation productOrderInformation)
    {
        int i = productOrderInformationService.maintainUpdate(productOrderInformation);
        if (i > 0) {
            return AjaxResult.success("操作成功!");
        } else if (i == -1) {
            return AjaxResult.error("未查到此订单编号的数据,请检查后重试,或切换编号类型!!!");
        } else if (i == -2) {
            return AjaxResult.error("当前订单已流入其他工序,内容已不可更改!!!");
        } else {
            return AjaxResult.error("操作失败!请联系管理员!!!");
        }
    }


    /**
     * Go 可入库订单
     * @return
     */
    @RequiresPermissions("backstage:information:canBePutInStorage")
    @RequestMapping("/canBePutInStorage")
    public String CanBePutInStorage()
    {
        return  prefix+"/warehouse/canBePutInStorage";

    }

    /**
     * Go 在库订单
     * @return
     */
    @RequiresPermissions("backstage:information:inTheWarehouse")
    @RequestMapping("/inTheWarehouse")
    public String inTheWarehouse()
    {
        return  prefix+"/warehouse/inTheWarehouse";

    }

    /**
     * Go 已出库订单
     * @return
     */
    @RequiresPermissions("backstage:information:haveOutbound")
    @RequestMapping("/haveOutbound")
    public String haveOutbound()
    {
        return  prefix+"/warehouse/haveOutbound";

    }

    /**
     * 删除
     *
     * @param ids id
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-11-18 15:56:06
     */
    @RequiresPermissions("backstage:information:remove")
    @Log(title = "删除订单数据", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(productOrderInformationService.deleteProductOrderInformationByIds(ids));
    }

}
