package com.ruoyi.backstage.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.PUpdateLog;
import com.ruoyi.backstage.service.IPUpdateLogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 更新日志Controller
 * 
 * @author xin_xin
 * @date 2022-05-13
 */
@Controller
@RequestMapping("/backstage/updateLog")
public class PUpdateLogController extends BaseController
{
    private String prefix = "backstage/updateLog";

    @Autowired
    private IPUpdateLogService pUpdateLogService;

    @RequiresPermissions("backstage:updateLog:view")
    @GetMapping()
    public String updateLog()
    {
        return prefix + "/updateLog";
    }

    /**
     * 查询更新日志列表
     */
    @RequiresPermissions("backstage:updateLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PUpdateLog pUpdateLog)
    {
        startPage();
        List<PUpdateLog> list = pUpdateLogService.selectPUpdateLogList(pUpdateLog);
        return getDataTable(list);
    }

    /**
     * 导出更新日志列表
     */
    @RequiresPermissions("backstage:updateLog:export")
    @Log(title = "更新日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PUpdateLog pUpdateLog)
    {
        List<PUpdateLog> list = pUpdateLogService.selectPUpdateLogList(pUpdateLog);
        ExcelUtil<PUpdateLog> util = new ExcelUtil<PUpdateLog>(PUpdateLog.class);
        return util.exportExcel(list, "更新日志数据");
    }

    /**
     * 新增更新日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存更新日志
     */
    @RequiresPermissions("backstage:updateLog:add")
    @Log(title = "更新日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PUpdateLog pUpdateLog)
    {
        return toAjax(pUpdateLogService.insertPUpdateLog(pUpdateLog));
    }

    /**
     * 修改更新日志
     */
    @RequiresPermissions("backstage:updateLog:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PUpdateLog pUpdateLog = pUpdateLogService.selectPUpdateLogById(id);
        mmap.put("pUpdateLog", pUpdateLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存更新日志
     */
    @RequiresPermissions("backstage:updateLog:edit")
    @Log(title = "更新日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PUpdateLog pUpdateLog)
    {
        return toAjax(pUpdateLogService.updatePUpdateLog(pUpdateLog));
    }

    /**
     * 删除更新日志
     */
    @RequiresPermissions("backstage:updateLog:remove")
    @Log(title = "更新日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pUpdateLogService.deletePUpdateLogByIds(ids));
    }
}
