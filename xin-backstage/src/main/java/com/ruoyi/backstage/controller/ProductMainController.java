package com.ruoyi.backstage.controller;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.utils.file.FileUploadUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.ProductMain;
import com.ruoyi.backstage.service.ProductMainService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 产品管理Controller
 *
 * @author xin-xin
 * @date 2021-12-30
 */
@Controller
@RequestMapping("/backstage/main")
public class ProductMainController extends BaseController {
    private String prefix = "backstage/management";

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ProductMainService productMainService;

    @RequiresPermissions("backstage:main:view")
    @GetMapping()
    public String main() {
        return prefix + "/main";
    }

    /**
     * 色号规格
     * @return
     */
    @RequiresPermissions("backstage:main:psview")
    @GetMapping("psmain")
    public String psmain() {
        return prefix + "/psmain";
    }

    /**
     * 查询产品管理列表
     */
    @RequiresPermissions("backstage:main:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductMain productMain) {
        startPage();
        List<ProductMain> list = productMainService.selectProductMainList(productMain);
        return getDataTable(list);
    }

    /**
     * 2022-07-11
     * 查询编号是否重复
     */
    @RequiresPermissions("backstage:main:add")
    @RequestMapping(value = "/yxNumberSearch", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult yxNumberSearch(String yxNumber) {
        int i = productMainService.yxNumberSearch(yxNumber);
        if(i > 0){
            return AjaxResult.error("当前编号已存在");
        }else{
            return AjaxResult.success("当前编号无重复");
        }
    }

    /**
     * 导出产品管理列表
     */
    @RequiresPermissions("backstage:main:export")
    @Log(title = "产品管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductMain productMain) {
        List<ProductMain> list = productMainService.selectProductMainList(productMain);
        ExcelUtil<ProductMain> util = new ExcelUtil<ProductMain>(ProductMain.class);
        return util.exportExcel(list, "产品管理数据");
    }

    /**
     * 新增产品管理
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 图片裁剪
     * @return
     */
    @GetMapping("/trimImg")
    public String trimImg() {
        return prefix + "/trimImg";
    }

    /**
     * 成品缩略图片上传
     */
    @RequiresPermissions("backstage:main:imagesUpload")
    @PostMapping("/pathThumbnailUpload")
    @ResponseBody
    public AjaxResult pathThumbnailUpload(MultipartFile file) throws Exception {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPathThumbnail();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 成品清晰图片上传
     */
    @RequiresPermissions("backstage:main:imagesUpload")
    @PostMapping("/clearImagesUpload")
    @ResponseBody
    public AjaxResult clearImagesUpload(MultipartFile file) throws Exception {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPathClearImages();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 其他图片上传
     *
     * @param file 文件
     * @return {@link JSONObject }
     * @author xin_xin
     * @date 2022-11-02 19:46:09
     */
    @RequiresPermissions("backstage:main:imagesUpload")
    @PostMapping("/otherImagesUpload")
    @ResponseBody
    public JSONObject otherImagesUpload(MultipartFile file) throws Exception {
        JSONObject jsonObject = new JSONObject();
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPathOtherImages();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            jsonObject.put("location", url);
            return jsonObject;
        }
        catch (Exception e)
        {
            jsonObject.put("上传错误", "500");
            return jsonObject;
        }
    }


    /**
     * 产品数据添加
     * 2022-01-04
     * @param productMain
     * @return
     */
    @RequiresPermissions("backstage:main:add")
    @Log(title = "产品添加", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductMain productMain) {
        int i = productMainService.mainAdd(productMain);
        if (i > 0) {
            return AjaxResult.success("添加成功!");
        } else if (i == -1) {
            return AjaxResult.warn("当前 一新编号 数据已存在!请谨慎操作!!!");
        } else {
            return AjaxResult.error("添加失败!请联系管理员!!!");
        }
    }


    /**
     * 修改产品管理
     */
    @RequiresPermissions("backstage:main:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        ProductMain productMain = productMainService.selectProductMainById(id);
        mmap.put("productMain", productMain);
        return prefix + "/edit";
    }

    /**
     * 产品细节内容
     *
     * @param id   id
     * @param mmap mmap
     * @return {@link String }
     * @author xin_xin
     * @date 2022-11-01 15:52:20
     */
    @RequiresPermissions("backstage:main:details")
    @GetMapping("/details/{id}")
    public String details(@PathVariable("id") Long id, ModelMap mmap) {
        ProductMain productMain = productMainService.selectProductMainById(id);
        mmap.put("productMain", productMain);
        return prefix + "/details";
    }

    /**
     * 更新产品信息
     *
     * @param productMain 产品主要
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-10-27 19:49:24
     */
    @RequiresPermissions("backstage:main:edit")
    @Log(title = "产品编辑", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/updateProductMain")
    @ResponseBody
    public AjaxResult updateProductMain(ProductMain productMain) {
        return toAjax(productMainService.updateProductMain(productMain));
    }

    /**
     * 2022-01-04
     * 图片删除
     */
    @RequiresPermissions("backstage:main:deleteImg")
    @Log(title = "删除产品图片", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/deleteImg", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteImg(HttpServletRequest request) {
        int i = productMainService.deleteImg(request);
        if(i == 200){
            return AjaxResult.success("图片-删除成功!");
        }else if (i == 404){
            return AjaxResult.warn("删除失败,图片不存在!!!");
        }else{
            return AjaxResult.error("图片-删除失败,未知错误!请联系管理员!!!");
        }
    }


    /**
     * 删除产品管理
     */
    @RequiresPermissions("backstage:main:remove")
    @Log(title = "产品删除", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(productMainService.deleteProductMainByIds(ids));
    }


    /**
     * 图片显示
     *
     * @param request  请求
     * @param response 响应
     * @author xin_xin
     * @date 2022-11-07 10:44:53
     */
    @RequestMapping("/pictureDisplay")
    @ResponseBody
    public void pictureDisplay(HttpServletRequest request, HttpServletResponse response) {
        productMainService.pictureDisplay(request,response);
    }

}
