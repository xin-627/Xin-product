package com.ruoyi.backstage.controller;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.PRegister;
import com.ruoyi.backstage.service.IPRegisterService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 注册申请Controller
 * 
 * @author xin_xin
 * @date 2022-05-13
 */
@Controller
@RequestMapping("/backstage/register")
public class PRegisterController extends BaseController
{
    private String prefix = "backstage/register";

    @Autowired
    private IPRegisterService pRegisterService;

    @RequiresPermissions("backstage:register:view")
    @GetMapping()
    public String register()
    {
        return prefix + "/register";
    }

    /**
     * 查询注册申请列表
     */
    @RequiresPermissions("backstage:register:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PRegister pRegister)
    {
        startPage();
        List<PRegister> list = pRegisterService.selectPRegisterList(pRegister);
        return getDataTable(list);
    }

    /**
     * 导出注册申请列表
     */
    @RequiresPermissions("backstage:register:export")
    @Log(title = "注册申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PRegister pRegister)
    {
        List<PRegister> list = pRegisterService.selectPRegisterList(pRegister);
        ExcelUtil<PRegister> util = new ExcelUtil<PRegister>(PRegister.class);
        return util.exportExcel(list, "注册申请数据");
    }

    /**
     * 新增注册申请
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存注册申请
     */
    @RequiresPermissions("backstage:register:add")
    @Log(title = "注册申请", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PRegister pRegister)
    {
        return toAjax(pRegisterService.insertPRegister(pRegister));
    }

    /**
     * 修改注册申请
     */
    @RequiresPermissions("backstage:register:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PRegister pRegister = pRegisterService.selectPRegisterById(id);
        mmap.put("pRegister", pRegister);
        return prefix + "/edit";
    }

    /**
     * 修改保存注册申请
     */
    @RequiresPermissions("backstage:register:edit")
    @Log(title = "注册申请", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PRegister pRegister)
    {
        return toAjax(pRegisterService.updatePRegister(pRegister));
    }

    /**
     * 删除注册申请
     */
    @RequiresPermissions("backstage:register:remove")
    @Log(title = "注册申请", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pRegisterService.deletePRegisterByIds(ids));
    }

    /**
     * 校验手机号码
     */
    /*@PostMapping("/checkPhoneUnique")
    @ResponseBody
    public String checkPhoneUnique(PRegister pRegister)
    {
        return pRegisterService.checkPhoneUnique(pRegister);
    }*/

}
