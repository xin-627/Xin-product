package com.ruoyi.backstage.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.PWarehouseCacheFile;
import com.ruoyi.backstage.service.IPWarehouseCacheFileService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库位缓存Controller
 * 
 * @author xin_xin
 * @date 2022-09-16
 */
@Controller
@RequestMapping("/backstage/warehouseCacheFile")
public class PWarehouseCacheFileController extends BaseController
{
    private String prefix = "backstage/warehouseCacheFile";

    @Autowired
    private IPWarehouseCacheFileService pWarehouseCacheFileService;

    @RequiresPermissions("backstage:warehouseCacheFile:view")
    @GetMapping()
    public String warehouseCacheFile()
    {
        return prefix + "/warehouseCacheFile";
    }

    /**
     * 查询库位缓存列表
     */
    @RequiresPermissions("backstage:warehouseCacheFile:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PWarehouseCacheFile pWarehouseCacheFile)
    {
        startPage();
        List<PWarehouseCacheFile> list = pWarehouseCacheFileService.selectPWarehouseCacheFileList(pWarehouseCacheFile);
        return getDataTable(list);
    }

    /**
     * 导出库位缓存列表
     */
    @RequiresPermissions("backstage:warehouseCacheFile:export")
    @Log(title = "库位缓存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PWarehouseCacheFile pWarehouseCacheFile)
    {
        List<PWarehouseCacheFile> list = pWarehouseCacheFileService.selectPWarehouseCacheFileList(pWarehouseCacheFile);
        ExcelUtil<PWarehouseCacheFile> util = new ExcelUtil<PWarehouseCacheFile>(PWarehouseCacheFile.class);
        return util.exportExcel(list, "库位缓存数据");
    }

    /**
     * 新增库位缓存
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存库位缓存
     */
    @RequiresPermissions("backstage:warehouseCacheFile:add")
    @Log(title = "库位缓存", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PWarehouseCacheFile pWarehouseCacheFile)
    {
        return toAjax(pWarehouseCacheFileService.insertPWarehouseCacheFile(pWarehouseCacheFile));
    }

    /**
     * 修改库位缓存
     */
    @RequiresPermissions("backstage:warehouseCacheFile:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PWarehouseCacheFile pWarehouseCacheFile = pWarehouseCacheFileService.selectPWarehouseCacheFileById(id);
        mmap.put("pWarehouseCacheFile", pWarehouseCacheFile);
        return prefix + "/edit";
    }

    /**
     * 修改保存库位缓存
     */
    @RequiresPermissions("backstage:warehouseCacheFile:edit")
    @Log(title = "库位缓存", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PWarehouseCacheFile pWarehouseCacheFile)
    {
        return toAjax(pWarehouseCacheFileService.updatePWarehouseCacheFile(pWarehouseCacheFile));
    }

    /**
     * 删除库位缓存
     */
    @RequiresPermissions("backstage:warehouseCacheFile:remove")
    @Log(title = "库位缓存", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pWarehouseCacheFileService.deletePWarehouseCacheFileByIds(ids));
    }
}
