package com.ruoyi.backstage.controller;

import java.util.List;

import com.ruoyi.backstage.domain.ProductCarousel;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.utils.file.FileUploadUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.PLogo;
import com.ruoyi.backstage.service.IPLogoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * logoController
 * 
 * @author xin-xin
 * @date 2022-06-08
 */
@Controller
@RequestMapping("/backstage/logo")
public class PLogoController extends BaseController
{
    private String prefix = "backstage/logo";

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private IPLogoService pLogoService;

    @RequiresPermissions("backstage:logo:view")
    @GetMapping()
    public String logo()
    {
        return prefix + "/logo";
    }

    /**
     * 查询logo列表
     */
    @RequiresPermissions("backstage:logo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PLogo pLogo)
    {
        startPage();
        List<PLogo> list = pLogoService.selectPLogoList(pLogo);
        return getDataTable(list);
    }

    /**
     * 导出logo列表
     */
    @RequiresPermissions("backstage:logo:export")
    @Log(title = "logo", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PLogo pLogo)
    {
        List<PLogo> list = pLogoService.selectPLogoList(pLogo);
        ExcelUtil<PLogo> util = new ExcelUtil<PLogo>(PLogo.class);
        return util.exportExcel(list, "logo数据");
    }

    /**
     * 新增logo
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存logo
     */
    @RequiresPermissions("backstage:logo:add")
    @Log(title = "logo", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PLogo pLogo)
    {
        return toAjax(pLogoService.insertPLogo(pLogo));
    }

    /**
     * 修改logo
     */
    @RequiresPermissions("backstage:logo:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PLogo pLogo = pLogoService.selectPLogoById(id);
        mmap.put("pLogo", pLogo);
        return prefix + "/edit";
    }


    /**
     * 修改保存logo
     */
    @RequiresPermissions("backstage:logo:edit")
    @Log(title = "logo修改", businessType = BusinessType.UPDATE)
    @PostMapping("/editState")
    @ResponseBody
    public AjaxResult editSave(PLogo pLogo)
    {
        return toAjax(pLogoService.updatePLogo(pLogo));
    }



    /**
     * 删除logo
     */
    @RequiresPermissions("backstage:logo:remove")
    @Log(title = "logo-删除", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pLogoService.deletePLogoByIds(ids));
    }



    /**
     * logo图上传
     *
     * @param file 文件
     * @return {@link AjaxResult }
     * @author xin_xin
     * @date 2022-11-01 14:11:21
     */
    @RequiresPermissions("backstage:logo:logoImgUpload")
    @PostMapping("/logoImgUpload")
    @ResponseBody
    public AjaxResult logoImgUpload(MultipartFile file) throws Exception {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPathLogoImg();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 2022-06-09
     * logo图删除
     */
    @RequiresPermissions("backstage:logo:deleteImg")
    @Log(title = "logo图删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/deleteImg", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteImg(HttpServletRequest request) {
        int i = pLogoService.deleteImg(request);
        if(i == 200){
            return AjaxResult.success("图片-删除成功!");
        }else if (i == 404){
            return AjaxResult.warn("图片不存在!!");
        }else{
            return AjaxResult.error("图片-删除失败!请联系管理员!!!");
        }
    }

    /**
     * 图片显示  logo
     * @param request  请求
     * @param response 响应
     * @author xin_xin
     * @date 2022-11-05 15:03:18
     */

    @RequestMapping("/pictureDisplayLogo")
    @ResponseBody
    public void pictureDisplayLogo(HttpServletRequest request, HttpServletResponse response) {
        pLogoService.pictureDisplayLogo(request, response);
    }
}
