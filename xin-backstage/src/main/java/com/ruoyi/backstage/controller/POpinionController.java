package com.ruoyi.backstage.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.POpinion;
import com.ruoyi.backstage.service.IPOpinionService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 意见与建议Controller
 * 
 * @author xin_xin
 * @date 2022-05-13
 */
@Controller
@RequestMapping("/backstage/opinion")
public class POpinionController extends BaseController
{
    private String prefix = "backstage/opinion";

    @Autowired
    private IPOpinionService pOpinionService;

    @RequiresPermissions("backstage:opinion:view")
    @GetMapping()
    public String opinion()
    {
        return prefix + "/opinion";
    }

    /**
     * 查询意见与建议列表
     */
    @RequiresPermissions("backstage:opinion:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(POpinion pOpinion)
    {
        startPage();
        List<POpinion> list = pOpinionService.selectPOpinionList(pOpinion);
        return getDataTable(list);
    }

    /**
     * 导出意见与建议列表
     */
    @RequiresPermissions("backstage:opinion:export")
    @Log(title = "意见与建议", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(POpinion pOpinion)
    {
        List<POpinion> list = pOpinionService.selectPOpinionList(pOpinion);
        ExcelUtil<POpinion> util = new ExcelUtil<POpinion>(POpinion.class);
        return util.exportExcel(list, "意见与建议数据");
    }

    /**
     * 新增意见与建议
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("userName", getSysUser().getUserName());
        return prefix + "/add";
    }

    /**
     * 新增保存意见与建议
     */
    @RequiresPermissions("backstage:opinion:add")
    @Log(title = "意见与建议", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(POpinion pOpinion)
    {
        return toAjax(pOpinionService.insertPOpinion(pOpinion));
    }

    /**
     * 修改意见与建议
     */
    @RequiresPermissions("backstage:opinion:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        POpinion pOpinion = pOpinionService.selectPOpinionById(id);
        mmap.put("pOpinion", pOpinion);
        return prefix + "/edit";
    }

    /**
     * 修改保存意见与建议
     */
    @RequiresPermissions("backstage:opinion:edit")
    @Log(title = "意见与建议", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(POpinion pOpinion)
    {
        return toAjax(pOpinionService.updatePOpinion(pOpinion));
    }

    /**
     * 删除意见与建议
     */
    @RequiresPermissions("backstage:opinion:remove")
    @Log(title = "意见与建议", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pOpinionService.deletePOpinionByIds(ids));
    }
}
