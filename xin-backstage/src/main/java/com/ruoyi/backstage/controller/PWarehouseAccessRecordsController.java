package com.ruoyi.backstage.controller;

import java.util.List;

import com.ruoyi.backstage.domain.ProductOrderInformation;
import com.ruoyi.backstage.service.ProductOrderInformationService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.backstage.domain.PWarehouseAccessRecords;
import com.ruoyi.backstage.service.IPWarehouseAccessRecordsService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 仓库出入库记录Controller
 * 
 * @author xin_xin
 * @date 2022-09-16
 */
@Controller
@RequestMapping("/backstage/warehouseAccessRecords")
public class PWarehouseAccessRecordsController extends BaseController
{
    private String prefix = "backstage/warehouseAccessRecords";

    @Autowired
    private IPWarehouseAccessRecordsService pWarehouseAccessRecordsService;

    @Autowired
    private ProductOrderInformationService productOrderInformationService;

    @RequiresPermissions("backstage:warehouseAccessRecords:view")
    @GetMapping()
    public String warehouseAccessRecords()
    {
        return prefix + "/warehouseAccessRecords";
    }



    /**
     * 查询仓库出入库记录列表
     */
    @RequiresPermissions("backstage:warehouseAccessRecords:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductOrderInformation productOrderInformation)
    {
        startPage();
        List<ProductOrderInformation> list = productOrderInformationService.selectAccessRecordsList(productOrderInformation);
        return getDataTable(list);
    }


    /**
     * 导出仓库出入库记录列表
     */
    @RequiresPermissions("backstage:warehouseAccessRecords:export")
    @Log(title = "仓库出入库记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PWarehouseAccessRecords pWarehouseAccessRecords)
    {
        List<PWarehouseAccessRecords> list = pWarehouseAccessRecordsService.selectPWarehouseAccessRecordsList(pWarehouseAccessRecords);
        ExcelUtil<PWarehouseAccessRecords> util = new ExcelUtil<PWarehouseAccessRecords>(PWarehouseAccessRecords.class);
        return util.exportExcel(list, "仓库出入库记录数据");
    }

    /**
     * 新增仓库出入库记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存仓库出入库记录
     */
    @RequiresPermissions("backstage:warehouseAccessRecords:add")
    @Log(title = "仓库出入库记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PWarehouseAccessRecords pWarehouseAccessRecords)
    {
        return toAjax(pWarehouseAccessRecordsService.insertPWarehouseAccessRecords(pWarehouseAccessRecords));
    }

    /**
     * 修改仓库出入库记录
     */
    @RequiresPermissions("backstage:warehouseAccessRecords:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PWarehouseAccessRecords pWarehouseAccessRecords = pWarehouseAccessRecordsService.selectPWarehouseAccessRecordsById(id);
        mmap.put("pWarehouseAccessRecords", pWarehouseAccessRecords);
        return prefix + "/edit";
    }

    /**
     * 修改保存仓库出入库记录
     */
    @RequiresPermissions("backstage:warehouseAccessRecords:edit")
    @Log(title = "仓库出入库记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PWarehouseAccessRecords pWarehouseAccessRecords)
    {
        return toAjax(pWarehouseAccessRecordsService.updatePWarehouseAccessRecords(pWarehouseAccessRecords));
    }

    /**
     * 删除仓库出入库记录
     */
    @RequiresPermissions("backstage:warehouseAccessRecords:remove")
    @Log(title = "仓库出入库记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pWarehouseAccessRecordsService.deletePWarehouseAccessRecordsByIds(ids));
    }
}
