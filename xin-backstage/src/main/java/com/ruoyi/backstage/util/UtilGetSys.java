package com.ruoyi.backstage.util;

import com.ruoyi.common.utils.security.PermissionUtils;

public class UtilGetSys {

    // 获取当前的用户名称
    public static String getUserName() {
        return (String) PermissionUtils.getPrincipalProperty("userName");
    }


    /**
     * 获取文件地址
     *
     * @return
     */
    public static String getPath() {
        return (String) "D:\\YiXin-service\\SystemFolder\\Xin-product\\YixinProductSystem";
    }

}
