package com.ruoyi.backstage.util;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * author xin-xin
 * 2022-02-10
 */
public class UtilDate {


    /**
     * 获取现在时间
     *
     * @return返回字符串格式 yyyy-MM-dd HH:mm:ss
     */
    public static String getStringDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    /**
     * 获取现在时间
     *
     * @return 返回短时间字符串格式yyyy-MM-dd
     */
    public static String getStringDateShort() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    /**
     * 获取时间 小时:分;秒 HH:mm:ss
     *
     * @return
     */
    public static String getTimeShort() {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date currentTime = new Date();
        String dateString = formatter.format(currentTime);
        return dateString;
    }


    /**
     * Sring格式转Date格式
     * @return
     */
    public static Date switchingTime(String time) {
        Date releaseDate = null;
        if (StringUtils.isNotEmpty(time)) {//判断为空
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                releaseDate = formatter.parse(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return releaseDate;
    }
    /**
     * Sring格式转Date格式
     */
    public static Date switchingTimeShort(String time) {
        Date releaseDate = null;
        if (StringUtils.isNotEmpty(time)) {//判断为空
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                releaseDate = formatter.parse(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return releaseDate;
    }
}
