package com.ruoyi.backstage.util;

import com.ruoyi.backstage.domain.ProductClassify;

import java.util.ArrayList;
import java.util.List;

public class TreeUtilClassification {
    /**
     * 后端产品管理  分类树
     * 2022-01-11
     * @param treeList
     * @param pid
     * @return
     */
    public static List<ProductClassify> toTree(List<ProductClassify> treeList, Long pid) {
        List<ProductClassify> retList = new ArrayList<>();
        for (ProductClassify parent : treeList) {
            if (pid.equals(parent.getPid())) {
                retList.add(findChildren(parent, treeList));
            }
        }
        return retList;
    }

    private static ProductClassify findChildren(ProductClassify parent, List<ProductClassify> treeList) {
        for (ProductClassify child : treeList) {
            if (parent.getId().equals(child.getPid())) {
                if (parent.getChildren() == null) {
                    parent.setChildren(new ArrayList<>());
                }
                parent.getChildren().add(findChildren(child, treeList));
            }
        }
        return parent;
    }
}
