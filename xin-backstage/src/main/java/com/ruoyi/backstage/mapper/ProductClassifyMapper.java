package com.ruoyi.backstage.mapper;

import java.util.List;
import com.ruoyi.backstage.domain.ProductClassify;
import org.springframework.stereotype.Repository;

/**
 * 分类菜单/分类管理Mapper接口
 * 
 * @author xin-xin
 * @date 2022-01-11
 */
@Repository
public interface ProductClassifyMapper 
{
    /**
     * 查询分类菜单/分类管理
     * 
     * @param id 分类菜单/分类管理主键
     * @return 分类菜单/分类管理
     */
    public ProductClassify selectProductClassifyById(Long id);

    /**
     * 查询分类菜单/分类管理列表
     * 
     * @param productClassify 分类菜单/分类管理
     * @return 分类菜单/分类管理集合
     */
    public List<ProductClassify> selectProductClassifyList(ProductClassify productClassify);

    /**
     * 新增分类菜单/分类管理
     * 
     * @param productClassify 分类菜单/分类管理
     * @return 结果
     */
    public int insertProductClassify(ProductClassify productClassify);

    /**
     * 修改分类菜单/分类管理
     * 
     * @param productClassify 分类菜单/分类管理
     * @return 结果
     */
    public int updateProductClassify(ProductClassify productClassify);

    /**
     * 删除分类菜单/分类管理
     * 
     * @param id 分类菜单/分类管理主键
     * @return 结果
     */
    public int deleteProductClassifyById(Long id);

    /**
     * 批量删除分类菜单/分类管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductClassifyByIds(String[] ids);
}
