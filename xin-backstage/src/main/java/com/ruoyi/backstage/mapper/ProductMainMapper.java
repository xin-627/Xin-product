package com.ruoyi.backstage.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.backstage.domain.ProductMain;
import org.springframework.stereotype.Repository;

/**
 * 产品管理Mapper接口
 * 
 * @author xin-xin
 * @date 2022-01-08
 */
@Repository
public interface ProductMainMapper 
{
    /**
     * 查询产品管理
     * 
     * @param id 产品管理主键
     * @return 产品管理
     */
    public ProductMain selectProductMainById(Long id);

    /**
     * 查询产品管理列表
     * 
     * @param productMain 产品管理
     * @return 产品管理集合
     */
    public List<ProductMain> selectProductMainList(ProductMain productMain);

    /**
     * 前端查询产品列数据
     * @param productMain
     * @return
     */
    public List<ProductMain> leadingProductMainList(ProductMain productMain);

    /**
     * 新增产品管理
     * 
     * @param productMain 产品管理
     * @return 结果
     */
    public int insertProductMain(ProductMain productMain);

    /**
     * 修改产品管理
     * 
     * @param productMain 产品管理
     * @return 结果
     */
    public int updateProductMain(ProductMain productMain);


    /**
     销量更新
     2022-06-23
     财务审核通过时更新
     */
    public int salesVolumeUpdate();

    /**
     * 删除产品管理
     * 
     * @param id 产品管理主键
     * @return 结果
     */
    public int deleteProductMainById(Long id);

    /**
     * 批量删除产品管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductMainByIds(String[] ids);

    /**
     * 编号查重
     * @param map
     * @return
     */
    public List<ProductMain> yxNumberSearch(Map map);


}
