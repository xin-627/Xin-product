package com.ruoyi.backstage.mapper;

import java.util.List;
import com.ruoyi.backstage.domain.PLogo;
import org.springframework.stereotype.Repository;

/**
 * logoMapper接口
 * 
 * @author xin-xin
 * @date 2022-06-08
 */
@Repository
public interface PLogoMapper 
{
    /**
     * 查询logo
     * 
     * @param id logo主键
     * @return logo
     */
    public PLogo selectPLogoById(Long id);

    /**
     * 查询logo列表
     * 
     * @param pLogo logo
     * @return logo集合
     */
    public List<PLogo> selectPLogoList(PLogo pLogo);

    /**
     * 新增logo
     * 
     * @param pLogo logo
     * @return 结果
     */
    public int insertPLogo(PLogo pLogo);

    /**
     * 修改logo
     * 
     * @param pLogo logo
     * @return 结果
     */
    public int updatePLogo(PLogo pLogo);

    /**
     * 删除logo
     * 
     * @param id logo主键
     * @return 结果
     */
    public int deletePLogoById(Long id);

    /**
     * 批量删除logo
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePLogoByIds(String[] ids);
}
