package com.ruoyi.backstage.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.backstage.domain.ProductMain;
import com.ruoyi.backstage.domain.ProductOrderInformation;
import org.springframework.stereotype.Repository;

/**
 * 订单管理Mapper接口
 * 
 * @author xin-xin
 * @date 2022-02-16
 */
@Repository
public interface ProductOrderInformationMapper 
{
    /**
     * 查询订单管理
     * 
     * @param id 订单管理主键
     * @return 订单管理
     */
    public ProductOrderInformation selectProductOrderInformationById(Long id);

    /**
     * 查询订单管理列表
     * 
     * @param productOrderInformation 订单管理
     * @return 订单管理集合
     */
    public List<ProductOrderInformation> selectProductOrderInformationList(ProductOrderInformation productOrderInformation);

    /**
     * 新增订单管理
     * 
     * @param productOrderInformation 订单管理
     * @return 结果
     */
    public int insertProductOrderInformation(ProductOrderInformation productOrderInformation);

    /**
     * 修改订单管理
     * 
     * @param productOrderInformation 订单管理
     * @return 结果
     */
    public int updateProductOrderInformation(ProductOrderInformation productOrderInformation);

    /**
     * 删除订单管理
     * 
     * @param id 订单管理主键
     * @return 结果
     */
    public int deleteProductOrderInformationById(Long id);

    /**
     * 批量删除订单管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductOrderInformationByIds(String[] ids);

    /**
     * 订单批量操作
     * @param productOrderInformation
     * @return
     */
    public int orderReview(ProductOrderInformation productOrderInformation);

    /**
     * 订单批量操作 清除核回厂时间
     * @param productOrderInformation
     * @return
     */
    public int cCheckObliterateUpdate(ProductOrderInformation productOrderInformation);

    /**
     * 更新 全部 计划到货时间 需要回厂时间
     * @return
     */
    public  int purchasingDataUpdate();

    /**
     * 驳回原因 数据查询
     * @return
     */
    public ProductOrderInformation overruleSearch(Long id);

    /**
     * 查询客户订单列表
     *
     * @param productOrderInformation 订单主
     * @return 订单主集合
     */
    public List<ProductOrderInformation> selectCustomerGroupList(ProductOrderInformation productOrderInformation);



    /**
     * 修改客户信息
     * @param
     */
    public int updateClientInformation();




    /**
     * 订单查询
     *
     * @param productOrderInformation 订单主
     * @return 订单主集合
     */
    public List<ProductOrderInformation> selectAllOrdersList(ProductOrderInformation productOrderInformation);


    /**
     * 客服打印 订单查询
     *
     * @param productOrderInformation 订单主
     * @return 订单主集合
     */
    public List<ProductOrderInformation> selectAfterSaleList(ProductOrderInformation productOrderInformation);

    /**
     * 数据统计查询
     */
    public ProductOrderInformation dataStatistics(Long statusId);

    /**
     * 条码数据查询
     * @param productOrderInformation
     * @return
     */
    public List<ProductOrderInformation> selectBarCodeList(ProductOrderInformation productOrderInformation);

    /**
     * 修改订单数据
     * @return
     */
    public int updateBarcodeGroupStatus(ProductOrderInformation productOrderInformation);



    /**
     * 条码数据查询
     * @param productOrderInformation
     * @return
     */
    public List<ProductOrderInformation> selectHaveOutboundList(ProductOrderInformation productOrderInformation);


    /**
     * 销量排行
     * @return
     */
    public  List<ProductOrderInformation> searchSalesVolumeList(ProductOrderInformation productOrderInformation);

    /**
     * 产品占比
     * @return
     */
    public  List<ProductOrderInformation> searchClassifyList(ProductOrderInformation productOrderInformation);

    /**
     * 下单量排行
     * @return
     */
    public  List<ProductOrderInformation> searchPlaceAnOrderNumList();

    /**
     * 入库时更新数据
     * @return
     */
    public int updateSweepCodeAccess(ProductOrderInformation productOrderInformation);


    /**
     * 出库时更新数据
     * @return
     */
    public int updateSweepCodeOutbound(ProductOrderInformation productOrderInformation);


    /**
     * 出入库记录查询
     * @return
     */
    public  List<ProductOrderInformation> selectAccessRecordsList(ProductOrderInformation productOrderInformation);

    /**
     * 根据 库位id查询 订单信息
     * @return
     */
    public  List<ProductOrderInformation> selectLocationIdList(ProductOrderInformation productOrderInformation);

    /**
     * 查询已入库_没有条码数据的订单
     * @param productOrderInformation
     * @return
     */
    public  List<ProductOrderInformation> selectNoBarCodeNumberList(ProductOrderInformation productOrderInformation);

    /**
     * 查询Max维修单号
     * @return
     */
    public  ProductOrderInformation maxMaintainNumberSearch();

    /**
     * 根据编号查询数据
     *
     * @param orderNumber 订单号
     * @return {@link ProductOrderInformation }
     * @author xin_xin
     * @date 2022-10-14 18:25:03
     */
    public ProductOrderInformation selectOrderNumber(String orderNumber);


    /**
     * 客服打印列表
     *
     * @param productOrderInformation 产品订单信息
     * @return {@link List }<{@link ProductOrderInformation }>
     * @author xin_xin
     * @date 2022-10-20 10:00:14
     */
    public List<ProductOrderInformation> selectAfterSalesPrint(ProductOrderInformation productOrderInformation);

    /**
     * 维修单驳回数量
     *
     * @return {@link Long }
     * @author xin_xin
     * @date 2022-11-18 16:58:57
     */
    public  ProductOrderInformation selectMaintainReject();
}
