package com.ruoyi.backstage.mapper;

import java.util.List;
import com.ruoyi.backstage.domain.ProductCarousel;
import org.springframework.stereotype.Repository;

/**
 * 轮播图Mapper接口
 * 
 * @author xin-xin
 * @date 2022-01-11
 */
@Repository
public interface ProductCarouselMapper 
{
    /**
     * 查询轮播图
     * 
     * @param id 轮播图主键
     * @return 轮播图
     */
    public ProductCarousel selectProductCarouselById(Long id);

    /**
     * 查询轮播图列表
     * 
     * @param productCarousel 轮播图
     * @return 轮播图集合
     */
    public List<ProductCarousel> selectProductCarouselList(ProductCarousel productCarousel);

    /**
     * 新增轮播图
     * 
     * @param productCarousel 轮播图
     * @return 结果
     */
    public int insertProductCarousel(ProductCarousel productCarousel);

    /**
     * 修改轮播图
     * 
     * @param productCarousel 轮播图
     * @return 结果
     */
    public int updateProductCarousel(ProductCarousel productCarousel);

    /**
     * 删除轮播图
     * 
     * @param id 轮播图主键
     * @return 结果
     */
    public int deleteProductCarouselById(Long id);

    /**
     * 批量删除轮播图
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductCarouselByIds(String[] ids);


}
