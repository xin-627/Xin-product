package com.ruoyi.backstage.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.backstage.domain.ProductPigment;
import org.springframework.stereotype.Repository;

/**
 * 产品色号组Mapper接口
 * 
 * @author xin-xin
 * @date 2022-01-05
 */
@Repository
public interface ProductPigmentMapper 
{
    /**
     * 查询产品色号组
     * 
     * @param id 产品色号组主键
     * @return 产品色号组
     */
    public ProductPigment selectProductPigmentById(Long id);

    /**
     * 查询产品色号组列表
     * 
     * @param productPigment 产品色号组
     * @return 产品色号组集合
     */
    public List<ProductPigment> selectProductPigmentList(ProductPigment productPigment);

    /**
     * 新增产品色号组
     * 
     * @param productPigment 产品色号组
     * @return 结果
     */
    public int insertProductPigment(ProductPigment productPigment);

    /**
     * 修改产品色号组
     * 
     * @param productPigment 产品色号组
     * @return 结果
     */
    public int updateProductPigment(ProductPigment productPigment);

    /**
     * 删除产品色号组
     * 
     * @param id 产品色号组主键
     * @return 结果
     */
    public int deleteProductPigmentById(Long id);

    /**
     * 批量删除产品色号组
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductPigmentByIds(String[] ids);

    /**
     * 色号查重
     * @param map
     * @return
     */
    public List<ProductPigment> pigmentDuplicateChecking(Map map);


    /**
     *根据id去查询相关 色号数据
     * @param pigmentId
     * @return
     */
    public ProductPigment pigmentIdDataSearch(String pigmentId);

}
