package com.ruoyi.backstage.mapper;

import java.util.List;
import com.ruoyi.backstage.domain.PRegister;
import com.ruoyi.common.core.domain.entity.SysUser;
import org.springframework.stereotype.Repository;

/**
 * 注册申请Mapper接口
 * 
 * @author xin_xin
 * @date 2022-05-13
 */
@Repository
public interface PRegisterMapper 
{
    /**
     * 查询注册申请
     * 
     * @param id 注册申请主键
     * @return 注册申请
     */
    public PRegister selectPRegisterById(Long id);

    /**
     * 查询注册申请列表
     * 
     * @param pRegister 注册申请
     * @return 注册申请集合
     */
    public List<PRegister> selectPRegisterList(PRegister pRegister);

    /**
     * 新增注册申请
     * 
     * @param pRegister 注册申请
     * @return 结果
     */
    public int insertPRegister(PRegister pRegister);

    /**
     * 修改注册申请
     * 
     * @param pRegister 注册申请
     * @return 结果
     */
    public int updatePRegister(PRegister pRegister);

    /**
     * 删除注册申请
     * 
     * @param id 注册申请主键
     * @return 结果
     */
    public int deletePRegisterById(Long id);

    /**
     * 批量删除注册申请
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePRegisterByIds(String[] ids);

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return 结果
     */
    public PRegister checkPhoneUnique(String phonenumber);
}
