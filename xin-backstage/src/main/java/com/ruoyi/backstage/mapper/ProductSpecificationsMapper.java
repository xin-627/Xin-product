package com.ruoyi.backstage.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.backstage.domain.ProductSpecifications;
import org.springframework.stereotype.Repository;

/**
 * 产品规格组Mapper接口
 * 
 * @author xin-xin
 * @date 2022-01-05
 */
@Repository
public interface ProductSpecificationsMapper 
{
    /**
     * 查询产品规格组
     * 
     * @param id 产品规格组主键
     * @return 产品规格组
     */
    public ProductSpecifications selectProductSpecificationsById(Long id);

    /**
     * 查询产品规格组列表
     * 
     * @param productSpecifications 产品规格组
     * @return 产品规格组集合
     */
    public List<ProductSpecifications> selectProductSpecificationsList(ProductSpecifications productSpecifications);

    /**
     * 新增产品规格组
     * 
     * @param productSpecifications 产品规格组
     * @return 结果
     */
    public int insertProductSpecifications(ProductSpecifications productSpecifications);

    /**
     * 修改产品规格组
     * 
     * @param productSpecifications 产品规格组
     * @return 结果
     */
    public int updateProductSpecifications(ProductSpecifications productSpecifications);

    /**
     * 删除产品规格组
     * 
     * @param id 产品规格组主键
     * @return 结果
     */
    public int deleteProductSpecificationsById(Long id);

    /**
     * 批量删除产品规格组
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductSpecificationsByIds(String[] ids);

    /**
     * 规格查询重
     * @param map
     * @return
     */
    public List<ProductSpecifications> pecificationsDuplicateChecking(Map map);
}
