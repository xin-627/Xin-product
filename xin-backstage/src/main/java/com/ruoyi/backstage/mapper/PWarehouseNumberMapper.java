package com.ruoyi.backstage.mapper;

import java.util.List;
import com.ruoyi.backstage.domain.PWarehouseNumber;

/**
 * 仓库库位Mapper接口
 * 
 * @author xin_xin
 * @date 2022-09-13
 */
public interface PWarehouseNumberMapper 
{
    /**
     * 查询仓库库位
     *
     * @param id 仓库库位主键
     * @return 仓库库位
     */
    public PWarehouseNumber selectPWarehouseNumberById(Long id);

    /**
     * 查询仓库库位列表
     * 
     * @param pWarehouseNumber 仓库库位
     * @return 仓库库位集合
     */
    public List<PWarehouseNumber> selectPWarehouseNumberList(PWarehouseNumber pWarehouseNumber);

    /**
     * 新增仓库库位
     * 
     * @param pWarehouseNumber 仓库库位
     * @return 结果
     */
    public int insertPWarehouseNumber(PWarehouseNumber pWarehouseNumber);

    /**
     * 修改仓库库位
     * 
     * @param pWarehouseNumber 仓库库位
     * @return 结果
     */
    public int updatePWarehouseNumber(PWarehouseNumber pWarehouseNumber);

    /**
     * 删除仓库库位
     * 
     * @param id 仓库库位主键
     * @return 结果
     */
    public int deletePWarehouseNumberById(Long id);

    /**
     * 批量删除仓库库位
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePWarehouseNumberByIds(String[] ids);


    /**
     * 出入库时的更新
     *
     * @param pWarehouseNumber 仓库库位
     * @return 结果
     */
    public int updateSweepCodeAccess(PWarehouseNumber pWarehouseNumber);


    /**
     * 查询仓库库位
     *
     * @param shelfStateCode 仓库库位主键
     * @return 仓库库位
     */
    public PWarehouseNumber selectShelfStateCodeData(String shelfStateCode);


    /**
     * 查询仓库库位列表
     */
    public List<PWarehouseNumber> selectLocationList(PWarehouseNumber pWarehouseNumber);


    /**
     * 更新库位状态
     *
     * @return int
     * @author xin_xin
     * @date 2022-12-06 12:26:45
     */
    public int updateShelfState();

}
