package com.ruoyi.backstage.mapper;

import java.util.List;
import com.ruoyi.backstage.domain.POpinion;
import org.springframework.stereotype.Repository;

/**
 * 意见与建议Mapper接口
 * 
 * @author xin_xin
 * @date 2022-05-13
 */
@Repository
public interface POpinionMapper 
{
    /**
     * 查询意见与建议
     * 
     * @param id 意见与建议主键
     * @return 意见与建议
     */
    public POpinion selectPOpinionById(Long id);

    /**
     * 查询意见与建议列表
     * 
     * @param pOpinion 意见与建议
     * @return 意见与建议集合
     */
    public List<POpinion> selectPOpinionList(POpinion pOpinion);

    /**
     * 新增意见与建议
     * 
     * @param pOpinion 意见与建议
     * @return 结果
     */
    public int insertPOpinion(POpinion pOpinion);

    /**
     * 修改意见与建议
     * 
     * @param pOpinion 意见与建议
     * @return 结果
     */
    public int updatePOpinion(POpinion pOpinion);

    /**
     * 删除意见与建议
     * 
     * @param id 意见与建议主键
     * @return 结果
     */
    public int deletePOpinionById(Long id);

    /**
     * 批量删除意见与建议
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePOpinionByIds(String[] ids);
}
