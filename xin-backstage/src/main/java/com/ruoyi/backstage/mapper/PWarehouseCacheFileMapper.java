package com.ruoyi.backstage.mapper;

import java.util.List;
import com.ruoyi.backstage.domain.PWarehouseCacheFile;
import com.ruoyi.backstage.domain.PWarehouseNumber;

/**
 * 库位缓存Mapper接口
 * 
 * @author xin_xin
 * @date 2022-09-16
 */
public interface PWarehouseCacheFileMapper 
{
    /**
     * 查询库位缓存
     * 
     * @param id 库位缓存主键
     * @return 库位缓存
     */
    public PWarehouseCacheFile selectPWarehouseCacheFileById(Long id);

    /**
     * 查询库位缓存列表
     * 
     * @param pWarehouseCacheFile 库位缓存
     * @return 库位缓存集合
     */
    public List<PWarehouseCacheFile> selectPWarehouseCacheFileList(PWarehouseCacheFile pWarehouseCacheFile);

    /**
     * 新增库位缓存
     * 
     * @param pWarehouseCacheFile 库位缓存
     * @return 结果
     */
    public int insertPWarehouseCacheFile(PWarehouseCacheFile pWarehouseCacheFile);

    /**
     * 修改库位缓存
     * 
     * @param pWarehouseCacheFile 库位缓存
     * @return 结果
     */
    public int updatePWarehouseCacheFile(PWarehouseCacheFile pWarehouseCacheFile);

    /**
     * 删除库位缓存
     * 
     * @param id 库位缓存主键
     * @return 结果
     */
    public int deletePWarehouseCacheFileById(Long id);

    /**
     * 批量删除库位缓存
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePWarehouseCacheFileByIds(String[] ids);

    /**
     * 查询库位缓存
     */
    public PWarehouseCacheFile selectUserIdData(Long userId);

    /**
     * 让所有的库位 user_id 都失效
     *
     * @param pWarehouseCacheFile 库位缓存
     * @return 结果
     */
    public int updateUserIdShelfState(PWarehouseCacheFile pWarehouseCacheFile);
}
