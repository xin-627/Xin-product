package com.ruoyi.backstage.mapper;

import java.util.List;
import com.ruoyi.backstage.domain.PWarehouse;
import org.springframework.stereotype.Repository;

/**
 * 仓库标签Mapper接口
 * 
 * @author xin_xin
 * @date 2022-07-12
 */
@Repository
public interface PWarehouseMapper 
{
    /**
     * 查询仓库标签
     * 
     * @param id 仓库标签主键
     * @return 仓库标签
     */
    public PWarehouse selectPWarehouseById(Long id);
    /**
     * 查询仓库标签列表
     * 
     * @param pWarehouse 仓库标签
     * @return 仓库标签集合
     */
    public List<PWarehouse> selectPWarehouseList(PWarehouse pWarehouse);

    /**
     * 新增仓库标签
     * 
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    public int insertPWarehouse(PWarehouse pWarehouse);

    /**
     * 修改仓库标签
     * 
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    public int updatePWarehouse(PWarehouse pWarehouse);

    /**
     * 删除仓库标签
     * 
     * @param id 仓库标签主键
     * @return 结果
     */
    public int deletePWarehouseById(Long id);

    /**
     * 批量删除仓库标签
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePWarehouseByIds(String[] ids);

    /**
     * 查询标签数据(序号)
     * @return
     */
    public  List<PWarehouse> selectGroupingData();

    /**
     * 修改序号
     * @param ordersId
     * @return
     */
    public int updateSerialNumber(Long ordersId);

    /**
     * 修改总包数
     * @return
     */
    public int updateQuantity();

    /**
     * 扫码出库
     *
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    public int sweepCodeOutboundUpdate(PWarehouse pWarehouse);


    /**
     * 查询条码数据
     * @param barCode
     * @return
     */
    public PWarehouse selectOrderData(Long barCode);

    /**
     * By order_id查询条码数据
     * 2022-10-10
     * @param orderId
     * @return
     */
    public PWarehouse selectOrderDataById(Long orderId);

}
