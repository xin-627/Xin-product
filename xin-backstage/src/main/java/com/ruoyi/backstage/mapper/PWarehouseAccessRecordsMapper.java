package com.ruoyi.backstage.mapper;

import java.util.List;
import com.ruoyi.backstage.domain.PWarehouseAccessRecords;

/**
 * 仓库出入库记录Mapper接口
 * 
 * @author xin_xin
 * @date 2022-09-16
 */
public interface PWarehouseAccessRecordsMapper 
{
    /**
     * 查询仓库出入库记录
     * 
     * @param id 仓库出入库记录主键
     * @return 仓库出入库记录
     */
    public PWarehouseAccessRecords selectPWarehouseAccessRecordsById(Long id);

    /**
     * 查询仓库出入库记录列表
     * 
     * @param pWarehouseAccessRecords 仓库出入库记录
     * @return 仓库出入库记录集合
     */
    public List<PWarehouseAccessRecords> selectPWarehouseAccessRecordsList(PWarehouseAccessRecords pWarehouseAccessRecords);

    /**
     * 新增仓库出入库记录
     * 
     * @param pWarehouseAccessRecords 仓库出入库记录
     * @return 结果
     */
    public int insertPWarehouseAccessRecords(PWarehouseAccessRecords pWarehouseAccessRecords);

    /**
     * 修改仓库出入库记录
     * 
     * @param pWarehouseAccessRecords 仓库出入库记录
     * @return 结果
     */
    public int updatePWarehouseAccessRecords(PWarehouseAccessRecords pWarehouseAccessRecords);

    /**
     * 删除仓库出入库记录
     * 
     * @param id 仓库出入库记录主键
     * @return 结果
     */
    public int deletePWarehouseAccessRecordsById(Long id);

    /**
     * 批量删除仓库出入库记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePWarehouseAccessRecordsByIds(String[] ids);
}
