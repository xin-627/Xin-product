package com.ruoyi.backstage.service;

import java.util.List;
import com.ruoyi.backstage.domain.PWarehouseNumber;

/**
 * 仓库库位Service接口
 * 
 * @author xin_xin
 * @date 2022-09-13
 */
public interface IPWarehouseNumberService 
{
    /**
     * 查询仓库库位
     * 
     * @param id 仓库库位主键
     * @return 仓库库位
     */
    public PWarehouseNumber selectPWarehouseNumberById(Long id);

    /**
     * 查询仓库库位列表
     * 
     * @param pWarehouseNumber 仓库库位
     * @return 仓库库位集合
     */
    public List<PWarehouseNumber> selectPWarehouseNumberList(PWarehouseNumber pWarehouseNumber);

    /**
     * 新增仓库库位
     * 
     * @param pWarehouseNumber 仓库库位
     * @return 结果
     */
    public int insertPWarehouseNumber(PWarehouseNumber pWarehouseNumber);

    /**
     * 修改仓库库位
     * 
     * @param pWarehouseNumber 仓库库位
     * @return 结果
     */
    public int updatePWarehouseNumber(PWarehouseNumber pWarehouseNumber);

    /**
     * 批量删除仓库库位
     * 
     * @param ids 需要删除的仓库库位主键集合
     * @return 结果
     */
    public int deletePWarehouseNumberByIds(String ids);

    /**
     * 删除仓库库位信息
     * 
     * @param id 仓库库位主键
     * @return 结果
     */
    public int deletePWarehouseNumberById(Long id);


    /**
     * 查询仓库库位列表2022-09-27
     */
    public List<PWarehouseNumber> selectLocationList(PWarehouseNumber pWarehouseNumber);
}
