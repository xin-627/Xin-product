package com.ruoyi.backstage.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.backstage.domain.ProductClassify;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 分类菜单/分类管理Service接口
 * 
 * @author xin-xin
 * @date 2022-01-11
 */
public interface ProductClassifyService
{
    /**
     * 查询分类菜单/分类管理
     * 
     * @param id 分类菜单/分类管理主键
     * @return 分类菜单/分类管理
     */
    public ProductClassify selectProductClassifyById(Long id);

    /**
     * 查询分类菜单/分类管理列表
     * 
     * @param productClassify 分类菜单/分类管理
     * @return 分类菜单/分类管理集合
     */
    public List<ProductClassify> selectProductClassifyList(ProductClassify productClassify);

    /**
     * 新增分类菜单/分类管理
     * 
     * @param productClassify 分类菜单/分类管理
     * @return 结果
     */
    public int insertProductClassify(ProductClassify productClassify);

    /**
     * 修改分类菜单/分类管理
     * 
     * @param productClassify 分类菜单/分类管理
     * @return 结果
     */
    public int updateProductClassify(ProductClassify productClassify);

    /**
     * 批量删除分类菜单/分类管理
     * 
     * @param ids 需要删除的分类菜单/分类管理主键集合
     * @return 结果
     */
    public int deleteProductClassifyByIds(String ids);

    /**
     * 删除分类菜单/分类管理信息
     * 
     * @param id 分类菜单/分类管理主键
     * @return 结果
     */
    public int deleteProductClassifyById(Long id);

    /**
     * 查询分类菜单/分类管理树列表
     * 
     * @return 所有分类菜单/分类管理信息
     */
    public List<Ztree> selectProductClassifyTree();

    /**
     * 分类数
     * @return
     */
    public Map<String,Object> TreeUtilClassification();

    /**
     * 前端动态分类菜单
     * @return
     */
    public Map<String,Object> TreeUtilClassify();
}
