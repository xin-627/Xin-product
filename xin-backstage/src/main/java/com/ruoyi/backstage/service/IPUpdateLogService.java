package com.ruoyi.backstage.service;

import java.util.List;
import com.ruoyi.backstage.domain.PUpdateLog;

/**
 * 更新日志Service接口
 * 
 * @author xin_xin
 * @date 2022-05-13
 */
public interface IPUpdateLogService 
{
    /**
     * 查询更新日志
     * 
     * @param id 更新日志主键
     * @return 更新日志
     */
    public PUpdateLog selectPUpdateLogById(Long id);

    /**
     * 查询更新日志列表
     * 
     * @param pUpdateLog 更新日志
     * @return 更新日志集合
     */
    public List<PUpdateLog> selectPUpdateLogList(PUpdateLog pUpdateLog);

    /**
     * 新增更新日志
     * 
     * @param pUpdateLog 更新日志
     * @return 结果
     */
    public int insertPUpdateLog(PUpdateLog pUpdateLog);

    /**
     * 修改更新日志
     * 
     * @param pUpdateLog 更新日志
     * @return 结果
     */
    public int updatePUpdateLog(PUpdateLog pUpdateLog);

    /**
     * 批量删除更新日志
     * 
     * @param ids 需要删除的更新日志主键集合
     * @return 结果
     */
    public int deletePUpdateLogByIds(String ids);

    /**
     * 删除更新日志信息
     * 
     * @param id 更新日志主键
     * @return 结果
     */
    public int deletePUpdateLogById(Long id);
}
