package com.ruoyi.backstage.service;

import java.util.List;
import com.ruoyi.backstage.domain.PWarehouseAccessRecords;

/**
 * 仓库出入库记录Service接口
 * 
 * @author xin_xin
 * @date 2022-09-16
 */
public interface IPWarehouseAccessRecordsService 
{
    /**
     * 查询仓库出入库记录
     * 
     * @param id 仓库出入库记录主键
     * @return 仓库出入库记录
     */
    public PWarehouseAccessRecords selectPWarehouseAccessRecordsById(Long id);

    /**
     * 查询仓库出入库记录列表
     * 
     * @param pWarehouseAccessRecords 仓库出入库记录
     * @return 仓库出入库记录集合
     */
    public List<PWarehouseAccessRecords> selectPWarehouseAccessRecordsList(PWarehouseAccessRecords pWarehouseAccessRecords);

    /**
     * 新增仓库出入库记录
     * 
     * @param pWarehouseAccessRecords 仓库出入库记录
     * @return 结果
     */
    public int insertPWarehouseAccessRecords(PWarehouseAccessRecords pWarehouseAccessRecords);

    /**
     * 修改仓库出入库记录
     * 
     * @param pWarehouseAccessRecords 仓库出入库记录
     * @return 结果
     */
    public int updatePWarehouseAccessRecords(PWarehouseAccessRecords pWarehouseAccessRecords);

    /**
     * 批量删除仓库出入库记录
     * 
     * @param ids 需要删除的仓库出入库记录主键集合
     * @return 结果
     */
    public int deletePWarehouseAccessRecordsByIds(String ids);

    /**
     * 删除仓库出入库记录信息
     * 
     * @param id 仓库出入库记录主键
     * @return 结果
     */
    public int deletePWarehouseAccessRecordsById(Long id);
}
