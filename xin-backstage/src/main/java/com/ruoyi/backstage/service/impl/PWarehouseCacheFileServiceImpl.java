package com.ruoyi.backstage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.PWarehouseCacheFileMapper;
import com.ruoyi.backstage.domain.PWarehouseCacheFile;
import com.ruoyi.backstage.service.IPWarehouseCacheFileService;
import com.ruoyi.common.core.text.Convert;

/**
 * 库位缓存Service业务层处理
 * 
 * @author xin_xin
 * @date 2022-09-16
 */
@Service
public class PWarehouseCacheFileServiceImpl implements IPWarehouseCacheFileService 
{
    @Autowired
    private PWarehouseCacheFileMapper pWarehouseCacheFileMapper;

    /**
     * 查询库位缓存
     * 
     * @param id 库位缓存主键
     * @return 库位缓存
     */
    @Override
    public PWarehouseCacheFile selectPWarehouseCacheFileById(Long id)
    {
        return pWarehouseCacheFileMapper.selectPWarehouseCacheFileById(id);
    }

    /**
     * 查询库位缓存列表
     * 
     * @param pWarehouseCacheFile 库位缓存
     * @return 库位缓存
     */
    @Override
    public List<PWarehouseCacheFile> selectPWarehouseCacheFileList(PWarehouseCacheFile pWarehouseCacheFile)
    {
        return pWarehouseCacheFileMapper.selectPWarehouseCacheFileList(pWarehouseCacheFile);
    }

    /**
     * 新增库位缓存
     * 
     * @param pWarehouseCacheFile 库位缓存
     * @return 结果
     */
    @Override
    public int insertPWarehouseCacheFile(PWarehouseCacheFile pWarehouseCacheFile)
    {
        pWarehouseCacheFile.setCreateTime(DateUtils.getNowDate());
        return pWarehouseCacheFileMapper.insertPWarehouseCacheFile(pWarehouseCacheFile);
    }

    /**
     * 修改库位缓存
     * 
     * @param pWarehouseCacheFile 库位缓存
     * @return 结果
     */
    @Override
    public int updatePWarehouseCacheFile(PWarehouseCacheFile pWarehouseCacheFile)
    {
        pWarehouseCacheFile.setUpdateTime(DateUtils.getNowDate());
        return pWarehouseCacheFileMapper.updatePWarehouseCacheFile(pWarehouseCacheFile);
    }

    /**
     * 批量删除库位缓存
     * 
     * @param ids 需要删除的库位缓存主键
     * @return 结果
     */
    @Override
    public int deletePWarehouseCacheFileByIds(String ids)
    {
        return pWarehouseCacheFileMapper.deletePWarehouseCacheFileByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除库位缓存信息
     * 
     * @param id 库位缓存主键
     * @return 结果
     */
    @Override
    public int deletePWarehouseCacheFileById(Long id)
    {
        return pWarehouseCacheFileMapper.deletePWarehouseCacheFileById(id);
    }
}
