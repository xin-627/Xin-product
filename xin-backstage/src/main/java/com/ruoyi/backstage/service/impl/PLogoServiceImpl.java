package com.ruoyi.backstage.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.ruoyi.backstage.domain.ProductCarousel;
import com.ruoyi.backstage.util.UtilGetSys;
import com.ruoyi.common.config.ServerConfig;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.PLogoMapper;
import com.ruoyi.backstage.domain.PLogo;
import com.ruoyi.backstage.service.IPLogoService;
import com.ruoyi.common.core.text.Convert;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.ruoyi.common.config.RuoYiConfig.getProfile;

/**
 * logoService业务层处理
 *
 * @author xin-xin
 * @date 2022-06-08
 */
@Service
public class PLogoServiceImpl implements IPLogoService {
    @Autowired
    private PLogoMapper pLogoMapper;

    /**
     * 查询logo
     *
     * @param id logo主键
     * @return logo
     */
    @Override
    public PLogo selectPLogoById(Long id) {
        return pLogoMapper.selectPLogoById(id);
    }

    /**
     * 查询logo列表
     *
     * @param pLogo logo
     * @return logo
     */
    @Override
    public List<PLogo> selectPLogoList(PLogo pLogo) {
        return pLogoMapper.selectPLogoList(pLogo);
    }

    /**
     * 新增logo
     *
     * @param pLogo logo
     * @return 结果
     */
    @Override
    public int insertPLogo(PLogo pLogo) {
        return pLogoMapper.insertPLogo(pLogo);
    }

    /**
     * 修改logo
     *
     * @param pLogo logo
     * @return 结果
     */
    @Override
    public int updatePLogo(PLogo pLogo) {
        return pLogoMapper.updatePLogo(pLogo);
    }

    /**
     * 批量删除logo
     *
     * @param ids 需要删除的logo主键
     * @return 结果
     */
    @Override
    public int deletePLogoByIds(String ids) {
        return pLogoMapper.deletePLogoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除logo信息
     *
     * @param id logo主键
     * @return 结果
     */
    @Override
    public int deletePLogoById(Long id) {
        return pLogoMapper.deletePLogoById(id);
    }


    /**
     * 删除img
     *
     * @param request 请求
     * @return int
     * @author xin_xin
     * @date 2022-11-01 14:21:37
     */
    @Override
    public int deleteImg(HttpServletRequest request) {
        int integer = 0;
        String imgUrl = request.getParameter("imgUrl");//图片路径
        List<String> list = Arrays.asList(imgUrl.split(","));
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                String data = list.get(i).replace("/profile/", "/");//删除关键词
                File file = new File(getProfile() + data);
                if (file.exists() == true) {
                    //判断图片是否存在
                    Boolean flag = false;
                    flag = file.delete();
                    if (flag) {
                        integer = 200;
                    } else {
                        integer = 500;
                    }
                } else {
                    integer = 404;
                }

            }
        } else {
            integer = 404;
        }
        return integer;
    }

    @Override
    public void pictureDisplayLogo(HttpServletRequest request, HttpServletResponse response) {
        PLogo logoData = pLogoMapper.selectPLogoById(Long.valueOf(1));
        String imgData = logoData.getImage().replace("/profile/", "/");//删除关键词
        String filePath = UtilGetSys.getPath() + imgData;//图地址
        FileInputStream in;
        try {
            request.setCharacterEncoding("utf-8");
            //页面img标签中src中传入的是真实图片地址路径
            String path = filePath;
            String filePathECoding = new String(path.trim().getBytes("UTF-8"), "UTF-8");
            response.setContentType("application/octet-stream;charset=UTF-8");
            //图片读取路径
            in = new FileInputStream(filePathECoding);
            // 得到文件大小
            int i = in.available();
            //创建存放文件内容的数组
            byte[] data = new byte[i];
            in.read(data);
            in.close();
            //把图片写出去
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            outputStream.write(data);
            //将缓存区的数据进行输出
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            //e.printStackTrace();//输出错误,控制台会报错
            System.out.println(e.getMessage());//仅输出错误警告
            return;
        }
    }

}
