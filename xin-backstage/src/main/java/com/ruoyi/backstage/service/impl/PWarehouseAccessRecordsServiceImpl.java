package com.ruoyi.backstage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.PWarehouseAccessRecordsMapper;
import com.ruoyi.backstage.domain.PWarehouseAccessRecords;
import com.ruoyi.backstage.service.IPWarehouseAccessRecordsService;
import com.ruoyi.common.core.text.Convert;

/**
 * 仓库出入库记录Service业务层处理
 * 
 * @author xin_xin
 * @date 2022-09-16
 */
@Service
public class PWarehouseAccessRecordsServiceImpl implements IPWarehouseAccessRecordsService 
{
    @Autowired
    private PWarehouseAccessRecordsMapper pWarehouseAccessRecordsMapper;

    /**
     * 查询仓库出入库记录
     * 
     * @param id 仓库出入库记录主键
     * @return 仓库出入库记录
     */
    @Override
    public PWarehouseAccessRecords selectPWarehouseAccessRecordsById(Long id)
    {
        return pWarehouseAccessRecordsMapper.selectPWarehouseAccessRecordsById(id);
    }

    /**
     * 查询仓库出入库记录列表
     * 
     * @param pWarehouseAccessRecords 仓库出入库记录
     * @return 仓库出入库记录
     */
    @Override
    public List<PWarehouseAccessRecords> selectPWarehouseAccessRecordsList(PWarehouseAccessRecords pWarehouseAccessRecords)
    {
        return pWarehouseAccessRecordsMapper.selectPWarehouseAccessRecordsList(pWarehouseAccessRecords);
    }

    /**
     * 新增仓库出入库记录
     * 
     * @param pWarehouseAccessRecords 仓库出入库记录
     * @return 结果
     */
    @Override
    public int insertPWarehouseAccessRecords(PWarehouseAccessRecords pWarehouseAccessRecords)
    {
        pWarehouseAccessRecords.setCreateTime(DateUtils.getNowDate());
        return pWarehouseAccessRecordsMapper.insertPWarehouseAccessRecords(pWarehouseAccessRecords);
    }

    /**
     * 修改仓库出入库记录
     * 
     * @param pWarehouseAccessRecords 仓库出入库记录
     * @return 结果
     */
    @Override
    public int updatePWarehouseAccessRecords(PWarehouseAccessRecords pWarehouseAccessRecords)
    {
        pWarehouseAccessRecords.setUpdateTime(DateUtils.getNowDate());
        return pWarehouseAccessRecordsMapper.updatePWarehouseAccessRecords(pWarehouseAccessRecords);
    }

    /**
     * 批量删除仓库出入库记录
     * 
     * @param ids 需要删除的仓库出入库记录主键
     * @return 结果
     */
    @Override
    public int deletePWarehouseAccessRecordsByIds(String ids)
    {
        return pWarehouseAccessRecordsMapper.deletePWarehouseAccessRecordsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除仓库出入库记录信息
     * 
     * @param id 仓库出入库记录主键
     * @return 结果
     */
    @Override
    public int deletePWarehouseAccessRecordsById(Long id)
    {
        return pWarehouseAccessRecordsMapper.deletePWarehouseAccessRecordsById(id);
    }
}
