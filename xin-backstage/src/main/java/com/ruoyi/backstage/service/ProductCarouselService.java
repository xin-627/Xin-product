package com.ruoyi.backstage.service;

import java.util.List;
import com.ruoyi.backstage.domain.ProductCarousel;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 轮播图Service接口
 * 
 * @author xin-xin
 * @date 2022-01-11
 */
public interface ProductCarouselService
{
    /**
     * 查询轮播图
     * 
     * @param id 轮播图主键
     * @return 轮播图
     */
    public ProductCarousel selectProductCarouselById(Long id);

    /**
     * 查询轮播图列表
     * 
     * @param productCarousel 轮播图
     * @return 轮播图集合
     */
    public List<ProductCarousel> selectProductCarouselList(ProductCarousel productCarousel);

    /**
     * 新增轮播图
     * 
     * @param productCarousel 轮播图
     * @return 结果
     */
    public int insertProductCarousel(ProductCarousel productCarousel);

    /**
     * 修改轮播图
     * 
     * @param productCarousel 轮播图
     * @return 结果
     */
    public int updateProductCarousel(ProductCarousel productCarousel);

    /**
     * 批量删除轮播图
     * 
     * @param ids 需要删除的轮播图主键集合
     * @return 结果
     */
    public int deleteProductCarouselByIds(String ids);

    /**
     * 删除轮播图信息
     * 
     * @param id 轮播图主键
     * @return 结果
     */
    public int deleteProductCarouselById(Long id);


    /**
     * 轮播如展示
     * @param request
     * @param response
     * @return
     */
    public void showImg(HttpServletRequest request, HttpServletResponse response);

    /**
     * svg解析
     * @param request
     * @param response
     * @return
     */
    public void analysisOfSVG(HttpServletRequest request, HttpServletResponse response);


    /**
     * 轮播图
     * @param request
     * @return
     */
    public int deleteImg(HttpServletRequest request);




}
