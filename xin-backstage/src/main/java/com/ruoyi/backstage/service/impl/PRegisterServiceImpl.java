package com.ruoyi.backstage.service.impl;

import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.PRegisterMapper;
import com.ruoyi.backstage.domain.PRegister;
import com.ruoyi.backstage.service.IPRegisterService;
import com.ruoyi.common.core.text.Convert;

/**
 * 注册申请Service业务层处理
 *
 * @author xin_xin
 * @date 2022-05-13
 */
@Service
public class PRegisterServiceImpl implements IPRegisterService {
    @Autowired
    private PRegisterMapper pRegisterMapper;

    /**
     * 查询注册申请
     *
     * @param id 注册申请主键
     * @return 注册申请
     */
    @Override
    public PRegister selectPRegisterById(Long id) {
        return pRegisterMapper.selectPRegisterById(id);
    }

    /**
     * 查询注册申请列表
     *
     * @param pRegister 注册申请
     * @return 注册申请
     */
    @Override
    public List<PRegister> selectPRegisterList(PRegister pRegister) {
        return pRegisterMapper.selectPRegisterList(pRegister);
    }

    /**
     * 新增注册申请
     *
     * @param pRegister 注册申请
     * @return 结果
     */
    @Override
    public int insertPRegister(PRegister pRegister) {
        pRegister.setCreateTime(DateUtils.getNowDate());
        return pRegisterMapper.insertPRegister(pRegister);
    }

    /**
     * 修改注册申请
     *
     * @param pRegister 注册申请
     * @return 结果
     */
    @Override
    public int updatePRegister(PRegister pRegister) {
        pRegister.setUpdateTime(DateUtils.getNowDate());
        return pRegisterMapper.updatePRegister(pRegister);
    }

    /**
     * 批量删除注册申请
     *
     * @param ids 需要删除的注册申请主键
     * @return 结果
     */
    @Override
    public int deletePRegisterByIds(String ids) {
        return pRegisterMapper.deletePRegisterByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除注册申请信息
     *
     * @param id 注册申请主键
     * @return 结果
     */
    @Override
    public int deletePRegisterById(Long id) {
        return pRegisterMapper.deletePRegisterById(id);
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param
     * @return
     */
    @Override
    public String checkPhoneUnique(PRegister pRegister) {
        PRegister info = pRegisterMapper.checkPhoneUnique(pRegister.getPhonenumber());
        if (StringUtils.isNotNull(info)){
            if (info.getPhonenumber().equals(pRegister.getPhonenumber())) {
                return UserConstants.USER_PHONE_NOT_UNIQUE;
            }
        }else{
            return UserConstants.USER_PHONE_UNIQUE;
        }
        return UserConstants.USER_PHONE_UNIQUE;

    }
}
