package com.ruoyi.backstage.service.impl;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.ruoyi.backstage.util.UtilGetSys;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.ProductCarouselMapper;
import com.ruoyi.backstage.domain.ProductCarousel;
import com.ruoyi.backstage.service.ProductCarouselService;
import com.ruoyi.common.core.text.Convert;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.Document;
import javax.swing.text.Element;

import static com.ruoyi.common.config.RuoYiConfig.getProfile;

/**
 * 轮播图Service业务层处理
 *
 * @author xin-xin
 * @date 2022-01-11
 */
@Service
public class ProductCarouselServiceImpl implements ProductCarouselService {
    @Autowired
    private ProductCarouselMapper productCarouselMapper;

    /**
     * 查询轮播图
     *
     * @param id 轮播图主键
     * @return 轮播图
     */
    @Override
    public ProductCarousel selectProductCarouselById(Long id) {
        return productCarouselMapper.selectProductCarouselById(id);
    }

    /**
     * 查询轮播图列表
     *
     * @param productCarousel 轮播图
     * @return 轮播图
     */
    @Override
    public List<ProductCarousel> selectProductCarouselList(ProductCarousel productCarousel) {
        return productCarouselMapper.selectProductCarouselList(productCarousel);
    }

    /**
     * 新增轮播图
     *
     * @param productCarousel 轮播图
     * @return 结果
     */
    @Override
    public int insertProductCarousel(ProductCarousel productCarousel) {
        return productCarouselMapper.insertProductCarousel(productCarousel);
    }

    /**
     * 修改轮播图
     *
     * @param productCarousel 轮播图
     * @return 结果
     */
    @Override
    public int updateProductCarousel(ProductCarousel productCarousel) {
        return productCarouselMapper.updateProductCarousel(productCarousel);
    }

    /**
     * 批量删除轮播图
     *
     * @param ids 需要删除的轮播图主键
     * @return 结果
     */
    @Override
    public int deleteProductCarouselByIds(String ids) {
        return productCarouselMapper.deleteProductCarouselByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除轮播图信息
     *
     * @param id 轮播图主键
     * @return 结果
     */
    @Override
    public int deleteProductCarouselById(Long id) {
        return productCarouselMapper.deleteProductCarouselById(id);
    }



    /**
     * 轮播图展示
     *
     * @param request
     * @param response
     */
    @Override
    public void showImg(HttpServletRequest request, HttpServletResponse response) {
        String imgUrl = request.getParameter("imgUrl");//获取文件名
        String imgData = imgUrl.replace("/profile/", "/");//删除关键词
        String filePath = UtilGetSys.getPath() + imgData;//图地址
        FileInputStream in;
        try {
            request.setCharacterEncoding("utf-8");
            //页面img标签中src中传入的是真实图片地址路径
            String path = filePath;
            String filePathECoding = new String(path.trim().getBytes("UTF-8"), "UTF-8");
            response.setContentType("application/octet-stream;charset=UTF-8");
            //图片读取路径
            in = new FileInputStream(filePathECoding);
            // 得到文件大小
            int i = in.available();
            //创建存放文件内容的数组
            byte[] data = new byte[i];
            in.read(data);
            in.close();
            //把图片写出去
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            outputStream.write(data);
            //将缓存区的数据进行输出
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            //e.printStackTrace();//输出错误,控制台会报错
            System.out.println(e.getMessage());//仅输出错误警告
            return;
        }
    }

    /**
     * svg展示
     * @param request
     */
    @Override
    public void analysisOfSVG(HttpServletRequest request, HttpServletResponse response) {
        String imgUrl = request.getParameter("imgUrl");//获取文件名
        String fileName = UtilGetSys.getPath() + imgUrl;;
        File imageFile = new File(fileName);
        if (imageFile.exists()) {
            FileInputStream fis = null;
            OutputStream os = null;
            try {
                fis = new FileInputStream(imageFile);
                os = response.getOutputStream();
                int count = 0;
                byte[] buffer = new byte[1024 * 8];
                while ((count = fis.read(buffer)) != -1) {
                    os.write(buffer, 0, count);
                    os.flush();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    fis.close();
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }



    /**
     * 轮播图删除
     *
     * @param request
     * @return
     */
    @Override
    public int deleteImg(HttpServletRequest request) {
        int integer = 0;
        String imgUrl = request.getParameter("imgUrl");//图片路径
        List<String> list = Arrays.asList(imgUrl.split(","));
        if(list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                String data = list.get(i).replace("/profile/", "/");//删除关键词
                File file = new File(getProfile() + data);
                if (file.exists() == true) {
                    //判断图片是否存在
                    Boolean flag = false;
                    flag = file.delete();
                    if (flag) {
                        integer = 200;
                    } else {
                        integer = 500;
                    }
                } else {
                    integer = 404;
                }

            }
        }else{
            integer = 404;
        }
        return integer;
    }



}
