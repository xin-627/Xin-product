package com.ruoyi.backstage.service;

import java.util.List;

import com.ruoyi.backstage.domain.ProductMain;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 产品管理Service接口
 * 
 * @author xin-xin
 * @date 2022-01-08
 */
public interface ProductMainService
{



    /**
     * 查询产品管理
     * 
     * @param id 产品管理主键
     * @return 产品管理
     */
    public ProductMain selectProductMainById(Long id);


    /**
     * 查询产品编号
     *
     * @param yxNumber
     * @return 产品管理
     */
    public int yxNumberSearch(String yxNumber);

    /**
     * 查询产品管理列表
     * 
     * @param productMain 产品管理
     * @return 产品管理集合
     */
    public List<ProductMain> selectProductMainList(ProductMain productMain);

    /**
     * 新增产品管理
     * 
     * @param productMain 产品管理
     * @return 结果
     */
    public int insertProductMain(ProductMain productMain);

    /**
     * 修改产品管理
     * 
     * @param productMain 产品管理
     * @return 结果
     */
    public int updateProductMain(ProductMain productMain);

    /**
     * 批量删除产品管理
     * 
     * @param ids 需要删除的产品管理主键集合
     * @return 结果
     */
    public int deleteProductMainByIds(String ids);

    /**
     * 删除产品管理信息
     * 
     * @param id 产品管理主键
     * @return 结果
     */
    public int deleteProductMainById(Long id);

    /**
     * 产品添加
     * @param productMain
     * @return
     */
    public int mainAdd(ProductMain productMain);


    /**
     * 删除图片
     * @param request
     * @return
     */
    public int deleteImg(HttpServletRequest request);


    /**
     * 前端产品数据显示
     * @param productMain
     * @return
     */
    public List<ProductMain> leadingProductMainList(ProductMain productMain);


    /**
     * 图片显示
     *
     * @param request  请求
     * @param response 响应
     * @author xin_xin
     * @date 2022-11-05 14:56:37
     */
    public void pictureDisplay(HttpServletRequest request, HttpServletResponse response);
}
