package com.ruoyi.backstage.service;

import java.util.List;
import com.ruoyi.backstage.domain.PWarehouse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 仓库标签Service接口
 * 
 * @author xin_xin
 * @date 2022-07-12
 */
public interface IPWarehouseService 
{
    /**
     * 查询仓库标签
     * 
     * @param id 仓库标签主键
     * @return 仓库标签
     */
    public PWarehouse selectPWarehouseById(Long id);

    /**
     * 查询仓库标签列表
     * 
     * @param pWarehouse 仓库标签
     * @return 仓库标签集合
     */
    public List<PWarehouse> selectPWarehouseList(PWarehouse pWarehouse);

    /**
     * 新增仓库标签
     * 
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    public int insertPWarehouse(PWarehouse pWarehouse);

    /**
     * 修改仓库标签
     * 
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    public int updatePWarehouse(PWarehouse pWarehouse);

    /**
     * 批量删除仓库标签
     * 
     * @param ids 需要删除的仓库标签主键集合
     * @return 结果
     */
    public int deletePWarehouseByIds(String ids);

    /**
     * 删除仓库标签信息
     * 
     * @param id 仓库标签主键
     * @return 结果
     */
    public int deletePWarehouseById(Long id);



    /**
     * 订单操作
     * @param id
     * @return
     */
    public int ordersAccess(Long id, Long number);


    /**
     * 扫码入库-选库位
     *
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    public int sweepCodeAccessEdit(PWarehouse pWarehouse);


    /**
     * 扫码入库-扫库位码
     *
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    public int shelfBarCodeAccessEdit(PWarehouse pWarehouse);

    /**
     * 扫码出库
     *
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    public int sweepCodeOutboundEdit(PWarehouse pWarehouse);


}
