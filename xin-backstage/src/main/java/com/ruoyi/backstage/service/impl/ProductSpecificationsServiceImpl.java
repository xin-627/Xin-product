package com.ruoyi.backstage.service.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.backstage.util.UtilGetSys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.ProductSpecificationsMapper;
import com.ruoyi.backstage.domain.ProductSpecifications;
import com.ruoyi.backstage.service.ProductSpecificationsService;
import com.ruoyi.common.core.text.Convert;


/**
 * 产品规格组Service业务层处理
 *
 * @author xin-xin
 * @date 2022-01-05
 */
@Service
public class ProductSpecificationsServiceImpl implements ProductSpecificationsService {
    @Autowired
    private ProductSpecificationsMapper productSpecificationsMapper;

    /**
     * 查询产品规格组
     *
     * @param id 产品规格组主键
     * @return 产品规格组
     */
    @Override
    public ProductSpecifications selectProductSpecificationsById(Long id) {
        return productSpecificationsMapper.selectProductSpecificationsById(id);
    }

    /**
     * 查询产品规格组列表
     *
     * @param productSpecifications 产品规格组
     * @return 产品规格组
     */
    @Override
    public List<ProductSpecifications> selectProductSpecificationsList(ProductSpecifications productSpecifications) {
        return productSpecificationsMapper.selectProductSpecificationsList(productSpecifications);
    }

    /**
     * 新增产品规格组
     *
     * @param productSpecifications 产品规格组
     * @return 结果
     */
    @Override
    public int insertProductSpecifications(ProductSpecifications productSpecifications) {
        return productSpecificationsMapper.insertProductSpecifications(productSpecifications);
    }

    /**
     * 修改产品规格组
     *
     * @param productSpecifications 产品规格组
     * @return 结果
     */
    @Override
    public int updateProductSpecifications(ProductSpecifications productSpecifications) {
        int integer;
        Map<String,Object> params = new HashMap();
        params.put("id", productSpecifications.getId());
        params.put("specifications", productSpecifications.getSpecifications());
        params.put("productId", productSpecifications.getProductId());
        List<ProductSpecifications> pigmentData = productSpecificationsMapper.pecificationsDuplicateChecking(params);//获取是否已有数据
        if (pigmentData.size() == 0) {
            productSpecifications.setRefreshName(UtilGetSys.getUserName());//修改人
            integer = productSpecificationsMapper.updateProductSpecifications(productSpecifications);
        }else{
           integer = -1 ;
        }
        return integer;
    }

    /**
     * 批量删除产品规格组
     *
     * @param ids 需要删除的产品规格组主键
     * @return 结果
     */
    @Override
    public int deleteProductSpecificationsByIds(String ids) {
        return productSpecificationsMapper.deleteProductSpecificationsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除产品规格组信息
     *
     * @param id 产品规格组主键
     * @return 结果
     */
    @Override
    public int deleteProductSpecificationsById(Long id) {
        return productSpecificationsMapper.deleteProductSpecificationsById(id);
    }

    @Override
    public int specificationsAdd(ProductSpecifications data) {
        int integer;
        Map<String, Object> params = new HashMap();
        params.put("specifications", data.getSpecifications());
        params.put("productId", data.getProductId());
        List<ProductSpecifications> specificationsData = productSpecificationsMapper.pecificationsDuplicateChecking(params);//获取是否已有数据
        if (specificationsData.size() == 0) {
            data.setCreator(UtilGetSys.getUserName());//创建人
            integer = productSpecificationsMapper.insertProductSpecifications(data);
        } else {
            integer = -1;
        }
        return integer;
    }

}
