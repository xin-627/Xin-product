package com.ruoyi.backstage.service;

import java.util.List;

import com.ruoyi.backstage.domain.ProductSpecifications;

/**
 * 产品规格组Service接口
 * 
 * @author xin-xin
 * @date 2022-01-05
 */
public interface ProductSpecificationsService
{
    /**
     * 查询产品规格组
     * 
     * @param id 产品规格组主键
     * @return 产品规格组
     */
    public ProductSpecifications selectProductSpecificationsById(Long id);

    /**
     * 查询产品规格组列表
     * 
     * @param productSpecifications 产品规格组
     * @return 产品规格组集合
     */
    public List<ProductSpecifications> selectProductSpecificationsList(ProductSpecifications productSpecifications);

    /**
     * 新增产品规格组
     * 
     * @param productSpecifications 产品规格组
     * @return 结果
     */
    public int insertProductSpecifications(ProductSpecifications productSpecifications);

    /**
     * 修改产品规格组
     * 
     * @param productSpecifications 产品规格组
     * @return 结果
     */
    public int updateProductSpecifications(ProductSpecifications productSpecifications);

    /**
     * 批量删除产品规格组
     * 
     * @param ids 需要删除的产品规格组主键集合
     * @return 结果
     */
    public int deleteProductSpecificationsByIds(String ids);

    /**
     * 删除产品规格组信息
     * 
     * @param id 产品规格组主键
     * @return 结果
     */
    public int deleteProductSpecificationsById(Long id);

    /**
     * 规格添加
     * @param data
     * @return
     */
    public int specificationsAdd(ProductSpecifications data);
}
