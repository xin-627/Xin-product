package com.ruoyi.backstage.service;

import java.util.List;
import com.ruoyi.backstage.domain.ProductPigment;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 产品色号组Service接口
 * 
 * @author xin-xin
 * @date 2022-01-05
 */
public interface ProductPigmentService
{
    /**
     * 查询产品色号组
     * 
     * @param id 产品色号组主键
     * @return 产品色号组
     */
    public ProductPigment selectProductPigmentById(Long id);

    /**
     * 查询产品色号组列表
     * 
     * @param productPigment 产品色号组
     * @return 产品色号组集合
     */
    public List<ProductPigment> selectProductPigmentList(ProductPigment productPigment);

    /**
     * 新增产品色号组
     * 
     * @param productPigment 产品色号组
     * @return 结果
     */
    public int insertProductPigment(ProductPigment productPigment);

    /**
     * 修改产品色号组
     * 
     * @param productPigment 产品色号组
     * @return 结果
     */
    public int updateProductPigment(ProductPigment productPigment);


    /**
     * 批量删除产品色号组
     * 
     * @param ids 需要删除的产品色号组主键集合
     * @return 结果
     */
    public int deleteProductPigmentByIds(String ids);

    /**
     * 删除产品颜色组信息
     * 
     * @param id 产品颜色组主键
     * @return 结果
     */
    public int deleteProductPigmentById(Long id);


    /**
     * 产品颜色添加
     * @param productPigment
     * @return
     */
    public int pigmentAdd(ProductPigment productPigment);

    /**
     * 删除图片
     * @param request
     * @return
     */
    public int deleteImg(HttpServletRequest request);

    /**
     * 编辑 验证色号是否重复
     * @param productPigment
     * @return
     */
    public int pigmentDuplicateChecking(ProductPigment productPigment);

}
