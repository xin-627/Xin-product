package com.ruoyi.backstage.service;

import java.util.List;
import com.ruoyi.backstage.domain.PLogo;
import com.ruoyi.backstage.domain.ProductCarousel;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * logoService接口
 * 
 * @author xin-xin
 * @date 2022-06-08
 */
public interface IPLogoService 
{
    /**
     * 查询logo
     * 
     * @param id logo主键
     * @return logo
     */
    public PLogo selectPLogoById(Long id);

    /**
     * 查询logo列表
     * 
     * @param pLogo logo
     * @return logo集合
     */
    public List<PLogo> selectPLogoList(PLogo pLogo);

    /**
     * 新增logo
     * 
     * @param pLogo logo
     * @return 结果
     */
    public int insertPLogo(PLogo pLogo);

    /**
     * 修改logo
     * 
     * @param pLogo logo
     * @return 结果
     */
    public int updatePLogo(PLogo pLogo);

    /**
     * 批量删除logo
     * 
     * @param ids 需要删除的logo主键集合
     * @return 结果
     */
    public int deletePLogoByIds(String ids);

    /**
     * 删除logo信息
     * 
     * @param id logo主键
     * @return 结果
     */
    public int deletePLogoById(Long id);



    /**
     * logo图删除
     * @param request
     * @return
     */
    public int deleteImg(HttpServletRequest request);


    /**
     * 图片显示 logo
     *
     * @param request  请求
     * @param response 响应
     * @author xin_xin
     * @date 2022-11-05 15:31:34
     */
    public void pictureDisplayLogo(HttpServletRequest request, HttpServletResponse response);
}
