package com.ruoyi.backstage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.POpinionMapper;
import com.ruoyi.backstage.domain.POpinion;
import com.ruoyi.backstage.service.IPOpinionService;
import com.ruoyi.common.core.text.Convert;

/**
 * 意见与建议Service业务层处理
 * 
 * @author xin_xin
 * @date 2022-05-13
 */
@Service
public class POpinionServiceImpl implements IPOpinionService 
{
    @Autowired
    private POpinionMapper pOpinionMapper;

    /**
     * 查询意见与建议
     * 
     * @param id 意见与建议主键
     * @return 意见与建议
     */
    @Override
    public POpinion selectPOpinionById(Long id)
    {
        return pOpinionMapper.selectPOpinionById(id);
    }

    /**
     * 查询意见与建议列表
     * 
     * @param pOpinion 意见与建议
     * @return 意见与建议
     */
    @Override
    public List<POpinion> selectPOpinionList(POpinion pOpinion)
    {
        return pOpinionMapper.selectPOpinionList(pOpinion);
    }

    /**
     * 新增意见与建议
     * 
     * @param pOpinion 意见与建议
     * @return 结果
     */
    @Override
    public int insertPOpinion(POpinion pOpinion)
    {
        pOpinion.setCreateTime(DateUtils.getNowDate());
        return pOpinionMapper.insertPOpinion(pOpinion);
    }

    /**
     * 修改意见与建议
     * 
     * @param pOpinion 意见与建议
     * @return 结果
     */
    @Override
    public int updatePOpinion(POpinion pOpinion)
    {
        pOpinion.setUpdateTime(DateUtils.getNowDate());
        return pOpinionMapper.updatePOpinion(pOpinion);
    }

    /**
     * 批量删除意见与建议
     * 
     * @param ids 需要删除的意见与建议主键
     * @return 结果
     */
    @Override
    public int deletePOpinionByIds(String ids)
    {
        return pOpinionMapper.deletePOpinionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除意见与建议信息
     * 
     * @param id 意见与建议主键
     * @return 结果
     */
    @Override
    public int deletePOpinionById(Long id)
    {
        return pOpinionMapper.deletePOpinionById(id);
    }
}
