package com.ruoyi.backstage.service;

import java.util.List;

import com.ruoyi.backstage.domain.ProductMain;
import com.ruoyi.backstage.domain.ProductOrderInformation;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;

import javax.servlet.http.HttpServletRequest;

/**
 * 订单管理Service接口
 * 
 * @author xin-xin
 * @date 2022-02-16
 */
public interface ProductOrderInformationService
{
    /**
     * 查询订单管理
     * 
     * @param id 订单管理主键
     * @return 订单管理
     */
    public ProductOrderInformation selectProductOrderInformationById(Long id);

    /**
     * 查询订单管理列表
     * 
     * @param productOrderInformation 订单管理
     * @return 订单管理集合
     */
    public List<ProductOrderInformation> selectProductOrderInformationList(ProductOrderInformation productOrderInformation);


    /**
     * 查询订单管理列表
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理集合
     */
    public List<ProductOrderInformation> selectAllOrdersList(ProductOrderInformation productOrderInformation);


    /**
     * 客服- 订单管理列表
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理集合
     */
    public List<ProductOrderInformation> selectAfterSaleList(ProductOrderInformation productOrderInformation);

    /**
     * 新增订单管理
     * 
     * @param productOrderInformation 订单管理
     * @return 结果
     */
    public int insertProductOrderInformation(ProductOrderInformation productOrderInformation);

    /**
     * 修改订单管理
     * 
     * @param productOrderInformation 订单管理
     * @return 结果
     */
    public int updateProductOrderInformation(ProductOrderInformation productOrderInformation);

    /**
     * 财务核对订单修改
     *
     * @param productOrderInformation 订单管理
     * @return 结果
     */
    public int financialCheckUpdates(ProductOrderInformation productOrderInformation);

    /**
     * 批量删除订单管理
     * 
     * @param ids 需要删除的订单管理主键集合
     * @return 结果
     */
    public int deleteProductOrderInformationByIds(String ids);

    /**
     * 删除订单管理信息
     * 
     * @param id 订单管理主键
     * @return 结果
     */
    public int deleteProductOrderInformationById(Long id);

    /**
     * 订单操作
     * @param request
     * @param id
     * @return
     */
    public int check(HttpServletRequest request, String id);

    /**
     * 订单操作
     * @param request
     * @param id
     * @return
     */
    public int cCheckObliterateUpdate(HttpServletRequest request, String id);
    /**
     * 仓库数据更新
     * @return
     */
    public int warehouseUpdate(ProductOrderInformation productOrderInformation);

    /**
     * 订单采购 数据修改
     * @param request
     * @param id
     * @return
     */
    public int purchasingDataUpdate(HttpServletRequest request, String id);

    /**
     * 查询订单主列表
     *
     * @param productOrderInformation 订单主
     * @return 订单主集合
     */
    public List<ProductOrderInformation> selectCustomerGroupList(ProductOrderInformation productOrderInformation);

    /**
     * 数据统计查询
     *
     */
    public ProductOrderInformation dataStatistics(Long statusId);


    /**
     * 查询订单条码列表
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理集合
     */
    public List<ProductOrderInformation> selectBarCodeList(ProductOrderInformation productOrderInformation);

    /**
     * 已出库订单查询
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理集合
     */
    public List<ProductOrderInformation> selectHaveOutboundList(ProductOrderInformation productOrderInformation);


    /**
     * 出库时
     * 订单查询
     * @param productOrderInformation 订单管理
     * @return 订单管理集合
     */
    public List<ProductOrderInformation> selectSweepCodeOutboundList(ProductOrderInformation productOrderInformation);


    /**
     * 查询销量数据
     * @return
     */
    public  List<ProductOrderInformation> searchSalesVolumeList(ProductOrderInformation productOrderInformation);


    /**
     * 产品占比
     * @return
     */
    public  List<ProductOrderInformation> searchClassifyList(ProductOrderInformation productOrderInformation);


    /**
     * 下单量排行
     * @return
     */
    public  List<ProductOrderInformation> searchPlaceAnOrderNumList();


    /**
     * 订单出入库记录查询
     */
    public List<ProductOrderInformation> selectAccessRecordsList(ProductOrderInformation productOrderInformation);


    /**
     * 根据库位id 查询订单数据
     */
    public List<ProductOrderInformation> selectLocationIdList(ProductOrderInformation productOrderInformation);


    /**
     * 查询已入库_没有条码数据的订单
     */
    public List<ProductOrderInformation> selectNoBarCodeNumberList(ProductOrderInformation productOrderInformation);

    /**
     * 已入库 但没有条码的订单 update
     * @param id
     * @return
     */
    public int noBarCodeNumberUpdate(String id);

    /**
     * 维修单修改
     *
     * @param productOrderInformation 订单管理
     * @return 结果
     */
    public int maintainUpdate(ProductOrderInformation productOrderInformation);


    /**
     * 客服打印列表
     *
     * @param productOrderInformation 产品订单信息
     * @return {@link List }<{@link ProductOrderInformation }>
     * @author xin_xin
     * @date 2022-10-20 09:58:20
     */
    public List<ProductOrderInformation> selectAfterSalesPrint(ProductOrderInformation productOrderInformation);

    /**
     *  维修单驳回数量
     *
     * @return int
     * @author xin_xin
     * @date 2022-11-18 17:00:43
     */
    public ProductOrderInformation selectMaintainReject();
}
