package com.ruoyi.backstage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.PUpdateLogMapper;
import com.ruoyi.backstage.domain.PUpdateLog;
import com.ruoyi.backstage.service.IPUpdateLogService;
import com.ruoyi.common.core.text.Convert;

/**
 * 更新日志Service业务层处理
 * 
 * @author xin_xin
 * @date 2022-05-13
 */
@Service
public class PUpdateLogServiceImpl implements IPUpdateLogService 
{
    @Autowired
    private PUpdateLogMapper pUpdateLogMapper;

    /**
     * 查询更新日志
     * 
     * @param id 更新日志主键
     * @return 更新日志
     */
    @Override
    public PUpdateLog selectPUpdateLogById(Long id)
    {
        return pUpdateLogMapper.selectPUpdateLogById(id);
    }

    /**
     * 查询更新日志列表
     * 
     * @param pUpdateLog 更新日志
     * @return 更新日志
     */
    @Override
    public List<PUpdateLog> selectPUpdateLogList(PUpdateLog pUpdateLog)
    {
        return pUpdateLogMapper.selectPUpdateLogList(pUpdateLog);
    }

    /**
     * 新增更新日志
     * 
     * @param pUpdateLog 更新日志
     * @return 结果
     */
    @Override
    public int insertPUpdateLog(PUpdateLog pUpdateLog)
    {
        pUpdateLog.setCreateTime(DateUtils.getNowDate());
        return pUpdateLogMapper.insertPUpdateLog(pUpdateLog);
    }

    /**
     * 修改更新日志
     * 
     * @param pUpdateLog 更新日志
     * @return 结果
     */
    @Override
    public int updatePUpdateLog(PUpdateLog pUpdateLog)
    {
        return pUpdateLogMapper.updatePUpdateLog(pUpdateLog);
    }

    /**
     * 批量删除更新日志
     * 
     * @param ids 需要删除的更新日志主键
     * @return 结果
     */
    @Override
    public int deletePUpdateLogByIds(String ids)
    {
        return pUpdateLogMapper.deletePUpdateLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除更新日志信息
     * 
     * @param id 更新日志主键
     * @return 结果
     */
    @Override
    public int deletePUpdateLogById(Long id)
    {
        return pUpdateLogMapper.deletePUpdateLogById(id);
    }
}
