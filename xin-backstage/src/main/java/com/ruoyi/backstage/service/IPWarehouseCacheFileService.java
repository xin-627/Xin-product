package com.ruoyi.backstage.service;

import java.util.List;
import com.ruoyi.backstage.domain.PWarehouseCacheFile;

/**
 * 库位缓存Service接口
 * 
 * @author xin_xin
 * @date 2022-09-16
 */
public interface IPWarehouseCacheFileService 
{
    /**
     * 查询库位缓存
     * 
     * @param id 库位缓存主键
     * @return 库位缓存
     */
    public PWarehouseCacheFile selectPWarehouseCacheFileById(Long id);

    /**
     * 查询库位缓存列表
     * 
     * @param pWarehouseCacheFile 库位缓存
     * @return 库位缓存集合
     */
    public List<PWarehouseCacheFile> selectPWarehouseCacheFileList(PWarehouseCacheFile pWarehouseCacheFile);

    /**
     * 新增库位缓存
     * 
     * @param pWarehouseCacheFile 库位缓存
     * @return 结果
     */
    public int insertPWarehouseCacheFile(PWarehouseCacheFile pWarehouseCacheFile);

    /**
     * 修改库位缓存
     * 
     * @param pWarehouseCacheFile 库位缓存
     * @return 结果
     */
    public int updatePWarehouseCacheFile(PWarehouseCacheFile pWarehouseCacheFile);

    /**
     * 批量删除库位缓存
     * 
     * @param ids 需要删除的库位缓存主键集合
     * @return 结果
     */
    public int deletePWarehouseCacheFileByIds(String ids);

    /**
     * 删除库位缓存信息
     * 
     * @param id 库位缓存主键
     * @return 结果
     */
    public int deletePWarehouseCacheFileById(Long id);
}
