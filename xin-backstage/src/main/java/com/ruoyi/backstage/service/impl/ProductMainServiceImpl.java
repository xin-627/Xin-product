package com.ruoyi.backstage.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.ruoyi.backstage.util.UtilGetSys;
import com.ruoyi.common.config.ServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.ProductMainMapper;
import com.ruoyi.backstage.domain.ProductMain;
import com.ruoyi.backstage.service.ProductMainService;
import com.ruoyi.common.core.text.Convert;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.ruoyi.common.config.RuoYiConfig.getProfile;

/**
 * 产品管理Service业务层处理
 *
 * @author xin-xin
 * @date 2022-01-08
 */
@Service
public class ProductMainServiceImpl implements ProductMainService {
    @Autowired
    private ProductMainMapper productMainMapper;

    /**
     * 查询产品管理
     *
     * @param id 产品管理主键
     * @return 产品管理
     */
    @Override
    public ProductMain selectProductMainById(Long id) {
        return productMainMapper.selectProductMainById(id);
    }

    /**
     * 查询产品管理
     *
     * @return 产品管理
     */
    @Override
    public int yxNumberSearch(String yxNumber) {
        int integer = 0;
        Map<String, Object> params = new HashMap();
        params.put("yxNumber", yxNumber);
        List<ProductMain> yxNumberData = productMainMapper.yxNumberSearch(params);//获取是否已有数据
        integer = yxNumberData.size();
        return integer;
    }

    /**
     * 查询产品管理列表
     *
     * @param productMain 产品管理
     * @return 产品管理
     */
    @Override
    public List<ProductMain> selectProductMainList(ProductMain productMain) {
        return productMainMapper.selectProductMainList(productMain);
    }

    /**
     * 前端查询产品列数据
     *
     * @param productMain
     * @return
     */
    @Override
    public List<ProductMain> leadingProductMainList(ProductMain productMain) {
        return productMainMapper.leadingProductMainList(productMain);
    }


    /**
     * 新增产品管理
     *
     * @param productMain 产品管理
     * @return 结果
     */
    @Override
    public int insertProductMain(ProductMain productMain) {
        return productMainMapper.insertProductMain(productMain);
    }


    /**
     * 批量删除产品管理
     *
     * @param ids 需要删除的产品管理主键
     * @return 结果
     */
    @Override
    public int deleteProductMainByIds(String ids) {
        return productMainMapper.deleteProductMainByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除产品管理信息
     *
     * @param id 产品管理主键
     * @return 结果
     */
    @Override
    public int deleteProductMainById(Long id) {
        return productMainMapper.deleteProductMainById(id);
    }


    /**
     * 添加
     *
     * @param productMain
     * @return
     */
    @Override
    public int mainAdd(ProductMain productMain) {
        int integer;
        Map<String, Object> params = new HashMap();
        params.put("yxNumber", productMain.getYxNumber());
        List<ProductMain> yxNumberData = productMainMapper.yxNumberSearch(params);//获取是否已有数据
        if (yxNumberData.size() == 0) {
            productMain.setCreator(UtilGetSys.getUserName());//创建人
            integer = productMainMapper.insertProductMain(productMain);
        } else if (yxNumberData.size() > 1) {
            integer = -1;
        } else {
            integer = -2;
        }
        return integer;
    }

    /**
     * 产品管理编辑
     *
     * @param productMain 产品管理
     * @return int
     * @author xin_xin
     * @date 2022-10-27 19:15:01
     */
    @Override
    public int updateProductMain(ProductMain productMain) {
        productMain.setRefreshName(UtilGetSys.getUserName());//更新者
        return productMainMapper.updateProductMain(productMain);
    }

    /**
     * 删除图片
     *
     * @param request
     * @return
     */
    @Override
    public int deleteImg(HttpServletRequest request) {
        int integer = 0;
        String imgUrl = request.getParameter("imgUrl");//图片路径
        List<String> list = Arrays.asList(imgUrl.split(","));
        if(list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                String data = list.get(i).replace("/profile/", "/");//删除关键词
                File file = new File(getProfile() + data);
                if (file.exists() == true) {
                    //判断图片是否存在
                    Boolean flag = false;
                    flag = file.delete();
                    if (flag) {
                        integer = 200;
                    } else {
                        integer = 500;
                    }
                } else {
                    integer = 404;
                }

            }
        }else{
            integer = 404;
        }
        return integer;
    }

    @Override
    public void pictureDisplay(HttpServletRequest request, HttpServletResponse response) {
        String imgUrl = request.getParameter("imgUrl");//获取文件名
        String imgData = imgUrl.replace("/profile/", "/");//删除关键词
        String filePath = UtilGetSys.getPath() + imgData;//图地址
        FileInputStream in;
        try {
            request.setCharacterEncoding("utf-8");
            //页面img标签中src中传入的是真实图片地址路径
            String path = filePath;
            String filePathECoding = new String(path.trim().getBytes("UTF-8"), "UTF-8");
            response.setContentType("application/octet-stream;charset=UTF-8");
            //图片读取路径
            in = new FileInputStream(filePathECoding);
            // 得到文件大小
            int i = in.available();
            //创建存放文件内容的数组
            byte[] data = new byte[i];
            in.read(data);
            in.close();
            //把图片写出去
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            outputStream.write(data);
            //将缓存区的数据进行输出
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            //e.printStackTrace();//输出错误,控制台会报错
            System.out.println(e.getMessage());//仅输出错误警告
            return;
        }
    }


}
