package com.ruoyi.backstage.service.impl;

import java.util.*;

import com.ruoyi.backstage.domain.PWarehouse;
import com.ruoyi.backstage.domain.PWarehouseAccessRecords;
import com.ruoyi.backstage.domain.ProductMain;
import com.ruoyi.backstage.mapper.PWarehouseAccessRecordsMapper;
import com.ruoyi.backstage.mapper.PWarehouseMapper;
import com.ruoyi.backstage.mapper.ProductMainMapper;
import com.ruoyi.backstage.util.UtilDate;
import com.ruoyi.backstage.util.UtilGetSys;
import com.ruoyi.backstage.util.maintainNumberGenerate;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.ProductOrderInformationMapper;
import com.ruoyi.backstage.domain.ProductOrderInformation;
import com.ruoyi.backstage.service.ProductOrderInformationService;
import com.ruoyi.common.core.text.Convert;

import javax.servlet.http.HttpServletRequest;

import static com.ruoyi.backstage.util.UtilDate.switchingTime;
import static com.ruoyi.backstage.util.UtilDate.switchingTimeShort;

/**
 * 订单管理Service业务层处理
 *
 * @author xin-xin
 * @date 2022-02-16
 */
@Service
public class ProductOrderInformationServiceImpl implements ProductOrderInformationService {
    @Autowired
    private ProductOrderInformationMapper productOrderInformationMapper;

    @Autowired
    private ProductMainMapper productMainMapper;

    @Autowired
    private PWarehouseMapper pWarehouseMapper;

    @Autowired
    private PWarehouseAccessRecordsMapper pWarehouseAccessRecordsMapper;

    @Autowired
    private SysDictDataMapper dictDataMapper;

    /**
     * 下单量排行
     *
     * @return
     */
    @Override
    public List<ProductOrderInformation> searchPlaceAnOrderNumList() {
        return productOrderInformationMapper.searchPlaceAnOrderNumList();
    }

    /**
     * 产品占比
     *
     * @param productOrderInformation 产品订单信息
     * @return {@link List }<{@link ProductOrderInformation }>
     * @author xin_xin
     * @date 2022/10/13
     */
    @Override
    public List<ProductOrderInformation> searchClassifyList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.searchClassifyList(productOrderInformation);
    }

    /**
     * 查询销量数据
     *
     * @return
     */
    @Override
    public List<ProductOrderInformation> searchSalesVolumeList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.searchSalesVolumeList(productOrderInformation);
    }


    /**
     * 查询订单管理
     *
     * @param id 订单管理主键
     * @return 订单管理
     */
    @Override
    public ProductOrderInformation selectProductOrderInformationById(Long id) {
        return productOrderInformationMapper.selectProductOrderInformationById(id);
    }

    /**
     * 统计数据查询
     */
    @Override
    public ProductOrderInformation dataStatistics(Long statusId) {
        ProductOrderInformation d = productOrderInformationMapper.dataStatistics(statusId);
        return d;
    }

    /**
     * 查询订单管理列表
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理
     */
    @Override
    public List<ProductOrderInformation> selectProductOrderInformationList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.selectProductOrderInformationList(productOrderInformation);
    }

    /**
     * 查询订单管理列表
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理
     */
    @Override
    public List<ProductOrderInformation> selectAllOrdersList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.selectAllOrdersList(productOrderInformation);
    }


    /**
     * 查询订单管理列表
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理
     */
    @Override
    public List<ProductOrderInformation> selectAfterSaleList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.selectAfterSaleList(productOrderInformation);
    }

    /**
     * 查询客户订单信息
     */
    @Override
    public List<ProductOrderInformation> selectCustomerGroupList(ProductOrderInformation productOrderInformation) {
        productOrderInformationMapper.updateClientInformation();//同步客户表数据
        return productOrderInformationMapper.selectCustomerGroupList(productOrderInformation);
    }

    /**
     * 修改订单管理
     *
     * @param productOrderInformation 订单管理
     * @return 结果
     */
    @Override
    public int updateProductOrderInformation(ProductOrderInformation productOrderInformation) {
        int integer = 0;
        productOrderInformation.setRefreshName(UtilGetSys.getUserName());//更新者
        integer = productOrderInformationMapper.updateProductOrderInformation(productOrderInformation);
        if (integer > 0) {
            productOrderInformationMapper.purchasingDataUpdate();
            //更新 全部 计划到货时间 需要回厂时间
        }
        return integer;
    }


    /**
     * 修改订单管理
     *
     * @param productOrderInformation 订单管理
     * @return 结果
     */
    @Override
    public int financialCheckUpdates(ProductOrderInformation productOrderInformation) {
        int integer = 0;
        if (StringUtils.isNotEmpty(productOrderInformation.getcCheckResult()) && productOrderInformation.getcCheckResult().equals("checkCompleted")) {
            productOrderInformation.setcCheckResult("√");
            productOrderInformation.setcCheckState(Long.valueOf("2"));
            productOrderInformation.setcCheckTime(getDate());
        }
        productOrderInformation.setRefreshName(UtilGetSys.getUserName());//更新者
        integer = productOrderInformationMapper.updateProductOrderInformation(productOrderInformation);
        return integer;
    }

    /**
     * 修改订单管理
     *
     * @return 结果
     */
    @Override
    public int warehouseUpdate(ProductOrderInformation productOrderInformation) {
        int integer = 0;
        productOrderInformation.setRefreshName(UtilGetSys.getUserName());//更新者
        integer = productOrderInformationMapper.updateProductOrderInformation(productOrderInformation);
        return integer;
    }


    /**
     * 批量删除订单管理
     *
     * @param ids 需要删除的订单管理主键
     * @return 结果
     */
    @Override
    public int deleteProductOrderInformationByIds(String ids) {
        return productOrderInformationMapper.deleteProductOrderInformationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单管理信息
     *
     * @param id 订单管理主键
     * @return 结果
     */
    @Override
    public int deleteProductOrderInformationById(Long id) {
        return productOrderInformationMapper.deleteProductOrderInformationById(id);
    }


    /**
     * 订单操作
     *
     * @param request
     * @param id
     * @return
     */
    @Override
    public int check(HttpServletRequest request, String id) {
        int integer = 0;
        String checkStatus = request.getParameter("checkStatus");//操作标识
        String overrule = request.getParameter("overrule");//驳回理由
        String deliveryMethod = request.getParameter("deliveryMethod");//提货方式
        List<String> list = Arrays.asList(id.split(","));
        //必须判断空值 前端传值有空不然会覆盖
        for (int i = 0; i < list.size(); i++) {
            ProductOrderInformation info = new ProductOrderInformation();
            info.setId(Long.valueOf(list.get(i)));
            if (StringUtils.isNotEmpty(checkStatus)) {
                if (checkStatus.equals("approve")) {//审核通过
                    info.setStatusCode(Long.valueOf("20"));
                    info.setStateDescription("待采购");
                    info.setAuditTime(getDate());//审核时间
                    info.setcGiveTime(getDateShort());//采购接单时间
                    info.setcCheckState(Long.valueOf("1"));//核算员核对状态
                    productMainMapper.salesVolumeUpdate();//更新销量
                }
                if (checkStatus.equals("refuse")) {//财务审核-不通过
                    String overruleData = getOverrule(list.get(i));
                    info.setStatusCode(Long.valueOf("15"));
                    info.setStateDescription("财务退回");
                    info.setOverrule(overrule + "　　" + overruleData);
                    info.setAuditTime(getDate());
                    info.setcCheckState(Long.valueOf("0"));//核算员核对状态
                }
                if (checkStatus.equals("cancel")) {//取消采购
                    info.setStatusCode(Long.valueOf("20"));
                    info.setStateDescription("取消采购");
                    info.setAuditTime(getDate());
                    info.setcCheckState(Long.valueOf("0"));//核算员核对状态
                }
                if (checkStatus.equals("confirmArrive")) {//确认回厂
                    info.setStatusCode(Long.valueOf("30"));
                    info.setStateDescription("已入库");
                    info.setcRevertTime(getDateShort());
                }
                if (checkStatus.equals("warehouseOutbound")) {//确认出库
                    info.setStatusCode(Long.valueOf("35"));
                    info.setStateDescription("已出库");
                    info.setDeliveryMethod(deliveryMethod);//提货方式
                    info.setDeliveryTime(getDate());
                }
                if (checkStatus.equals("cCheckEmptyData")) {//财务核算-清除供应商数据
                    info.setSupplier("");
                }
                if (checkStatus.equals("cCheckRevocation")) {//财务核算-撤销核对
                    info.setcCheckResult("");
                    info.setcCheckState(Long.valueOf("1"));
                }
                if (checkStatus.equals("cCheckAccomplish")) {//财务核算-核对完成
                    info.setcCheckResult("√");
                    info.setcCheckState(Long.valueOf("2"));
                    info.setcCheckTime(getDate());
                }
                if (checkStatus.equals("confirmPrint")) {//售后确定打印
                    info.setPrintStatus(1L);

                }
                if (checkStatus.equals("cancelPrint")) {//售后取消打印
                    info.setPrintStatus(2L);
                }
                info.setRefreshName(UtilGetSys.getUserName());//更新者
            }
            integer = productOrderInformationMapper.orderReview(info);
        }
        return integer;
    }

    /**
     * 清除核回厂数据
     *
     * @param request
     * @param id
     * @return
     */
    @Override
    public int cCheckObliterateUpdate(HttpServletRequest request, String id) {
        int integer = 0;
        List<String> list = Arrays.asList(id.split(","));
        //必须判断空值 前端传值有空不然会覆盖
        for (int i = 0; i < list.size(); i++) {
            ProductOrderInformation info = new ProductOrderInformation();
            info.setId(Long.valueOf(list.get(i)));
            info.setRefreshName(UtilGetSys.getUserName());//更新者
            integer = productOrderInformationMapper.cCheckObliterateUpdate(info);
        }
        return integer;
    }


    /**
     * 订单采购
     *
     * @param request
     * @param id
     * @return
     */
    @Override
    public int purchasingDataUpdate(HttpServletRequest request, String id) {
        int integer = 0;
        String cPurchaseTime = request.getParameter("cPurchaseTime");//采购时间
        String supplier = request.getParameter("supplier");//供应商
        String cPeriod = request.getParameter("cPeriod");//周期
        String cRevertTime = request.getParameter("cRevertTime");//实际回厂时间
        String cProcurementRemark = request.getParameter("cProcurementRemark");//采购备注
        String checkStatus = request.getParameter("checkStatus");//退回标识
        String overrule = request.getParameter("overrule");//退回理由
        List<String> list = Arrays.asList(id.split(","));
        //必须判断空值 前端传值有空不然会覆盖
        for (int i = 0; i < list.size(); i++) {
            ProductOrderInformation info = new ProductOrderInformation();
            info.setId(Long.valueOf(list.get(i)));
            if (StringUtils.isNotEmpty(cPurchaseTime)) {//采购时间
                info.setcPurchaseTime(switchingTimeShort(cPurchaseTime));
                info.setStateDescription("待回厂");
                info.setStatusCode(Long.valueOf("25"));
            }
            if (StringUtils.isNotEmpty(supplier)) {//供应商
                info.setSupplier(supplier);
            }
            if (StringUtils.isNotEmpty(cPeriod)) {//周期
                info.setcPeriod(Long.valueOf(cPeriod));
            }
            if (StringUtils.isNotEmpty(checkStatus)) {
                String overruleData = getOverrule(list.get(i));
                if (checkStatus.equals("refuse")) {//采购退回
                    info.setStatusCode(Long.valueOf("15"));
                    info.setStateDescription("采购退回");
                    info.setOverrule(overrule + "　　" + overruleData);
                    info.setAuditTime(getDate());//审核时间
                    info.setcCheckState(Long.valueOf("0"));//核算员核对状态
                }
                if (checkStatus.equals("refuseCw")) {//采购退回至财务
                    info.setStatusCode(Long.valueOf("10"));
                    info.setStateDescription("待审核");
                    info.setAuditTime(getDate());
                    info.setcCheckState(Long.valueOf("0"));//核算员核对状态
                }
            }

            if (StringUtils.isNotEmpty(cRevertTime)) {//实际回厂时间
                if (info.getStatusCode().equals(25)) {
                    info.setcRevertTime(switchingTimeShort(cRevertTime));
                    info.setStateDescription("已入库");
                    info.setStatusCode(Long.valueOf("30"));
                } else {
                    integer = -1;
                    return integer;
                }

            }
            if (StringUtils.isNotEmpty(cProcurementRemark)) {//采购备注
                info.setcProcurementRemark(cProcurementRemark);
            }
            info.setRefreshName(UtilGetSys.getUserName());//更新者
            integer = productOrderInformationMapper.orderReview(info);
        }
        return integer;
    }


    /**
     * 查询 退回理由
     *
     * @param id
     * @return
     */
    private String getOverrule(String id) {
        String overruleData = "";
        ProductOrderInformation overruleSearch = productOrderInformationMapper.overruleSearch(Long.valueOf(id));
        if (overruleSearch == null) {
            overruleData = "";
        } else {
            overruleData = String.valueOf(overruleSearch.getOverrule());
        }
        return overruleData;
    }

    /**
     * 查询条码订单列表
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理
     */
    @Override
    public List<ProductOrderInformation> selectBarCodeList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.selectBarCodeList(productOrderInformation);
    }

    /**
     * 已出库订单查询列表
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理
     */
    @Override
    public List<ProductOrderInformation> selectHaveOutboundList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.selectHaveOutboundList(productOrderInformation);
    }

    /**
     * 出入库库时订单查询
     *
     * @param productOrderInformation 订单管理
     * @return 订单管理
     */
    @Override
    public List<ProductOrderInformation> selectSweepCodeOutboundList(ProductOrderInformation productOrderInformation) {
        Long orderId;
        ProductOrderInformation data = new ProductOrderInformation();
        String barCode = productOrderInformation.getBarCode();
        boolean barCodeResult = barCode.startsWith("KWM-");
        if (barCodeResult == true) {
            orderId = Long.valueOf(-1);
            data.setId(orderId);
        }
        if (barCodeResult == false) {
            if (StringUtils.isNotEmpty(productOrderInformation.getBarCode())) {
                PWarehouse orderData = pWarehouseMapper.selectOrderData(Long.valueOf(productOrderInformation.getBarCode()));
                orderId = orderData.getOrdersId();
            } else {
                orderId = Long.valueOf(-1);
            }
            data.setId(orderId);
        }
        return productOrderInformationMapper.selectBarCodeList(data);
    }


    /**
     * 订单出入库记录查询
     */
    @Override
    public List<ProductOrderInformation> selectAccessRecordsList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.selectAccessRecordsList(productOrderInformation);
    }

    /**
     * 根据库位id查询订单数据 2022-09-27
     */
    @Override
    public List<ProductOrderInformation> selectLocationIdList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.selectLocationIdList(productOrderInformation);
    }

    /**
     * 查询已入库_没有条码数据的订单
     *
     * @param productOrderInformation
     * @return
     */
    @Override
    public List<ProductOrderInformation> selectNoBarCodeNumberList(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.selectNoBarCodeNumberList(productOrderInformation);
    }

    /**
     * 已入库订单-生成条码-并出库
     *
     * @param id
     * @return
     */
    @Override
    public int noBarCodeNumberUpdate(String id) {
        int integer = 0;
        if (StringUtils.isNotEmpty(id)) {
            //必须判断空值 前端传值有空不然会覆盖
            ProductOrderInformation orderInformationData = productOrderInformationMapper.selectProductOrderInformationById(Long.valueOf(id));
            String barCode = String.valueOf(System.currentTimeMillis());
            PWarehouse data = new PWarehouse();
            data.setOrdersId(Long.valueOf(id));//订单id
            data.setBarCode(barCode);//条码
            data.setStatusCode(Long.valueOf("2"));//条码状态  "出库"
            data.setEntranceTime(orderInformationData.getcRevertTime());//入库时间
            data.setEntranceName(UtilGetSys.getUserName());//入库人
            data.setSweepName(UtilGetSys.getUserName());//出库人
            data.setSweepTime(getDate());//出库时间-完整时间
            data.setQuantity(1L);
            data.setSerialNumber(1L);
            data.setCreator(UtilGetSys.getUserName());//创建人
            integer = pWarehouseMapper.insertPWarehouse(data);
            if (integer > 0) {
                ProductOrderInformation info = new ProductOrderInformation();
                info.setId(Long.valueOf(id));
                info.setStatusCode(Long.valueOf("35"));//已出库
                info.setDeliveryTime(getDateShort());//出库时间-短时间
                info.setStateDescription("已出库");
                info.setRefreshName(UtilGetSys.getUserName());//更新者
                productOrderInformationMapper.orderReview(info);

                int stateCode = 0;
                PWarehouseAccessRecords recordsRk = getPWarehouseAccessRecords(id);
                recordsRk.setStatusCode(0L);//入库
                stateCode = pWarehouseAccessRecordsMapper.insertPWarehouseAccessRecords(recordsRk);
                if (stateCode > 0) {
                    PWarehouseAccessRecords recordsCk = getPWarehouseAccessRecords(id);
                    recordsCk.setStatusCode(1L);//出库
                    pWarehouseAccessRecordsMapper.insertPWarehouseAccessRecords(recordsCk);
                }
            }
        } else {
            integer = 0;
        }
        return integer;
    }


    private PWarehouseAccessRecords getPWarehouseAccessRecords(String id) {
        PWarehouse orderData = pWarehouseMapper.selectOrderDataById(Long.valueOf(id));
        PWarehouseAccessRecords records = new PWarehouseAccessRecords();
        records.setBarCodeId(orderData.getId());//条码id
        records.setOrdersId(orderData.getOrdersId());//订单id
        records.setShelfId(orderData.getLocationId());//货架id
        return records;
    }


    /**
     * 新增售后维修单
     *
     * @param productOrderInformation 产品订单信息
     * @return int
     * @author xin_xin
     * @date 2022-10-13 14:38:55
     */
    @Override
    public int insertProductOrderInformation(ProductOrderInformation productOrderInformation) {
        int integer = 0;
        String maintainNumber = String.valueOf(0);
        String maxMaintainNumber;
        ProductOrderInformation maxMaintainNumberSearch = productOrderInformationMapper.maxMaintainNumberSearch();//查找最大维修单号
        if (maxMaintainNumberSearch == null) {
            maxMaintainNumber = "WX2210000";
        } else {
            maxMaintainNumber = maxMaintainNumberSearch.getOrderNumber();
        }
        if (productOrderInformation.getNumberType().equals("W")) {
            getDictLabel(productOrderInformation);//获取店名名
            productOrderInformation.setInitialOrderNumber("无订单编号");
            getStatus(productOrderInformation);//状态-公共
        }
        if (productOrderInformation.getNumberType().equals("J")) {
            getDictLabel(productOrderInformation);//获取店名名
            getStatus(productOrderInformation);//状态-公共
        }
        if (productOrderInformation.getNumberType().equals("X")) {
            ProductOrderInformation orderData = productOrderInformationMapper.selectOrderNumber(productOrderInformation.getInitialOrderNumber());
            if (orderData != null) {
                productOrderInformation.setCustomerName(orderData.getCustomerName());//客户名
                productOrderInformation.setCustomerTelephone(orderData.getCustomerTelephone());//客户电话
                productOrderInformation.setCustomerAddress(orderData.getCustomerAddress());//客户地址
                productOrderInformation.setClientNumber(orderData.getClientNumber());//客户编号
                productOrderInformation.setClientId(orderData.getClientId());//客户id
                productOrderInformation.setStatusId(orderData.getStatusId());//下单人id
                productOrderInformation.setStorefront(orderData.getStorefront());//店面
                productOrderInformation.setStorefrontCoding(orderData.getStorefrontCoding());//店面id
                productOrderInformation.setStorefrontType(orderData.getStorefrontType());//店面类型
                productOrderInformation.setcPeriod(orderData.getcPeriod());//采购周期
                productOrderInformation.setProductImg(orderData.getProductImg());//图片
                productOrderInformation.setSupplier(orderData.getSupplier());//供应商
                productOrderInformation.setProductName(orderData.getProductName());//名称
                getStatus(productOrderInformation);//状态-公共
            } else {
                integer = -1;
                return integer;
            }

        }
        maintainNumber = maintainNumberGenerate.getInstance().generateMaintainNumber(maxMaintainNumber);//生成的单号
        productOrderInformation.setYxNumber("维修单");
        productOrderInformation.setOrderNumber(maintainNumber);//维修单号
        productOrderInformation.setOrderTime(getDateShort());//下单时间
        productOrderInformation.setOrderType(2L);//订单类型 1:正常单 2:维修单
        productOrderInformation.setCreator(UtilGetSys.getUserName());//创建者
        integer = productOrderInformationMapper.insertProductOrderInformation(productOrderInformation);
        return integer;
    }

    /**
     * 维修单修改
     *
     * @param productOrderInformation 产品订单信息
     * @return int
     * @author xin_xin
     * @date 2022-10-21 10:33:54
     */
    @Override
    public int maintainUpdate(ProductOrderInformation productOrderInformation) {
        int integer = 0;
        if (productOrderInformation.getStatusCode() < 20) {
            if (productOrderInformation.getNumberType().equals("W")) {
                getDictLabel(productOrderInformation);//获取店名名
                productOrderInformation.setInitialOrderNumber("无订单编号");
                getStatus(productOrderInformation);//状态-公共
            }
            if (productOrderInformation.getNumberType().equals("J")) {
                getDictLabel(productOrderInformation);//获取店名名
                getStatus(productOrderInformation);//状态-公共
            }
            if (productOrderInformation.getNumberType().equals("X")) {
                ProductOrderInformation orderData = productOrderInformationMapper.selectOrderNumber(productOrderInformation.getInitialOrderNumber());

                if (orderData != null) {
                    productOrderInformation.setCustomerName(orderData.getCustomerName());//客户名
                    productOrderInformation.setCustomerTelephone(orderData.getCustomerTelephone());//客户电话
                    productOrderInformation.setCustomerAddress(orderData.getCustomerAddress());//客户地址
                    productOrderInformation.setClientNumber(orderData.getClientNumber());//客户编号
                    productOrderInformation.setClientId(orderData.getClientId());//客户id
                    productOrderInformation.setStatusId(orderData.getStatusId());//下单人id
                    productOrderInformation.setStorefront(orderData.getStorefront());//店面
                    productOrderInformation.setStorefrontCoding(orderData.getStorefrontCoding());//店面id
                    productOrderInformation.setStorefrontType(orderData.getStorefrontType());//店面类型
                    productOrderInformation.setcPeriod(orderData.getcPeriod());//采购周期
                    productOrderInformation.setProductImg(orderData.getProductImg());//图片
                    productOrderInformation.setSupplier(orderData.getSupplier());//供应商
                    productOrderInformation.setProductName(orderData.getProductName());//名称
                    getStatus(productOrderInformation);//状态-公共
                } else {
                    integer = -1;
                    return integer;
                }

            }
            productOrderInformation.setYxNumber("维修单");
            productOrderInformation.setOrderTime(getDateShort());//下单时间
            productOrderInformation.setRefreshName(UtilGetSys.getUserName());//更新者
            integer = productOrderInformationMapper.updateProductOrderInformation(productOrderInformation);
        }else {
            integer = -2;
        }
        return integer;
    }


    /**
     * 获得状态
     *
     * @param productOrderInformation 产品订单信息
     * @author xin_xin
     * @date 2022-11-23 09:23:11
     */
    private void getStatus(ProductOrderInformation productOrderInformation) {
        if (productOrderInformation.getStorefrontType() == 1) {//直营不需要审核
            productOrderInformation.setStateDescription("待采购");//状态描述
            productOrderInformation.setStatusCode(20L);//状态码
        }
        if (productOrderInformation.getStorefrontType() == 2) {
            productOrderInformation.setStateDescription("待审核");//状态描述
            productOrderInformation.setStatusCode(10L);//状态码
        }
    }

    private void getDictLabel(ProductOrderInformation productOrderInformation) {
        SysDictData dictData = dictDataMapper.selectDictDataById(Long.valueOf(productOrderInformation.getStorefrontCoding()));//查店面
        productOrderInformation.setStorefront(dictData.getDictLabel());//下单店面名
    }

    @Override
    public List<ProductOrderInformation> selectAfterSalesPrint(ProductOrderInformation productOrderInformation) {
        return productOrderInformationMapper.selectAfterSalesPrint(productOrderInformation);
    }

    /**
     * 维修单驳回数量
     *
     * @return int
     * @author xin_xin
     * @date 2022-11-18 17:03:31
     */
    public ProductOrderInformation selectMaintainReject() {
        return productOrderInformationMapper.selectMaintainReject();
    }


    /**
     * 获取日期短
     * * @return 返回短时间字符串格式yyyy-MM-dd
     *
     * @return {@link Date }
     * @author xin_xin
     * @date 2022-10-14 18:36:30
     */
    private Date getDateShort() {//短时间
        return switchingTimeShort(UtilDate.getStringDateShort());
    }

    /**
     * 获取日期
     * 完整时间
     *
     * @return {@link Date }
     * @author xin_xin
     * @date 2022-10-14 18:36:41
     */
    private Date getDate() {//完整时间
        return switchingTime(UtilDate.getStringDate());
    }

}
