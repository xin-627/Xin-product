package com.ruoyi.backstage.service.impl;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import com.ruoyi.backstage.util.UtilGetSys;
import com.ruoyi.common.utils.security.PermissionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.ProductPigmentMapper;
import com.ruoyi.backstage.domain.ProductPigment;
import com.ruoyi.backstage.service.ProductPigmentService;
import com.ruoyi.common.core.text.Convert;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.ruoyi.common.config.RuoYiConfig.getProfile;

/**
 * 产品色号组Service业务层处理
 *
 * @author xin-xin
 * @date 2022-01-05
 */
@Service
public class ProductPigmentServiceImpl implements ProductPigmentService {
    @Autowired
    private ProductPigmentMapper productPigmentMapper;

    /**
     * 查询产品色号组
     *
     * @param id 产品色号组主键
     * @return 产品色号组
     */
    @Override
    public ProductPigment selectProductPigmentById(Long id) {
        return productPigmentMapper.selectProductPigmentById(id);
    }

    /**
     * 查询产品色号组列表
     *
     * @param productPigment 产品色号组
     * @return 产品色号组
     */
    @Override
    public List<ProductPigment> selectProductPigmentList(ProductPigment productPigment) {
        return productPigmentMapper.selectProductPigmentList(productPigment);
    }

    /**
     * 新增产品色号组
     *
     * @param productPigment 产品色号组
     * @return 结果
     */
    @Override
    public int insertProductPigment(ProductPigment productPigment) {
        return productPigmentMapper.insertProductPigment(productPigment);
    }



    /**
     * 修改产品色号组
     *
     * @param productPigment 产品色号组
     * @return 结果
     */
    @Override
    public int updateProductPigment(ProductPigment productPigment) {
        return productPigmentMapper.updateProductPigment(productPigment);
    }

    /**
     * 批量删除产品色号组
     *
     * @param ids 需要删除的产品色号组主键
     * @return 结果
     */
    @Override
    public int deleteProductPigmentByIds(String ids) {
        return productPigmentMapper.deleteProductPigmentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除产品色号组信息
     *
     * @param id 产品色号组主键
     * @return 结果
     */
    @Override
    public int deleteProductPigmentById(Long id)
    {
        return productPigmentMapper.deleteProductPigmentById(id);
    }

    /**
     * 删除img
     *
     * @param request 请求
     * @return int
     * @author xin_xin
     * @date 2022-10-31 12:01:39
     */
    @Override
    public int deleteImg(HttpServletRequest request) {
        int integer = 0;
        String imgUrl = request.getParameter("imgUrl");//图片路径
        List<String> list = Arrays.asList(imgUrl.split(","));
        if(list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                String data = list.get(i).replace("/profile/", "/");//删除关键词
                File file = new File(getProfile() + data);
                if (file.exists() == true) {
                    //判断图片是否存在
                    Boolean flag = false;
                    flag = file.delete();
                    if (flag) {
                        integer = 200;
                    } else {
                        integer = 500;
                    }
                } else {
                    integer = 404;
                }

            }
        }else{
            integer = 404;
        }
        return integer;
    }


    /**
     * 规格创建添加,图片上传
     * @param productPigment
     * @return
     */
    @Override
    public int pigmentAdd(ProductPigment productPigment) {
        int integer;
        Map<String, Object> params = new HashMap();
        params.put("pigment", productPigment.getPigment());
        params.put("productId", productPigment.getProductId());
        List<ProductPigment> pigmentData = productPigmentMapper.pigmentDuplicateChecking(params);//获取是否已有数据
        if (pigmentData.size() == 0) {
            productPigment.setCreator(UtilGetSys.getUserName());//创建人
            integer = productPigmentMapper.insertProductPigment(productPigment);
        } else {
            integer = -1;
        }
        return integer;
    }

    /**
     * 色号重复检查
     *
     * @param productPigment 产品色号
     * @return int
     * @author xin_xin
     * @date 2022-10-31 14:21:16
     */
    @Override
    public int pigmentDuplicateChecking(ProductPigment productPigment) {
        int integer;
        Map<String, Object> params = new HashMap();
        params.put("pigment", productPigment.getPigment());
        params.put("productId", productPigment.getProductId());
        List<ProductPigment> pigmentData = productPigmentMapper.pigmentDuplicateChecking(params);//获取是否已有数据
        if (pigmentData.size() > 0) {
            integer = 1;
        } else {
            integer = -1;
        }
        return integer;
    }


}
