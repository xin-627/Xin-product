package com.ruoyi.backstage.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruoyi.backstage.domain.*;
import com.ruoyi.backstage.mapper.*;
import com.ruoyi.backstage.util.UtilDate;
import com.ruoyi.backstage.util.UtilGetSys;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.service.IPWarehouseService;
import com.ruoyi.common.core.text.Convert;

import javax.print.attribute.standard.PresentationDirection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.ruoyi.backstage.util.UtilDate.switchingTime;
import static com.ruoyi.backstage.util.UtilDate.switchingTimeShort;
import static com.ruoyi.common.utils.ShiroUtils.getUserId;

/**
 * 仓库标签Service业务层处理
 *
 * @author xin_xin
 * @date 2022-07-12
 */
@Service
public class PWarehouseServiceImpl implements IPWarehouseService {
    @Autowired
    private PWarehouseMapper pWarehouseMapper;

    @Autowired
    private ProductOrderInformationMapper productOrderInformationMapper;

    @Autowired
    private PLogoMapper pLogoMapper;

    @Autowired
    private PWarehouseNumberMapper pWarehouseNumberMapper;

    @Autowired
    private PWarehouseAccessRecordsMapper pWarehouseAccessRecordsMapper;

    @Autowired
    private PWarehouseCacheFileMapper pWarehouseCacheFileMapper;

    /**
     * 查询仓库标签
     *
     * @param id 仓库标签主键
     * @return 仓库标签
     */
    @Override
    public PWarehouse selectPWarehouseById(Long id) {
        return pWarehouseMapper.selectPWarehouseById(id);
    }

    /**
     * 查询仓库标签列表
     *
     * @param pWarehouse 仓库标签
     * @return 仓库标签
     */
    @Override
    public List<PWarehouse> selectPWarehouseList(PWarehouse pWarehouse) {
        return pWarehouseMapper.selectPWarehouseList(pWarehouse);
    }

    /**
     * 新增仓库标签
     *
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    @Override
    public int insertPWarehouse(PWarehouse pWarehouse) {
        return pWarehouseMapper.insertPWarehouse(pWarehouse);
    }

    /**
     * 修改仓库标签
     *
     * @param pWarehouse 仓库标签
     * @return 结果
     */
    @Override
    public int updatePWarehouse(PWarehouse pWarehouse) {
        return pWarehouseMapper.updatePWarehouse(pWarehouse);
    }

    /**
     * 批量删除仓库标签
     *
     * @param ids 需要删除的仓库标签主键
     * @return 结果
     */
    @Override
    public int deletePWarehouseByIds(String ids) {
        return pWarehouseMapper.deletePWarehouseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除仓库标签信息
     *
     * @param id 仓库标签主键
     * @return 结果
     */
    @Override
    public int deletePWarehouseById(Long id) {
        return pWarehouseMapper.deletePWarehouseById(id);
    }

    /**
     * 订单入库操作
     *
     * @param number
     * @param id
     * @return
     */
    @Override
    public int ordersAccess(Long id, Long number) {
        int integer = 0;
        for (int i = 0; i < number; i++) {
            String barCode = String.valueOf(System.currentTimeMillis());
            PWarehouse data = new PWarehouse();
            data.setOrdersId(id);
            data.setBarCode(barCode);//条码
            data.setStatusCode(Long.valueOf("0"));//条码状态  "生成"
            data.setCreator(UtilGetSys.getUserName());//创建人
            integer = pWarehouseMapper.insertPWarehouse(data);
            try {
                Thread.sleep(20);//休眠20毫秒
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (integer > 0) {
            ProductOrderInformation info = new ProductOrderInformation();
            info.setId(id);
            info.setStateDescription("已回厂,等待入库中");
            info.setStatusCode(Long.valueOf("27"));//状态码
            info.setcRevertTime(getDateShort());//回厂时间
            info.setRefreshName(UtilGetSys.getUserName());//更新者
            int integers = productOrderInformationMapper.orderReview(info);
            if (integers < 0) {
                integer = 0;
            }
        }
        List<PWarehouse> list = new ArrayList();
        list = pWarehouseMapper.selectGroupingData();
        for (int i = 0; i < list.size(); i++) {
            PWarehouse pw = list.get(i);
            pWarehouseMapper.updateSerialNumber(pw.getOrdersId());//更新标签序号
        }
        pWarehouseMapper.updateQuantity();//更新总包数

        return integer;
    }

    /**
     * 扫码入库  - 选库位
     *
     * @param pWarehouse 仓库标签
     * @return
     */
    @Override
    public int sweepCodeAccessEdit(PWarehouse pWarehouse) {
        int integer = 0;
        PWarehouse orderData = pWarehouseMapper.selectOrderData(Long.valueOf(pWarehouse.getBarCode()));
        String statusCode = String.valueOf(orderData.getStatusCode());
        if (StringUtils.isNotEmpty(statusCode)) {
            if (orderData.getStatusCode() == 0) {
                PWarehouse data = new PWarehouse();
                data.setBarCode(pWarehouse.getBarCode());//根据条码 更新
                data.setEntranceTime(getDate());//入库时间
                data.setEntranceName(UtilGetSys.getUserName());//入库人
                data.setStatusCode(Long.valueOf("1"));//条码状态  "入库"
                data.setLocationId(pWarehouse.getLocationId());//库位id
                data.setLocation(pWarehouse.getLocation());//库位信息
                integer = pWarehouseMapper.sweepCodeOutboundUpdate(data);
                if (integer > 0) {
                    /*更新订单状态*/
                    ProductOrderInformation pData = new ProductOrderInformation();
                    pData.setId(orderData.getOrdersId());
                    pData.setStatusCode(Long.valueOf("30"));
                    pData.setDeliveryTime(getDateShort());
                    pData.setRefreshName(UtilGetSys.getUserName());
                    productOrderInformationMapper.updateSweepCodeAccess(pData);

                    /*更新仓储货架状态*/
                    PWarehouseNumber nData = new PWarehouseNumber();
                    nData.setId(pWarehouse.getLocationId());
                    nData.setUpdateBy(UtilGetSys.getUserName());
                    pWarehouseNumberMapper.updateSweepCodeAccess(nData);
                }
                /*入库记录*/
                warehouseAccessRecords(orderData);
            } else if (orderData.getStatusCode() == 1) {
                /* 已入库*/
                integer = -1;
            } else if (orderData.getStatusCode() == 2) {
                /* 已出库*/
                integer = -2;
            } else {
                /* 未知错误*/
                integer = -10;
            }
        } else {
            //没有找到相关条码的信息
            integer = -3;
        }
        return integer;
    }


    /**
     * 扫码入库  - 扫库位码
     *
     * @param pWarehouse 仓库标签
     * @return
     */
    @Override
    public int shelfBarCodeAccessEdit(PWarehouse pWarehouse) {
        int integer = 0;
        String barCode = pWarehouse.getBarCode();
        boolean barCodeResult = barCode.startsWith("KWM-");
        if (barCodeResult == true) {
            int stateCode = 0;
            PWarehouseNumber nData = pWarehouseNumberMapper.selectShelfStateCodeData(pWarehouse.getBarCode());//查库位信息

            PWarehouseCacheFile fData = new PWarehouseCacheFile();
            fData.setUserId(getUserId());//用户_id
            fData.setShelfNumber(nData.getShelfNumber());//库位编号
            fData.setShelfBarCode(nData.getShelfBarCode());//库位条码
            fData.setShelfId(nData.getId());//库位id
            fData.setShelfState(1L);//有效
            fData.setCreator(UtilGetSys.getUserName());//创建人
            stateCode = pWarehouseCacheFileMapper.insertPWarehouseCacheFile(fData);//插入数据
            if(stateCode > 0){
                //库位数据创建成功
                integer = -4;
            }else{
                //库位数据创建失败
                integer = -5;
            }
        }
        if (barCodeResult == false) {
            PWarehouseCacheFile selectFData = pWarehouseCacheFileMapper.selectUserIdData(getUserId());//去暂存表查询有效库位码
            if(selectFData != null){
                PWarehouse orderData = pWarehouseMapper.selectOrderData(Long.valueOf(pWarehouse.getBarCode()));//去查条码信息
                if (orderData.getStatusCode() == 0) {
                    PWarehouse data = new PWarehouse();
                    data.setBarCode(pWarehouse.getBarCode());//根据条码 更新
                    data.setEntranceTime(getDate());//入库时间
                    data.setEntranceName(UtilGetSys.getUserName());//入库人
                    data.setStatusCode(Long.valueOf("1"));//条码状态  "入库"
                    data.setLocationId(selectFData.getShelfId());//库位id
                    data.setLocation(selectFData.getShelfNumber());//库位码
                    integer = pWarehouseMapper.sweepCodeOutboundUpdate(data);
                    if (integer > 0) {
                        //更新暂存库位数据
                        updateShelfState();

                        //更新订单状态
                        ProductOrderInformation pData = new ProductOrderInformation();
                        pData.setId(orderData.getOrdersId());
                        pData.setStatusCode(Long.valueOf("30"));
                        pData.setDeliveryTime(getDateShort());
                        pData.setRefreshName(UtilGetSys.getUserName());
                        productOrderInformationMapper.updateSweepCodeAccess(pData);
                        System.out.println("更新订单状态----"+productOrderInformationMapper.updateSweepCodeAccess(pData));

                        //更新仓储货架状态
                        PWarehouseNumber nData = new PWarehouseNumber();
                        nData.setId(selectFData.getShelfId());
                        nData.setUpdateBy(UtilGetSys.getUserName());
                        pWarehouseNumberMapper.updateSweepCodeAccess(nData);
                        System.out.println("更新仓储货架状态----"+pWarehouseNumberMapper.updateSweepCodeAccess(nData));
                    }
                    //入库记录
                    warehouseAccessRecords(orderData);
                } else if (orderData.getStatusCode() == 1) {
                    // 已入库
                    //更新暂存库位数据
                    updateShelfState();
                    integer = -1;
                } else if (orderData.getStatusCode() == 2) {
                    // 已出库
                    //更新暂存库位数据
                    updateShelfState();
                    integer = -2;
                } else {
                    //未知错误
                    integer = -10;
                }
            }else{
                //没有库位信息
                integer = -3;
            }

        }
        return integer;
    }

    private void updateShelfState() {
        PWarehouseCacheFile uFData = new PWarehouseCacheFile();
        uFData.setUserId(getUserId());//用户id
        uFData.setShelfState(Long.valueOf("2"));//让库位失效
        pWarehouseCacheFileMapper.updateUserIdShelfState(uFData);
    }


    /**
     * 扫码出库
     *
     * @param pWarehouse 仓库标签
     * @return
     */
    @Override
    public int sweepCodeOutboundEdit(PWarehouse pWarehouse) {
        int integer = 0;
        PWarehouse orderData = pWarehouseMapper.selectOrderData(Long.valueOf(pWarehouse.getBarCode()));
        if (orderData.getStatusCode() == 1) {
            PWarehouse data = new PWarehouse();
            data.setBarCode(pWarehouse.getBarCode());//根据条码去更新
            data.setSweepTime(getDate());//出库时间
            data.setSweepName(UtilGetSys.getUserName());//出库人
            data.setStatusCode(Long.valueOf("2"));//条码状态 "出库"
            data.setRefreshName(UtilGetSys.getUserName());//更新人
            integer = pWarehouseMapper.sweepCodeOutboundUpdate(data);
            if (integer > 0) {
                /*更新订单状态*/
                ProductOrderInformation pData = new ProductOrderInformation();
                pData.setId(orderData.getOrdersId());
                pData.setStatusCode(Long.valueOf("35"));
                pData.setDeliveryTime(getDateShort());
                pData.setRefreshName(UtilGetSys.getUserName());
                productOrderInformationMapper.updateSweepCodeOutbound(pData);

                /*更新仓储货架状态*/
                PWarehouseNumber nData = new PWarehouseNumber();
                nData.setId(pWarehouse.getLocationId());
                nData.setUpdateBy(UtilGetSys.getUserName());
                pWarehouseNumberMapper.updateSweepCodeAccess(nData);
            }
            /*出库记录*/
            warehouseAccessRecords(orderData);
        } else if (orderData.getStatusCode() == 0) {
            /* 未入库*/
            integer = -1;
        } else if (orderData.getStatusCode() == 2) {
            /* 已出库*/
            integer = -2;
        } else {
            /* 未知错误*/
            integer = -10;
        }
        return integer;
    }

    /*入库记录*/
    private void warehouseAccessRecords(PWarehouse orderData) {
        PWarehouseAccessRecords records = new PWarehouseAccessRecords();
        records.setBarCodeId(orderData.getId());//条码id
        records.setOrdersId(orderData.getOrdersId());//订单id
        records.setShelfId(orderData.getLocationId());//货架id
        records.setStatusCode(orderData.getStatusCode());//类型
        pWarehouseAccessRecordsMapper.insertPWarehouseAccessRecords(records);
    }




    private Date getDateShort() {//短时间
        return switchingTimeShort(UtilDate.getStringDateShort());
    }

    private Date getDate() {//完整时间
        return switchingTime(UtilDate.getStringDate());
    }//完整时间

}
