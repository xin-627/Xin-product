package com.ruoyi.backstage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.PWarehouseNumberMapper;
import com.ruoyi.backstage.domain.PWarehouseNumber;
import com.ruoyi.backstage.service.IPWarehouseNumberService;
import com.ruoyi.common.core.text.Convert;

/**
 * 仓库库位Service业务层处理
 * 
 * @author xin_xin
 * @date 2022-09-13
 */
@Service
public class PWarehouseNumberServiceImpl implements IPWarehouseNumberService 
{
    @Autowired
    private PWarehouseNumberMapper pWarehouseNumberMapper;


    /**
     * 查询仓库库位列表
     * 2022-09-27
     */
    @Override
    public List<PWarehouseNumber> selectLocationList(PWarehouseNumber pWarehouseNumber)
    {
        return pWarehouseNumberMapper.selectLocationList(pWarehouseNumber);
    }


    /**
     * 查询仓库库位
     * 
     * @param id 仓库库位主键
     * @return 仓库库位
     */
    @Override
    public PWarehouseNumber selectPWarehouseNumberById(Long id)
    {
        return pWarehouseNumberMapper.selectPWarehouseNumberById(id);
    }

    /**
     * 查询仓库库位列表
     * 
     * @param pWarehouseNumber 仓库库位
     * @return 仓库库位
     */
    @Override
    public List<PWarehouseNumber> selectPWarehouseNumberList(PWarehouseNumber pWarehouseNumber)
    {
        pWarehouseNumberMapper.updateShelfState();//全局更新库位状态
        return pWarehouseNumberMapper.selectPWarehouseNumberList(pWarehouseNumber);
    }

    /**
     * 新增仓库库位
     * 
     * @param pWarehouseNumber 仓库库位
     * @return 结果
     */
    @Override
    public int insertPWarehouseNumber(PWarehouseNumber pWarehouseNumber)
    {
        pWarehouseNumber.setCreateTime(DateUtils.getNowDate());
        return pWarehouseNumberMapper.insertPWarehouseNumber(pWarehouseNumber);
    }

    /**
     * 修改仓库库位
     * 
     * @param pWarehouseNumber 仓库库位
     * @return 结果
     */
    @Override
    public int updatePWarehouseNumber(PWarehouseNumber pWarehouseNumber)
    {
        pWarehouseNumber.setUpdateTime(DateUtils.getNowDate());
        return pWarehouseNumberMapper.updatePWarehouseNumber(pWarehouseNumber);
    }

    /**
     * 批量删除仓库库位
     * 
     * @param ids 需要删除的仓库库位主键
     * @return 结果
     */
    @Override
    public int deletePWarehouseNumberByIds(String ids)
    {
        return pWarehouseNumberMapper.deletePWarehouseNumberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除仓库库位信息
     * 
     * @param id 仓库库位主键
     * @return 结果
     */
    @Override
    public int deletePWarehouseNumberById(Long id)
    {
        return pWarehouseNumberMapper.deletePWarehouseNumberById(id);
    }
}
