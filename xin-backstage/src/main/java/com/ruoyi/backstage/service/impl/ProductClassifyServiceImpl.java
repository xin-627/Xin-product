package com.ruoyi.backstage.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.backstage.util.TreeUtilClassification;
import com.ruoyi.backstage.util.TreeUtilClassify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.backstage.mapper.ProductClassifyMapper;
import com.ruoyi.backstage.domain.ProductClassify;
import com.ruoyi.backstage.service.ProductClassifyService;
import com.ruoyi.common.core.text.Convert;

/**
 * 分类菜单/分类管理Service业务层处理
 * 
 * @author xin-xin
 * @date 2022-01-11
 */
@Service
public class ProductClassifyServiceImpl implements ProductClassifyService
{
    @Autowired
    private ProductClassifyMapper productClassifyMapper;

    /**
     * 查询分类菜单/分类管理
     * 
     * @param id 分类菜单/分类管理主键
     * @return 分类菜单/分类管理
     */
    @Override
    public ProductClassify selectProductClassifyById(Long id)
    {
        return productClassifyMapper.selectProductClassifyById(id);
    }

    /**
     * 查询分类菜单/分类管理列表
     * 
     * @param productClassify 分类菜单/分类管理
     * @return 分类菜单/分类管理
     */
    @Override
    public List<ProductClassify> selectProductClassifyList(ProductClassify productClassify)
    {
        return productClassifyMapper.selectProductClassifyList(productClassify);
    }

    /**
     * 新增分类菜单/分类管理
     * 
     * @param productClassify 分类菜单/分类管理
     * @return 结果
     */
    @Override
    public int insertProductClassify(ProductClassify productClassify)
    {
        return productClassifyMapper.insertProductClassify(productClassify);
    }

    /**
     * 修改分类菜单/分类管理
     * 
     * @param productClassify 分类菜单/分类管理
     * @return 结果
     */
    @Override
    public int updateProductClassify(ProductClassify productClassify)
    {
        return productClassifyMapper.updateProductClassify(productClassify);
    }

    /**
     * 批量删除分类菜单/分类管理
     * 
     * @param ids 需要删除的分类菜单/分类管理主键
     * @return 结果
     */
    @Override
    public int deleteProductClassifyByIds(String ids)
    {
        return productClassifyMapper.deleteProductClassifyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除分类菜单/分类管理信息
     * 
     * @param id 分类菜单/分类管理主键
     * @return 结果
     */
    @Override
    public int deleteProductClassifyById(Long id)
    {
        return productClassifyMapper.deleteProductClassifyById(id);
    }

    /**
     * 查询分类菜单/分类管理树列表
     * 
     * @return 所有分类菜单/分类管理信息
     */
    @Override
    public List<Ztree> selectProductClassifyTree()
    {
        List<ProductClassify> productClassifyList = productClassifyMapper.selectProductClassifyList(new ProductClassify());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (ProductClassify productClassify : productClassifyList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(productClassify.getId());
            ztree.setpId(productClassify.getPid());
            ztree.setName(productClassify.getTitle());
            ztree.setTitle(productClassify.getTitle());
            ztrees.add(ztree);
        }
        return ztrees;
    }

    /**
     * 分类管理树
     * @return
     */
    @Override
    public Map<String, Object> TreeUtilClassification() {
        Map<String, Object> map = new HashMap<>();
        List<ProductClassify> productClassificationList = productClassifyMapper.selectProductClassifyList(new ProductClassify());
        List<ProductClassify> ztrees = new ArrayList<>();
        for (ProductClassify productClassification : productClassificationList)
        {
            ProductClassify ztree = new ProductClassify();
            ztree.setId(productClassification.getId());
            ztree.setPid(productClassification.getPid());
            ztree.setName(productClassification.getTitle());
            ztree.setValue(productClassification.getValue());
            ztree.setSelected(productClassification.getSelected());
            ztree.setDisabled(productClassification.getDisabled());
            ztrees.add(ztree);
        }
        map.put("data", TreeUtilClassification.toTree(ztrees, 0L));
        return map;
    }

    /**
     * 前端动态菜单
     * @return
     */
    @Override
    public Map<String, Object> TreeUtilClassify() {
        Map<String, Object> map = new HashMap<>();
        List<ProductClassify> productClassificationList = productClassifyMapper.selectProductClassifyList(new ProductClassify());
        List<ProductClassify> ztrees = new ArrayList<>();
        for (ProductClassify productClassification : productClassificationList)
        {
            ProductClassify ztree = new ProductClassify();
            ztree.setId(productClassification.getId());
            ztree.setPid(productClassification.getPid());
            ztree.setTitle(productClassification.getTitle());
            ztree.setTarget(productClassification.getTarget());
            ztree.setHref(productClassification.getHref());
            ztree.setTemplet(productClassification.getTemplet());
            ztrees.add(ztree);
        }
        map.put("data", TreeUtilClassify.toTree(ztrees, 0L));
        return map;
    }
}
