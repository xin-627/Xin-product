/*编号- 色号效验*/
function verificationInput($, url, condition) {
    let data;
    let focusValue;
    $("#verificationInput").focus(function () {
        let that = $(this);
        let value = that.val();
        that.val('');
        that.val(value);
        focusValue = value;
    });
    $("#verificationInput").blur(function () {
        let that = $(this);
        let blurValue = that.val();
        that.val('');
        that.val(blurValue);
        if (focusValue != blurValue) {
            if (condition == 'main') {
                data = {yxNumber: blurValue};
            }
            if (condition == 'pigment') {
                data = {
                    pigment: blurValue,
                    productId: productId
                };
            }
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                success: function (res) {
                    if (res.code == '0') {
                        iziToast.destroy();
                        iziToast.success({
                            message: res.msg,
                            onOpening: function () {
                                $("#verificationInput").addClass("greenInput");
                                enableSaveBtn($);//启用
                            },
                        });
                    } else {
                        $("#verificationInput").removeClass("greenInput");
                        iziToast.error({
                            message: res.msg,
                            timeout: 0,
                            onOpening: function () {
                                $("#verificationInput").addClass("redInput");
                                forbiddenSaveBtn($);//禁用
                            },
                            onClosing: function () {
                                enableSaveBtn($);//启用
                            },
                        });
                    }
                }
            });
        }
    });
}


/*分类菜单*/
function getClassifySelect($, initValue) {
    /*xmSelect     satrt*/
    var classifySelect = xmSelect.render({
        el: '#classifySelect',
        model: {label: {type: 'text'}},
        clickClose: true,
        filterable: true,
        name: 'classify',
        tips: '请选择产品分类',
        layVerify: 'required',
        layReqText: '产品分类-不能为空',
        initValue: initValue,//默认选中
        prop: {
            value: 'name',
        },
        tree: {
            show: true,
            strict: false,
            expandedKeys: [-1],
        },
        theme: {
            color: '#41c4f1',//自定义主题
        },
        iconfont: {
            parent: 'hidden',//父节点图标, 值为hidden时, 隐藏
        },
        on: function (data) {
            if (data.isAdd) {
                return data.change.slice(0, 1)
            }
        },
        height: '500px',
    })
    $.ajax({
        url: ctx + 'backstage/classify/treeClassificationData',
        type: "get",
        success: function (res) {
            classifySelect.update({
                data: res.data,
            })
        }
    });
    /*xmSelect     end*/
}


/*子级删除图片*/
function sublevelDeleteImg(prefix, $, imgUrl) {
    $.ajax({
        url: prefix + '/deleteImg?imgUrl=' + imgUrl,
        type: 'post',
        success: function (res) {
            if (res.code == "0") {
                parent.iziToast.success({
                    message: res.msg,
                    onOpened: function () {
                        parent.layer.close(parent.layer.index);
                    }
                });
            } else if (res.code == "301") {
                parent.iziToast.warning({
                    message: res.msg,
                    onOpened: function () {
                        parent.layer.close(parent.layer.index);
                    }
                });
            } else {
                parent.iziToast.error({
                    message: res.msg,
                    onOpened: function () {
                        parent.layer.close(parent.layer.index);
                    }
                });
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            parent.iziToast.loading({message: '请求失败!请稍后重试或 联系管理员', timeout: 30000,});
        }
    });
}


/*父级删除图片*/
function parentLevelDeleteImg(prefix, $, imgUrl) {
    $.ajax({
        url: prefix + '/deleteImg?imgUrl=' + imgUrl,
        type: 'post',
        success: function (res) {
            if (res.code == "0") {
                iziToast.success({message: res.msg});
            } else if (res.code == "301") {
                iziToast.warning({message: res.msg});
            } else {
                iziToast.error({message: res.msg});
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            iziToast.loading({message: '请求失败!请稍后重试或 联系管理员', timeout: 30000,});
        }
    });
}

/**
 * 编辑前删除原先的图片
 * @param prefix
 * @param $
 * @param imgUrl
 */
function editDeleteImg(prefix, $, imgUrl) {
    $.ajax({
        url: prefix + '/deleteImg?imgUrl=' + imgUrl,
        type: 'post',
        success: function (res) {
            if (res.code == "0") {
                iziToast.success({message: res.msg});
            } else if (res.code == "301") {
                iziToast.warning({message: res.msg});
            } else {
                iziToast.error({message: res.msg});
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            iziToast.loading({message: '请求失败!请稍后重试或 联系管理员', timeout: 30000,});
        }
    });
}

/**
 * 产品状态
 * @param d
 * @returns {string}
 */
function getState(d) {
    let state;
    if (d.state == 0) {
        state = '<span class="ebe-badge ebe-tag-red">下架</span>';
    } else if (d.state == 1) {
        state = '<span class="ebe-badge ebe-tag-green">上架</span>';
    } else if (d.state == 2) {
        state = '<span class="ebe-badge ebe-tag-violet">待上架</span>';
    } else {
        state = d.state
    }
    return state;
}


/**
 * 色号图
 * @param d
 * @returns {string}
 */
function getImages(d) {
    return '<img src="' + d.images  + '"  onerror="this.onerror=null;this.src=ctx+\'backstage/images/notfound/404.svg\'"  height="45px" width="45px"  class="imgNav"'
}

/**
 * 状态
 * @param d
 * @returns {string}
 */
function getPsState(d) {
    let state;
    if (d.state == 0) {
        state = '<span class="ebe-badge ebe-tag-red">禁用</span>';
    } else if (d.state == 1) {
        state = '<span class="ebe-badge ebe-tag-green">启用</span>';
    }else {
        state = d.state
    }
    return state;
}


/**
 * 富文本
 * @param $
 * @param tinymce
 * @param elemId
 * @param elemData
 * @param elemType
 * @param elemHeight
 */
function tinymceRender($,tinymce,elemId,elemData,elemType,elemHeight) {
    tinymce.render({
        elem: elemId,
        height: elemHeight,
        min_height: 200,
        resize: 'both',
        images_upload_handler: function (blobInfo, succFun, failFun ) {
            var xhr, formData;
            var file = blobInfo.blob();//转化为易于理解的file对象
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', prefix + "/otherImagesUpload");
            xhr.onload = function() {
                var json;
                if (xhr.status != 200) {
                    failFun('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(xhr.responseText);
                if (!json || typeof json.location != 'string') {
                    failFun('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                succFun(json.location);
            };
            formData = new FormData();
            formData.append('file', file, file.name );//此处与源文档不一样
            xhr.send(formData);
        }
    },function(opt,edit){
        $('#NotiflixLoadingWrap').css('animation-duration', '400ms').css('display', 'none');
        if(elemType == 'edit'){
            tinymce.get(elemId).setContent(elemData);
        }
        edit.on('FullscreenStateChanged', function(data){//解决全屏遮挡问题
            if(data.state){
                $(".tox-fullscreen").css("z-index","9999999");
            } else {
                $(".tox-fullscreen").css("z-index","99");
            }
        });

    });
}

