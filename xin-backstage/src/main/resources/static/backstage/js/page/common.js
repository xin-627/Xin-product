;!function () {
    if (layui === undefined) {
        console.error('请先引用layui.js文件.');
    } else {

        var modules = {
            common: 'extendModules/common',
            eleTree: 'extendModules/eleTree',
            treeTable: 'extendModules/treeTable'
        };
        layui.config({
            base: '/js/'
        }).extend(modules);
    }
}();

/*菜单选中*/
function selectMenu(menuUrl) {
    var menuObj = $('div.layui-side-menu a[href="' + menuUrl + '"]');
    menuObj.parents('li.layui-nav-item').addClass('layui-nav-itemed');
    menuObj.parents('dl.layui-nav-child').parent().addClass('layui-nav-itemed');
    menuObj.parent().addClass('layui-this');
}

/**
 * @Description 获取 datagrid pageList
 * @returns {Array}
 */
function getPageList() {
    pageList = [2, 3, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200];
    return pageList;
}

function getPageListLg() {
    pageListLg = [2, 3, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];
    return pageListLg;
}

function getPageListSm() {
    pageListSm = [2, 3, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
    return pageListSm;
}

/**
 * @Description 获取datagrid pageSize
 * @returns
 */
function getPageSize() {
    var pageList = getPageList();
    return pageList[0];
}

/**
 * @Description 表格加载异常提示
 * @returns
 */
function getLoadErrorMsg() {
    var text = {none: '哦豁!!暂无相关数据'};
    return text;
}

/**
 * 自定义分页
 * @returns {{layout: string[], last: string, theme: string, first: string}}
 */
function getLayout() {
    var layout = { //自定义分页
        layout: ['page', 'next', 'count', 'limit', 'refresh', 'skip']
        , theme: '#41c4f1'
        , last: '尾页'
        , first: '首页'
    }
    return layout;
}

/**
 * 获取排序字段
 * */
function getSort(tableId) {
    var sortParam = null;
    var obj = $('[lay-id="' + tableId + '"] .layui-table-sort.layui-inline[lay-sort="desc"]');
    if (obj.length > 0) {
        sortParam = {sortField: obj.parents('th').attr('data-field'), sortMethod: 'desc'};
        return sortParam;
    }
    obj = $('[lay-id="' + tableId + '"] .layui-table-sort.layui-inline[lay-sort="asc"]');
    if (obj.length > 0) {
        sortParam = {sortField: obj.parents('th').attr('data-field'), sortMethod: 'asc'};
        return sortParam;
    }
    return sortParam;
}

/**
 * 把一个表单序列化成一个json对象
 *@Description 把一个表单序列化成一个json对象
 *@param formObj 表单对象
 */
function serializeJson(formObj) {
    //把一个表单序列化成一个json对象
    var serializeObj = {};
    $.each(formObj.serializeArray(), function (i, item) {
        serializeObj[item.name] = $.trim(item.value);
    });
    return serializeObj;
}

/**
 * 自定义详情
 * @param d
 * @returns {string}
 */
function getDyestuff(d) {
    let dyestuff;
    let specifications;
    let customName;
    let customSkin;
    let customQuality;
    let customSpecifications;
    let customDyestuff;
    let customOther;
    let customChangeColour;
    if (d.dyestuff == '自定义') {
        if (d.customDyestuff == null || d.customDyestuff == "") {
            dyestuff = "<label style=\"color: #b800e6\">请填写ZDY色号</label>&nbsp;"
        } else {
            dyestuff = ""
        }
    } else {
        if (d.dyestuff == null || d.dyestuff == "") {
            dyestuff = ""
        } else {
            dyestuff = "<label style=\"color: #999999\">「色号」：</label><span>" + d.dyestuff + "</span>&nbsp;"
        }
    }
    if (d.specifications == "自定义") {
        if (d.customSpecifications == null || d.customSpecifications == "") {
            specifications = "<label style=\"color: #ff0066\">请填写ZDY规格</label>&nbsp;"
        } else {
            specifications = ""
        }
    } else {
        if (d.specifications == null || d.specifications == "") {
            specifications = ""
        } else {
            specifications = "<label style=\"color: #999999\">「规格」：</label><span>" + d.specifications + "</span>&nbsp;"
        }
    }

    if (d.customName == null || d.customName == "") {
        customName = ""
    } else {
        customName = "<label style=\"color: #999999\">「品类」：</label><span style='font-weight:bold;'>" + d.customName + "</span>&nbsp;"
    }
    if (d.customSkin == null || d.customSkin == "") {
        customSkin = ""
    } else {
        customSkin = "<label style=\"color: #999999\">「型号」：</label><span>" + d.customSkin + "</span>&nbsp;"
    }
    if (d.customQuality == null || d.customQuality == "") {
        customQuality = ""
    } else {
        customQuality = "<label style=\"color: #999999\">「材质」：</label><span>" + d.customQuality + "</span>&nbsp;"
    }
    if (d.customSpecifications == null || d.customSpecifications == "") {
        customSpecifications = ""
    } else {
        customSpecifications = "<label style=\"color: #999999\">「规格」：</label><span>" + d.customSpecifications + "</span>&nbsp;"
    }
    if (d.customDyestuff == null || d.customDyestuff == "") {
        customDyestuff = ""
    } else {
        customDyestuff = "<label style=\"color: #999999\">「色号」：</label><span>" + d.customDyestuff + "</span>&nbsp;"
    }

    if(d.orderType == 1){
        if (d.customOther == null || d.customOther == "") {
            customOther = ""
        } else {
            customOther = "<label style=\"color: #999999\">「其他」：</label><span>" + d.customOther + "</span>&nbsp;"
        }
    }else if(d.orderType == 2){
        if (d.customOther == null || d.customOther == "") {
            customOther = ""
        } else {
            customOther = "<label style=\"color: #999999\">「下单内容」：</label><span>" + d.customOther + "</span>&nbsp;"
        }
    }else{
        customOther = "<label style=\"color: #999999\">「其他」：</label><span>" + d.customOther + "</span>&nbsp;"
    }
    if (d.customChangeColour == null || d.customChangeColour == "") {
        customChangeColour = ""
    } else {
        customChangeColour = "<label style=\"color: #999999\">「功能位方向」：</label><span>" + d.customChangeColour + "</span>&nbsp;"
    }
    return dyestuff + specifications + customName + customSkin + customQuality + customSpecifications + customDyestuff + customOther + customChangeColour;
};

/**
 * 订单状态
 * @param d
 * @returns {string}
 */
function getStateDescription(d) {
    let stateDescription;
    if(d.statusCode == '0'){
        stateDescription = "<span style=\"color:#d9d9d9;text-decoration:underline;\">"+ d.stateDescription +"</span>"
    }else if(d.statusCode == '5'){
        stateDescription = "<span style=\"color:#ff0066;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '10'){
        stateDescription = "<span style=\"color:#8c1aff;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '15'){
        stateDescription = "<span style=\"color:#ff0066;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '20'){
        stateDescription = "<span style=\"color:#8080ff;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '25'){
        stateDescription = "<span style=\"color:#ff5500;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '27'){
        stateDescription = "<span style=\"color:#6600ff;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '30'){
        stateDescription = "<span style=\"color:#1a8cff;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == '35'){
        stateDescription = "<span style=\"color:#00994d;\">" +d.stateDescription  +"</span>"
    }else if(d.statusCode == null || d.statusCode == ""){
        stateDescription = ""
    }else{
        stateDescription = "<span style=\"color:#000;\">" +d.stateDescription +"</span>"
    }
    return stateDescription;
}


/**
 * 店面类型
 * @param d
 * @returns {string}
 */
function getStorefrontType(d) {
    let storefrontType;
    if(d.storefrontType == '1'){
        storefrontType = "<span style=\"color:#00b359;\">直营</span>"
    }else if(d.storefrontType == '2'){
        storefrontType = "<span style=\"color:#004c99;\">加盟</span>"
    }else{
        storefrontType = ""
    }
    return  storefrontType;

}
/**
 * 订单类型
 * @param d
 * @returns {string}
 */
function getMaintainType(d) {
    let maintainType;
    if(d.maintainType == '1'){
        maintainType = "<span style=\"color:#ff0066;\">维修单</span>"
    }else if(d.maintainType == '2'){
        maintainType = "<span style=\"color:#007fff;\">增补单</span>"
    }else{
        maintainType = ""
    }
    return  maintainType;

}


/**
 * 主图
 * @param d
 * @returns {string}
 */
function getProductImg(d) {
    return '<img src="' + ctx + 'backstage/main/pictureDisplay?imgUrl=' + d.productImg + '" onerror="this.onerror=null;this.src=ctx+\'backstage/images/notfound/404.svg\'" height="45px" width="45px"  class="imgNav">'
}

/**
 * 高清图
 * @param d
 * 加载太慢 弃用
 * @returns {string}
 */
function getClearImages(d) {
    return '<img src="' + ctx + 'backstage/main/pictureDisplay?imgUrl=' + d.clearImages + '" onerror="this.onerror=null;this.src=ctx+\'backstage/images/notfound/404.svg\'" height="45px" width="45px"  class="imgNav">'
}


/**
 * 订单备注
 * @param d
 */
function getOrderRemark(d) {
    let cProcurementRemark;
    let remarks;
    let overrule;
    if (d.cProcurementRemark == "" || d.cProcurementRemark == null) {
        cProcurementRemark = "";
    } else {
        cProcurementRemark = "<label style=\"color: #999999\">「采购备注」：</label><span>" + d.cProcurementRemark + "</span>&nbsp;";
    }
    if (d.remarks == "" || d.remarks == null) {
        remarks = "";
    } else {
        remarks = "<label style=\"color: #999999\">「订单备注」：</label><span>" + d.remarks + "</span>&nbsp;";
    }
    if (d.statusCode === 15 ) {
        if (d.overrule == "" || d.overrule == null) {
            overrule = "";
        } else {
            overrule = "<label style=\"color: #bb33ff\">「驳回理由」：</label><span>" + d.overrule + "</span>&nbsp;";
        }
    }else{
        overrule = "";
    }
    return cProcurementRemark + remarks + overrule
}

/**
 * 客服打印页单独引用
 * @param d
 * @returns {string}
 */
function getDetailsPrint(d) {
    return getDyestuff(d)+getOrderRemark(d);
};

/**
 * 下单人
 * @param d
 */
function getContactEay(d) {
    let orders;
   let contactWay;
   if(d.orders == "" || d.orders == null){
       orders = "";
   }else{
       orders = d.orders;
   }
    if(d.contactWay == "" || d.contactWay == null){
        contactWay = "";
    }else{
        contactWay = d.contactWay;
    }
    return orders + contactWay
}


/**
 * 售后导出 占用数据
 */
function getNullData() {
    let nullData = ""
    return nullData;
}

/**
 * 售后打印页备注
 */
function getPrintOrderRemark(d) {
    let cProcurementRemark;
    let remark;
    let location;
    if (d.cProcurementRemark == "" || d.cProcurementRemark == null) {
        cProcurementRemark = "";
    } else {
        cProcurementRemark = "<label style=\"color: #999999\">「采购备注」：</label><span>" + d.cProcurementRemark + "</span>&nbsp;";
    }
    if (d.remark == "" || d.remark == null) {
        remark = "";
    } else {
        remark = "<label style=\"color: #999999\">「订单备注」：</label><span>" + d.remark + "</span>&nbsp;";
    }
    if (d.location == "" || d.location == null) {
        location = "";
    } else {
        location = "<label style=\"color: #999999\">「库位」：</label><span>" + d.location + "</span>&nbsp;";
    }
    return cProcurementRemark + remark + location
}

//当前系统时间: yyyy-mm-dd hh:mm:ss
function getNowDate() {
    var date = new Date();
    var sign1 = '-';
    var sign2 = ':';
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var weekArr = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
    var week = weekArr[date.getDay()];
    // 前面加0;
    if (month >= 1 && month <= 9) {
        month = '0' + month;
    }
    if (day >= 0 && day <= 9) {
        day = '0' + day;
    }
    if (hour >= 0 && hour <= 9) {
        hour = '0' + hour;
    }
    if (minutes >= 0 && minutes <= 9) {
        minutes = '0' + minutes;
    }
    if (seconds >= 0 && seconds <= 9) {
        seconds = '0' + seconds;
    }
    var currentDate = year + sign1 + month + sign1 + day +' '+ + hour +sign2 + minutes + sign2 + seconds   ;
    return currentDate;
}


/*维修单-店面*/
function getMStorefront(m) {
    let MStorefront;
    if(m.storefront == null || m.storefront ==""){
        MStorefront = "";
    }else{
        MStorefront = m.storefront;
    }
    return  MStorefront;
}

/*维修单-店面类型*/
function getMStorefrontType(m) {
    let MStorefrontType;
    if(m.storefrontType == '1'){
        MStorefrontType = "直营"
    }else if(m.storefrontType == '2'){
        MStorefrontType = "加盟"
    }else{
        MStorefrontType = ""
    }
    return  MStorefrontType;
}


/*维修单-采购备注*/
function getMcProcurementRemark(m) {
    let McProcurementRemark;
    if(m.cProcurementRemark == null || m.cProcurementRemark ==""){
        McProcurementRemark = "";
    }else{
        McProcurementRemark = m.cProcurementRemark;
    }
    return  McProcurementRemark;
}

/*成品管理页 2022-10*/
/*提交按钮的操作*/
/*按钮禁用*/
function forbiddenSaveBtn($) {
    $("#saveBtn").attr("disabled", "disabled").addClass("prohibit");
};
/*按钮启用*/
function enableSaveBtn($) {
    $("#saveBtn").removeAttr("disabled").removeClass("prohibit");
};

/**图片查看 设定范围 id="imagesView"  2022-11-07*/
function getImagesView() {
    let viewer = new Viewer(document.getElementById('imagesView'), {url: 'src'});
    return viewer;
}


function  getPrintStatus(d){
    let printStatus;

    if(d.printStatus == '1'){
        printStatus = "<span style=\"color:#00994d;\">打印</span>"
    }else{
        printStatus = "<span style=\"color:#ff0066;\">否</span>"
    }
    return printStatus;
}

/**
 * 产品名称跟包件信息
 */
function getQuantityOrSerialNumber(d){
    let quantity;
    let productName = d.productName;
    if(d.quantity > 0){
        quantity = "<span>"+productName+ "-"+ d.quantity + "-"+ d.serialNumber+ "</span>&nbsp;"
        return quantity ;
    }else{
        return productName ;
    }

}

/**
 * 包件状态
 */
function getWStatusCode(d) {
    let wStatusCode;
    if (d.wStatusCode == 1) {
        wStatusCode = "<span style='color: #00b386'>已入库</span>"
    } else if (d.wStatusCode == 2) {
        wStatusCode = "<span style='color: #a605fa'>已出库</span>"
    } else {
        wStatusCode = ""
    }
    return wStatusCode;
}