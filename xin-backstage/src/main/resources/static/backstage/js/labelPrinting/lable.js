$(document).ready(function () {
    //处理按钮事件
    $('#chuli').click(function () {
        $('#mydata').html('');
        $('#xiang_ed').html('');
        $('#f_checkbox').attr('checked', false);
 
        //获取消息文本框的文本内容
        let xiaoxi = $('#xiaoxi').val();
        let fenge = xiaoxi.split('\n');
        let i = 0;
        let fenge_len = fenge.length;
 
        //将数组循环写入表格
        for (i; i < fenge_len; i++) {
            fenge[i] = fenge[i].split(',');
 
            //验证字母数字组合且不少于4个字符正则
            if (/^[A-Za-z0-9]{4,}$/.test(fenge[i][0]) === false) { continue; }
 
            //将数据写入表格
            $('#mydata').append(
                '<tr><td><div class="th1" style="width: 28px;"><input type="checkbox" class="checkbox"></div></td>' +
                '<td><div class="th1"><input type="text" value="' + fenge[i][0] + '"></div></td>' +
                '<td><div class="th2"><input type="text" value="' + fenge[i][1] + '"></div></td>' +
                '<td><div class="th3"><input type="text" value="' + fenge[i][2] + '"></div></td>' +
                '<td><div class="th4"><input type="text" value="' + fenge[i][3] + '"></div></td>' +
                '<td><div class="th5"><input type="text" value="' + fenge[i][4] + '"></div></td>' +
                '<td><div class="th6"><input type="text" value="' + fenge[i][5] + '"></div></td>' +
                '<td><div class="th1"><a href="javascript:;">删除</a></div></td></tr>'
            );
        }
 
        $('#xiang').text(' 共 ' + $('#mydata tr').length + ' 项');
 
        //全选或全不选
        $('#f_checkbox').on('click', function () {
            $('tbody input:checkbox').prop('checked', $(this).prop('checked'));
 
            if ($('#f_checkbox').prop('checked')) {
                $('#xiang_ed').text('； 已选择 ' + $('#mydata tr').length + ' 项');
            } else { $('#xiang_ed').text(''); }
 
        })
 
        //tbody_input点击事件
        $('tbody input:checkbox').on('click', function () {
            if ($('tbody input:checkbox').length === $('tbody input:checked').length) {
                $('#f_checkbox').prop('checked', true);
            } else {
                $('#f_checkbox').prop('checked', false);
            }
 
            $('#xiang_ed').text('； 已选择 ' + $('tbody input:checked').length + ' 项');
        })
 
        //删除操作事件
        $('tbody a').on('click', function () {
            if ($('tbody input:checked').length > 0) {
 
                $('tbody input:checked').each(function () {
                    $(this).parent().parent().parent().remove();
                })
 
                $('#f_checkbox').attr('checked', false);
                $('#xiang').text(' 共 ' + $('#mydata tr').length + ' 项');
                $('#xiang_ed').html('');
 
            } else { alert('未选择!') }
        })
 
        $('#mydata').css('text-align', 'center');
 
    })
 
    //打印按钮事件
    $('#dayin').click(function () {
        $('#print').html('');
        var data = [];
 
        //读取表格内容
        $('#mydata tr').each(function (index) {
            data[index] = [];
            $(this).find('input').each(function () {
                data[index].push($(this).val());
            })
        })
 
        let canku = $('#canku').val();
        let i = 0;
        let data_len = data.length;
 
        //循环生成小标签
        for (i; i < data_len; i++) {
            $('#print').append(
                '<div class="lable">' +
                '<svg class="barcode"></svg>' +
                '<div class="k_left"><span class="k_canku">' + canku + '<span><span>' + getNowDate() + '<span><span>' + data[i][5] + '<span><span>' + data[i][6] + '<span></div>' +
                '<div class="k_left2">' + data[i][2] + ',' + data[i][3] + ',' + data[i][4] + '</div>' +
                '</div>'
            );
 
            //JsBarcode生成条形码
            $('.barcode:last').JsBarcode(data[i][1], {
                height: 40,
                textMargin: 0,
                fontSize: 30,
                fontOptions: 'bold',
                margin: 5
            })
 
        }
 
        //设置打印分页
        $('#print .lable').css('page-break-before', 'always');
        $('#print .lable:first').attr('style', '');
 
        //打印窗口
        var newWindow = window.open("打印窗口", "_blank");
        var docStr = '<style>' + document.getElementsByTagName("style")[0].innerHTML + '</style><div id="print">' + document.getElementById('print').innerHTML + '</div>';
        newWindow.document.write(docStr);
        newWindow.document.close();
        newWindow.print();
        newWindow.close();
    })
})
 
//当前系统时间: yyyy-mm-dd hh:mm:ss
function getNowDate() {
    var date = new Date();
    var sign1 = '-';
    var sign2 = ':';
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var weekArr = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    var week = weekArr[date.getDay()];
    // 前面加0;
    if (month >= 1 && month <= 9) {
        month = '0' + month;
    }
    if (day >= 0 && day <= 9) {
        day = '0' + day;
    }
    if (hour >= 0 && hour <= 9) {
        hour = '0' + hour;
    }
    if (minutes >= 0 && minutes <= 9) {
        minutes = '0' + minutes;
    }
    if (seconds >= 0 && seconds <= 9) {
        seconds = '0' + seconds;
    }
    var currentdate = year + sign1 + month + sign1 + day;
    return currentdate;
}