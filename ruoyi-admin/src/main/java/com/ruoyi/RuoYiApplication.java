package com.ruoyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author ruoyi
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class RuoYiApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(RuoYiApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  xin-系统启动成功   ლ(´ڡ`ლ)ﾞ               \n" +
                "                                                               \n" +
                " Xin                Xin    XinXin   xin    xinXinXin           \n" +
                "   Xin           Xin        xin     xin Xin         xin        \n" +
                "     Xin       Xin          xin     xin               xin      \n" +
                "       Xin   Xin            xin     xin               xin      \n" +
                "         XinXin             xin     xin               xin      \n" +
                "       Xin   Xin            xin     xin               xin      \n" +
                "     Xin       Xin          xin     xin               xin      \n" +
                "   Xin           Xin        xin     xin               xin      \n" +
                " Xin               Xin      xin     xin               xin      ");
    }
}