package com.ruoyi.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 全局配置类
 * 
 * @author ruoyi
 */
@Component
@ConfigurationProperties(prefix = "ruoyi")
public class RuoYiConfig
{
    /** 项目名称 */
    private static String name;

    /** 版本 */
    private static String version;

    /** 版权年份 */
    private static String copyrightYear;

    /** 实例演示开关 */
    private static boolean demoEnabled;

    /** 上传路径 */
    private static String profile;

    /** 获取地址开关 */
    private static boolean addressEnabled;

    public static String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        RuoYiConfig.name = name;
    }

    public static String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        RuoYiConfig.version = version;
    }

    public static String getCopyrightYear()
    {
        return copyrightYear;
    }

    public void setCopyrightYear(String copyrightYear)
    {
        RuoYiConfig.copyrightYear = copyrightYear;
    }

    public static boolean isDemoEnabled()
    {
        return demoEnabled;
    }

    public void setDemoEnabled(boolean demoEnabled)
    {
        RuoYiConfig.demoEnabled = demoEnabled;
    }

    public static String getProfile()
    {
        return profile;
    }

    public void setProfile(String profile)
    {
        RuoYiConfig.profile = profile;
    }

    public static boolean isAddressEnabled()
    {
        return addressEnabled;
    }

    public void setAddressEnabled(boolean addressEnabled)
    {
        RuoYiConfig.addressEnabled = addressEnabled;
    }

    /**
     * 获取导入上传路径
     */
    public static String getImportPath()
    {
        return getProfile() + "/import";
    }

    /**
     * 获取头像上传路径
     */
    public static String getAvatarPath()
    {
        return getProfile() + "/avatar";
    }

    /**
     * 获取下载路径
     */
    public static String getDownloadPath()
    {
        return getProfile() + "/download/";
    }

    /**
     * 获取上传路径
     */
    public static String getUploadPath()
    {
        return getProfile() + "/upload";
    }

    /**
     * 缩略图上传路径
     * @return {@link String }
     * @author xin_xin
     * @date 2022-10-27 11:36:26
     */

    public static String getUploadPathThumbnail()
    {
        return getProfile() + "/Thumbnail";
    }

    /**
     * 清晰图像上传路径 主图
     *
     * @return {@link String }
     * @author xin_xin
     * @date 2022-10-27 11:38:46
     */
    public static String getUploadPathClearImages()
    {
        return getProfile() + "/ClearImages";
    }

    /**
     * 得到其他图像上传路径
     *
     * @return {@link String }
     * @author xin_xin
     * @date 2022-11-02 19:47:03
     */
    public static String getUploadPathOtherImages()
    {
        return getProfile() + "/OtherImages";
    }
    /**
     * 色号图 上传路径
     *
     * @return {@link String }
     * @author xin_xin
     * @date 2022-10-27 11:41:14
     */
    public static String getUploadPathArtworkMaster()
    {
        return getProfile() + "/ArtworkMaster";
    }

    /**
     * 维修单问题图片 上传路径
     *
     * @return {@link String }
     * @author xin_xin
     * @date 2022-10-27 11:43:34
     */
    public static String getUploadPathMaintain()
    {
        return getProfile() + "/MaintainFiles";
    }

    /**
     * 轮播图上传路径
     *
     * @return {@link String }
     * @author xin_xin
     * @date 2022-11-01 10:49:12
     */
    public static String getUploadPathCarousel()
    {
        return getProfile() + "/Carousel";
    }

    /**
     * 上传路径 logo图
     *
     * @return {@link String }
     * @author xin_xin
     * @date 2022-11-01 14:10:56
     */
    public static String getUploadPathLogoImg()
    {
        return getProfile() + "/LogoImg";
    }

}
